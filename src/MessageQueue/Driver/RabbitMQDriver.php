<?php
declare(strict_types=1);

namespace Telenor\MessageQueue\Driver;

use Amp\Loop;
use AMQPException;
use Exception;
use InvalidArgumentException;
use LogicException;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Connection\AMQPSocketConnection;
use PhpAmqpLib\Exception\AMQPIOException;
use PhpAmqpLib\Exception\AMQPProtocolException;
use PhpAmqpLib\Exception\AMQPRuntimeException;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use Telenor\Logging\TLog;
use Telenor\System\Config;
use Telenor\System\Error\BadConfigurationError;
use Telenor\System\Exception\ConnectionException;
use Telenor\System\Utils;
use Throwable;
use function define;
use function in_array;
use function is_bool;
use function preg_match;
use function round;
use function sprintf;
use function strlen;
use function strtolower;

class RabbitMQDriver implements IMessageQueueDriver
{
	use TLog;

	public const CONF_RABBITMQ_QUEUE_NAME = 'RABBITMQ_QUEUE_NAME';
	public const CONF_RABBITMQ_ROUTING_KEY = 'RABBITMQ_ROUTING_KEY';

	public const CONSUMER_CALLABLE = 'callable';
	public const CONSUMER_QUEUE_NAME = 'queueName';

	public const VALID_EXCHANGE_TYPES = [
		'direct',
		'fanout',
		'topic',
		'headers',
	];

	/** @var string */
	protected $routingKey;

	/** @var string */
	protected $queueName;

	/** @var string */
	protected $exchange;

	/** @var string */
	protected $retryExchange;

	/** @var string */
	protected $retryRoutingKey;

	/** @var string */
	protected $exchangeType;

	/** @var int */
	protected $retryTTL;

	/** @var int */
	protected $retryCount;

	/** @var string */
	protected $host;

	/** @var int */
	protected $port;

	/** @var string */
	protected $username;

	/** @var string */
	protected $password;

	/** @var AbstractConnection */
	protected $connection;

	/** @var AMQPChannel */
	protected $channel;

	/** @var bool */
	protected $blocked = false;

	/** @var bool */
	protected $disconnected = false;

	/** @var bool */
	protected $connectFailed = false;

	/** @var bool */
	protected $autoDeleteQueue = false;

	/** @var callable[] */
	protected $consumers = [];

	/** @var callable */
	protected $messageExpiredCallable;

	/** @var callable[] */
	protected $connectionChangedCallables = [];

	/**
	 * Initiates a RabbitMQ driver.
	 *
	 * @throws BadConfigurationError If required configurations are not fulfilled.
	 * @throws Exception If failed to connect.
	 */
	public function __construct()
	{
		$this->setHost(Config::envString('RABBITMQ_HOSTNAME', 'localhost'));
		$this->setPort(Config::envInt('RABBITMQ_PORT', 5672));
		$this->setUsername(Config::envString('RABBITMQ_USERNAME'));
		$this->setPassword(Config::envString('RABBITMQ_PASSWORD'));
		$this->setRoutingKey(Config::envString(self::CONF_RABBITMQ_ROUTING_KEY, ''));
		$this->setQueueName(Config::envString(self::CONF_RABBITMQ_QUEUE_NAME, $this->getRoutingKey()));
		$this->setExchange(Config::envString('RABBITMQ_EXCHANGE', ''));
		$this->setExchangeType(Config::envString('RABBITMQ_EXCHANGE_TYPE', 'topic'));
		$this->setRetryRoutingKey(Config::envString('RABBITMQ_RETRY_ROUTING_KEY', 'pol.retry'));
		$this->setRetryExchange(Config::envString('RABBITMQ_RETRY_EXCHANGE', 'pol.ex.retry'));
		$this->setRetryTTL(Config::envInt('RABBITMQ_RETRY_TTL', 10000));
		$this->setRetryCount(Config::envInt('RABBITMQ_RETRY_COUNT', 1080));
		$this->setAutoDeleteQueue(Config::envBool('RABBITMQ_AUTO_DELETE_QUEUE', false));

		if (Config::envBool('RABBITMQ_DEBUG', false))
			define('AMQP_DEBUG', true);

		$this->enableHeartbeat();
	}

	/**
	 * Retrieves the name of the current listen queue (if any).
	 *
	 * @return string The queue name.
	 */
	public function getQueueName(): string
	{
		return $this->queueName;
	}

	/**
	 * Sets the queue name to listen on.
	 * After changing this value reconnect() should be called.
	 *
	 * @param string $queueName The name of the queue to listen on.
	 * @throws BadConfigurationError If invalid queue name was supplied.
	 */
	public function setQueueName(string $queueName): void
	{
		if (strlen($queueName) > 255)
			throw new BadConfigurationError(__METHOD__, 'Routing key exceeds 255 characters');

		$this->queueName = $queueName;
	}

	/**
	 * Retrieves the name of the current routing key.
	 *
	 * @return string The routing key.
	 */
	public function getRoutingKey(): string
	{
		return $this->routingKey;
	}

	/**
	 * Sets the routing key to use.
	 * After changing this value reconnect() should be called.
	 *
	 * @param string $routingKey The routing key to use.
	 * @throws BadConfigurationError If invalid routing key was supplied.
	 */
	public function setRoutingKey(string $routingKey): void
	{
		if (strlen($routingKey) > 255)
			throw new BadConfigurationError(__METHOD__, 'Routing key exceeds 255 characters');

		$this->routingKey = $routingKey;
	}

	/**
	 * Retrieves the name of the current exchange.
	 *
	 * @return string The exchange.
	 */
	public function getExchange(): string
	{
		return $this->exchange;
	}

	/**
	 * Sets the exchange to use.
	 * After changing this value reconnect() should be called.
	 *
	 * @param string $exchange The exchange to use.
	 * @throws BadConfigurationError If invalid exchange was supplied.
	 */
	public function setExchange(string $exchange): void
	{
		$this->validateExchange($exchange);
		$this->exchange = $exchange;
	}

	/**
	 * Retrieves the name of the current retry routing key.
	 *
	 * @return string The retry routing key.
	 */
	public function getRetryRoutingKey(): string
	{
		return $this->retryRoutingKey;
	}

	/**
	 * Sets the retry routing key to use.
	 * After changing this value reconnect() should be called.
	 *
	 * @param string $retryRoutingKey The retry routing key to use.
	 * @throws BadConfigurationError If invalid retry routing key was supplied.
	 */
	public function setRetryRoutingKey(string $retryRoutingKey): void
	{
		if (strlen($retryRoutingKey) > 255)
			throw new BadConfigurationError(__METHOD__, 'Routing key exceeds 255 characters');

		$this->retryRoutingKey = $retryRoutingKey;
	}

	/**
	 * Retrieves the name of the current retry exchange.
	 *
	 * @return string The retry exchange.
	 */
	public function getRetryExchange(): string
	{
		return $this->retryExchange;
	}

	/**
	 * Sets the retry exchange to use.
	 * After changing this value reconnect() should be called.
	 *
	 * @param string $retryExchange The exchange to use.
	 * @throws BadConfigurationError If invalid exchange was supplied.
	 */
	public function setRetryExchange(string $retryExchange): void
	{
		$this->validateExchange($retryExchange);
		$this->retryExchange = $retryExchange;
	}

	/**
	 * Validates whether an exchange name is valid or now. Returns
	 * nothing, but throws exception on validation errors.
	 *
	 * @param string $exchange The name of the exchange to validate.
	 * @throws BadConfigurationError If invalid exchange name was supplied.
	 */
	protected function validateExchange(string $exchange): void
	{
		if (strlen($exchange) > 255)
			throw new BadConfigurationError(__METHOD__, 'Exchange exceeds 255 characters');

		// Exchange names starting with 'amq.' are reserved by the RabbitMQ internals
		if (preg_match('#^amq.#i', $exchange))
			throw new BadConfigurationError(__METHOD__, 'Exchange name must not begin with \'amq.\'');

	}

	/**
	 * Retrieves the time-to-live value for when a dead-letter message is scheduled for requeuing (in ms).
	 *
	 * @return int The current TTL count.
	 */
	public function getRetryTTL(): int
	{
		return $this->retryTTL;
	}

	/**
	 * Sets the time-to-live value for when a dead-letter message is scheduled for requeuing (in ms).
	 *
	 * @param int $retryTTL
	 */
	public function setRetryTTL(int $retryTTL): void
	{
		if ($retryTTL < 1)
			$retryTTL = 1;

		$this->retryTTL = $retryTTL;
	}

	/**
	 * Retrieves the maximum number of retries to handle a message.
	 *
	 * @return int The current retry count.
	 */
	public function getRetryCount(): int
	{
		return $this->retryCount;
	}

	/**
	 * Sets the maxmimum number of retries for a message to be handled correctly.
	 *
	 * @param int $retryCount The new retry count.
	 */
	public function setRetryCount(int $retryCount): void
	{
		if ($retryCount < 1)
			$retryCount = 1;
		$this->retryCount = $retryCount;
	}

	/**
	 * Retrieves the type of the current exchange.
	 *
	 * @return string The exchange type.
	 */
	public function getExchangeType(): string
	{
		return $this->exchangeType;
	}

	/**
	 * Sets the exchange type to use.
	 * After changing this value reconnect() should be called.
	 *
	 * @param string $exchangeType The exchange type to use.
	 * @throws InvalidArgumentException If invalid exchange type was supplied.
	 */
	public function setExchangeType(string $exchangeType): void
	{
		$exchangeType = strtolower($exchangeType);
		if (!in_array($exchangeType, self::VALID_EXCHANGE_TYPES, true))
			throw new InvalidArgumentException("Invalid exchange type given: $exchangeType");

		$this->exchangeType = $exchangeType;
	}

	/**
	 * @return string The host to connect to (or already is connected to)
	 */
	public function getHost(): string
	{
		return $this->host;
	}

	/**
	 * Sets the host to connect to. After changing this value reconnect() should be called.
	 *
	 * @param string $host The hostname or IP address to connect to.
	 */
	public function setHost(string $host): void
	{
		if (Utils::isStringNullOrEmpty($host))
			throw new InvalidArgumentException('Invalid IP/hostname specified');

		$this->host = $host;
	}

	/**
	 * @return int The TCP port of the remote end point.
	 */
	public function getPort(): int
	{
		return $this->port;
	}

	/**
	 * Sets the TCP port to connect to.
	 *
	 * @param int $port The new port to use.
	 * @throws BadConfigurationError If invalid TCP port specified.
	 */
	public function setPort(int $port): void
	{
		if ($port < 0 || $port > 65535)
			throw new BadConfigurationError(__METHOD__, 'Port number must be between 0-65535');

		$this->port = $port;
	}

	/**
	 * @return string The username for message queue authentication.
	 */
	public function getUsername(): string
	{
		return $this->username;
	}

	/**
	 * @param string $username The new username for message queue authentication.
	 */
	public function setUsername(string $username): void
	{
		if (Utils::isStringNullOrEmpty($username))
			throw new InvalidArgumentException('Invalid username specified');

		$this->username = $username;
	}

	/**
	 * @return string The password for message queue authentication.
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	/**
	 * @param string $password The new password for message queue authentication.
	 */
	public function setPassword(string $password): void
	{
		if (Utils::isStringNullOrEmpty($password))
			throw new InvalidArgumentException('Invalid password specified');

		$this->password = $password;
	}

	/**
	 * Gets whether a queue is being deleted on disconnect.
	 *
	 * @return bool True if the queue is being deleted, false if not.
	 */
	public function getAutoDeleteQueue(): bool
	{
		return $this->autoDeleteQueue;
	}

	/**
	 * Sets whether a queue is being deleted on disconnect.
	 *
	 * @param bool $autoDeleteQueue True if the queue should be deleted, false if not.
	 */
	public function setAutoDeleteQueue(bool $autoDeleteQueue): void
	{
		$this->autoDeleteQueue = $autoDeleteQueue;
	}

	protected function enableHeartbeat(): void
	{
		/*
		* TODO:
		* This is a very ugly hack - we can find a better solution, I bet.. But the case is that the connection
		* to the message queue depends on heartbeats, and RabbitMQ closes connection without PHP-AMQPLIB
		* ever really realizes.. So messages can be send to a non-existing queue/exchange without knowing (wtf??)
		* So long-running workers need to send heartbeat periodically - we do that with our Amphp library here
		*/
		Loop::repeat(1000, function () {
			try {
				/*
				 * We will be called in this function every second (1000ms), however, we also need to
				 * consume all messages if some is in queue for us (yes, wait() actually consumes messages)
				 * and wait for a timeout if no new messages are ready for us. We will timeout after 0.1 second
				 * of waiting. This is probably good enough.
				 * This should be OK for both consumers and publishers. While consumers will empty any incoming
				 * messages, the publishers will timeout fairly quick. These values might needs to be tweaked
				 * differently based on the type of worker, but lets see if this is sufficient for all workers.
				 */
				if (!$this->isConnected())
					$this->reconnect();

				while ($this->isConnected())
					$this->channel->wait(null, false, 0.1);

			} catch (AMQPTimeoutException $e) {
			} catch (AMQPProtocolException | AMQPRuntimeException | AMQPIOException | ConnectionException $e) {

				if (!$this->disconnected) {
					$this->logWarning('Connection broken to host ' . $this->getConnectionInfo() . ', reconnecting..',
						[$e]);
				}
				$this->reconnect();

			} catch (AMQPException $e) {
				$this->disconnect();
				throw new ConnectionException('Message queue connection failed', $e);
			}
		});
	}

	/**
	 * (Re)connects to the message queue.
	 */
	public function connect(): void
	{
		if ($this->isConnected())
			return;

		try {
			// We can set a friendly connection name - is visible on the Connection tab in the rabbitmq mgmt gui
			AbstractConnection::$LIBRARY_PROPERTIES['connection_name'] = [
				'S',
				Utils::getUniqueWorkerName(),
			];

			// Now create the connection to rabbitmq
			$this->connection = new AMQPSocketConnection($this->getHost(), $this->getPort(), $this->getUsername(),
				$this->getPassword());

			// Set a block event handler - this will be called when the server sends a block command to the client
			$this->connection->set_connection_block_handler(function ($reason) {
				$this->blocked = true;
				$this->logWarning("Message queue connection blocked. Reason: $reason");
			});

			// Set an ublock event handler - this will be called when the server sends an ublock command to the client
			$this->connection->set_connection_unblock_handler(function () {
				$this->blocked = false;
				$this->logNotice('Message queue connection unblocked');
			});

			$queueArgs = new AMQPTable();
			if (!Utils::isStringNullOrEmpty($this->getRetryExchange())) {
				$queueArgs->set('x-dead-letter-exchange', $this->getRetryExchange());
			}

			// Standard/normal queue declaration
			$routingKey = $this->getRoutingKey();
			if (Utils::isStringNullOrEmpty($routingKey))
				$routingKey = $this->getQueueName();

			$this->channel = $this->connection->channel();
			$this->channel->exchange_declare($this->getExchange(), $this->getExchangeType(), false, true, false);

			if (!Utils::isStringNullOrEmpty($this->getQueueName())) {
				$this->channel->queue_declare($this->getQueueName(), false, true, false, $this->getAutoDeleteQueue(),
					false, $queueArgs);
				$this->channel->queue_bind($this->getQueueName(), $this->getExchange(), $routingKey);
			}

			// Retry queue declaration
			$queueArgs = new AMQPTable([
				'x-dead-letter-exchange' => $this->getExchange(),
				'x-message-ttl'          => $this->getRetryTTL(),
			]);
			$this->channel->queue_declare($this->getRetryRoutingKey(), false, true, false, false, false, $queueArgs);
			$this->channel->exchange_declare($this->getRetryExchange(), 'fanout', false, true, false);
			$this->channel->queue_bind($this->getRetryRoutingKey(), $this->getRetryExchange());

			$this->logNotice('Connected to message queue ' . $this->getConnectionInfo());

			// Re-register any consumers after reconnect
			if ($this->disconnected) {
				foreach ($this->consumers as $consumer)
					$this->registerConsumer($consumer[self::CONSUMER_CALLABLE], $consumer[self::CONSUMER_QUEUE_NAME]);
			}

			// Log some details about the retry TTL
			$retriesPerMinute = round(60 / ($this->getRetryTTL() / 1000), 1);
			$retryCountMinutes = round($this->getRetryCount() / $retriesPerMinute, 1);
			$this->logInfo(sprintf('Message retries per minute: %d, expected to keep in retry loop for %d minute(s)',
				$retriesPerMinute, $retryCountMinutes));

			$this->disconnected = false;
			$this->connectFailed = false;
			$this->connectionChanged(true);

		} catch (Exception $e) {
			if (!$this->connectFailed) {
				$this->connectFailed = true;
				$this->logWarning('Failed to (re)connect. Retrying periodically..', [$e]);
				$this->disconnect();
			}
		}
	}

	/**
	 * @return bool True if successfully connected, false otherwise.
	 */
	public function isConnected(): bool
	{
		return $this->connection !== null && $this->connection->isConnected() && $this->channel !== null && $this->channel->is_open();
	}

	/**
	 * Gracefully disconnects from the message queue.
	 */
	public function disconnect(): void
	{
		try {
			if ($this->isConnected())
				$this->logNotice('Disconnecting from message queue ' . $this->getConnectionInfo());
			if ($this->channel !== null)
				$this->channel->close();
			if ($this->connection !== null)
				$this->connection->close();
		} catch (Exception $e) {
			$this->logError('Failed to close connection to Message Queue', [$e]);
		} finally {
			$this->connection = null;
			$this->channel = null;
			$this->disconnected = true;
			$this->connectionChanged(false);
		}
	}

	/**
	 * Disconnects (if already connected) and opens a new connection.
	 */
	public function reconnect(): void
	{
		$this->disconnect();
		$this->connect();
	}

	/**
	 * Formats the current connection information as a string.
	 *
	 * @return string The connection info.
	 */
	public function getConnectionInfo(): string
	{
		return sprintf('%s:%s (mq=%s, ex=%s, rk=%s)', $this->getHost(), $this->getPort(), $this->getQueueName(),
			$this->getExchange(), $this->getRoutingKey());
	}

	/**
	 * Publishes the serializable data to the message queue.
	 *
	 * @param string $data The data to send to the message queue.
	 * @param string|null $queueName An alternative queue name to publish to (default = configured message queue name).
	 * @throws ConnectionException If failed to reconnect when a connection is lost.
	 */
	public function publish(string $data, string $queueName = null): void
	{
		if ($this->blocked)
			throw new ConnectionException('Message Queue connection blocked');

		if ($queueName === null)
			$queueName = $this->getRoutingKey();

		if (Utils::isStringNullOrEmpty($queueName))
			$queueName = $this->getQueueName();

		if (Utils::isStringNullOrEmpty($queueName))
			throw new BadConfigurationError(self::CONF_RABBITMQ_QUEUE_NAME . '|' . self::CONF_RABBITMQ_ROUTING_KEY,
				'Either a queue name or a routing key must be specified');

		$msg = new AMQPMessage($data, [
			'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
		]);

		try {
			if (!$this->isConnected())
				$this->reconnect();

			if ($this->isConnected())
				$this->channel->basic_publish($msg, $this->getExchange(), $queueName, true);

		} catch (Throwable $t) {
			$this->disconnect();
			throw new ConnectionException('Failed to publish message to queue', $t);
		}
	}

	protected function connectionChanged(bool $connected): void
	{
		foreach ($this->connectionChangedCallables as $connectionChangedCallable) {
			try {
				$connectionChangedCallable($connected);
			} catch (Exception $e) {
			}
		}
	}

	public function onConnectionChanged(callable $callback): void
	{
		$this->connectionChangedCallables[] = $callback;
	}

	/**
	 * Executes a callback when a message has been received. The callback must return a boolean value.
	 *
	 * @param callable $callback The callback to run when a message has been consumed.
	 * @param string|null $queueName An alternative queue name.
	 * @throws ConnectionException If not able to communicate with the host.
	 */
	public function onMessageConsumed(callable $callback, string $queueName = null): void
	{
		if ($queueName === null)
			$queueName = $this->getQueueName();

		if (Utils::isStringNullOrEmpty($queueName))
			throw new BadConfigurationError(self::CONF_RABBITMQ_QUEUE_NAME,
				'Queue name required for registering a message callback');

		$this->consumers[] = [
			self::CONSUMER_CALLABLE   => $callback,
			self::CONSUMER_QUEUE_NAME => $queueName,
		];

		if ($this->isConnected())
			$this->registerConsumer($callback, $queueName);
	}

	public function onMessageExpired(callable $callback): void
	{
		$this->messageExpiredCallable = $callback;
	}

	/**
	 * Register a message consumer. The supplied callback will be executed when a new message arrives.
	 *
	 * @param callable $callback The callback to run.
	 * @param string|null $queueName An alternative queue name.
	 * @throws ConnectionException If not able to communicate with the host.
	 */
	protected function registerConsumer(callable $callback, string $queueName): void
	{
		try {
			$this->logInfo("Registering message callback on queue: $queueName");

			// Register callback for consumed messages. This will call the supplied $callback
			// function when a message has been consumed. Furthermore the message will be ack'ed
			// if the callback returns true, otherwise the message will be nack'ed.
			$this->channel->basic_consume($queueName, '', false, false, false, false,
				function ($message) use ($callback) {

					/** @var AMQPMessage $message */
					$tag = $message->delivery_info['delivery_tag'];
					$logAdditionalInfo = [
						'tag' => $tag,
					];

					$this->logDebug('Consumed message', $logAdditionalInfo);

					// Check if a message has been nack'ed previously and is a candidate to be ack'ed with an
					// expiration reason. If that is the case, the message will be ack'ed and an error is logged.
					if ($this->checkExpiration($message)) {
						$this->logError('Message expired after ' . $this->getRetryCount() . " retries, ack'ing message",
							[
								'message-body' => $message->body,
								'message-ttl'  => $this->getRetryTTL(),
							]);
						$this->logDebug('Acknowledging message after expiration', $logAdditionalInfo);

						if ($this->messageExpiredCallable === null) {
							$this->logDebug('No expired message handler registered, sending ACK');
							$this->channel->basic_ack($tag);
							return;
						}

						$messageExpiredCallbackReturnValue = false;
						try {
							$messageExpiredCallback = $this->messageExpiredCallable;
							$messageExpiredCallbackReturnValue = $messageExpiredCallback($message->body, [
								'exchange'    => $message->delivery_info['exchange'],
								'routing_key' => $message->delivery_info['routing_key'],
								'retries'     => $this->getRetryCount(),
								'message-ttl' => $this->getRetryTTL(),
							]);
						} catch (Exception $e) {
							$this->logError('Exception occurred while handling expired message', [$e]);
						}

						if ($messageExpiredCallbackReturnValue) {
							$this->logDebug('Successfully handled expired message, sending ACK');
							$this->channel->basic_ack($tag);
						} else {
							$this->logWarning('Failed to handle expired message, sending NACK');
							$this->channel->basic_nack($tag);
						}
						return;
					}

					$callbackReturnValue = $callback($message->body, [
						'exchange'    => $message->delivery_info['exchange'],
						'routing_key' => $message->delivery_info['routing_key'],
					]);

					if (!is_bool($callbackReturnValue))
						throw new LogicException('Wrong return type from message callback. Expected boolean value, got ' . gettype($callbackReturnValue));

					if ($callbackReturnValue) {
						$this->logDebug('Acknowledging message', $logAdditionalInfo);
						$this->channel->basic_ack($tag);
					} else {
						$this->logDebug('NOT acknowledging message', $logAdditionalInfo);
						$this->channel->basic_nack($tag);
					}
				});
		} /** @noinspection PhpRedundantCatchClauseInspection */ catch (AMQPIOException $e) {
			throw new ConnectionException('No connection to message queue', $e);
		}
	}

	/**
	 * Checks if a dead-letter message should be marked as expired after a configured amount of retries.
	 * The number of retries will be incremented by RabbitMQ in the message headers, so we simply check
	 * if the retry count exceeds the maximum number of retries.
	 *
	 * @param AMQPMessage $message The incoming message to check.
	 * @return bool True if the retry count has been exceeded, false if not.
	 */
	protected function checkExpiration(AMQPMessage $message): bool
	{
		if (!$message->has('application_headers'))
			return false;

		$headers = $message->get('application_headers');
		if (!($headers instanceof AMQPTable))
			return false;

		$data = $headers->getNativeData();
		if (!isset($data['x-death']))
			return false;

		$lastEntry = current($data['x-death']);
		if (!isset($lastEntry['count']))
			return false;

		$this->logInfo(sprintf('Current retry: %d, max retries: %d', $lastEntry['count'], $this->getRetryCount()));

		return (((int)$lastEntry['count']) >= $this->getRetryCount());
	}
}