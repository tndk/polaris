<?php
declare(strict_types=1);

define('ROOT_DIR', dirname(__DIR__, 2));
define('ENV_FILE', ROOT_DIR . '/.env');
require_once ROOT_DIR . '/vendor/autoload.php';

# Read all environment variables in .env file
if (file_exists(ENV_FILE)) {
	foreach (file(ENV_FILE) as $line) {
		if (preg_match('/^[A-Z]/', $line)) {
			[$name, $value] = explode('=', trim($line), 2);
			if (!getenv($name))
				putenv("$name=$value");
		}
	}
}