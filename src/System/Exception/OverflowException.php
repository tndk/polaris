<?php
declare(strict_types=1);
namespace Telenor\System\Exception;

/**
 * Exception to use when an overflow occurs.
 */
class OverflowException extends PolarisException
{
}