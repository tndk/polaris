<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker\UserSync;

use Telenor\Model\IUser;

class ServicenowAllUsersSync extends AServicenowUserSync
{

	/** Fetch all Servicenow users
	 *
	 * @return IUser[]
	 */
	public function getUsers(): array
	{
		return $this->getAllUsers();

	}

}