<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\System\Pattern\ISingleton;
use Telenor\System\Pattern\TSingleton;

final class TSingletonTest extends TestCase
{
	public function testGetInstance(): void
	{
		/** @var ISingleton */
		$instance = new class
		{
			use TSingleton;

			public function __construct()
			{
			}
		};

		$this->assertSame($instance->getInstance(), $instance->getInstance());
	}
}