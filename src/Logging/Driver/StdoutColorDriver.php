<?php
declare(strict_types=1);
namespace Telenor\Logging\Driver;

use Psr\Log\LogLevel;

class StdoutColorDriver extends StdoutDriver
{
	#region Color definitions
	public const COLOR_BLACK = "\e[0;30m";
	public const COLOR_DARK_GREY = "\e[1;30m";
	public const COLOR_RED = "\e[0;31m";
	public const COLOR_LIGHT_RED = "\e[1;31m";
	public const COLOR_GREEN = "\e[0;32m";
	public const COLOR_LIGHT_GREEN = "\e[1;32m";
	public const COLOR_BROWN = "\e[0;33m";
	public const COLOR_YELLOW = "\e[1;33m";
	public const COLOR_BLUE = "\e[0;34m";
	public const COLOR_LIGHT_BLUE = "\e[1;34m";
	public const COLOR_MAGENTA = "\e[0;35m";
	public const COLOR_LIGHT_MAGENTA = "\e[1;35m";
	public const COLOR_CYAN = "\e[0;36m";
	public const COLOR_LIGHT_CYAN = "\e[1;36m";
	public const COLOR_LIGHT_GREY = "\e[0;37m";
	public const COLOR_WHITE = "\e[1;37m";

	public const COLOR_NONE = '';
	public const RESET_COLOR = "\e[0m";
	#endregion

	protected $logLevelColors = [
		LogLevel::EMERGENCY => self::COLOR_RED,
		LogLevel::ALERT     => self::COLOR_RED,
		LogLevel::CRITICAL  => self::COLOR_LIGHT_RED,
		LogLevel::ERROR     => self::COLOR_LIGHT_RED,
		LogLevel::WARNING   => self::COLOR_YELLOW,
		LogLevel::NOTICE    => self::COLOR_WHITE,
		LogLevel::INFO      => self::COLOR_NONE,
		LogLevel::DEBUG     => self::COLOR_NONE,
	];

	public function log($level, $message, array $context = []): void
	{
		$hasColor = (isset($this->logLevelColors[$level]) && $this->logLevelColors[$level] !== self::COLOR_NONE);

		if ($hasColor)
			$message = $this->logLevelColors[$level] . $message;

		parent::log($level, $message, $context);

		if ($hasColor)
			echo self::RESET_COLOR;
	}
}