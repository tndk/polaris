<?php
declare(strict_types=1);

namespace Telenor\Message;

use DateTime;
use Exception;
use Telenor\System\Exception\InvalidMessageException;
use Telenor\System\Utils;
use function in_array;

class SMSMessage extends APayloadMessage
{
	public const ATTR_TYPE_SMS = 'sms';
	public const ATTR_TYPE_PASSWORD_SMS = 'password sms';

	public const VALID_ATTR_TYPES = [
		self::ATTR_TYPE_SMS,
		self::ATTR_TYPE_PASSWORD_SMS,
	];

	/** @var string */
	protected $source;

	/** @var string */
	protected $sender;

	/** @var string */
	protected $recipient;

	/** @var string */
	protected $message;

	/**
	 * SMSMessage constructor.
	 *
	 * @param array $attributes
	 * @throws InvalidMessageException
	 */
	public function __construct(array $attributes = [])
	{
		if (!isset($attributes['transaction_id']))
			throw new InvalidMessageException('Did not find any transaction id in the message');

		if (!isset($attributes['timestamp']))
			throw new InvalidMessageException('Did not find any timestamp in the message');

		if (!isset($attributes['type']) || !in_array(strtolower($attributes['type']), self::VALID_ATTR_TYPES, true))
			throw new InvalidMessageException('Invalid sms type was found in the message attributes');

		if (!isset($attributes['payload']) || empty($attributes['payload']))
			throw new InvalidMessageException('Did not find any payload in the message');

		parent::__construct(strtolower($attributes['type']), 'create');

		try {
			$this->timestamp = new DateTime($attributes['timestamp']);
		} catch (Exception $e) {
			throw new InvalidMessageException('Invalid timestamp found in the message', $e);
		}

		$this->transactionId = $attributes['transaction_id'];
		$this->setSource($attributes['source'] ?? '(unknown)');
		$this->setSender($attributes['payload']['sender'] ?? null);
		$this->setRecipient($attributes['payload']['recipient'] ?? null);
		$this->setMessage($attributes['payload']['message'] ?? null);
	}

	/**
	 * @return string
	 */
	public function getSource(): string
	{
		return $this->source;
	}

	/**
	 * @param string $source
	 */
	public function setSource(string $source): void
	{
		$this->source = $source;
	}

	/**
	 * @return string
	 */
	public function getSender(): string
	{
		return $this->sender;
	}

	/**
	 * @param string $sender
	 * @throws InvalidMessageException
	 */
	public function setSender(?string $sender): void
	{
		if (Utils::isStringNullOrEmpty($sender))
			throw new InvalidMessageException('Did not find any sender in the message payload');

		$this->sender = $sender;
	}

	/**
	 * @return string
	 */
	public function getRecipient(): string
	{
		return $this->recipient;
	}

	/**
	 * @param string $recipient
	 * @throws InvalidMessageException
	 */
	public function setRecipient(?string $recipient): void
	{
		if (Utils::isStringNullOrEmpty($recipient))
			throw new InvalidMessageException('Did not find any recipient in the message payload');

		$this->recipient = $recipient;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}

	/**
	 * @param string $message
	 * @throws InvalidMessageException
	 */
	public function setMessage(?string $message): void
	{
		if (Utils::isStringNullOrEmpty($message))
			throw new InvalidMessageException('Did not find any text message in the message payload');

		$this->message = $message;
	}

	/**
	 * @return bool
	 */
	public function isPassword(): bool
	{
		return $this->getType() === self::ATTR_TYPE_PASSWORD_SMS;
	}

	public function toArray(): array
	{
		return [
			'sender'    => $this->getSender(),
			'recipient' => $this->getRecipient(),
			'message'   => $this->getMessage(),
		];
	}
}