<?php
declare(strict_types=1);
namespace Telenor\Worker\TicketWorker\Controller;

use Amp\Http\Server\Request;
use Amp\Http\Server\Response;
use Amp\Http\Server\Router;
use Exception;
use Telenor\Message\IncidentTicketMessage;
use Telenor\Service\IncidentService;
use Throwable;
use function json_encode;

class IncidentController extends ATicketController
{

	protected $ticketService;

	public function __construct(IncidentService $ticketService)
	{
		$this->ticketService = $ticketService;
	}

	/*
	TODO (HEIP):
	We need some sort of authentication on these requests. We should not
	blindly accept any requester here, as vendors can then retrieve all our tickets.
	 */

	public function index(): Response
	{
		try {
			// TODO: remove limit
			# $incidents = $this->client->index(['sysparm_limit' => 10]);
			$incidents = $this->ticketService->getIncidents(['sysparm_limit' => 10]);
		} catch (Exception $e) {
			return new Response(404, ['Content-Type' => 'application/json'],
				json_encode(['error' => 'No incidients found'], JSON_THROW_ON_ERROR));
		}
		return new Response(200, ['Content-Type' => 'application/json'],
			json_encode($incidents, JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES));
	}

	public function show(Request $request): Response
	{
		$id = $request->getAttribute(Router::class)['id'];

		try {
			$incident = $this->ticketService->getIncident($id);
			// $response = $this->client->get($id);
		} catch (Exception $e) {
			return new Response(404, ['Content-Type' => 'application/json'],
				json_encode(['error' => 'Incident not found']));
		}

		return new Response(200, ['Content-Type' => 'application/json'],
			json_encode($incident->getValues(), JSON_UNESCAPED_SLASHES));
	}

	public function insert(Request $request)
	{
		try {
			$data = yield from $this->getBody($request);

			$message = new IncidentTicketMessage(IncidentTicketMessage::TYPE_INCIDENT,
				IncidentTicketMessage::ACTION_CREATE);

			$this->createTicket($data, $message);
			return $this->handleMessage($message);

		} catch (Throwable $t) {
			return $this->handleException($t);

		}
	}

	public function update(Request $request)
	{
		try {
			$data = yield from $this->getBody($request);

			$message = new IncidentTicketMessage(IncidentTicketMessage::TYPE_INCIDENT,
				IncidentTicketMessage::ACTION_UPDATE);

			$this->updateTicket($data, $message);
			return $this->handleMessage($message);
		} catch (Throwable $t) {
			return $this->handleException($t);

		}

	}

}