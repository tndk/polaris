<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use Telenor\Message\TicketUpdateMessage;
use Telenor\System\Exception\InvalidMessageException;
use Telenor\System\Exception\ValidationException;
use Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler\NokiaTicketUpdateHandler;

class NokiaTicketUpdateHandlerTest extends TestCase
{
	protected $handler;

	public function __construct($name = null, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);

		putenv('NOKIA_SSDP_URL=http://localhost');
		$this->handler = new NokiaTicketUpdateHandler();
	}

	public function _ymlDataProviderCreateIncident(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_CreateIncident.yml', __DIR__, __CLASS__));
	}

	public function _ymlDataProviderUpdateIncident(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_UpdateIncident.yml', __DIR__, __CLASS__));
	}

	public function _ymlDataProviderCreateChange(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_CreateChange.yml', __DIR__, __CLASS__));
	}

	public function _ymlDataProviderUpdateChange(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_UpdateChange.yml', __DIR__, __CLASS__));
	}

	public function _ymlDataProviderFieldForceTasks(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_FieldForceTasks.yml', __DIR__, __CLASS__));
	}

	/**
	 * @dataProvider _ymlDataProviderCreateIncident
	 * @param array $input
	 * @param array $output
	 * @throws InvalidMessageException
	 * @throws ValidationException
	 */
	public function testCreateIncident(array $input, array $output): void
	{
		static::assertSame($output, $this->handler->mapCreateIncident(new TicketUpdateMessage($input)));
	}

	/**
	 * @dataProvider _ymlDataProviderUpdateIncident
	 * @param array $input
	 * @param array $output
	 * @throws InvalidMessageException
	 * @throws ValidationException
	 */
	public function testUpdateIncident(array $input, array $output): void
	{
		static::assertSame($output, $this->handler->mapUpdateIncident(new TicketUpdateMessage($input)));
	}

	/**
	 * @dataProvider _ymlDataProviderCreateChange
	 * @param array $input
	 * @param array $output
	 * @throws InvalidMessageException
	 * @throws ValidationException
	 */
	public function testCreateChange(array $input, array $output): void
	{
		static::assertSame($output, $this->handler->mapCreateChange(new TicketUpdateMessage($input)));
	}

	/**
	 * @dataProvider _ymlDataProviderUpdateChange
	 * @param array $input
	 * @param array $output
	 * @throws InvalidMessageException
	 * @throws ValidationException
	 */
	public function testUpdateChange(array $input, array $output): void
	{
		static::assertSame($output, $this->handler->mapUpdateChange(new TicketUpdateMessage($input)));
	}

	/**
	 * @dataProvider _ymlDataProviderFieldForceTasks
	 * @param array $input
	 * @param array $output
	 * @throws InvalidMessageException
	 * @throws ValidationException
	 */
	public function testHandleFieldForceTask(array $input, array $output): void
	{
		$requests = $this->handler->buildFieldForceTaskRequests(new TicketUpdateMessage($input));
		static::assertSame($output, iterator_to_array($requests));
	}

	public function testMapTicketPriorities(): void
	{
		static::assertSame(['1-Extensive/Widespread', '1-Critical'], $this->handler->mapTicketPriorities(1, 1));
		static::assertSame(['2-Significant/Large', '1-Critical'], $this->handler->mapTicketPriorities(1, 2));
		static::assertSame(['2-Significant/Large', '2-High'], $this->handler->mapTicketPriorities(1, 3));
		static::assertSame(['1-Extensive/Widespread', '2-High'], $this->handler->mapTicketPriorities(2, 1));
		static::assertSame(['3-Moderate/Limited', '2-High'], $this->handler->mapTicketPriorities(2, 2));
		static::assertSame(['3-Moderate/Limited', '3-Medium'], $this->handler->mapTicketPriorities(2, 3));
		static::assertSame(['1-Extensive/Widespread', '4-Low'], $this->handler->mapTicketPriorities(3, 1));
		static::assertSame(['3-Moderate/Limited', '4-Low'], $this->handler->mapTicketPriorities(3, 2));
		static::assertSame(['4-Minor/Local', '4-Low'], $this->handler->mapTicketPriorities(3, 3));
	}

	public function testMapTaskPriorities(): void
	{
		static::assertSame('Telenor P0', $this->handler->mapTaskPriorities(1, 1));
		static::assertSame('Telenor P1', $this->handler->mapTaskPriorities(1, 2));
		static::assertSame('Telenor P1', $this->handler->mapTaskPriorities(1, 3));
		static::assertSame('Telenor P2', $this->handler->mapTaskPriorities(2, 1));
		static::assertSame('Telenor P2', $this->handler->mapTaskPriorities(2, 2));
		static::assertSame('Telenor P3', $this->handler->mapTaskPriorities(2, 3));
		static::assertSame('Telenor P3', $this->handler->mapTaskPriorities(3, 1));
		static::assertSame('Telenor P4', $this->handler->mapTaskPriorities(3, 2));
		static::assertSame('Telenor P5', $this->handler->mapTaskPriorities(3, 3));
	}

	public function testMapIncidentStatus(): void
	{
		static::assertSame('New', $this->handler->mapIncidentStatus(TicketUpdateMessage::STATUS_NEW));
		static::assertSame('WorkInProgress', $this->handler->mapIncidentStatus(TicketUpdateMessage::STATUS_PENDING));
		static::assertSame('WorkInProgress', $this->handler->mapIncidentStatus(TicketUpdateMessage::STATUS_ON_HOLD));
		static::assertSame('Resolved', $this->handler->mapIncidentStatus(TicketUpdateMessage::STATUS_RESOLVED));
		static::assertSame('Closed', $this->handler->mapIncidentStatus(TicketUpdateMessage::STATUS_CLOSED));
		static::assertSame('Resolved', $this->handler->mapIncidentStatus(TicketUpdateMessage::STATUS_CANCELED));
	}

	public function testMapChangeStatus(): void
	{
		static::assertSame('New', $this->handler->mapChangeStatus(TicketUpdateMessage::STATUS_NEW));
		static::assertSame('New', $this->handler->mapChangeStatus(TicketUpdateMessage::STATUS_ASSESS));
		static::assertSame('New', $this->handler->mapChangeStatus(TicketUpdateMessage::STATUS_AUTHORIZE));
		static::assertSame('Assigned', $this->handler->mapChangeStatus(TicketUpdateMessage::STATUS_SCHEDULED));
		static::assertSame('WorkInProgress', $this->handler->mapChangeStatus(TicketUpdateMessage::STATUS_IMPLEMENT));
		static::assertSame('Resolved', $this->handler->mapChangeStatus(TicketUpdateMessage::STATUS_REVIEW));
		static::assertSame('Closed', $this->handler->mapChangeStatus(TicketUpdateMessage::STATUS_CLOSED));
		static::assertSame('Cancelled', $this->handler->mapChangeStatus(TicketUpdateMessage::STATUS_CANCELED));
	}

	public function testMapTaskMessageType(): void
	{
		static::assertSame('WORKLOG_UPDATE', $this->handler->mapTaskMessageType(TicketUpdateMessage::STATUS_OPEN));
		static::assertSame('WORKLOG_UPDATE',
			$this->handler->mapTaskMessageType(TicketUpdateMessage::STATUS_WORK_IN_PROGRESS));
		static::assertSame('WORKLOG_UPDATE', $this->handler->mapTaskMessageType(TicketUpdateMessage::STATUS_PENDING));
		static::assertSame('CLOSE', $this->handler->mapTaskMessageType(TicketUpdateMessage::STATUS_CLOSED_COMPLETE));
		static::assertSame('WORKLOG_UPDATE',
			$this->handler->mapTaskMessageType(TicketUpdateMessage::STATUS_CLOSED_INCOMPLETE));
		static::assertSame('WORKLOG_UPDATE',
			$this->handler->mapTaskMessageType(TicketUpdateMessage::STATUS_CLOSED_SKIPPED));
		static::assertSame('CANCEL', $this->handler->mapTaskMessageType(TicketUpdateMessage::STATUS_CANCEL));
		static::assertSame('RE-ASSIGN', $this->handler->mapTaskMessageType(TicketUpdateMessage::STATUS_REJECT));
		static::assertSame('WORKLOG_UPDATE', $this->handler->mapTaskMessageType(TicketUpdateMessage::STATUS_RESOLVE));

	}

	/** @testdox Fix the fucking retarded response from Nokia API */
	public function testFixTheFuckingRetardedResponseFromNokiaAPI(): void
	{
		// No content in message property
		static::assertSame(<<<EOT
		{
			"ticketID" : "INC000002795736",
			"status" : "OK",
			"message" : ""
		}
		EOT, $this->handler->fixTheFuckingRetardedResponseFromNokiaAPI(<<<EOT
		{
			"ticketID" : "INC000002795736",
			"status" : "OK",
			"message" : ""
		}
		EOT
		));

		// Valid content in message property
		static::assertSame(<<<EOT
		{
			"taskID" : "",
			"status" : "NOK",
			"message" : "WFMS requires 'additionalInfo' parameter to be set!"
		}
		EOT, $this->handler->fixTheFuckingRetardedResponseFromNokiaAPI(<<<EOT
		{
			"taskID" : "",
			"status" : "NOK",
			"message" : "WFMS requires 'additionalInfo' parameter to be set!"
		}
		EOT
		));

		// Invalid (unescaped) content in message property
		$invalidJSON = <<<EOT
		{
			"ticketID" : "CRQ000002147922",
			"status" : "OK",
			"message" : "You do not have permission to move to the status of "Implementation In Progress"."
		}
		EOT;

		static::assertSame(<<<EOT
		{
			"ticketID" : "CRQ000002147922",
			"status" : "OK",
			"message" : "You do not have permission to move to the status of \"Implementation In Progress\"."
		}
		EOT, $this->handler->fixTheFuckingRetardedResponseFromNokiaAPI($invalidJSON));

		// Now actually tro to decode the fixed JSON
		static::assertSame([
			'ticketID' => 'CRQ000002147922',
			'status'   => 'OK',
			'message'  => 'You do not have permission to move to the status of "Implementation In Progress".',
		], json_decode($this->handler->fixTheFuckingRetardedResponseFromNokiaAPI($invalidJSON), true, 512,
			JSON_THROW_ON_ERROR));
	}
}
