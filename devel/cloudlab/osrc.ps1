$env:OS_AUTH_URL = "https://10.207.134.250:5000/v3"
$env:OS_PROJECT_ID = "c4af2053f5094f3d9945806029b2e86b"
$env:OS_PROJECT_NAME = "Polaris"
$env:OS_USER_DOMAIN_NAME = "Default"
$env:OS_PROJECT_DOMAIN_ID = "default"
$env:OS_REGION_NAME = "RegionOne"
$env:OS_INTERFACE = "public"
$env:OS_IDENTITY_API_VERSION = 3

# read credentials from console
$env:OS_USERNAME = Read-Host -Prompt 'Enter OpenStack username'
$osPassword = Read-Host -Prompt 'Enter OpenStack password' -AsSecureString

# convert SecureString back into plaintext
$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($osPassword)
$env:OS_PASSWORD = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)