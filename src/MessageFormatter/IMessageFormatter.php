<?php
declare(strict_types=1);

namespace Telenor\MessageFormatter;

use Telenor\Message\IMessage;

interface IMessageFormatter
{
	public function serialize(IMessage $message): string;

	public function unserialize(string $message): IMessage;
}