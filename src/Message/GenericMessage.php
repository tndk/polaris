<?php
declare(strict_types=1);

namespace Telenor\Message;

class GenericMessage extends AMessage
{
	/** @var array */
	protected $data;

	public function __construct(array $data)
	{
		parent::__construct($data);
		$this->setData($data);
	}

	/**
	 * @return array
	 */
	public function getData(): array
	{
		return $this->data;
	}

	/**
	 * @param array $data
	 */
	public function setData(array $data): void
	{
		$this->data = $data;
	}

	public function getAttributes(): array
	{
		return array_merge(parent::getAttributes(), [
			'data' => $this->getData(),
		]);
	}
}