<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker\UserSync;

use Telenor\Model\IUser;
use Telenor\System\Exception\InvalidDataException;
use function count;

class WorkdaySingleUsersSync extends AWorkdayUsersSync
{

	protected $bindings;

	public function __construct($user)
	{
		$bindings = ['initials' => $user];

		parent::__construct($bindings);
	}

	/**
	 * Get a single user from Servicenow
	 *
	 * @return IUser[]
	 * @throws InvalidDataException When a single user is not found in the Workday database
	 */
	public function getUsers(): array
	{
		$this->sql .= ' and upper(e.initialer) = :initials';
		$users = $this->getAllUsers();

		if (count($users) !== 1) {
			{
				$this->logInfo('User not found in Workday', ['user' => $this->bindings]);
				throw new InvalidDataException('User not found in Workday');
			}
		}
		return $users;

	}
}