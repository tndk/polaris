<?php
/** @noinspection PhpUnused */
declare(strict_types=1);
namespace Telenor\Worker\TicketWorker\Controller;

use Amp\Http\Server\Request;
use Amp\Http\Server\Response;
use Amp\Http\Server\Router;
use DateTime;
use DateTimeZone;
use Exception;
use Generator;
use Telenor\Logging\TLog;
use Telenor\Message\ATicketMessage;
use Telenor\Message\ChangeTicketMessage;
use Telenor\Message\FieldForceTicketMessage;
use Telenor\Message\IncidentTicketMessage;
use Telenor\MessageQueue\TMessageQueue;
use Telenor\Model\ServiceNowAttachment;
use Telenor\Model\ServiceNowTask;
use Telenor\Service\ServiceNowTableClient;
use Telenor\System\Config;
use Telenor\System\Exception\InvalidRequestException;
use Telenor\System\Exception\PolarisException;
use Telenor\System\Exception\ValidationException;
use Telenor\System\Utils;
use Throwable;
use function base64_encode;
use function in_array;
use function is_array;
use function json_decode;
use function json_encode;
use function preg_match;
use function strtolower;

/**
 * Ticket handler for Nokia incidents, changes, and field force tasks.
 * This class handles the incoming HTTP requests from Nokia's systems
 * and utilizes the lagacy Remedy API.
 *
 * This class maps/converts data and sends it to ServiceNow through TicketNotificationWorker.
 *
 * All map* methods in this class does all the actual mapping, and we have
 * unit tests on all of those methods (+ a few more).
 */
class RemedyController
{
	use TLog;
	use TMessageQueue;

	// ServiceNow impact states
	public const SERVICENOW_IMPACT_1_HIGH = 1;
	public const SERVICENOW_IMPACT_2_MEDIUM = 2;
	public const SERVICENOW_IMPACT_3_LOW = 3;

	// ServiceNow urgency states
	public const SERVICENOW_URGENCY_1_HIGH = 1;
	public const SERVICENOW_URGENCY_2_MEDIUM = 2;
	public const SERVICENOW_URGENCY_3_LOW = 3;

	// Nokia impact states
	public const NOKIA_IMPACT_1_EXTENSIVE = '1-Extensive/Widespread';
	public const NOKIA_IMPACT_2_SIGNIFICANT = '2-Significant/Large';
	public const NOKIA_IMPACT_3_MODERATE = '3-Moderate/Limited';
	public const NOKIA_IMPACT_4_MINOR = '4-Minor/Localized';

	// Nokia urgency states
	public const NOKIA_URGENCY_1_CRITICAL = '1-Critical';
	public const NOKIA_URGENCY_2_HIGH = '2-High';
	public const NOKIA_URGENCY_3_MEDIUM = '3-Medium';
	public const NOKIA_URGENCY_4_LOW = '4-Low';

	//Nokia default planning message for all changes except minor
	public const NOKIA_DEFAULT_PLANNING_MESSAGE = 'Please check the details in the description field';

	protected $validAPIKeys = [];

	protected $changeTemplates = [];

	/** @var string */
	protected $source;

	/** @var string */
	protected $fieldSupplier;

	/** @var array */
	protected $assigneeMapping;

	public function __construct()
	{
		$this->validAPIKeys = Utils::mapStringToHashMap(Config::envString('REMEDY_API_KEYS'));
		$this->changeTemplates = Utils::mapStringToHashMap(Config::envString('CHANGE_TEMPLATES'));
		$this->source = Config::envString('REMEDY_API_SOURCE', 'nokia');
		$this->fieldSupplier = Config::envString('MYWFM_FIELD_SUPPLIER', 'Eltel');
		$this->assigneeMapping = Utils::mapStringToHashMap(Config::envString('ASSIGNEE_MAPPING',
			'Telenor=OPS SOC;Nokia=OPS Nokia'));
	}

	#region Create Ticket

	/**
	 * Builds a ticket creation payload and sends it to ServiceNow.
	 *
	 * @param Request $request The raw request coming from Nokia.
	 * @return Response|Generator The Response to send back to the client (Nokia).
	 */
	public function createTicket(Request $request)
	{
		try {
			$this->checkAPIKey($request);
			$data = $this->parseBody(yield $request->getBody()->buffer());

			// Build message to ServiceNow
			$this->logDebug('Building create ticket message', ['data' => $data]);
			$message = $this->buildCreateTicketMessage($data);

			// Push to queue
			$this->logInfo('Publishing message onto the queue', ['message' => $message]);
			$this->publishToQueue($message);

			return $this->createSuccessResponse($message->getCorrelationId());
		} catch (InvalidRequestException $e) {
			$this->logInfo('Detected invalid incoming request', [$e]);
			return $this->createFailureResponse($e->getMessage());
		} catch (Throwable $t) {
			$this->logError('Exception occurred while trying to create ticket', [$t]);
			return $this->createFailureResponse('Internal Server Error');
		}
	}

	/**
	 * Builds a create payload message to ServiceNow, either an incident or a change request.
	 *
	 * @param array $data The data received by Nokia.
	 * @return ATicketMessage The complete message to send to ServiceNow.
	 * @throws Exception On any error.
	 */
	public function buildCreateTicketMessage(array $data): ATicketMessage
	{
		try {
			$this->validateRequiredFields($data, ['Initiator', 'TicketType', 'Description', 'Impact', 'Severity']);

			switch ($data['TicketType']) {
				case 'ITIL RFC':
					// Changes requires some more fields in the JSON payload
					$this->validateRequiredFields($data, ['ChangeType', 'WorkStart', 'PermanentSolution']);
					$message = new ChangeTicketMessage(ChangeTicketMessage::ACTION_CREATE);
					$this->setCommonProperties($message, $data);
					$message->setCorrelationId($message->getSource() . '-' . Utils::generateChangeCorrelationId());
					break;
				case 'ITIL Incident':
					$message = new IncidentTicketMessage(IncidentTicketMessage::TYPE_INCIDENT,
						IncidentTicketMessage::ACTION_CREATE);
					$this->setCommonProperties($message, $data);
					$message->setCorrelationId($message->getSource() . '-' . Utils::generateIncidentCorrelationId());
					break;
				default:
					throw new InvalidRequestException('Unsupported ticket type supplied: ' . $data['TicketType']);
			}

			// ITIL Incident can either also be an Incident Task (a task created from an incident)
			// We identify this based off the ID Nokia gave it
			if (isset($data['ExternalTicketId']) && strpos($data['ExternalTicketId'], 'TAS') === 0) {
				$message->setCorrelationId($message->getSource() . '-' . Utils::generateTaskCorrelationId());
			}

			$message->addExtendedPayload('ExternalTicketId', $this->getFieldString($data, 'ExternalTicketId'));
			return $message;

		} catch (ValidationException $e) {
			throw new InvalidRequestException('Validation failed for property ' . $e->getProperty() . ': ' . $e->getMessage());
		}
	}
	#endregion

	#region Update Ticket
	/**
	 * Builds a ticket update payload and sends it to ServiceNow.
	 *
	 * @param Request $request The raw request coming from Nokia.
	 * @return Response|Generator The Response to send back to the client (Nokia).
	 */
	public function updateTicket(Request $request)
	{
		try {
			$this->checkAPIKey($request);
			$ticketID = $request->getAttribute(Router::class)['ticketID'];
			$data = $this->parseBody(yield $request->getBody()->buffer());

			// Build message to ServiceNow
			$this->logDebug('Building update ticket message', ['ticketID' => $ticketID, 'data' => $data]);
			$message = $this->buildUpdateTicketMessage($data, $ticketID);

			// Push to queue
			$this->logInfo('Publishing message onto the queue', ['message' => $message]);
			$this->publishToQueue($message);

			return $this->createSuccessResponse($message->getCorrelationId());
		} catch (InvalidRequestException $e) {
			$this->logInfo('Detected invalid incoming request', [$e]);
			return $this->createFailureResponse($e->getMessage());
		} catch (Throwable $t) {
			$this->logError('Exception occurred while trying to update ticket', [$t]);
			return $this->createFailureResponse('Internal Server Error');
		}
	}

	/**
	 * Builds an update payload message to ServiceNow, either an incident or a change request.
	 *
	 * @param array $data The data received by Nokia.
	 * @param string $ticketID The ticket ID to update (sent as URL parameter).
	 * @return ATicketMessage The complete message to send to ServiceNow.
	 * @throws InvalidRequestException If any validation error occurs, or invalid ticket ID received.
	 */
	public function buildUpdateTicketMessage(array $data, string $ticketID): ATicketMessage
	{
		try {

			// Try to figure out if this is an approval..
			// Approvals will set the TicketStatus to Assigned and ChangeApproval to Pending.
			// Most other things are null
			$isApprovalMessage = $this->isApprovalMessage($data);

			// Disable checks for Nokia worklog updates and approval messages
			if (!$isApprovalMessage && !isset($data['UpdateWorklog']) && isset($data['TicketStatus'])) {
				// A change from Nokia does not always contain the field TicketItem
				if ($this->isChange($ticketID)) {
					$this->validateRequiredFields($data, ['Impact', 'Severity']);
				} else {
					$this->validateRequiredFields($data, ['TicketItem', 'Impact', 'Severity']);
				}

			}

			switch ($ticketID) {
				case $this->isChange($ticketID):
					$message = new ChangeTicketMessage(ChangeTicketMessage::ACTION_UPDATE);
					break;
				case $this->isTask($ticketID):
				case $this->isIncident($ticketID):
					$message = new IncidentTicketMessage(IncidentTicketMessage::TYPE_INCIDENT,
						IncidentTicketMessage::ACTION_UPDATE);
					break;
				default:
					throw new InvalidRequestException('Could not determine ticket type from ticket ID');
			}

			// This sets nokia_approved = true in the payload. We also won't need common properties.
			if ($isApprovalMessage) {
				$message->setExternalApproval('true');
			}

			$message->setCorrelationId($ticketID);
			$this->setCommonProperties($message, $data);
			$message->setWorknotes($this->getFieldString($data, 'UpdateWorklog', ''));

			return $message;

		} catch (ValidationException $e) {
			// "Translate" the exception to a more generic one
			throw new InvalidRequestException('Validation failed for property ' . $e->getProperty() . ': ' . $e->getMessage());
		}
	}

	#endregion

	#region Add attachment(s) to ticket
	/**
	 * Adds attachment(s) to an existing ticket.
	 *
	 * @param Request $request The raw request coming from Nokia.
	 * @return Response|Generator The Response to send back to the client (Nokia).
	 */
	public function addAttachments(Request $request)
	{
		try {
			$this->checkAPIKey($request);
			$ticketID = $request->getAttribute(Router::class)['ticketID'];
			$data = $this->parseBody(yield $request->getBody()->buffer());

			// Build message to ServiceNow
			$this->logDebug('Building update attachment ticket message', ['ticketID' => $ticketID, 'data' => $data]);
			$message = $this->buildUpdateTicketMessageWithAttachments($data, $ticketID);

			// Push to queue
			$this->logInfo('Publishing message onto the queue', ['message' => $message]);
			$this->publishToQueue($message);

			return $this->createSuccessResponse($message->getCorrelationId());
		} catch (InvalidRequestException $e) {
			$this->logInfo('Detected invalid incoming request', [$e]);
			return $this->createFailureResponse($e->getMessage());
		} catch (Throwable $t) {
			$this->logError('Exception occurred while trying to add attachment to ticket', [$t]);
			return $this->createFailureResponse('Internal Server Error');
		}
	}

	/**
	 * Build a complete ticket message (incident, change request) and adds attachment to that message.
	 *
	 * @param array $data The data received by Nokia.
	 * @param string $ticketID The ticket ID to update (sent as URL parameter).
	 * @return ATicketMessage The complete message to send to ServiceNow.
	 * @throws InvalidRequestException If no attachments are found in the $data array, or invalid ticket ID received.
	 */
	public function buildUpdateTicketMessageWithAttachments(array &$data, string $ticketID): ATicketMessage
	{
		try {
			$this->validateRequiredFields($data, ['Attachments']);
			$attachments = $data['Attachments'];
			if (!is_array($attachments) || count($attachments) === 0)
				throw new InvalidRequestException('No attachments found in request');

			switch ($ticketID) {
				case $this->isChange($ticketID):
					$message = new ChangeTicketMessage(ChangeTicketMessage::ACTION_UPDATE);
					break;
				case $this->isTask($ticketID):
				case $this->isIncident($ticketID):
					$message = new IncidentTicketMessage(IncidentTicketMessage::TYPE_INCIDENT,
						IncidentTicketMessage::ACTION_UPDATE);
					break;
				default:
					throw new InvalidRequestException('Could not determine ticket type from ticket ID');
			}

			foreach ($attachments as $attachment) {
				$this->validateRequiredFields($attachment, ['FileName', 'File']);
				$message->addAttachment($attachment['FileName'], $attachment['File']);
			}

			$message->setStatus(ATicketMessage::STATUS_NO_CHANGE);
			$message->setCorrelationId($ticketID);
			$message->setSource($this->source);

			// MAALS: If worknotes is empty, add a useless message.
			// It's a mandatory field in ServiceNow...
			if (empty($message->getWorknotes())) {
				$message->setWorknotes('Adding attachment.');
			}

			return $message;

		} catch (ValidationException $e) {
			throw new InvalidRequestException('Validation failed for property ' . $e->getProperty() . ': ' . $e->getMessage());
		}
	}
	#endregion

	#region Get attachment(s) from ticket
	/**
	 * Retrieves all attachments directly from ServiceNow for a given ticket.
	 *
	 * @param Request $request The raw request coming from Nokia.
	 * @return Response The Response to send back to the client (Nokia).
	 */
	public function getAttachments(Request $request): Response
	{
		try {
			$this->checkAPIKey($request);
			$ticketID = $request->getAttribute(Router::class)['ticketID'];

			$attachmentsData = [
				'Attachments' => [],
			];

			$snClient = new ServiceNowTableClient(new ServiceNowTask());
			$ticketInfo = $snClient->get($ticketID);
			if (count($ticketInfo) !== 1) {
				$this->logNotice("Unable to find ticket in ServiceNow. TicketID: $ticketID", ['ticketID' => $ticketID]);
				return $this->createSuccessResponse($ticketID, $attachmentsData);
			}

			$ticketSysID = $ticketInfo[0]['sys_id'];
			$this->logDebug("Found sys_id ($ticketSysID) for ticket: $ticketID", [
				'sys_id'   => $ticketSysID,
				'ticketID' => $ticketID,
			]);

			$this->logDebug("Finding attachments for ticket: $ticketID");
			$snClient = new ServiceNowTableClient(new ServiceNowAttachment());
			$ticketAttachments = $snClient->get($ticketSysID);

			foreach ($ticketAttachments as $i => $attachment) {
				$attachmentsData['Attachments'][] = [
					'Target'   => (string)($i + 1),
					'FileName' => $attachment['file_name'],
					'File'     => base64_encode($attachment['file']),
				];
			}

			return $this->createSuccessResponse($ticketID, $attachmentsData);

		} catch (PolarisException $e) {
			$this->logInfo('Detected invalid incoming request', [$e]);
			return $this->createFailureResponse($e->getMessage());
		} catch (Throwable $t) {
			$this->logError('Exception occurred while trying to fetch attachments', [$t]);
			return $this->createFailureResponse('Internal Server Error');
		}
	}
	#endregion

	#region Update field force task ticket (bugbase)
	/**
	 * Builds a field force task update payload and sends it to ServiceNow.
	 *
	 * @param Request $request The raw request coming from Nokia.
	 * @return Response|Generator The Response to send back to the client (Nokia).
	 */
	public function updateTask(Request $request)
	{
		try {
			$this->checkAPIKey($request);
			$data = $this->parseBody(yield $request->getBody()->buffer());
			$taskID = $request->getAttribute(Router::class)['bugBaseID'];

			// Build message to ServiceNow
			$this->logDebug('Building task ticket message', ['taskID' => $taskID, 'data' => $data]);
			$message = $this->buildUpdateTaskTicketMessage($data, $taskID);

			// Push to queue
			$this->logInfo('Publishing message onto the queue', ['message' => $message]);
			$this->publishToQueue($message);

			return $this->createSuccessResponse($message->getCorrelationId());
		} catch (InvalidRequestException $e) {
			$this->logInfo('Detected invalid incoming request', [$e]);
			return $this->createFailureResponse($e->getMessage());
		} catch (Throwable $t) {
			$this->logError('Exception occurred while trying to update task', [$t]);
			return $this->createFailureResponse('Internal Server Error');
		}
	}

	/**
	 * Builds an update Field Force Task message to be sent to
	 * ServiceNow, from the data received by Nokia.
	 *
	 * @param array $data The data received by Nokia.
	 * @param string $taskID The task ID to update (sent as URL parameter).
	 * @return ATicketMessage The complete message to send to ServiceNow.
	 * @throws InvalidRequestException If an invalid timestamp was received by Nokia.
	 * @throws ValidationException If, for some reason, that the message to send to ServiceNow is invalid.
	 */
	public function buildUpdateTaskTicketMessage(array $data, string $taskID): ATicketMessage
	{
		$message = new FieldForceTicketMessage();

		// Map the status sent from Nokia (if non was received, we send NO_CHANGE to ServiceNow)
		switch (strtolower($data['Status'] ?? '')) {
			case '':
				$message->setStatus(ATicketMessage::STATUS_NO_CHANGE);
				break;
			case 'implementing':
			case 'submitted':
				$message->setStatus(FieldForceTicketMessage::STATUS_WORK_IN_PROGRESS);
				break;
			case 'on hold':
				$message->setStatus(FieldForceTicketMessage::STATUS_PENDING);
				break;
			case 'closed':
			case 'resolved':
				$message->setStatus(FieldForceTicketMessage::STATUS_RESOLVE);
				break;
		}

		if (isset($data['Description']))
			$message->setDescription($this->getFieldString($data, 'Description'));

		[$impact, $urgency] = $this->mapTaskPriorities($this->getFieldString($data, 'Priority', ''));

		$message->setSource($this->source);
		$message->setCorrelationId($taskID);
		$message->setDueDate($this->getFieldDateTime($data, 'DueDate'));
		$message->setImpact($impact);
		$message->setUrgency($urgency);
		$message->setWorknotes($this->getFieldString($data, 'UpdateWorklog', ''));

		// Add any additional data to the TnDK reference table in ServiceNow
		$message->addExtendedPayload('Item', $this->getFieldString($data, 'Item'));
		$message->addExtendedPayload('PCR', $this->getFieldString($data, 'PCR'));
		$message->addExtendedPayload('Supplier', $this->fieldSupplier);

		// Nokia only sends up to 5 file attachments in the same payload, while we accept
		// any number of file attachments. We add all 5 here (if found in the $data payload)
		$this->addMessageAttachment($message, $data, 'One');
		$this->addMessageAttachment($message, $data, 'Two');
		$this->addMessageAttachment($message, $data, 'Three');
		$this->addMessageAttachment($message, $data, 'Four');
		$this->addMessageAttachment($message, $data, 'Five');

		return $message;
	}

	/**
	 * Adds an attachment to a message (maybe only for Field Force Task messages), with the
	 * $data array as base. In the API, [AttachmentOne, AttachmentNameOne, ... Five] contains
	 * any file attachments (up to 5), and we need to put them into ServiceNow as well.
	 *
	 * @param ATicketMessage $message The message to add the file attachments to.
	 * @param array $data The data received by Nokia.
	 * @param string $key The name of the attachment entry, can be: One, Two, Three, Four, Five.
	 */
	protected function addMessageAttachment(ATicketMessage $message, array &$data, string $key): void
	{
		if (isset($data["Attachment$key"], $data["AttachmentName$key"]))
			$message->addAttachment($data["AttachmentName$key"], $data["Attachment$key"]);
	}
	#endregion

	#region Common functionality
	/**
	 * Sets common properties for both incidents and change requests.
	 *
	 * @param ATicketMessage $message The message to set properties on.
	 * @param array $data The data received by Nokia.
	 * @throws InvalidRequestException If the 'key' could not be parsed successfully to a DateTime object.
	 * @throws ValidationException If, for some reason, that the message to send to ServiceNow is invalid.
	 */
	protected function setCommonProperties(ATicketMessage $message, array &$data): void
	{

		if ($message instanceof IncidentTicketMessage) {

			if ($message->getAction() === IncidentTicketMessage::ACTION_CREATE) {
				[$impact, $urgency] = $this->mapTicketPriorities($this->getFieldInt($data, 'Impact'),
					$this->getFieldInt($data, 'Severity'));

				$message->setImpact($impact);
				$message->setUrgency($urgency);

			}

			//To ensure that if we receive an Incident update message with no Severity/Impact we will NOT default the TicketPriority to 3/3
			if ($message->getAction() === IncidentTicketMessage::ACTION_UPDATE && isset($data['Severity'], $data['Impact'])) {

				[$impact, $urgency] = $this->mapTicketPriorities($this->getFieldInt($data, 'Impact'),
					$this->getFieldInt($data, 'Severity'));

				$message->setImpact($impact);
				$message->setUrgency($urgency);

			}

			$message->setDescription($this->getFieldString($data, 'Description', ''));

			#region Convert ticket status
			switch (strtolower($data['TicketStatus'] ?? '')) {
				case '':
					$message->setStatus(ATicketMessage::STATUS_NO_CHANGE);
					break;
				case 'new':
					$message->setStatus(IncidentTicketMessage::STATUS_NEW);
					break;
				case 'assigned':
				case 'workinprogress':
					$message->setStatus(IncidentTicketMessage::STATUS_IN_PROGRESS);
					break;
				case 'pending':
					$message->setStatus(IncidentTicketMessage::STATUS_ON_HOLD);
					break;

				// TODO should we open for Closed...Currently all goes into resolved
				case 'closed':
				case 'resolved':
					$message->setStatus(IncidentTicketMessage::STATUS_RESOLVED);
					break;
			}
			#endregion

			#region Convert resolution code (if any)
			switch (strtolower($data['ClosingStatus'] ?? '')) {
				case 'success':
					$message->setResolutionCode(IncidentTicketMessage::RESOLUTION_CODE_RESOLVED_PERMANENTLY);
					break;
				case 'success w. problems':
					$message->setResolutionCode(IncidentTicketMessage::RESOLUTION_CODE_RESOLVED_WORKAROUND);
					break;
				case 'failed':
					$message->setResolutionCode(IncidentTicketMessage::RESOLUTION_CODE_NOT_SOLVED);
					break;
				case 'cancelled':
					$message->setStatus(IncidentTicketMessage::STATUS_CANCELED);
					break;
			}

			$message->setAssignedUser($this->getFieldString($data, 'Assigned', ''));

			if (isset($this->assigneeMapping[$message->getAssignedUser()]))
				$message->setAssignedGroup($this->assigneeMapping[$message->getAssignedUser()]);

			switch (strtolower($data['TicketStatusReason'] ?? '')) {
				case 'client action required':
					$message->setAssignedGroup('OPS SOC');
					$this->logInfo('Changing Assigment group due to TicketStatusReason',
						['TicketStatusReason' => $data['TicketStatusReason']]);

					break;
			}

			#endregion

		} else if ($message instanceof ChangeTicketMessage) {

			//If a change is created by Nokia it will be assigned to OPS Nokia, For any Change update messages from Nokia
			// Info related to assigment group will be ignored (No assigment group change in ServiceNow)
			if ($message->getAction() === ChangeTicketMessage::ACTION_CREATE) {
				$message->setAssignedUser($this->getFieldString($data, 'Assigned', ''));

				if (isset($this->assigneeMapping[$message->getAssignedUser()]))
					$message->setAssignedGroup($this->assigneeMapping[$message->getAssignedUser()]);

				//It is only allowed to set Urgency/Severity during Create. All updates related to Urgency/Severity will be ignored.
				[$impact, $urgency] = $this->mapTicketPriorities($this->getFieldInt($data, 'Impact'),
					$this->getFieldInt($data, 'Severity'));

				$message->setImpact($impact);
				$message->setUrgency($urgency);

			}

			$message->setDescription($this->getFieldString($data, 'ChangeDescription', ''));

			#region Convert ticket status
			switch (strtolower($data['TicketStatus'] ?? '')) {
				case '':
				case 'pending':
				case 'assigned':
					$message->setStatus(ATicketMessage::STATUS_NO_CHANGE);
					break;
				case 'workinprogress':
					$message->setStatus(ChangeTicketMessage::STATUS_IMPLEMENT);
					break;
				case 'new':
					$message->setStatus(ChangeTicketMessage::STATUS_NEW);
					break;
				case 'resolved':
					$message->setStatus(ChangeTicketMessage::STATUS_REVIEW);
					break;
				case 'closed':
					$message->setStatus(ChangeTicketMessage::STATUS_CLOSED);
					break;
			}
			#endregion

			if ($this->getFieldString($data, 'ChangeRFA') === 'RFA')
				$message->setStatus(ChangeTicketMessage::STATUS_AUTHORIZE);

			#region Handle Change types
			if ($this->getFieldString($data, 'ChangeType') === 'Minor') {
				$message->setStatus(ChangeTicketMessage::STATUS_SCHEDULED);  // Minor changes do not need approval therefore directly to Scheduled
				$message->setChangeType(ChangeTicketMessage::CHANGE_TYPE_NOKIA_MINOR);

				$initator = $data['Initiator'] ?? '';
				if (isset($this->changeTemplates[$initator]))
					$message->setChangeTemplate($this->changeTemplates[$initator]);
			}
			if ($this->getFieldString($data, 'ChangeType') === 'Major') {
				$message->setStatus(ChangeTicketMessage::STATUS_AUTHORIZE);
				$message->setCABRequired(ChangeTicketMessage::CAB_REQUIRED_TRUE);
				$message->setChangeType(ChangeTicketMessage::CHANGE_TYPE_NOKIA_MAJOR);

			}
			if ($this->getFieldString($data, 'ChangeType') === 'Significant') {
				$message->setStatus(ChangeTicketMessage::STATUS_AUTHORIZE);
				$message->setChangeType(ChangeTicketMessage::CHANGE_TYPE_NOKIA_SIGNIFICANT);

			}
			if ($this->getFieldString($data, 'ChangeType') === 'Emergency') {
				$message->setStatus(ChangeTicketMessage::STATUS_AUTHORIZE);
				$message->setChangeType(ChangeTicketMessage::CHANGE_TYPE_NOKIA_EMERGENCY);

			}

			#endregion

			#region Convert close code (if any)
			//switch (strtolower($data['ClosingStatus'] ?? '')) {
			switch (strtolower($data['ChangeClosingStatus'] ?? '')) {
				case 'success':
					$message->setCloseCode(ChangeTicketMessage::CLOSE_CODE_SUCCESSFUL);
					$message->setCloseNotes(ChangeTicketMessage::CLOSE_CODE_SUCCESSFUL);
					break;
				case 'success w. problems':
					$message->setCloseCode(ChangeTicketMessage::CLOSE_CODE_SUCCESSFUL_WITH_ISSUES);
					$message->setCloseNotes(ChangeTicketMessage::CLOSE_CODE_SUCCESSFUL_WITH_ISSUES);
					break;
				case 'failed':
					$message->setCloseCode(ChangeTicketMessage::CLOSE_CODE_UNSUCCESSFUL);
					$message->setCloseNotes(ChangeTicketMessage::CLOSE_CODE_UNSUCCESSFUL);
					break;
				case 'cancelled':
					$message->setStatus(ChangeTicketMessage::STATUS_CANCELED);
					$message->setCloseNotes(ChangeTicketMessage::STATUS_CANCELED);
					break;
			}
			#endregion

			//set default planning message for all change from Nokia except minor.
			//Justification,Implementation plan etc are mandatory in ServiceNow, but Nokia supply this in the description field. We there just insert a default text
			// saying Please check the details in the description field

			// TODO should this only be for action create??

			if ($this->getFieldString($data,
					'ChangeType') !== 'Minor' && $message->getAction() === ChangeTicketMessage::ACTION_CREATE) {
				$message->setJustification(self::NOKIA_DEFAULT_PLANNING_MESSAGE);
				$message->setImplementationPlan(self::NOKIA_DEFAULT_PLANNING_MESSAGE);
				$message->setRiskAndImpactAnalysis(self::NOKIA_DEFAULT_PLANNING_MESSAGE);
				$message->setBackoutPlan(self::NOKIA_DEFAULT_PLANNING_MESSAGE);
				$message->setTestPlan(self::NOKIA_DEFAULT_PLANNING_MESSAGE);

			}

			#region Set schedule attributes

			$message->setPlannedStartDate($this->tzConvert($this->getFieldDateTime($data, 'WorkStart')));
			$message->setPlannedEndDate($this->tzConvert($this->getFieldDateTime($data, 'PermanentSolution')));

			//Actual dates are mandatory in SN when we are moving to state Review. We are not sure Nokia sent these when the CR is in Completed or Closed state on Nokia side
			//If missing in Review state we use Planned dates as default
			//Since we receive Resolved status from Nokia for both Completed and Closed. We will update with correct values if we receive then when Nokia is closing.
			if ($message->getStatus() === ChangeTicketMessage::STATUS_REVIEW) {

				$message->setActualStartDate($this->tzConvert($this->getFieldDateTime($data,
						'ChangeDowntimeStart') ?? $this->getFieldDateTime($data, 'WorkStart')));
				$message->setActualEndDate($this->tzConvert($this->getFieldDateTime($data,
						'ChangeDowntimeEnd') ?? $this->getFieldDateTime($data, 'PermanentSolution')));

			}

			#endregion
		}

		$message->setShortDescription($this->getFieldString($data, 'Description', ''));
		$message->setSource($this->source);
		$message->setOpenedBy($this->getFieldString($data, 'Initiator', ''));
		$message->setWorknotes($this->getFieldString($data, 'Worklog', ''));

		$message->addExtendedPayload('Item', $this->getFieldString($data, 'TicketItem'));
	}

	/**
	 * Map Nokia ITSM Impact/Severity values to ServiceNow Impact/Urgency values.
	 * Nokia has 4 levels of impact and severity, while ServiceNow only has 3 values
	 * for impact and urgency.
	 *
	 * @param int $impact Nokia ITSM impact level (1-4).
	 * @param int $severity Nokia ITSM severity level (1-4).s
	 * @return array The impact and urgency as integer values. Format: [impact, urgency].
	 */
	public function mapTicketPriorities(int $impact, int $severity): array
	{
		if ($impact === 1 && $severity === 1)
			return [self::SERVICENOW_IMPACT_1_HIGH, self::SERVICENOW_URGENCY_1_HIGH];

		if ($impact === 1 && $severity === 2)
			return [self::SERVICENOW_IMPACT_1_HIGH, self::SERVICENOW_URGENCY_2_MEDIUM];

		if ($impact === 1 && $severity === 3)
			return [self::SERVICENOW_IMPACT_1_HIGH, self::SERVICENOW_URGENCY_3_LOW];

		if ($impact === 1 && $severity === 4)
			return [self::SERVICENOW_IMPACT_1_HIGH, self::SERVICENOW_URGENCY_3_LOW];

		if ($impact === 2 && $severity === 1)
			return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_1_HIGH];

		if ($impact === 2 && $severity === 2)
			return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_2_MEDIUM];

		if ($impact === 2 && $severity === 3)
			return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_2_MEDIUM];

		if ($impact === 2 && $severity === 4)
			return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_3_LOW];

		if ($impact === 3 && $severity === 1)
			return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_2_MEDIUM];

		if ($impact === 3 && $severity === 2)
			return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_2_MEDIUM];

		if ($impact === 3 && $severity === 3)
			return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_3_LOW];

		if ($impact === 3 && $severity === 4)
			return [self::SERVICENOW_IMPACT_3_LOW, self::SERVICENOW_URGENCY_2_MEDIUM];

		if ($impact === 4 && $severity === 1)
			return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_3_LOW];

		if ($impact === 4 && $severity === 2)
			return [self::SERVICENOW_IMPACT_3_LOW, self::SERVICENOW_URGENCY_2_MEDIUM];

		if ($impact === 4 && $severity === 3)
			return [self::SERVICENOW_IMPACT_3_LOW, self::SERVICENOW_URGENCY_2_MEDIUM];

		if ($impact === 4 && $severity === 4)
			return [self::SERVICENOW_IMPACT_3_LOW, self::SERVICENOW_URGENCY_3_LOW];

		return [self::SERVICENOW_IMPACT_3_LOW, self::SERVICENOW_URGENCY_3_LOW];
	}

	/**
	 * Map Field Force tasks priorities to ServiceNow Impact & Urgency.
	 * Converts "Telenor P0" .. "Telenor P5" to 1-3 impact/urgency values.
	 *
	 * @param string $priority The priority for the field force task.
	 * @return array The impact and urgency as integer values. Format: [impact, urgency].
	 */
	public function mapTaskPriorities(string $priority): array
	{
		switch (strtolower($priority)) {
			case 'telenor p0':
				return [self::SERVICENOW_IMPACT_1_HIGH, self::SERVICENOW_URGENCY_1_HIGH];
			case 'telenor p1':
				return [self::SERVICENOW_IMPACT_1_HIGH, self::SERVICENOW_URGENCY_2_MEDIUM];
			case 'telenor p2':
				return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_1_HIGH];
			case 'telenor p3':
				return [self::SERVICENOW_IMPACT_2_MEDIUM, self::SERVICENOW_URGENCY_3_LOW];
			case 'telenor p4':
				return [self::SERVICENOW_IMPACT_3_LOW, self::SERVICENOW_URGENCY_2_MEDIUM];
			default: // Also catches 'telenor p5'
				return [self::SERVICENOW_IMPACT_3_LOW, self::SERVICENOW_URGENCY_3_LOW];
		}
	}

	/**
	 * Parses a string as JSON and returns an array with the JSON values.
	 * This method simply ensures that JSON is always parsed the same way,
	 * and that the same Exception is thrown in parse failures.
	 *
	 * @param string $body The string to parse.
	 * @return array The parsed JSON array.
	 * @throws InvalidRequestException If invalid JSON was detected.
	 */
	protected function parseBody(string $body): array
	{
		try {
			return json_decode($body, true, 512, JSON_THROW_ON_ERROR);
		} /** @noinspection PhpRedundantCatchClauseInspection */ catch (Throwable $e) {
			$this->logDebug('Malformed Json body', ['body' => $body, 'exception' => $e]);
			throw new InvalidRequestException('Malformed data detected in request body');
		}
	}
	#endregion

	#region API field helpers
	/**
	 * Extracts the string value from a data array.
	 *
	 * @param array $data The complete data array.
	 * @param string $key The key to lookup in the 'data' array.
	 * @param string|null $defaultValue A default value to return, if the key is not found in the data array.
	 * @return string|null A string value The string value found in the data array.
	 */
	protected function getFieldString(array &$data, string $key, ?string $defaultValue = null): ?string
	{
		return $data[$key] ?? $defaultValue;
	}

	/**
	 * Tries to parse an integer in the 'key' key in the 'data' array.
	 *
	 * @param array $data The complete data array.
	 * @param string $key The key to lookup in the 'data' array.
	 * @return int An integer value based on the 'key' - 0 is returned if the key does not exist.
	 */
	protected function getFieldInt(array &$data, string $key): int
	{
		return (isset($data[$key]) && (is_int($data[$key]) || ctype_digit($data[$key]))) ? (int)$data[$key] : 0;
	}

	/**
	 * Extracts a 'key' from a 'data' array and converts it into a DateTime object.
	 *
	 * @param array $data The complete data array.
	 * @param string $key The key to lookup in the 'data' array.
	 * @return DateTime|null A DateTime object if parsed successfully, null if the key does not exist.
	 * @throws InvalidRequestException If the 'key' could not be parsed successfully to a DateTime object.
	 */
	protected function getFieldDateTime(array &$data, string $key): ?DateTime
	{
		try {
			if (isset($data[$key]))
				return new DateTime($data[$key]);
		} catch (Exception $e) {
			$this->logError("Failed to parse date in key $key: " . $data[$key]);
			throw new InvalidRequestException("Failed to parse date in field: $key");
		}
		return null;
	}
	#endregion

	#region Validation helpers
	/**
	 * Checks if an API key is valid - throws an exception if not.
	 *
	 * @param Request $request The complete incoming HTTP request.
	 * @throws InvalidRequestException If API key is invalid.
	 */
	protected function checkAPIKey(Request $request): void
	{
		$apiKey = $request->getAttribute(Router::class)['ApiKey'];
		if (!in_array($apiKey, $this->validAPIKeys, true))
			throw new InvalidRequestException("Unknown ApiKey: $apiKey");
	}

	/**
	 * Checks if fields (keys) exists in an array and throws an exception
	 * if one of the specified fields does not exist in the array.
	 *
	 * @param array $data The complete data set to check array keys for.
	 * @param array $fields The fields to check.
	 * @throws InvalidRequestException If a field does not exist, specifying which one missing.
	 */
	protected function validateRequiredFields(array &$data, array $fields): void
	{
		foreach ($fields as $field) {
			if (!isset($data[$field]))
				throw new InvalidRequestException("Required field missing: $field");
		}
	}

	/**
	 * @param DateTime $nokiadate
	 * @return DateTime
	 * @throws Exception
	 */
	protected function tzConvert(?DateTime $nokiadate): ?DateTime
	{
		if ($nokiadate === null) {
			return null;
		}

		//From Nokia we are getting UTC time which needs to be converted into Localtime
		try {

			$nokiadate->setTimezone(new DateTimeZone('Europe/Copenhagen'));

			return $nokiadate;
		} catch (Exception $e) {
			throw new InvalidRequestException('Time zone convertion failed: Unable to convert TZ on date');
		}

	}

	/**
	 * Determines if a ticket is an incident based on the ticket ID.
	 *
	 * @param string $ticketID The ticket ID to check.
	 * @return bool True if the ticket has been identified as an incident, false otherwise.
	 */
	protected function isIncident(string $ticketID): bool
	{
		return preg_match('#(^inc|-inc-)#i', $ticketID) === 1;
	}

	/**
	 * Determines if a ticket is an task based on the ticket ID.
	 *
	 * @param string $ticketID The ticket ID to check.
	 * @return bool True if the ticket has been identified as a task, false otherwise.
	 */
	protected function isTask(string $ticketID): bool
	{
		return preg_match('#(^tas|-tas-)#i', $ticketID) === 1;
	}

	/**
	 * Determines if a ticket is a change request based on the ticket ID.
	 *
	 * @param string $ticketID The ticket ID to check.
	 * @return bool True if the ticket has been identified as a change request, false otherwise.
	 */
	protected function isChange(string $ticketID): bool
	{
		return preg_match('#(^chg|-chg-)#i', $ticketID) === 1;
	}

	/**
	 * Determines if a request should be interpreted as just an approval.
	 * This is necessary for Outbound requests that Nokia must approve. When they approve,
	 * instead of sending a meaningful message, they send a state change.
	 *
	 * @param array $data
	 * @return bool
	 */
	protected function isApprovalMessage(array $data): bool
	{
		if (isset($data['TicketStatus']) && $data['TicketStatus'] === 'Assigned' && isset($data['ChangeApproval']) && $data['ChangeApproval'] === 'Pending' && isset($data['Assigned']) && $data['Assigned'] === 'Nokia' && is_null($data['ChangeRFA'])) {
			return true;
		}

		return false;
	}
	#endregion

	#region Response helpers
	/**
	 * Generates a general successful response to send back to the client.
	 *
	 * @param string $ticketID The generated ticket ID.
	 * @param array|null $extraData Any additional data to send in the JSON data.
	 * @return Response An instance of the AMPHP Response.
	 */
	protected function createSuccessResponse(string $ticketID, array $extraData = null): Response
	{
		return $this->createResponse(true, $ticketID, '', $extraData);
	}

	/**
	 * Generates a general error response to send back to the client.
	 *
	 * @param string $error Any error message.
	 * @return Response An instance of the AMPHP Response.
	 */
	protected function createFailureResponse(string $error): Response
	{
		return $this->createResponse(false, '', $error);
	}

	/**
	 * Generates a general response to send back to the client.
	 *
	 * @param bool $success Sends 'OK' if true, 'NOK' if false
	 * @param string $ticketID The generated ticket ID.
	 * @param string $error Any error message.
	 * @param array|null $extraData Any additional data to send in the JSON data.
	 * @return Response An instance of the AMPHP Response.
	 */
	protected function createResponse(bool $success, string $ticketID = '', string $error = '',
	                                  array $extraData = null): Response
	{
		$statusCode = 200;
		$headers = [
			'content-type' => 'application/json',
		];

		// Send JSON data back to the client
		// Merge the array below with the $extraData array (if any specified)
		$body = json_encode(array_merge([
			'TicketId' => $ticketID,
			'Status'   => ($success ? 'OK' : 'NOK'),
			'Error'    => $error,
		], $extraData ?? []), JSON_THROW_ON_ERROR);

		$response = new Response($statusCode, $headers, "$body\n");

		$this->logDebug('Sending response back to client', [
			'status code' => $statusCode,
			'headers'     => $headers,
			'body'        => $body,
		]);

		return $response;
	}
	#endregion
}