<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use Telenor\Message\ChangeTicketMessage;
use Telenor\MessageFormatter\JsonMessageFormatter;
use Telenor\System\Exception\InvalidDataException;
use Telenor\System\Exception\InvalidRequestException;
use Telenor\Worker\TicketWorker\Controller\ChangeController;

class ChangeControllerTest extends TestCase
{

	/** @var ChangeController */
	protected $controller;
	/** @var JsonMessageFormatter */
	protected $formatter;

	/**
	 * @param null $name
	 * @param array $data
	 * @param string $dataName
	 */
	public function __construct($name = null, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);
		$this->controller = new ChangeController();

		$this->formatter = new JsonMessageFormatter();

	}

	public function _ymlDataProviderCreateChange(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_CreateChange.yml', __DIR__, __CLASS__));
	}

	public function _ymlDataProviderCreateChangeFailed(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_CreateChangeFailed.yml', __DIR__, __CLASS__));
	}

	public function _ymlDataProviderUpdateChange(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_UpdateChange.yml', __DIR__, __CLASS__));
	}



	/**
	 * @dataProvider _ymlDataProviderCreateChange
	 * @param $input
	 * @param $output
	 * @throws InvalidRequestException
	 * @throws InvalidDataException
	 */
//	public function testInsert()
	public function testCreateChange($input, $output): void
	{
		//$data = ['source' => 'hest', 'short_description' => 'hesthest', 'description' => 'text', 'opened_by' => 'tpn'];

		$message = new ChangeTicketMessage(ChangeTicketMessage::ACTION_CREATE);

		$this->controller->PopulateChangeProperties($message, $input);

		$this->controller->createTicket($input, $message);

		$result = $this->handleMessage($message);

		unset($result['payload_original']['ExternalTicketId']);

		$this->formatter->removeNullRecursively($result);

		//static::assertSame(['action' => 'create'], $result);

		static::assertSame($output, $result);

	}

	/**
	 * @dataProvider _ymlDataProviderUpdateChange
	 * @param $input
	 * @param $output
	 * @throws InvalidDataException
	 * @throws InvalidRequestException
	 * @throws \Telenor\System\Exception\ValidationException
	 */
	public function testUpdateChange($input, $output): void
	{
		//$data = ['source' => 'hest', 'short_description' => 'hesthest', 'description' => 'text', 'opened_by' => 'tpn'];

		$message = new ChangeTicketMessage(ChangeTicketMessage::ACTION_UPDATE);

		$this->controller->PopulateChangeProperties($message, $input);

		$this->controller->updateTicket($input, $message);

		$result = $this->handleMessage($message);

		$this->formatter->removeNullRecursively($result);

		//static::assertSame(['action' => 'create'], $result);

		static::assertSame($output, $result);

	}

	/**
	 * @dataProvider _ymlDataProviderCreateChangeFailed
	 * @param $input
	 * @throws InvalidDataException
	 * @throws InvalidRequestException
	 * @throws \Telenor\System\Exception\ConnectionException
	 * @throws \Telenor\System\Exception\ValidationException
	 */
	public function testCreateChangeFailed($input): void
	{
		$this->expectException(InvalidDataException::class);
		$message = new ChangeTicketMessage(ChangeTicketMessage::ACTION_CREATE);

		$this->controller->PopulateChangeProperties($message, $input);

		$this->controller->createTicket($input, $message);

		$this->handleMessage($message);
	}

	/**
	 * @param ChangeTicketMessage $message
	 * @return array
	 * @throws InvalidDataException
	 */
	public function handleMessage(ChangeTicketMessage $message): array
	{

		$message->validate();

		$result = $message->getAttributes();

		unset($result['timestamp'], $result['transaction_id'], $result['worker'], $result['correlation_id']);

		return $result;

	}

}
