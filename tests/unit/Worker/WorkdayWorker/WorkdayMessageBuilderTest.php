<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\Model\ServicenowUser;
use Telenor\Model\WorkdayUser;
use Telenor\System\Exception\ValidationException;
use Telenor\Worker\WorkdayWorker\WorkdayController;

class WorkdayMessageBuilderTest extends TestCase
{

	public function testBuildUserInvalidInput()
	{
		$object = new WorkdayController();
		$wduser = null;
		$snuser = null;
		$this->expectException(ValidationException::class);

		$object->buildUser($wduser, $snuser);

	}

	public function testBuildUserCreate()
	{

		$wduser = new WorkdayUser();
		$snuser = new ServicenowUser();
		$wduser->setInitials('xxx');
		$wduser->setFirstname('mads');
		$wduser->setLastname('maDsen');
		$wduser->setFullname('mads maDsen');
		$wduser->setEmail('test@test.dk');
		$wduser->setPhone('+4599999999');
		$wduser->setCity('AAlborg');
		$wduser->setTitle('Service arbejder');
		$wduser->setEmployeeNumber(121212);
		$wduser->setEmployeeNumber1(121212);
		$wduser->setDepartment('Kundeservice');
		$wduser->setManager('HHH');
		$wduser->setCompany('Telenor');

		$user = new WorkdayController();
		$msg = $user->buildUser($wduser, null);

		$this->assertSame($wduser->getInitials(), $msg->getInitials());
		$this->assertSame($wduser->getFirstname(), $msg->getFirstname());
		$this->assertSame($wduser->getLastname(), $msg->getLastname());
		$this->assertSame($wduser->getFullname(), $msg->getFullname());
		$this->assertSame($wduser->getEmail(), $msg->getEmail());
		$this->assertSame($wduser->getPhone(), $msg->getGsm());
		$this->assertSame($wduser->getCity(), $msg->getLocation());
		$this->assertSame($wduser->getTitle(), $msg->getJob());
		$this->assertSame($wduser->getEmployeeNumber(), $msg->getGlobalEmployeeNumber());
		$this->assertSame($wduser->getEmployeeNumber(), $msg->getGlobalEmployeeNumber1());
		$this->assertSame($wduser->getDepartment(), $msg->getDepartment());
		$this->assertSame($wduser->getManager(), $msg->getManager());
		$this->assertSame($wduser->getCompany(), $msg->getCompany());
		$this->assertSame($msg->getAction(), $msg::ACTION_CREATE);

	}

	public function testBuildUserDelete()
	{

		$snuser = new ServicenowUser();

		$snuser->setInitials('xxx');
		$snuser->setFirstname('mads');
		$snuser->setLastname('maDsen');
		$snuser->setFullname('mads maDsen');
		$snuser->setEmail('test@test.dk');
		$snuser->setPhone('+4599999999');
		$snuser->setCity('AAlborg');
		$snuser->setTitle('Service arbejder');
		$snuser->setEmployeeNumber(121212);
		$snuser->setEmployeeNumber1(121212);
		$snuser->setDepartment('Kundeservice');
		$snuser->setManager('HHH');
		$snuser->setCompany('Telenor');
		$snuser->setActive(true);

		$user = new WorkdayController();
		$msg = $user->buildUser(null, $snuser);

		$this->assertSame($snuser->getInitials(), $msg->getInitials());
		$this->assertNotNull($msg->getInitials());
		$this->assertSame($msg->getAction(), $msg::ACTION_DELETE);
		$this->assertNull($msg->getFirstname());
		$this->assertNull($msg->getLastname());
		$this->assertNull($msg->getFullname());
		$this->assertNull($msg->getEmail());
		$this->assertNull($msg->getGsm());
		$this->assertNull($msg->getLocation());
		$this->assertNull($msg->getJob());
		$this->assertNull($msg->getGlobalEmployeeNumber());
		$this->assertNull($msg->getGlobalEmployeeNumber1());
		$this->assertNull($msg->getDepartment());
		$this->assertNull($msg->getManager());
		$this->assertNull($msg->getCompany());

	}

	public function testBuildUserDeleteActiveNo()
	{

		$snuser = new ServicenowUser();

		$snuser->setInitials('xxx');
		$snuser->setFirstname('mads');
		$snuser->setLastname('maDsen');
		$snuser->setFullname('mads maDsen');
		$snuser->setEmail('test@test.dk');
		$snuser->setPhone('+4599999999');
		$snuser->setCity('AAlborg');
		$snuser->setTitle('Service arbejder');
		$snuser->setEmployeeNumber(121212);
		$snuser->setEmployeeNumber1(121212);
		$snuser->setDepartment('Kundeservice');
		$snuser->setManager('HHH');
		$snuser->setCompany('Telenor');
		$snuser->setActive(false);

		$user = new WorkdayController();
		$msg = $user->buildUser(null, $snuser);

		$this->assertNull($msg);

	}

	public function testBuildUserUpdateDiff()
	{
		$wduser = new WorkdayUser();
		$wduser->setInitials('xxx');
		$wduser->setFirstname('mads');
		$wduser->setLastname('maDsen');
		$wduser->setFullname('mads maDsen');
		$wduser->setEmail('test@test.dk');
		$wduser->setPhone('+4599999999');
		$wduser->setCity('AAlborg');
		$wduser->setTitle('Service arbejder');
		$wduser->setEmployeeNumber(121212);
		$wduser->setEmployeeNumber1(121212);
		$wduser->setDepartment('Kundeservice');
		$wduser->setManager('HHH');
		$wduser->setCompany('Telenor');

		$snuser = new ServicenowUser();
		$snuser->setInitials('xxx');
		$snuser->setFirstname('Mads');
		$snuser->setLastname('MaDsen');
		$snuser->setFullname('Mads MaDsen');
		$snuser->setEmail('Test@test.dk');
		$snuser->setPhone('+4511111111');
		$snuser->setCity('AAlborg SV');
		$snuser->setTitle('HelpDesk');
		$snuser->setEmployeeNumber(454545);
		$snuser->setEmployeeNumber1(454545);
		$snuser->setDepartment('Suport');
		$snuser->setManager('MMM');
		$snuser->setCompany('Telenor DK');

		$user = new WorkdayController();
		$msg = $user->buildUser($wduser, $snuser);

		$this->assertSame($wduser->getInitials(), $msg->getInitials());
		$this->assertSame($wduser->getFirstname(), $msg->getFirstname());
		$this->assertSame($wduser->getLastname(), $msg->getLastname());
		$this->assertSame($wduser->getFullname(), $msg->getFullname());
		$this->assertSame($wduser->getEmail(), $msg->getEmail());
		$this->assertSame($wduser->getPhone(), $msg->getGsm());
		$this->assertSame($wduser->getCity(), $msg->getLocation());
		$this->assertSame($wduser->getTitle(), $msg->getJob());
		$this->assertSame($wduser->getEmployeeNumber(), $msg->getGlobalEmployeeNumber());
		$this->assertSame($wduser->getEmployeeNumber1(), $msg->getGlobalEmployeeNumber1());
		$this->assertSame($wduser->getDepartment(), $msg->getDepartment());
		$this->assertSame($wduser->getManager(), $msg->getManager());
		$this->assertSame($wduser->getCompany(), $msg->getCompany());
		$this->assertSame($msg->getAction(), $msg::ACTION_UPDATE);

		$snuser->setEmployeeNumber1(121212);
		$msg = $user->buildUser($wduser, $snuser);
		$this->assertNull($msg->getGlobalEmployeeNumber1());

	}

}
