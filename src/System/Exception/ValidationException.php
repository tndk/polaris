<?php
declare(strict_types=1);

namespace Telenor\System\Exception;

use Exception;

class ValidationException extends Exception
{
	protected $property;
	protected $value;
	protected $content;

	public function __construct(string $property, ?string $value, ?string $content = '', string $message = null)
	{
		$this->property = $property;
		$this->value = $value;
		$this->content = $content;
		parent::__construct($message ?? $content);
	}

	public function getProperty(): ?string
	{
		return $this->property;
	}

	public function getValue(): ?string
	{
		return $this->value;
	}

	public function getContent(): ?string
	{
		return $this->content;
	}

}