<?php
declare(strict_types=1);

namespace Telenor\Worker\DebugWorker;

use Telenor\Logging\Driver\MessageQueueDriver;
use Telenor\Logging\LoggingController;
use Telenor\Logging\TLog;
use Telenor\Message\GenericMessage;
use Telenor\Message\IMessage;
use Telenor\System\Config;
use Telenor\System\Utils;
use Telenor\Worker\AWorker\AMessageQueueWorker;
use Throwable;
use function array_merge;
use function is_bool;
use function is_scalar;
use function is_string;
use function json_encode;
use function preg_match;
use function strpos;

class DebugWorker extends AMessageQueueWorker
{
	use TLog;

	/** @var bool */
	protected $asJson;

	/** @var bool */
	protected $prettyJson;

	/** @var string[] */
	protected $filter;

	public function __construct()
	{
		foreach (LoggingController::getInstance()->getDrivers() as $driver) {
			if ($driver instanceof MessageQueueDriver)
				Utils::kill('Refusing to start debug worker with the ' . MessageQueueDriver::class . ' driver, in order to prevent infinite message loop');
		}
	}

	public function start()
	{
		$this->asJson = Config::envBool('DEBUG_OUTPUT_JSON', false);
		$this->prettyJson = Config::envBool('DEBUG_PRETTY_JSON', true);
		$filterString = Config::envString('DEBUG_FILTER', '');
		foreach (explode(';', $filterString) as $filter) {
			if (strpos($filter, '=') === false)
				continue;
			[$key, $value] = explode('=', $filter, 2);
			if (!Utils::isStringNullOrEmpty($key) && !Utils::isStringNullOrEmpty($value))
				$this->filter[$key] = $value;
		}

		parent::start();

		if (!empty($this->filter)) {
			$this->logInfo('Using filter(s):');
			foreach ($this->filter as $key => $value)
				$this->logInfo("<$key>=<$value>");
		} else {
			$this->logInfo('Using no filter');
		}
	}

	public function onMessageConsumed(IMessage $message, array $metaData = null): bool
	{
		try {
			if (!($message instanceof GenericMessage))
				return true;

			$attributes = $message->getData();
			if (empty($attributes)) {
				$this->logDebug('No attributes found in message, skipping');
				return true;
			}

			if (!empty($this->filter)) {
				$filterAttributes = array_merge($attributes, $metaData);
				$accept = true;
				foreach ($this->filter as $key => $value)
					$accept &= (is_scalar($value) && isset($filterAttributes[$key]) && preg_match("/$value/i", $this->toString($filterAttributes[$key])));

				if (!$accept) {
					$this->logDebug('Message rejected due to filter');
					return true;
				}
			}

			$str = "\nMESSAGE CONTENT (" . http_build_query($metaData, '', ', ') . '):';
			if ($this->asJson) {
				$str .= "\n" . json_encode($attributes, $this->prettyJson ? JSON_PRETTY_PRINT : 0);
			} else {
				Utils::toStringRecursive($str, $attributes);
			}
			echo "$str\n";

		} catch (Throwable $t) {
			$this->logError('Failed printing message', [$t]);
		}
		return true;
	}

	protected function toString($value): string
	{
		if (is_string($value))
			return $value;

		if (is_bool($value))
			return $value ? 'true' : 'false';

		return '' . $value;
	}
}