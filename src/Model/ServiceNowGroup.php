<?php
declare(strict_types=1);

namespace Telenor\Model;

use function strncmp;

class ServiceNowGroup extends AServiceNowModel
{
	public function getEndpoint(): string
	{
		return 'now/table/group';
	}

	public function getFields(): array
	{
		return [
			'number',
			'sys_id',
			'short_description',
			'description',
			'category',
			'opened_at',
			'state',
			'priority',
			'impact',
			'urgency',
		];
	}

	public function getFilters(string $id): array
	{
		if (strncmp($id, 'INC', 3) === 0 || strncmp($id, 'CHG', 3) === 0)
			return ['number' => $id];

		if (preg_match('/^\w+-\w+$/', $id))
			return ['correlation_id' => $id];

		return parent::getFilters($id);
	}
}