<?php
declare(strict_types=1);

namespace Telenor\Model;

use Telenor\System\Utils;

abstract class AUser implements IUser
{

	protected $lastname;
	protected $firstname;
	protected $fullname;
	protected $city;
	protected $employeeNumber;
	protected $employeeNumber1;
	protected $email;
	protected $title;
	protected $phone;
	protected $company;
	protected $initials;
	protected $department;
	protected $manager;
	protected $active = false;

	public function __construct(string $initials = null, string $email = null)
	{
		if (!Utils::isStringNullOrEmpty($initials))
			$this->setInitials($initials);

		if (!Utils::isStringNullOrEmpty($email))
			$this->setEmail($email);

	}

//Get commands
	public function getLastname(): ?string
	{
		return $this->lastname;
	}

	public function getFirstname(): ?string
	{
		return $this->firstname;
	}

	public function getFullname(): ?string
	{
		return $this->fullname;
	}

	public function getCity(): ?string
	{
		return $this->city;
	}

	public function getEmployeeNumber(): ?int
	{
		return $this->employeeNumber;
	}

	public function getEmployeeNumber1(): ?int
	{
		return $this->employeeNumber1;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function getTitle(): ?string
	{
		return $this->title;
	}

	public function getPhone(): ?string
	{
		return $this->phone;
	}

	public function getCompany(): ?string
	{
		return $this->company;
	}

	public function getInitials(): ?string
	{
		return $this->initials;
	}

	public function getDepartment(): ?string
	{
		return $this->department;
	}

	public function getManager(): ?string
	{
		return $this->manager;
	}

	public function getActive(): bool
	{
		return $this->active;
	}

// Set commands
	public function setLastname(?string $value): void
	{
		$this->lastname = $value;
	}

	public function setFirstname(?string $value): void
	{
		$this->firstname = $value;
	}

	public function setFullname(?string $value): void
	{
		$this->fullname = $value;
	}

	public function setCity(?string $value): void
	{
		$this->city = $value;
	}

	public function setEmployeeNumber(?int $value): void
	{
		$this->employeeNumber = $value;
	}

	public function setEmployeeNumber1(?int $value): void
	{
		$this->employeeNumber1 = $value;
	}

	public function setEmail(?string $value): void
	{
		$this->email = $value;
	}

	public function setTitle(?string $value): void
	{
		$this->title = $value;
	}

	public function setPhone(?string $value): void
	{
		$this->phone = $value;
	}

	public function setCompany(?string $value): void
	{
		$this->company = $value;
	}

	public function setInitials(?string $value): void
	{
		$this->initials = $value;
	}

	public function setDepartment(?string $value): void
	{
		$this->department = $value;
	}

	public function setManager(?string $value): void
	{
		$this->manager = $value;
	}

	public function setActive(bool $active): void
	{
		$this->active = $active;
	}

}