<?php
declare(strict_types=1);
namespace Telenor\Service;

use Telenor\Model\ServiceNowIncident;

class IncidentService
{
	/** @var ServiceNowTableClient */
	protected $client;

	public function __construct()
	{
		$this->client = new ServiceNowTableClient(new ServiceNowIncident());
	}

	public function getIncident(string $id): ServiceNowIncident
	{
		// ServiceNow always returns a list of results
		$response = $this->client->get($id);
		return new ServiceNowIncident($response[0]);
	}

	public function getIncidents(array $filters = []): array
	{
		return $this->client->query($filters);
	}

	public function createFromFields(array $fields): ServiceNowIncident
	{
		$incident = new ServiceNowIncident();
	}
}