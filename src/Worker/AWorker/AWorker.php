<?php
declare(strict_types=1);
namespace Telenor\Worker\AWorker;

use Telenor\Logging\TLog;
use Telenor\MessageQueue\MessageQueueController;
use Telenor\MessageQueue\TMessageQueue;
use Telenor\System\Error\BadConfigurationError;
use Telenor\Worker\IWorker;
use Throwable;

/**
 * A common class to use for all workers in the application.
 */
abstract class AWorker implements IWorker
{
	use TLog;
	use TMessageQueue;

	/**
	 * @throws BadConfigurationError
	 */
	public function start()
	{
		try {
			MessageQueueController::getInstance()->connect();
		} catch (Throwable $t) {
			$this->logError('Could not connect to message queue', [$t]);
		}
	}

	/**
	 * @throws BadConfigurationError
	 */
	public function stop()
	{
		$this->logInfo('Gracefully stopping worker..');
		MessageQueueController::getInstance()->disconnect();
	}
}