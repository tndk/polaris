<?php
declare(strict_types=1);
namespace Telenor\Worker;

/**
 * Interface to be used for all workers in the application.
 */
interface IWorker
{
	/**
	 * Starts a worker. This will be called when the entry point is ready to start the worker.
	 */
	public function start();

	/**
	 * Stops a worker. This is called when a SIGTERM is received from the OS.
	 */
	public function stop();
}