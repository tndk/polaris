<?php
declare(strict_types=1);

namespace Telenor\Worker\XMLGatewayWorker;

use Amp\Http\Server\Router;
use Telenor\Worker\AWorker\AWebWorker;

class XMLGatewayWorker extends AWebWorker
{

	/**
	 * @var TicketMapper Message builder
	 */
	protected $messageBuilder;

	public function __construct(TicketMapper $messageBuilder)
	{
		$this->messageBuilder = $messageBuilder;
	}

	protected function setupRoutes(Router $router): void
	{
		$handler = new XMLGatewayController($this->messageBuilder);
		$router->addRoute('GET', '/xmlgateway/{app}/{method}', $handler);
		$router->addRoute('POST', '/xmlgateway/{app}/{method}', $handler);
	}
}
