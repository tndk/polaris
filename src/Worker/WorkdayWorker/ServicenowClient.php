<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker;

use http\Exception\UnexpectedValueException;
use Telenor\Model\ServicenowUser;
use Telenor\System\Config;
use function json_decode;

class ServicenowClient
{
	private $username;
	private $password;

	public function query()
	{
		$this->setUsername(Config::envString('SERVICENOW_USERNAME'));
		$this->setPassword(Config::envString('SERVICENOW_PASSWORD'));
		$client = new \GuzzleHttp\Client();
		$res = $client->request('GET', 'https://tndkuat.service-now.com/api/now/table/sys_user?sysparm_query=u_company!=TCS', [
			//$res = $client->request('GET', 'https://telenordkpov.service-now.com/api/now/table/sys_user?sysparm_limit=6000&active=true', [
			//$res = $client->request('GET', 'https://telenordkpov.service-now.com/api/now/table/sys_user?sysparm_limit=6000&active=true&sysparm_query=user_name=heip^ORuser_name=pegu', [
			//$res = $client->request('GET', 'https://telenordkpov.service-now.com/api/now/table/sys_user?active=true&sysparm_fields=user_name%2Cemail%2Cemployee_number%2Ctitle%2Cu_company%2Cu_department%2Cmanager%2Cfirst_name%2Clast_name%2Cname%2Ccity%2Cphone', [
			'auth' => [$this->getUsername(), $this->getPassword()],
		]);

		$statusCode = $res->getStatusCode();

		if ($statusCode !== 200) {
			throw new UnexpectedValueException("Error fetching data from Service Now");
		}

		return (string)$res->getBody();
	}

	public function getAllUsers()
	{
		$sn_user_rebuild = [];
		$sn_users = [];
		$result = $this->query();
		$object = json_decode($result, false);

		foreach ($object->result as $key => $value) {
			$sn_users[$value->sys_id] = $value;
		}

		foreach ($sn_users as $value) {
			$sn_user = new ServicenowUser();
			$sn_user->setInitials($value->user_name);
			$sn_user->setLastname($value->last_name);
			$sn_user->setFirstname($value->first_name);
			$sn_user->setFullname($value->first_name . ' ' . $value->last_name);
			$sn_user->setCity($value->city);
			$sn_user->setEmployeeNumber((int)$value->employee_number);
			$sn_user->setEmail($value->email);
			$sn_user->setTitle($value->title);
			$sn_user->setPhone($value->phone);
			$sn_user->setCompany($value->u_company);
			$sn_user->setDepartment($value->u_department);
			if ($value->manager) //Attach manager if exist
			{
				$sn_user->setManager($this->findManager($sn_users, $value->manager->value));
			} else {
				$sn_user->setManager('');

			}
			$sn_user_rebuild[$value->user_name] = $sn_user;
		}
		return $sn_user_rebuild;
	}

	public function findManager($array, $sys_id): ?string
	{
		return $array[$sys_id]->user_name ?? '';
	}

	/**
	 * @return mixed
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * @param mixed $username
	 */
	public function setUsername($username): void
	{
		$this->username = $username;
	}

	/**
	 * @return mixed
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param mixed $password
	 */
	public function setPassword($password): void
	{
		$this->password = $password;
	}

}