<?php
declare(strict_types=1);

namespace Telenor\System\Exception;

class UnhandledMessageException extends PolarisException
{
}