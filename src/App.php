<?php
declare(strict_types=1);

namespace {

	use Psr\Log\LogLevel;
	use Telenor\Logging\LoggingController;
	use Telenor\System\Utils;

	define('APP_ROOT', dirname(__DIR__));
	require APP_ROOT . '/vendor/autoload.php';

	set_exception_handler(static function (Throwable $t) {
		try {
			LoggingController::getInstance()->log(LogLevel::EMERGENCY, 'Uncaught exception', [$t]);
			echo "Stack trace:\n" . $t->getTraceAsString() . "\n";
		} catch (Throwable $t2) {
			Utils::kill(sprintf('Failed to log uncaught exception (%s) at %s:%d: %s', Utils::getShortClassName($t2),
				$t2->getFile(), $t2->getLine(), $t2->getMessage()), 3);
		}
	});

	set_error_handler(static function ($errno, $errstr, $errfile, $errline) {
		try {
			$exception = new ErrorException($errstr, $errno, 1, $errfile ?? '(null)', $errline ?? '(null)');
			LoggingController::getInstance()->log(LogLevel::EMERGENCY, 'Uncaught error', [$exception]);
		} catch (Throwable $t) {
			Utils::kill(sprintf('Failed to log uncaught error (%s) at %s:%d: %s', Utils::getShortClassName($t),
				$t->getFile(), $t->getLine(), $t->getMessage()), 3);
		}
		return false;
	});
}

namespace Telenor {

	use Amp\Loop;
	use Amp\Loop\UnsupportedFeatureException;
	use DI\ContainerBuilder;
	use Psr\Container\ContainerInterface;
	use Telenor\Logging\TLog;
	use Telenor\System\Config;
	use Telenor\System\Error\BadConfigurationError;
	use Telenor\System\Utils;
	use Telenor\Worker\IWorker;

	class App
	{
		use TLog;

		protected $worker;

		public function __construct()
		{
			try {

				$builder = new ContainerBuilder();
				$builder->addDefinitions(__DIR__ . '/bootstrap.php');

				/** @var ContainerInterface $container */
				$container = $builder->build();

				Config::loadEnvironmentsFromDirectory('/run/secrets/*');
				Config::loadEnvironmentsFromDirectory('/*.conf');

				if (!Config::envBool('WORKER_ENABLED', true))
					Utils::kill('Worker disabled. Enable with WORKER_ENABLED=true', 0);

				/** @var IWorker $worker */
				$this->worker = $container->get('worker');
				$this->logInfo('Initializing ' . Utils::getShortWorkerName());

				// Listen for SIGTERM (sent by docker) and gracefully close the worker
				if (PHP_OS_FAMILY === 'Linux') {
					$this->registerShutdownSignal(SIGINT);
					$this->registerShutdownSignal(SIGTERM);
				}

				$this->worker->start();
				Loop::run(function () {
					$this->logInfo(Utils::getShortWorkerName() . ' started');
				});
			} catch (BadConfigurationError $e) {
				$err = 'Environment variable not found: ' . $e->getConfigName();
				if (!Utils::isStringNullOrEmpty($e->getMessage()))
					$err .= "\nAdditional error message: " . $e->getMessage();
				Utils::kill($err);
			}
		}

		protected function registerShutdownSignal(int $signal): void
		{
			$worker = $this->worker;
			try {
				Loop::onSignal($signal, static function (string $watcherId) use ($worker) {
					Loop::cancel($watcherId);
					$worker->stop();
					Loop::stop();
				});
			} catch (UnsupportedFeatureException $e) {
				Utils::kill("Failed to register shutdown signal: $signal");
			}
		}
	}

	new App();
}