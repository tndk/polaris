<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker\UserSync;

use Telenor\Model\IUser;
use function json_decode;

class ServicenowSingleUserSync extends AServicenowUserSync
{

	protected $username;

	public function __construct($username)
	{
		$this->username = $username;
		parent::__construct();
	}

	/**Fetch a single user from Servicenow based on input
	 *
	 * @return IUser
	 */
	public function getUsers(): array
	{
		$users = [];

		$this->servicenowApiUri .= "^user_name=$this->username";
		$snuser = $this->getAllUsers();

		if ($snuser) {
			/** @var IUser $user */
			$user = $snuser[$this->username];
			$usermanager = json_decode($this->query(), false);

			if (isset ($usermanager->result[0]->manager->link)) {
				$manager_link = $usermanager->result[0]->manager->link;

				$this->setServicenowApiUri($manager_link);
				$manager = json_decode($this->query(), false);
				$managerusername = $manager->result->user_name;
				$user->setManager($managerusername);

			}
			$users[$this->username] = $user;
			return $users;
		}

		$this->logInfo('User not found in Servicenow', ['User' => $this->username]);
		return $snuser;

	}
}