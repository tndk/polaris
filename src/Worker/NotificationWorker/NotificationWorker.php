<?php
declare(strict_types=1);

namespace Telenor\Worker\NotificationWorker;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Telenor\Message\IMessage;
use Telenor\Message\NotificationMessage;
use Telenor\Message\SMSMessage;
use Telenor\System\Config;
use Telenor\Worker\AWorker\AMessageQueueWorker;
use function json_encode;

class NotificationWorker extends AMessageQueueWorker
{
	public function onMessageConsumed(IMessage $message, array $metaData = null): bool
	{
		if ($message instanceof NotificationMessage) {

			$attachedMessage = $message->getAttachedMessage();

			if ($attachedMessage instanceof SMSMessage)
				return $this->handleSMSMessage($message, $attachedMessage);
		}

		$this->logError('Received unexpected message', [
			'message' => $message,
		]);
		return false;
	}

	protected function handleSMSMessage(NotificationMessage $message, SMSMessage $smsMessage): bool
	{
		try {
			$this->logInfo('Handling SMS message', [
				'message' => $smsMessage,
			]);

			$requestPayload = json_encode([
				'transaction_id' => $smsMessage->getTransactionId(),
				'status'         => $message->getStatus(),
			], JSON_THROW_ON_ERROR);

			$this->logInfo("Posting payload to ServiceNow: $requestPayload");

			// TODO: We should merge this into the ServicenowClient class in the WorkdayWorker
			$username = Config::envString('SERVICENOW_USERNAME');
			$password = Config::envString('SERVICENOW_PASSWORD');
			$url = 'https://' . Config::envString('SERVICENOW_INSTANCE') . '.service-now.com/api/tgssa/tndk_sms_rest_api';

			$client = new Client();

			$response = $client->request('PUT', $url, [
				'auth'    => [$username, $password],
				'headers' => ['Accept' => 'application/json'],
				'body'    => $requestPayload,
			]);

			$statusText = $response->getStatusCode() . ' ' . $response->getReasonPhrase();
			$logArgs = [
				'url'          => $url,
				'username'     => $username,
				'statuscode'   => $response->getStatusCode(),
				'statusphrase' => $response->getReasonPhrase(),
				'headers'      => $response->getHeaders(),
				'protocol'     => $response->getProtocolVersion(),
				'body'         => $response->getBody(),
			];

			// Response code was 200 ok? Then we remove the message from our queue
			if ($response->getStatusCode() === 200) {
				$this->logInfo("Got successful response from ServiceNow: $statusText", $logArgs);
				return true;
			}

			// Any other response codes will return in NACKing the message - thus we will retry again later
			$this->logError("Got failure response from ServiceNow: $statusText", $logArgs);
			return false;

		} catch (GuzzleException | Exception $e) {
			$this->logError('Failed to handle ServiceNow API request', [$e]);
			return false;
		}
	}
}