<?php
declare(strict_types=1);
namespace Telenor\System;

use Exception;
use Symfony\Component\Yaml\Yaml;
use Telenor\System\Error\BadConfigurationError;
use Telenor\System\Exception\BadFormatException;
use Telenor\System\Exception\FileNotFoundException;
use function ctype_digit;
use function fgets;
use function file_exists;
use function getenv;
use function preg_match;
use function putenv;
use function strtolower;

class Config
{
	public static function loadEnvironmentsFromDirectory(string $glob): void
	{
		foreach (glob($glob) as $file) {

			// Skip loading if file does not exist
			if (!file_exists($file))
				return;

			// Open file if possible - otherwise ignore file read
			if (!$fp = fopen($file, 'rb'))
				return;

			// Read the file line by line and add the environment variable, if not already set
			while (($line = fgets($fp)) !== false) {

				if (([$key, $value] = self::extractLine($line)) === null)
					continue;

				if (getenv($key) === false)
					putenv("$key=$value");
			}

			fclose($fp);
		}
	}

	protected static function extractLine(string $line): ?array
	{
		return preg_match('#^(?:export )?(\w+) *= *([\'"])?(.*?)\2?$#i', $line, $matches) ? [
			$matches[1],
			$matches[3],
		] : null;
	}

	/**
	 * Reads a configuration value from the environment (typically defined in the docker-compose file) as a string.
	 *
	 * @param string $name The name of the environment variable.
	 * @param string|null $default A default value to use, if the environment value does not exist.
	 * @return string The value of the environment variable.
	 * @throws BadConfigurationError If the environment variable is not declared and no default value is set.
	 */
	public static function envString(string $name, string $default = null): string
	{
		$value = getenv($name);
		if (!$value) {
			if ($default === null)
				throw new BadConfigurationError($name);
			$value = $default;
		}

		return $value;
	}

	/**
	 * Reads a configuration value from the environment (typically defined in the docker-compose file) as an integer.
	 *
	 * @param string $name The name of the environment variable.
	 * @param int|null $default A default value to use, if the environment value does not exist.
	 * @return int The value of the environment variable.
	 * @throws BadConfigurationError If the environment variable is not declared and no default value is set.
	 */
	public static function envInt(string $name, int $default = null): int
	{
		$value = getenv($name);
		if (!$value || !ctype_digit($value)) {
			if ($default === null)
				throw new BadConfigurationError($name);
			$value = $default;
		}
		return (int)$value;
	}

	/**
	 * Reads a configuration value from the environment (typically defined in the docker-compose file) as a boolean
	 * value.
	 *
	 * @param string $name The name of the environment variable.
	 * @param bool $default A default value to use, if the environment value does not exist.
	 * @return bool The value of the environment variable.
	 * @throws BadConfigurationError If the environment variable is not declared and no default value is set.
	 */
	public static function envBool(string $name, bool $default = null): bool
	{
		$value = getenv($name);
		if ($value === false) {
			if ($default === null)
				throw new BadConfigurationError($name);
			$value = $default;
		}

		if (is_bool($value))
			return $value;

		$value = strtolower('' . $value);

		if (!preg_match('#true|yes|1|false|no|0#', $value))
			throw new BadConfigurationError($name, 'Invalid value for boolean type');

		return !($value === '0' || $value === 'no' || $value === 'false');
	}

	/**
	 * Parses a complete YAML file and returns the parsed data in an unstructured array.
	 *
	 * @param string $workerConfigName The name of the .yml file to load (without file extension). Default=config
	 * @return array The parsed YAML data.
	 * @throws BadFormatException When the config file is not a valid YAML file.
	 * @throws FileNotFoundException When the config file does not exist.
	 */
	public static function yml(string $workerConfigName = 'config'): array
	{
		$configFile = APP_ROOT . '/config/workers/' . Utils::getShortWorkerName() . "/$workerConfigName.yml";
		if (!file_exists($configFile))
			throw new FileNotFoundException("File not found: $configFile", $configFile);

		try {
			$config = Yaml::parseFile($configFile);
		} catch (Exception $e) {
			throw new BadFormatException("YAML config file cannot be parsed: $configFile", $e);
		}

		return $config;
	}
}