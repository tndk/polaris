<?php
declare(strict_types=1);

namespace Telenor\Model;

class ServiceNowIncident extends AServiceNowModel
{
	public function getEndpoint(): string
	{
		return 'now/table/incident';
	}

	public function getFields(): array
	{
		return [
			'number',
			'sys_id',
			'short_description',
			'description',
			'category',
			'opened_at',
			'state',
			'priority',
			'impact',
			'urgency',
		];
	}

	public function getFilters(string $id): array
	{
		if (strncmp($id, 'INC', 3) === 0)
			return ['number' => $id];

		if (preg_match('/^\w+-\w+$/', $id))
			return ['correlation_id' => $id];

		return parent::getFilters($id);
	}
}