<?php
declare(strict_types=1);

namespace Telenor\MessageQueue;

use Exception;
use Telenor\Logging\TLog;
use Telenor\Message\IMessage;
use Telenor\MessageFormatter\IMessageFormatter;
use Telenor\MessageQueue\Driver\IMessageQueueDriver;
use Telenor\System\Exception\ConnectionException;
use Telenor\System\GlobalStorage;
use Telenor\System\Pattern\TSingleton;
use Telenor\System\Utils;

/** @method static MessageQueueController getInstance() */
class MessageQueueController
{
	use TLog;
	use TSingleton;

	/** @var IMessageQueueDriver */
	protected $driver;

	/** @var IMessageFormatter */
	protected $messageFormatter;

	public function initialize(): void
	{
		$this->driver = MessageQueueFactory::createDriver();
		$this->messageFormatter = MessageQueueFactory::createMessageFormatter();
	}

	/**
	 * @return IMessageQueueDriver The instance of the message queue driver to use.
	 */
	public function getDriver(): IMessageQueueDriver
	{
		return $this->driver;
	}

	public function getMessageFormatter(): IMessageFormatter
	{
		return $this->messageFormatter;
	}

	/**
	 * Connects to the message queue.
	 */
	public function connect(): void
	{
		$this->getDriver()->connect();
	}

	/**
	 * Disconnects from the message queue.
	 */
	public function disconnect(): void
	{
		$this->getDriver()->disconnect();
	}

	/**
	 * @return bool True if successfully connected to the message queue, false otherwise.
	 */
	public function isConnected(): bool
	{
		return $this->getDriver()->isConnected();
	}

	/**
	 * Publishes a message to the message queue.
	 *
	 * @param IMessage $message The message to send.
	 * @param string $queueName An alternative queue name to publish to (default = configured message queue name).
	 * @throws ConnectionException If connection problems occurs.
	 */
	public function publish(IMessage $message, string $queueName = null): void
	{
		$message->validate();

		$this->getDriver()->publish($this->getMessageFormatter()->serialize($message), $queueName);
	}

	public function registerOnMessageConsumed(IMessageConsumeHandler $handler, string $queueName = null): void
	{
		$this->getDriver()->onMessageConsumed(function (string $message, array $metaData = null) use ($handler) : bool {
			try {
				$parsedMessage = $this->getMessageFormatter()->unserialize($message);
				GlobalStorage::setUUID($parsedMessage->getRequestId() ?? Utils::generateUUID());
				return $handler->messageConsumed($parsedMessage, $metaData);
			} catch (Exception $e) {
				// We failed to unserialize the message - we will ack the message from the queue
				// so we do not keep trying unserializing when the payload is invalid
				return true;
			}
		}, $queueName);
	}

	public function registerOnMessageExpired(IMessageExpiredHandler $handler): void
	{
		$this->getDriver()->onMessageExpired(function (string $message, array $metaData = null) use ($handler) : bool {
			$parsedMessage = $this->messageFormatter->unserialize($message);
			GlobalStorage::setUUID($parsedMessage->getRequestId() ?? Utils::generateUUID());
			return $handler->messageExpired($parsedMessage, $metaData);
		});
	}

	public function registerOnConnectionChanged(callable $callable): void
	{
		$this->getDriver()->onConnectionChanged($callable);
	}
}