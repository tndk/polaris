<?php
declare(strict_types=1);

namespace Telenor\Message;

use DateTime;

interface IMessage
{
	public function getRequestId(): ?string;

	public function getTimestamp(): DateTime;

	public function getAttributes(): array;

	public function __toString(): string;

	public function validate(): void;
}