<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\Logging\Driver\MessageQueueDriver;

class MessageQueueDriverTest extends TestCase
{
	/** @var MessageQueueDriver */
	protected $instance;

	public function setUp(): void
	{
		$this->instance = new class extends MessageQueueDriver
		{
			public function testConvertLogContextRecursively(array $input): array
			{
				return $this->convertLogContextRecursively($input);
			}
		};
	}

	public function testConvertLogContextRecursively()
	{
		// No structural changes 1
		$arr = $this->instance->testConvertLogContextRecursively([]);
		static::assertSame([], $arr);

		// No structural changes 2
		$arr = $this->instance->testConvertLogContextRecursively([
			'int'        => 132,
			'float'      => 12.3,
			'bool_true'  => true,
			'bool_false' => false,
			'string'     => 'something',
		]);
		static::assertSame([
			'int'        => 132,
			'float'      => 12.3,
			'bool_true'  => true,
			'bool_false' => false,
			'string'     => 'something',
		], $arr);

		// No structural changes 3
		$arr = $this->instance->testConvertLogContextRecursively([
			'int'        => 132,
			'float'      => 12.3,
			'bool_true'  => true,
			'bool_false' => false,
			'string'     => 'something',
			'nested'     => [
				'string' => 'something else',
				'int'    => 234,
			],
		]);
		static::assertSame([
			'int'        => 132,
			'float'      => 12.3,
			'bool_true'  => true,
			'bool_false' => false,
			'string'     => 'something',
			'nested'     => [
				'string' => 'something else',
				'int'    => 234,
			],
		], $arr);

		// Throwables are 'flattened' - also nested
		$arr = $this->instance->testConvertLogContextRecursively([
			'int'                => 132,
			'string'             => 'something',
			new Exception('a message'),
			'exception_with_key' => new Exception('a keyed message'),
			'nested'             => [
				'string' => 'something else',
				'int'    => 234,
				new Error('another message'),
			],
		]);
		// - As the stacktrace is not the same from workstation to workstation, we simply check if the key exists
		//   in the converted array, and then nukes the stacktraces and check the rest of the complete array
		static::assertArrayHasKey('stacktrace', $arr[0]['exception']);
		static::assertArrayHasKey('stacktrace', $arr['exception_with_key']['exception']);
		static::assertArrayHasKey('stacktrace', $arr['nested'][0]['exception']);
		unset($arr[0]['exception']['stacktrace'], $arr['exception_with_key']['exception']['stacktrace'], $arr['nested'][0]['exception']['stacktrace']);
		static::assertSame([
			'int'                => 132,
			'string'             => 'something',
			0                    => [
				'exception' => [
					'type'    => Exception::class,
					'message' => 'a message',
				],
			],
			'exception_with_key' => [
				'exception' => [
					'type'    => Exception::class,
					'message' => 'a keyed message',
				],
			],
			'nested'             => [
				'string' => 'something else',
				'int'    => 234,
				0        => [
					'exception' => [
						'type'    => Error::class,
						'message' => 'another message',
					],
				],
			],
		], $arr);
	}
}
