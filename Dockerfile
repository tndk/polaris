# PLATFORM IMAGE

FROM php:7.3.9-cli-alpine3.10 AS polaris-platform-1

ARG DEV_PKGS="autoconf gcc g++ make"

ENV TZ='Europe/Copenhagen'

RUN \
	apk update && \
	apk add yaml-dev tzdata $DEV_PKGS && \
	docker-php-ext-install bcmath pcntl sockets pdo_mysql && \
	pecl install yaml && \
	echo "extension=yaml" > "$PHP_INI_DIR/conf.d/pecl-ext-yaml.ini" && \
	echo "date.timezone = Europe/Copenhagen" > "$PHP_INI_DIR/conf.d/misc.ini" && \
	mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
	apk del $DEV_PKGS && \
	rm /usr/src/* && \
	rm /usr/local/bin/phpdbg && \
	rm /usr/local/bin/php-cgi

# WORKER DEPENDENCY IMAGE

FROM polaris-platform-1 AS polaris-dependencies-1

ENV ROOT_DIR="/srv"
ARG DEV_PKGS="composer"

ENV COMPOSER_ALLOW_SUPERUSER=1

COPY composer.json composer.lock $ROOT_DIR/

RUN \
	apk update && \
	apk add $DEV_PKGS && \
	composer global require hirak/prestissimo 2>&1 && \
	composer i -n -d $ROOT_DIR --no-dev --no-ansi 2>&1 && \
	apk del $DEV_PKGS && \
	rm /var/cache/apk/*

# BASE IMAGE FOR WORKERS

FROM polaris-dependencies-1 AS polaris-base

ENV POLARIS_DIR="$ROOT_DIR/polaris"
ENV RELEASE_FILE="/release.conf"

COPY src $POLARIS_DIR
COPY config $ROOT_DIR/config

RUN dos2unix "$POLARIS_DIR/App.php"

WORKDIR $POLARIS_DIR
CMD ["php", "App.php"]

# These ONBUILD instructions will be executed when building the next image (our worker image)
ONBUILD ARG TELENOR_WORKER_NAME="<unknown>"
ONBUILD ARG TELENOR_WORKER_VERSION="<unknown>"
ONBUILD ARG TELENOR_BUILD_TIME="<unknown>"
ONBUILD ARG TELENOR_RELEASER_NAME="<unknown>"
ONBUILD ARG TELENOR_RELEASER_EMAIL="<unknown>"
ONBUILD RUN \
	echo "TELENOR_WORKER_VERSION=\"$TELENOR_WORKER_VERSION\"" >> $RELEASE_FILE && \
	echo "TELENOR_WORKER_NAME=\"$TELENOR_WORKER_NAME\"" >> $RELEASE_FILE && \
	echo "TELENOR_BUILD_TIME=\"$TELENOR_BUILD_TIME\"" >> $RELEASE_FILE && \
	echo "TELENOR_RELEASER_NAME=\"$TELENOR_RELEASER_NAME\"" >> $RELEASE_FILE && \
	echo "TELENOR_RELEASER_EMAIL=\"$TELENOR_RELEASER_EMAIL\"" >> $RELEASE_FILE