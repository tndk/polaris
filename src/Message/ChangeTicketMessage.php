<?php
declare(strict_types=1);

namespace Telenor\Message;

use DateTime;
use Telenor\System\Exception\InvalidDataException;
use Telenor\System\Utils;
use function array_merge;

class ChangeTicketMessage extends ATicketMessage
{
	public const STATUS_NEW = 'New';
	public const STATUS_ASSESS = 'Assess';
	public const STATUS_AUTHORIZE = 'Authorize';
	public const STATUS_SCHEDULED = 'Scheduled';
	public const STATUS_IMPLEMENT = 'Implement';
	public const STATUS_REVIEW = 'Review';
	public const STATUS_CLOSED = 'Closed';
	public const STATUS_CANCELED = 'Canceled';

	public const CLOSE_CODE_SUCCESSFUL = 'Successful';
	public const CLOSE_CODE_SUCCESSFUL_WITH_ISSUES = 'Successful with issues';
	public const CLOSE_CODE_UNSUCCESSFUL = 'Unsuccessful';

	public const CHANGE_TYPE_NOKIA_MINOR = 'nokia minor';
	public const CHANGE_TYPE_NOKIA_MAJOR = 'nokia major';
	public const CHANGE_TYPE_NOKIA_SIGNIFICANT = 'nokia significant';
	public const CHANGE_TYPE_NOKIA_EMERGENCY = 'nokia emergency';

	public const CHANGE_TYPE_STANDARD = 'standard';

	public const CAB_REQUIRED_TRUE = '1';

	/** @var string|null */
	protected $changeTemplate;

	/** @var DateTime */
	protected $plannedStartDate;

	/** @var DateTime */
	protected $plannedEndDate;

	/** @var DateTime */
	protected $actualStartDate;

	/** @var DateTime */
	protected $actualEndDate;

	/** @var ?DateTime $CABDate - Change Advisory Board */
	protected $CABDate;

	/** @var string|null */
	protected $closeCode;

	/** @var string|null */
	protected $closeNotes;

	/** @var string|null */
	protected $changeType;

	/** @var string|null */
	protected $CABRequired;

	/** @var string|null */
	protected $justification;

	/** @var string|null */
	protected $implementationPlan;

	/** @var string|null */
	protected $riskAndImpactAnalysis;

	/** @var string|null */
	protected $backoutPlan;

	/** @var string|null */
	Protected $testPlan;

	/** @var string|null */
	protected $externalApproval;

	/**
	 * ChangeTicketMessage constructor.
	 *
	 * @param string $action
	 */
	public function __construct(string $action)
	{
		parent::__construct('change', $action);
	}

	/**
	 * @return string|null
	 */
	public function getExternalApproval(): ?string
	{
		return $this->externalApproval;
	}

	/**
	 * @param string|null $externalApproval
	 */
	public function setExternalApproval(?string $externalApproval): void
	{
		$this->externalApproval = $externalApproval;
	}


	/**
	 * @return string|null
	 */
	public function getCABRequired(): ?string
	{
		return $this->CABRequired;
	}

	/**
	 * @param string|null $CABRequired
	 */
	public function setCABRequired(?string $CABRequired): void
	{
		$this->CABRequired = $CABRequired;
	}

	/**
	 * @return string|null
	 */
	public function getChangeType(): ?string
	{
		return $this->changeType;
	}

	/**
	 * @param string|null $changeType
	 */
	public function setChangeType(?string $changeType): void
	{
		$this->changeType = $changeType;
	}

	/**
	 * @return string|null
	 */
	public function getChangeTemplate(): ?string
	{
		return $this->changeTemplate;
	}

	/**
	 * @param string|null $changeTemplate
	 */
	public function setChangeTemplate(?string $changeTemplate): void
	{
		$this->changeTemplate = $changeTemplate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getPlannedStartDate(): ?DateTime
	{
		return $this->plannedStartDate;
	}

	/**
	 * @param DateTime|null $plannedStartDate
	 */
	public function setPlannedStartDate(?DateTime $plannedStartDate): void
	{
		$this->plannedStartDate = $plannedStartDate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getPlannedEndDate(): ?DateTime
	{
		return $this->plannedEndDate;
	}

	/**
	 * @param DateTime|null $plannedEndDate
	 */
	public function setPlannedEndDate(?DateTime $plannedEndDate): void
	{
		$this->plannedEndDate = $plannedEndDate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getCABDate(): ?DateTime
	{
		return $this->CABDate;
	}

	/**
	 * @param DateTime|null $CABDate
	 */
	public function setCABDate(?DateTime $CABDate): void
	{
		$this->CABDate = $CABDate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getActualStartDate(): ?DateTime
	{
		return $this->actualStartDate;
	}

	/**
	 * @param DateTime|null $actualStartDate
	 */
	public function setActualStartDate(?DateTime $actualStartDate): void
	{
		$this->actualStartDate = $actualStartDate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getActualEndDate(): ?DateTime
	{
		return $this->actualEndDate;
	}

	/**
	 * @param DateTime|null $actualEndDate
	 */
	public function setActualEndDate(?DateTime $actualEndDate): void
	{
		$this->actualEndDate = $actualEndDate;
	}

	/**
	 * @return string|null
	 */
	public function getCloseCode(): ?string
	{
		return $this->closeCode;
	}

	/**
	 * @param string|null $closeCode
	 */
	public function setCloseCode(?string $closeCode): void
	{
		$this->closeCode = $closeCode;
	}

	/**
	 * @return string|null
	 */
	public function getCloseNotes(): ?string
	{
		return $this->closeNotes;
	}

	/**
	 * @param string|null $closeNotes
	 */
	public function setCloseNotes(?string $closeNotes): void
	{
		$this->closeNotes = $closeNotes;
	}

	/**
	 * @return string|null
	 */
	public function getJustification(): ?string
	{
		return $this->justification;
	}

	/**
	 * @param string|null $justification
	 */
	public function setJustification(?string $justification): void
	{
		$this->justification = $justification;
	}

	/**
	 * @return string|null
	 */
	public function getImplementationPlan(): ?string
	{
		return $this->implementationPlan;
	}

	/**
	 * @param string|null $implementationPlan
	 */
	public function setImplementationPlan(?string $implementationPlan): void
	{
		$this->implementationPlan = $implementationPlan;
	}

	/**
	 * @return string|null
	 */
	public function getRiskAndImpactAnalysis(): ?string
	{
		return $this->riskAndImpactAnalysis;
	}

	/**
	 * @param string|null $riskAndImpactAnalysis
	 */
	public function setRiskAndImpactAnalysis(?string $riskAndImpactAnalysis): void
	{
		$this->riskAndImpactAnalysis = $riskAndImpactAnalysis;
	}

	/**
	 * @return string|null
	 */
	public function getBackoutPlan(): ?string
	{
		return $this->backoutPlan;
	}

	/**
	 * @param string|null $backoutPlan
	 */
	public function setBackoutPlan(?string $backoutPlan): void
	{
		$this->backoutPlan = $backoutPlan;
	}

	/**
	 * @return string|null
	 */
	public function getTestPlan(): ?string
	{
		return $this->testPlan;
	}

	/**
	 * @param string|null $testPlan
	 */
	public function setTestPlan(?string $testPlan): void
	{
		$this->testPlan = $testPlan;
	}

	protected function getValidStates(): array
	{
		return [
			self::STATUS_NEW,
			self::STATUS_ASSESS,
			self::STATUS_AUTHORIZE,
			self::STATUS_SCHEDULED,
			self::STATUS_IMPLEMENT,
			self::STATUS_REVIEW,
			self::STATUS_CLOSED,
			self::STATUS_CANCELED,
		];
	}

	/**
	 * @return array
	 */
	public function toArray(): array
	{

		return array_merge(parent::toArray(), [
			'change_template'      => $this->getChangeTemplate(),
			'planned_startdate'    => $this->convertDateTime($this->getPlannedStartDate()),
			'planned_enddate'      => $this->convertDateTime($this->getPlannedEndDate()),
			'actual_startdate'     => $this->convertDateTime($this->getActualStartDate()),
			'actual_enddate'       => $this->convertDateTime($this->getActualEndDate()),
			'cab_date'             => $this->convertDateTime($this->getCABDate()),
			'close_code'           => $this->getCloseCode(),
			'close_notes'          => $this->getCloseNotes(),
			'change_type'          => $this->getChangeType(),
			'cab_required'         => $this->getCABRequired(),
			'justification'        => $this->getJustification(),
			'implementation_plan'  => $this->getImplementationPlan(),
			'risk_impact_analysis' => $this->getRiskAndImpactAnalysis(),
			'backout_plan'         => $this->getBackoutPlan(),
			'test_plan'            => $this->getTestPlan(),
			'nokia_approved'       => $this->getExternalApproval()
		]);

	}

	public function validate(): void
	{
		parent::validate();

		if ($this->getStatus() === self::STATUS_CLOSED && Utils::isStringNullOrEmpty($this->getCloseNotes())) {
			throw new InvalidDataException('Missing CloseNotes');

		}

		if ($this->getStatus() === self::STATUS_REVIEW && ($this->getActualStartDate() === null || $this->getActualEndDate() === null)) {
			throw new InvalidDataException('Missing Actual dates');

		}

		if ($this->getStatus() === self::STATUS_CLOSED && (Utils::isStringNullOrEmpty($this->getCloseNotes()) || Utils::isStringNullOrEmpty($this->getCloseCode()))) {
			throw new InvalidDataException('Missing Close code or close notes');

		}

	}
}