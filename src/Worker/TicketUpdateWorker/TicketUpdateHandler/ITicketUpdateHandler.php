<?php
declare(strict_types=1);

namespace Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler;

use Telenor\Message\TicketUpdateMessage;
use Telenor\System\Exception\UnhandledMessageException;

interface ITicketUpdateHandler
{
	/**
	 * @param TicketUpdateMessage $message
	 * @return bool
	 * @throws UnhandledMessageException
	 */
	public function handleMessage(TicketUpdateMessage $message): bool;
}