<?php
declare(strict_types=1);

namespace Telenor\MessageQueue;

use Telenor\MessageFormatter\IMessageFormatter;
use Telenor\MessageFormatter\JsonMessageFormatter;
use Telenor\MessageQueue\Driver\IMessageQueueDriver;
use Telenor\MessageQueue\Driver\RabbitMQDriver;
use Telenor\System\Config;
use Telenor\System\Error\BadConfigurationError;
use Telenor\System\Utils;

class MessageQueueFactory
{
	protected const MESSAGE_QUEUE_DRIVER = 'MESSAGE_QUEUE_DRIVER';
	protected const MESSAGE_FORMATTER = 'MESSAGE_FORMATTER';

	/**
	 * Creates a driver that is used as a message queue driver. The driver can be defined
	 * with the MESSAGE_QUEUE_DRIVER environment variable, with default to RabbitMQDriver.
	 *
	 * @return IMessageQueueDriver An instance of a message queue driver.
	 * @throws BadConfigurationError If an invalid class is configured.
	 */
	public static function createDriver(): IMessageQueueDriver
	{
		$driverConf = Config::envString(self::MESSAGE_QUEUE_DRIVER, RabbitMQDriver::class);

		if (!Utils::implementsInterface($driverConf, IMessageQueueDriver::class))
			throw new BadConfigurationError(self::MESSAGE_QUEUE_DRIVER, 'No or invalid Message Queue driver specified');

		return new $driverConf();
	}

	public static function createMessageFormatter(): IMessageFormatter
	{
		$driverConf = Config::envString(self::MESSAGE_FORMATTER, JsonMessageFormatter::class);

		if ($driverConf === JsonMessageFormatter::class)
			return new JsonMessageFormatter();

		throw new BadConfigurationError(self::MESSAGE_QUEUE_DRIVER, 'No or invalid Message Formatter driver specified');
	}

}