<?php
declare(strict_types=1);

namespace Telenor\Worker\SMSWorker;

use Amp\Loop;
use DateTime;
use Exception;
use GsmEncoder;
use SMPP;
use SmppAddress;
use SmppClient;
use SocketTransportException;
use Telenor\LibraryFixes\php_smpp\SocketTransport;
use Telenor\Message\APayloadMessage;
use Telenor\Message\IMessage;
use Telenor\Message\NotificationMessage;
use Telenor\Message\SMSMessage;
use Telenor\MessageQueue\IMessageExpiredHandler;
use Telenor\MessageQueue\MessageQueueController;
use Telenor\System\Config;
use Telenor\System\Exception\ConnectionException;
use Telenor\System\GlobalStorage;
use Telenor\System\Utils;
use Telenor\Worker\AWorker\AMessageQueueWorker;
use function array_merge;

class SMSWorker extends AMessageQueueWorker
{
	/** @var DateTime */
	protected $lastSent;

	/** @var int */
	protected $idleTimeout;

	/** @var int */
	protected $receiveTimeout;

	/** @var int */
	protected $sendTimeout;

	/** @var string */
	protected $hostname;
	/** @var int */
	protected $port;

	/** @var string */
	protected $overrideSender;

	/** @var string */
	protected $username;

	/** @var string */
	protected $password;

	/** @var SocketTransport */
	protected $transport;

	/** @var SmppClient */
	protected $smppClient;

	/** @var string */
	protected $notificationQueueName;

	public function start()
	{
		parent::start();

		$this->lastSent = new DateTime();

		$this->idleTimeout = Config::envInt('SMPP_IDLE_TIMEOUT', 30);
		$this->receiveTimeout = 1000 * Config::envInt('SMPP_RECEIVE_TIMEOUT', 10);
		$this->sendTimeout = 1000 * Config::envInt('SMPP_SEND_TIMEOUT', 10);
		$this->username = Config::envString('SMPP_USERNAME');
		$this->overrideSender = Config::envString('SMPP_OVERRIDE_SENDER', '');
		$this->password = Config::envString('SMPP_PASSWORD');
		$this->notificationQueueName = Config::envString('NOTIFICATION_QUEUE_NAME', 'pol.notification');

		$debugHandler = function ($message) {
			$this->logDebug($message, ['logsource' => 'SMPP']);
		};

		$this->hostname = Config::envString('SMPP_HOSTNAME');
		$this->port = Config::envInt('SMPP_PORT', 2775);

		SocketTransport::$forceIpv4 = true;
		$this->transport = new SocketTransport([$this->hostname], $this->port, true, $debugHandler);
		$this->transport->setRecvTimeout($this->receiveTimeout);
		$this->transport->setSendTimeout($this->sendTimeout);
		$this->transport->debug = Config::envBool('SMPP_DEBUG', false);

		SmppClient::$csms_method = SmppClient::CSMS_8BIT_UDH;
		$this->smppClient = new SmppClient($this->transport, $debugHandler);
		$this->smppClient->debug = $this->transport->debug;

		Loop::repeat(1000, function () {
			$this->checkConnectivity();
		});

		try {
			MessageQueueController::getInstance()->registerOnMessageExpired(new class ($this) implements IMessageExpiredHandler
			{
				protected $parent;

				public function __construct(SMSWorker $parent)
				{
					$this->parent = $parent;
				}

				public function messageExpired(IMessage $message, array $metaData = null): bool
				{
					return $this->parent->onMessageExpired($message);
				}
			});
		} catch (ConnectionException $e) {
		}
	}

	public function onMessageConsumed(IMessage $message, array $metaData = null): bool
	{
		$this->logDebug('Received a new message from the queue, parsed message type: ' . Utils::getShortClassName($message));

		$additionalLogs = [];
		if ($message instanceof APayloadMessage)
			$additionalLogs['transaction_id'] = $message->getTransactionId();

		$this->logInfo('Handling message request_id: ' . GlobalStorage::getUUID(), $additionalLogs);

		if (!$message instanceof SMSMessage) {
			$this->logError('Did not receive a correct SMS payload message', [
				'classname' => Utils::getShortClassName($message),
			]);
			return false;
		}

		$this->lastSent = new DateTime();

		try {
			if (!$this->transport->isOpen()) {

				$this->logNotice('Opening connection to SMPP host: ' . $this->hostname . ':' . $this->port);
				$this->transport->open();

				$this->logDebug('Binding receiver');
				$this->smppClient->bindReceiver($this->username, $this->password);
			}

			if (!Utils::isStringNullOrEmpty($this->overrideSender)) {
				$this->logDebug('Sender override enabled; sending as: ' . $this->overrideSender);
				$sender = $this->overrideSender;
			} else {
				$sender = $message->getSender();
			}

			$ton = SMPP::TON_ALPHANUMERIC;
			if (preg_match('#^\+?[0-9 \-]+$#', $sender))
				$ton = SMPP::TON_UNKNOWN;

			$smppSender = new SmppAddress($sender, $ton);
			$smppRecipient = new SmppAddress($message->getRecipient());
			$smppMessage = GsmEncoder::utf8_to_gsm0338($message->getMessage());

			$smsLog = [
				'sender'    => $message->getSender(),
				'recipient' => $message->getRecipient(),
				'message'   => $message->isPassword() ? '** PASSWORD SMS - MESSAGE BODY REMOVED **' : $message->getMessage(),
				'ton'       => $ton,
			];

			$this->logDebug('Sending SMS', $smsLog);

			$smppMessageId = $this->smppClient->sendSMS($smppSender, $smppRecipient, $smppMessage);

			$this->logInfo('Successfully sent SMS, got smppMessageId: ' . $smppMessageId, array_merge($smsLog, [
				'smppMessageId' => $smppMessageId,
			]));

			$notificationMessage = new NotificationMessage();
			$notificationMessage->setAttachedMessage($message);
			$notificationMessage->setStatus('success');
			$this->publishToQueue($notificationMessage, $this->notificationQueueName);

			return true;
		} catch (Exception $e) {
			$this->logError('Failed sending SMS', [$e]);

			if ($e instanceof SocketTransportException) {
				$this->logNotice('Connection broken, re-establishing connection..');
				try {
					if ($this->transport->isOpen())
						$this->transport->close();
				} catch (Exception $e) {
					$this->logError('Failed to close connection gracefully', [$e]);
				}
			}

			return false;
		}
	}

	public function onMessageExpired(IMessage $message): bool
	{
		$this->logDebug('Publishing an expired message to the notification queue', [
			'message' => $message,
		]);

		$notificationMessage = new NotificationMessage();
		$notificationMessage->setAttachedMessage($message);
		$notificationMessage->setStatus('failure');

		try {
			$this->publishToQueue($notificationMessage, $this->notificationQueueName);
			return true;
		} catch (ConnectionException $e) {
			$this->logError('Failed to publish message to notification queue', [
				$e,
				'message' => $message,
			]);
			return false;
		}
	}

	protected function checkConnectivity(): void
	{
		if (!$this->transport->isOpen())
			return;

		$now = new DateTime();
		$shouldClose = ($now->diff($this->lastSent)->s >= $this->idleTimeout);

		try {
			$this->smppClient->enquireLink();
			$this->smppClient->respondEnquireLink();
		} catch (Exception $e) {
			$this->logWarning('Failed to keep SMPP connection alive, closing..');
			$shouldClose = true;
			$this->transport->close();
		}

		if ($shouldClose && $this->transport->isOpen()) {
			$this->logNotice('Idle timeout occurred, closing connection to SMPP host');
			try {
				$this->smppClient->close();
			} catch (Exception $e) {
				$this->logError('Failed to gracefully close the connection to the SMPP host');
			}
		}
	}
}