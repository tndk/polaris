<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;
use Telenor\Logging\LoggingController;

class LoggingControllerTest extends TestCase
{
	/** @var LoggingController */
	protected $logger;

	protected function setUp(): void
	{
		putenv('LOG_DRIVER=Telenor\Logging\Driver\NullDriver');
		$this->logger = LoggingController::getInstance();
	}

	public function testShouldLog(): void
	{
		#region LogLevel::DEBUG
		$this->logger->setLogLevel(LogLevel::DEBUG);
		$this->assertTrue($this->logger->shouldLog(LogLevel::DEBUG));
		$this->assertTrue($this->logger->shouldLog(LogLevel::INFO));
		$this->assertTrue($this->logger->shouldLog(LogLevel::NOTICE));
		$this->assertTrue($this->logger->shouldLog(LogLevel::WARNING));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ERROR));
		$this->assertTrue($this->logger->shouldLog(LogLevel::CRITICAL));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ALERT));
		$this->assertTrue($this->logger->shouldLog(LogLevel::EMERGENCY));
		#endregion

		#region LogLevel::INFO
		$this->logger->setLogLevel(LogLevel::INFO);
		$this->assertFalse($this->logger->shouldLog(LogLevel::DEBUG));
		$this->assertTrue($this->logger->shouldLog(LogLevel::INFO));
		$this->assertTrue($this->logger->shouldLog(LogLevel::NOTICE));
		$this->assertTrue($this->logger->shouldLog(LogLevel::WARNING));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ERROR));
		$this->assertTrue($this->logger->shouldLog(LogLevel::CRITICAL));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ALERT));
		$this->assertTrue($this->logger->shouldLog(LogLevel::EMERGENCY));
		#endregion

		#region LogLevel::NOTICE
		$this->logger->setLogLevel(LogLevel::NOTICE);
		$this->assertFalse($this->logger->shouldLog(LogLevel::DEBUG));
		$this->assertFalse($this->logger->shouldLog(LogLevel::INFO));
		$this->assertTrue($this->logger->shouldLog(LogLevel::NOTICE));
		$this->assertTrue($this->logger->shouldLog(LogLevel::WARNING));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ERROR));
		$this->assertTrue($this->logger->shouldLog(LogLevel::CRITICAL));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ALERT));
		$this->assertTrue($this->logger->shouldLog(LogLevel::EMERGENCY));
		#endregion

		#region LogLevel::WARNING
		$this->logger->setLogLevel(LogLevel::WARNING);
		$this->assertFalse($this->logger->shouldLog(LogLevel::DEBUG));
		$this->assertFalse($this->logger->shouldLog(LogLevel::INFO));
		$this->assertFalse($this->logger->shouldLog(LogLevel::NOTICE));
		$this->assertTrue($this->logger->shouldLog(LogLevel::WARNING));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ERROR));
		$this->assertTrue($this->logger->shouldLog(LogLevel::CRITICAL));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ALERT));
		$this->assertTrue($this->logger->shouldLog(LogLevel::EMERGENCY));
		#endregion

		#region LogLevel::ERROR
		$this->logger->setLogLevel(LogLevel::ERROR);
		$this->assertFalse($this->logger->shouldLog(LogLevel::DEBUG));
		$this->assertFalse($this->logger->shouldLog(LogLevel::INFO));
		$this->assertFalse($this->logger->shouldLog(LogLevel::NOTICE));
		$this->assertFalse($this->logger->shouldLog(LogLevel::WARNING));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ERROR));
		$this->assertTrue($this->logger->shouldLog(LogLevel::CRITICAL));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ALERT));
		$this->assertTrue($this->logger->shouldLog(LogLevel::EMERGENCY));
		#endregion

		#region LogLevel::CRITICAL
		$this->logger->setLogLevel(LogLevel::CRITICAL);
		$this->assertFalse($this->logger->shouldLog(LogLevel::DEBUG));
		$this->assertFalse($this->logger->shouldLog(LogLevel::INFO));
		$this->assertFalse($this->logger->shouldLog(LogLevel::NOTICE));
		$this->assertFalse($this->logger->shouldLog(LogLevel::WARNING));
		$this->assertFalse($this->logger->shouldLog(LogLevel::ERROR));
		$this->assertTrue($this->logger->shouldLog(LogLevel::CRITICAL));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ALERT));
		$this->assertTrue($this->logger->shouldLog(LogLevel::EMERGENCY));
		#endregion

		#region LogLevel::ALERT
		$this->logger->setLogLevel(LogLevel::ALERT);
		$this->assertFalse($this->logger->shouldLog(LogLevel::DEBUG));
		$this->assertFalse($this->logger->shouldLog(LogLevel::INFO));
		$this->assertFalse($this->logger->shouldLog(LogLevel::NOTICE));
		$this->assertFalse($this->logger->shouldLog(LogLevel::WARNING));
		$this->assertFalse($this->logger->shouldLog(LogLevel::ERROR));
		$this->assertFalse($this->logger->shouldLog(LogLevel::CRITICAL));
		$this->assertTrue($this->logger->shouldLog(LogLevel::ALERT));
		$this->assertTrue($this->logger->shouldLog(LogLevel::EMERGENCY));
		#endregion

		#region LogLevel::EMERGENCY
		$this->logger->setLogLevel(LogLevel::EMERGENCY);
		$this->assertFalse($this->logger->shouldLog(LogLevel::DEBUG));
		$this->assertFalse($this->logger->shouldLog(LogLevel::INFO));
		$this->assertFalse($this->logger->shouldLog(LogLevel::NOTICE));
		$this->assertFalse($this->logger->shouldLog(LogLevel::WARNING));
		$this->assertFalse($this->logger->shouldLog(LogLevel::ERROR));
		$this->assertFalse($this->logger->shouldLog(LogLevel::CRITICAL));
		$this->assertFalse($this->logger->shouldLog(LogLevel::ALERT));
		$this->assertTrue($this->logger->shouldLog(LogLevel::EMERGENCY));
		#endregion

		#region Exception handling
		$this->expectException(InvalidArgumentException::class);
		$this->logger->shouldLog('Something entirely wrong log level');
		#endregion

		#region Wrong case
		$this->logger->setLogLevel('DeBuG');
		$this->assertFalse($this->logger->shouldLog(LogLevel::DEBUG));
		$this->assertFalse($this->logger->shouldLog(LogLevel::INFO));
		$this->assertFalse($this->logger->shouldLog(LogLevel::NOTICE));
		$this->assertFalse($this->logger->shouldLog(LogLevel::WARNING));
		$this->assertFalse($this->logger->shouldLog(LogLevel::ERROR));
		$this->assertFalse($this->logger->shouldLog(LogLevel::CRITICAL));
		$this->assertFalse($this->logger->shouldLog(LogLevel::ALERT));
		$this->assertTrue($this->logger->shouldLog(LogLevel::EMERGENCY));
		#endregion
	}
}