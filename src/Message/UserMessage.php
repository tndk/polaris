<?php
declare(strict_types=1);

namespace Telenor\Message;

use Telenor\System\Utils;
use UnexpectedValueException;

class UserMessage extends APayloadMessage
{
	protected $lastname;
	protected $firstname;
	protected $fullname;
	protected $location;
	protected $globalEmployeeNumber;
	protected $globalEmployeeNumber1;
	protected $email;
	protected $job;
	protected $gsm;
	protected $company;
	protected $initials;
	protected $department;
	protected $manager;

	public const MAX_LENGTH_USERNAME = 40;
	public const MAX_LENGTH_LASTNAME = 50;
	public const MAX_LENGTH_FIRSTNAME = 50;
	public const MAX_LENGTH_EMAIL = 100;
	public const MAX_LENGTH_JOB = 100;
	public const MAX_LENGTH_FULLNAME = 151;
	public const MAX_LENGTH_PHONE = 40;
	public const MAX_LENGTH_MANAGER = 32;
	public const MAX_LENGTH_CITY = 100;
	public const MAX_LENGTH_DEPARTMENT = 100;
	public const MAX_LENGTH_COMPANY = 40;

	public function __construct(string $action)
	{
		parent::__construct('user', $action);
	}

	public function getLastname(): ?string
	{
		return $this->lastname;
	}

	public function getFirstname(): ?string
	{
		return $this->firstname;
	}

	public function getFullname(): ?string
	{
		return $this->fullname;
	}

	public function getLocation(): ?string
	{
		return $this->location;
	}

	public function getGlobalEmployeeNumber(): ?int
	{
		return $this->globalEmployeeNumber;
	}

	public function getGlobalEmployeeNumber1(): ?int
	{
		return $this->globalEmployeeNumber1;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function getJob(): ?string
	{
		return $this->job;
	}

	public function getGsm(): ?string
	{
		return $this->gsm;
	}

	public function getCompany(): ?string
	{
		return $this->company;
	}

	public function getInitials(): ?string
	{
		return $this->initials;
	}

	public function getDepartment(): ?string
	{
		return $this->department;
	}

	public function getManager(): ?string
	{
		return $this->manager;
	}

	public function setInitials(string $value): void
	{

		$this->initials = $this->cutValueLength($value, self::MAX_LENGTH_USERNAME);
	}

	public function setLastname(string $value): void
	{
		$this->lastname = $this->cutValueLength($value, self::MAX_LENGTH_LASTNAME);
	}

	public function setFirstname(string $value): void
	{
		$this->firstname = $this->cutValueLength($value, self::MAX_LENGTH_FIRSTNAME);
	}

	public function setFullname(string $value): void
	{

		$this->fullname = $this->cutValueLength($value, self::MAX_LENGTH_FULLNAME);
	}

	public function setCity($value): void
	{
		$this->location = $this->cutValueLength($value, self::MAX_LENGTH_CITY);
	}

	public function setGlobalEmployeeNumber(int $value): void
	{

		if ($value < 0) {
			throw new  UnexpectedValueException('Negative number found');
		}

		$this->globalEmployeeNumber = $value;
	}

	public function setGlobalEmployeeNumber1(int $value): void
	{

		if ($value < 0) {
			throw new  UnexpectedValueException('Negative number found');
		}

		$this->globalEmployeeNumber1 = $value;
	}

	public function setTitle($value): void
	{
		$this->job = $this->cutValueLength($value, self::MAX_LENGTH_JOB);
	}

	public function setPhone($value): void
	{
		$this->gsm = $this->cutValueLength($value, self::MAX_LENGTH_PHONE);
	}

	public function setEmail($value): void
	{

		$this->email = $this->cutValueLength($value, self::MAX_LENGTH_EMAIL);

	}

	public function setCompany($value): void
	{
		$this->company = $this->cutValueLength($value, self::MAX_LENGTH_COMPANY);
	}

	public function setDepartment($value): void
	{
		$this->department = $this->cutValueLength($value, self::MAX_LENGTH_DEPARTMENT);
	}

	public function setManager($value): void
	{
		$this->manager = $this->cutValueLength($value, self::MAX_LENGTH_MANAGER);
	}

	public function toArray(): array
	{
		return [
			'lokations_navn'          => $this->getLocation(),
			'Initialer'               => $this->getInitials(),
			'organisations_navn'      => $this->getDepartment(),
			'Efternavn'               => $this->getLastname(),
			'Fornavn'                 => $this->getFirstname(),
			'global_employee_number'  => $this->getGlobalEmployeeNumber(),
			'global_employee_number1' => $this->getGlobalEmployeeNumber1(),
			'e_mail'                  => $this->getEmail(),
			'Job'                     => $this->getJob(),
			'full_name'               => $this->getFullname(),
			'Gsm'                     => $this->getGsm(),
			'selskap_navn'            => $this->getCompany(),
			'manager'                 => $this->getManager(),
		];

	}

	protected function cutValueLength(?string $value, int $maxLength)
	{

		if (!Utils::isStringNullOrEmpty($value)) {
			return substr($value, 0, $maxLength);
		}
	}

}