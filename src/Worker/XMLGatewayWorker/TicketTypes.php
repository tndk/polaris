<?php
declare(strict_types=1);

namespace Telenor\Worker\XMLGatewayWorker;

class TicketTypes
{
	public const PROBLEM = 0;
	public const QUESTION = 1;
	public const ASSIGNMENT = 2;
	public const RFI_REQUEST_FOR_INFO = 3;
	public const SUPPORT = 4;
	public const DRIFT = 5;
	public const BESTILLING = 6;
	public const ITIL_INCIDENT = 7;
	public const ITIL_KNOWN_ERROR = 8;
	public const ITIL_PROBLEM = 9;
	public const ITIL_RFC = 10;
	public const ITIL_SERVICE_REQUEST = 11;
	public const UDVIDET_SERVICE_REQUEST = 12;
	public const ITIL_SR_FAST_TRACK = 13;
	public const _CLEAR_ = 14;
}