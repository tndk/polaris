<?php
declare(strict_types=1);

use Amp\Http\Server\Router;
use PHPUnit\Framework\TestCase;
use Telenor\Worker\AWorker\AWebWorker;

class AWebWorkerTest extends TestCase
{
	public function testGetRouter(): void
	{
		$worker = new class extends AWebWorker
		{
			public function __construct()
			{
			}

			protected function setupRoutes(Router $router): void
			{
			}
		};

		$this->assertSame($worker->getRouter(), $worker->getRouter());
	}
}
