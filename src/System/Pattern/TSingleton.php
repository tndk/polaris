<?php
declare(strict_types=1);

namespace Telenor\System\Pattern;

/**
 * Trait to be used by any class that needs to act as a singleton.
 * The trait implements the ISingleton interface.
 */
trait TSingleton
{
	/** @var TSingleton */
	private static $instance;

	/*
	 * Set the constructor private in order to ensure that no new instance
	 * will be created by other parties.
	 */
	private function __construct()
	{
	}

	/**
	 * Gets an instance of the class itself. A new (the only one) instance will
	 * be created if not already created (that will be the first time it is called)
	 *
	 * @return mixed An instance of the class the trait belongs to.
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			$clazz = static::class;
			self::$instance = new $clazz;
			self::$instance->initialize();
		}

		return self::$instance;
	}

	/*
	 * A reserved method to be overridden in child classes and will be executed
	 * right after the instance is created in the {@see getInstance()} method.
	 */
	protected function initialize(): void
	{
	}
}