<?php
declare(strict_types=1);

namespace Telenor\Model;

use Telenor\System\Exception\ValidationException;
use Telenor\System\Utils;
use function preg_match;

class WorkdayUser extends AUser
{

	public function setFullname(?string $value): void
	{

		if (Utils::isStringNullOrEmpty($value)) {
			throw new  ValidationException(__METHOD__, $value, 'Empty value found');
		}

		if (preg_match('/^[\d+]+$/', $value)) {
			throw new ValidationException(__METHOD__, $value, 'Contains illegal characters');
		}

		parent::setFullname($value);
	}

	public function setEmail(?string $value): void
	{
		if (Utils::isStringNullOrEmpty($value)) {
			throw new ValidationException(__METHOD__, $value, 'Null or Empty value found');
		}
		parent::setEmail($value);
	}

	public function setInitials(?string $value): void
	{
		if (Utils::isStringNullOrEmpty($value)) {
			throw new ValidationException(__METHOD__, 'null', 'Null or Empty value found');
		}

		parent::setInitials($value);
	}

}