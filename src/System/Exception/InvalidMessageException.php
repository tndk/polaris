<?php
declare(strict_types=1);
namespace Telenor\System\Exception;

/**
 * Exception to use when a message format is not correct.
 */
class InvalidMessageException extends PolarisException
{
}