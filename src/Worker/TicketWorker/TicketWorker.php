<?php
declare(strict_types=1);

namespace Telenor\Worker\TicketWorker;

use Amp\Http\Server\Router;
use Telenor\Service\IncidentService;
use Telenor\Worker\AWorker\AWebWorker;
use Telenor\Worker\TicketWorker\Controller\ChangeController;
use Telenor\Worker\TicketWorker\Controller\IncidentController;
use Telenor\Worker\TicketWorker\Controller\RemedyController;

class TicketWorker extends AWebWorker
{
	protected $ticketService;

	public function __construct(IncidentService $service)
	{
		$this->ticketService = $service;
	}

	protected function setupRoutes(Router $router): void
	{
		// REST routes for incidents
		$incidentsController = new IncidentController($this->ticketService);
		$this->addGetRouteHandler('/api/incidents[/]', $incidentsController, 'index');
		$this->addGetRouteHandler('/api/incidents/{id}', $incidentsController, 'show');
		$this->addPostRouteHandler('/api/incidents[/]', $incidentsController, 'insert');
		$this->addPutRouteHandler('/api/incidents/{id}', $incidentsController, 'update');
		$this->addPatchRouteHandler('/api/incidents/{id}', $incidentsController, 'update');
		$this->addDeleteRouteHandler('/api/incidents/{id}', $incidentsController, 'delete');

		// REST routes for changes
		$changeController = new ChangeController();
		$this->addGetRouteHandler('/api/changes[/]', $changeController, 'index');
		$this->addGetRouteHandler('/api/changes/{id}', $changeController, 'show');
		$this->addPostRouteHandler('/api/changes[/]', $changeController, 'insert');
		$this->addPutRouteHandler('/api/changes/{id}', $changeController, 'update');
		$this->addPatchRouteHandler('/api/changes/{id}', $changeController, 'update');
		$this->addDeleteRouteHandler('/api/changes/{id}', $changeController, 'delete');

		// REST-kinda routes for legacy Remedy JSON integration (Nokia uses this integration point)
		$remedyController = new RemedyController();
		$this->addPostRouteHandler('/remedyApi/helpDesk/createTicket/{ApiKey}', $remedyController, 'createTicket');
		$this->addPutRouteHandler('/remedyApi/helpDesk/updateTicket/{ApiKey}/{ticketID}', $remedyController,
			'updateTicket');
		$this->addPutRouteHandler('/remedyApi/helpDesk/putAttachments/{ApiKey}/{ticketID}', $remedyController,
			'addAttachments');
		$this->addGetRouteHandler('/remedyApi/helpDesk/getAttachments/{ApiKey}/{ticketID}', $remedyController,
			'getAttachments');
		$this->addPutRouteHandler('/remedyApi/bugBase/updateTicket/{ApiKey}/{bugBaseID}', $remedyController,
			'updateTask');
	}
}
