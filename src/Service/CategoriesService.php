<?php
declare(strict_types=1);

namespace Telenor\Service;

use Telenor\System\Config;

class CategoriesService
{
	protected $config;

	public function __construct()
	{
		$this->config = Config::yml();
	}

	/**
	 * This API endpoint is used by Skywalker to get the legacy Remedy group relations.
	 * This will eventually make API requests towards ServiceNow in the future, but for
	 * now the categories are the fixed ones used in Remedy.
	 */
	public function getCategories(): array
	{
		return $this->config['remedy'];
	}
}