<?php
declare(strict_types=1);

namespace Telenor\System\Error;

use Error;
use Throwable;

class PolarisError extends Error
{
	public function __construct(string $message, Throwable $previous = null)
	{
		parent::__construct($message, 0, $previous);
	}
}