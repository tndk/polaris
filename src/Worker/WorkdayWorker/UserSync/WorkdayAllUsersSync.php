<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker\UserSync;

use Telenor\Model\IUser;
use Telenor\System\Config;
use Telenor\System\Exception\InvalidDataException;
use function count;

class WorkdayAllUsersSync extends AWorkdayUsersSync
{
	protected $WorkdayDBMinCollected;

	public function __construct()
	{
		$this->setWorkdayDBMinCollected(Config::envString('WORKDAY_DB_MIN_COLLECTED'));
		$bindings = [];
		parent::__construct($bindings);
	}

	/**
	 * Get data from Workdak
	 *
	 * @return IUser[]
	 * @throws InvalidDataException When the number of rows are below the minimum amount required
	 */
	public function getUsers(): array
	{
		$users = $this->getAllUsers();
		if (count($users) < $this->getWorkdayDBMinCollected()) {
			{
				$this->logError('WorkdayAllUserSync returned unexpected number of rows', [
					'Rows_returned'     => count($users),
					'Min_Rows_expected' => $this->getWorkdayDBMinCollected(),
				]);

				throw new InvalidDataException('Incorrect numbeer og worday users received');
			}
		}
		return $users;
	}

	public function getWorkdayDBMinCollected(): string
	{
		return $this->WorkdayDBMinCollected;
	}

	public function setWorkdayDBMinCollected($WorkdayDBMinCollected): void
	{
		$this->WorkdayDBMinCollected = $WorkdayDBMinCollected;
	}

}