<?php
declare(strict_types=1);

namespace Telenor\Message;

use Telenor\System\Exception\InvalidDataException;
use Telenor\System\Exception\ValidationException;
use function in_array;

class IncidentTicketMessage extends ATicketMessage
{
	public const TYPE_INCIDENT = 'incident';
	public const TYPE_MAJORINCIDENT = 'majorincident';

	public const STATUS_NEW = 'New';
	public const STATUS_IN_PROGRESS = 'In Progress';
	public const STATUS_ON_HOLD = 'On Hold';
	public const STATUS_RESOLVED = 'Resolved';
	public const STATUS_CLOSED = 'Closed';
	public const STATUS_CANCELED = 'Canceled';

	public const RESOLUTION_CODE_RESOLVED_WORKAROUND = 'Resolved (Work Around)';
	public const RESOLUTION_CODE_RESOLVED_PERMANENTLY = 'Resolved (Permanently)';
	public const RESOLUTION_CODE_NOT_SOLVED = 'Not Solved';
	public const RESOLUTION_CODE_RESOLVED_BY_CALLER = 'Closed/Resolved By Caller';
	public const RESOLUTION_CODE_MISSING_INFORMATION = 'Missing Information';

	/** @var string|null */
	protected $resolutionCode;

	/**
	 * IncidentTicketMessage constructor.
	 *
	 * @param string $type
	 * @param string $action
	 * @throws ValidationException
	 */
	public function __construct(string $type, string $action)
	{
		if (!in_array($type, [
			self::TYPE_INCIDENT,
			self::TYPE_MAJORINCIDENT,
		], true))
			throw new ValidationException('type', $type, '', 'Invalid value');

		parent::__construct($type, $action);
	}

	/**
	 * @return string|null
	 */
	public function getResolutionCode(): ?string
	{
		return $this->resolutionCode;
	}

	/**
	 * @param string|null $resolutionCode
	 * @throws ValidationException
	 */
	public function setResolutionCode(?string $resolutionCode): void
	{
		if (!in_array($resolutionCode, [
			self::RESOLUTION_CODE_RESOLVED_WORKAROUND,
			self::RESOLUTION_CODE_RESOLVED_PERMANENTLY,
			self:: RESOLUTION_CODE_NOT_SOLVED,
			self::RESOLUTION_CODE_RESOLVED_BY_CALLER,
			self::RESOLUTION_CODE_MISSING_INFORMATION,
		], true))
			throw new ValidationException('ResolutionCode', $resolutionCode);

		$this->resolutionCode = $resolutionCode;
	}

	protected function getValidStates(): array
	{
		return [
			self::STATUS_NEW,
			self::STATUS_IN_PROGRESS,
			self::STATUS_ON_HOLD,
			self::STATUS_RESOLVED,
			self::STATUS_CLOSED,
			self::STATUS_CANCELED,
		];
	}

	/**
	 * @return array
	 * @throws InvalidDataException
	 */
	public function toArray(): array
	{
		return array_merge(parent::toArray(), [
			'resolution_code' => $this->getResolutionCode(),
		]);
	}
}