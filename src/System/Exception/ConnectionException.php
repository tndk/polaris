<?php
declare(strict_types=1);
namespace Telenor\System\Exception;

/**
 * Exception to use when a connection problem occurs.
 */
class ConnectionException extends PolarisException
{
}