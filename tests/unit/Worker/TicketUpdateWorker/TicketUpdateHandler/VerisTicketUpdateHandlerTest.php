<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler\VerisTicketUpdateHandler;

class VerisTicketUpdateHandlerTest extends TestCase
{
	protected $handler;

	protected function setUp(): void
	{
		putenv('VERIS_HOST=127.0.0.1:3333');

		$this->handler = new class extends VerisTicketUpdateHandler
		{
			// We convert protected method to a public one
			public function generateRequestTime(): string
			{
				return parent::generateRequestTime();
			}

			// We convert protected method to a public one
			public function generateTransactionId(): string
			{
				return parent::generateTransactionId();
			}

			// We convert protected method to a public one
			public function escape(string $data): string
			{
				return parent::escape($data);
			}
		};
	}

	public function testRequestTime(): void
	{
		static::assertRegExp('#^20[0-9]{12}$#', $this->handler->generateRequestTime());
	}

	public function testGenerateTransactionId(): void
	{
		static::assertRegExp('#^[0-9]{15}$#', $this->handler->generateTransactionId());
	}

	public function testEscape(): void
	{
		static::assertSame('Hi &lt;there&gt; &amp; &lt;you/&gt;', $this->handler->escape('Hi <there> & <you/>'));
	}
}