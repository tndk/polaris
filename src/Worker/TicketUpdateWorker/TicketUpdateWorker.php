<?php
declare(strict_types=1);

namespace Telenor\Worker\TicketUpdateWorker;

use Telenor\Message\APayloadMessage;
use Telenor\Message\IMessage;
use Telenor\Message\TicketUpdateMessage;
use Telenor\System\Exception\UnhandledMessageException;
use Telenor\System\GlobalStorage;
use Telenor\System\Utils;
use Telenor\Worker\AWorker\AMessageQueueWorker;
use Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler\ITicketUpdateHandler;
use Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler\NokiaTicketUpdateHandler;
use Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler\SkywalkerTicketUpdateHandler;
use Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler\VerisTicketUpdateHandler;
use Throwable;

class TicketUpdateWorker extends AMessageQueueWorker
{
	/** @var ITicketUpdateHandler[] */
	protected $ticketUpdateHandlers = [];

	public function __construct()
	{
		$this->ticketUpdateHandlers = [
			'veris'     => new VerisTicketUpdateHandler(),
			'skywalker' => new SkywalkerTicketUpdateHandler(),
			'nokia'     => new NokiaTicketUpdateHandler(),
		];
	}

	public function onMessageConsumed(IMessage $message, array $metaData = null): bool
	{
		try {
			$this->logDebug('Received a new message from the queue, parsed message type: ' . Utils::getShortClassName($message));

			$additionalLogs = [];
			if ($message instanceof APayloadMessage)
				$additionalLogs['transaction_id'] = $message->getTransactionId();

			$this->logInfo('Handling message request_id: ' . GlobalStorage::getUUID(), $additionalLogs);

			if (!$message instanceof TicketUpdateMessage) {
				$this->logError('Did not receive a correct TicketUpdate payload message', [
					'classname' => Utils::getShortClassName($message),
				]);
				return false;
			}

			if (!isset($this->ticketUpdateHandlers[$message->getSource()])) {
				$this->logError('Rejecting to handle message because no one can handle ticket source: ' . $message->getSource());
				return true;
			}

			$this->logInfo('Processing ticket update with source: ' . $message->getSource(), [
				'source'  => $message->getSource(),
				'message' => $message,
			]);

			$success = $this->ticketUpdateHandlers[$message->getSource()]->handleMessage($message);
			if ($success) {
				$this->logInfo('Successfully processed message', ['message' => $message]);
			} else {
				$this->logWarning('Failed to process message successfully', ['message' => $message]);
			}
			return $success;

		} catch (UnhandledMessageException $e) {
			$this->logNotice('Failed to process request', [$e]);
			return false;
		} catch (Throwable $t) {
			$this->logError('Received unexpected throwable', [$t]);
			return true;
		}
	}
}