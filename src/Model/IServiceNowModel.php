<?php
declare(strict_types=1);

namespace Telenor\Model;

interface IServiceNowModel
{
	public function getEndpoint(): string;

	public function getFields(): array;

	public function getFilters(string $id): array;
}