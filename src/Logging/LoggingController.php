<?php
declare(strict_types=1);
namespace Telenor\Logging;

use Exception;
use InvalidArgumentException;
use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;
use Telenor\Logging\Driver\ILogDriver;
use Telenor\Logging\Driver\MessageQueueDriver;
use Telenor\Logging\Driver\StdoutColorDriver;
use Telenor\System\Config;
use Telenor\System\Error\BadConfigurationError;
use Telenor\System\Pattern\TSingleton;
use Telenor\System\Utils;
use function class_exists;
use function class_implements;
use function explode;
use function in_array;
use function strtolower;

/** @method static LoggingController getInstance() */
class LoggingController extends AbstractLogger
{
	use TSingleton;

	/** @var ILogDriver[] */
	protected $drivers = [];

	/** @var ILogDriver */
	protected $fallbackDriver;

	/** @var string $logLevel */
	protected $logLevel;

	protected $logLevelMap = [
		LogLevel::EMERGENCY => 0,
		LogLevel::ALERT     => 1,
		LogLevel::CRITICAL  => 2,
		LogLevel::ERROR     => 3,
		LogLevel::WARNING   => 4,
		LogLevel::NOTICE    => 5,
		LogLevel::INFO      => 6,
		LogLevel::DEBUG     => 7,
	];

	/**
	 * @throws BadConfigurationError
	 */
	protected function initialize(): void
	{
		$this->setLogLevel(Config::envString('LOG_LEVEL', LogLevel::INFO));

		$this->setFallbackDriver($this->createDriverInstance(Config::envString('FALLBACK_LOG_DRIVER',
			StdoutColorDriver::class)));

		$configuredDrivers = explode(';', Config::envString('LOG_DRIVER', MessageQueueDriver::class));
		foreach ($configuredDrivers as $driver) {
			$this->addDriver($this->createDriverInstance($driver));
		}
	}

	/**
	 * @param string $driverName
	 * @return ILogDriver
	 * @throws BadConfigurationError
	 */
	protected function createDriverInstance(string $driverName): ILogDriver
	{
		if (!class_exists($driverName))
			throw new BadConfigurationError(__METHOD__, "Log driver class not found: $driverName");

		if (!in_array(ILogDriver::class, class_implements($driverName), true))
			throw new BadConfigurationError(__METHOD__,
				'Log driver class does not implement the ' . ILogDriver::class . ' interface');

		return new $driverName;
	}

	/**
	 * @return ILogDriver[]
	 */
	public function getDrivers(): array
	{
		return $this->drivers;
	}

	public function addDriver(ILogDriver $driver): void
	{
		$this->drivers[] = $driver;
	}

	/**
	 * @return ILogDriver
	 */
	public function getFallbackDriver(): ILogDriver
	{
		return $this->fallbackDriver;
	}

	/**
	 * @param ILogDriver $fallbackDriver
	 */
	public function setFallbackDriver(ILogDriver $fallbackDriver): void
	{
		$this->fallbackDriver = $fallbackDriver;
	}

	public function log($level, $message, array $context = []): void
	{
		$level = strtolower($level);
		if (!$this->shouldLog($level))
			return;

		foreach (self::getInstance()->getDrivers() as $driver) {
			try {
				$driver->log($level, $message, $context);
			} catch (Exception $e) {
				$driverName = Utils::getShortClassName($driver);
				$this->getFallbackDriver()->log(LogLevel::EMERGENCY,
					"Failed to log message via '$driverName' driver: [$level] $message.");

				$errorMessage = $e->getMessage();
				$previousError = $e->getPrevious();
				if ($previousError !== null)
					$errorMessage .= '. Details (' . Utils::getShortClassName($previousError) . '): ' . $previousError->getMessage();

				$this->getFallbackDriver()->log(LogLevel::EMERGENCY, "Exception: $errorMessage");
			}
		}
	}

	public function getLogLevel(): string
	{
		return $this->logLevel;
	}

	public function setLogLevel(string $level): void
	{
		$this->logLevel = strtolower($level);
	}

	public function shouldLog(string $level): bool
	{
		if (!array_key_exists($level, $this->logLevelMap))
			throw new InvalidArgumentException("Invalid log level given: $level");

		return $this->logLevelMap[$this->logLevel] >= $this->logLevelMap[$level];
	}
}
