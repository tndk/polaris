<?php
declare(strict_types=1);

namespace Telenor\LibraryFixes\php_smpp;

use ArrayIterator;
use SocketTransportException;
use function call_user_func;
use function floor;
use function socket_close;
use function socket_connect;
use function socket_create;
use function socket_last_error;
use function socket_set_option;
use function socket_strerror;

/**
 * HEIP: This class extends the functionality of the original php-smpp SocketTransport class.
 * I've done this in order to fix a bug in the library. Whenever we set forceIpv4=true, an
 * error is thrown every time we call the SocketTransport::open() method, because of an undefined
 * variable $socket6.
 * I have added a single linke to check if the variable exists, before it is used.
 *
 * Every line with an "HEIP" comment shows any modifications in this from.
 *
 * @package Telenor\LibraryFixes\php_smpp
 */
class SocketTransport extends \SocketTransport
{
	private function millisecToSolArray($millisec)
	{
		$usec = $millisec * 1000;
		return ['sec' => floor($usec / 1000000), 'usec' => $usec % 1000000];
	}

	public function open()
	{
		if (!self::$forceIpv4) {
			$socket6 = @socket_create(AF_INET6, SOCK_STREAM, SOL_TCP);
			if ($socket6 == false)
				throw new SocketTransportException('Could not create socket; ' . socket_strerror(socket_last_error()), socket_last_error());
			socket_set_option($socket6, SOL_SOCKET, SO_SNDTIMEO, $this->millisecToSolArray(self::$defaultSendTimeout));
			socket_set_option($socket6, SOL_SOCKET, SO_RCVTIMEO, $this->millisecToSolArray(self::$defaultRecvTimeout));
		}
		if (!self::$forceIpv6) {
			$socket4 = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
			if ($socket4 == false)
				throw new SocketTransportException('Could not create socket; ' . socket_strerror(socket_last_error()), socket_last_error());
			socket_set_option($socket4, SOL_SOCKET, SO_SNDTIMEO, $this->millisecToSolArray(self::$defaultSendTimeout));
			socket_set_option($socket4, SOL_SOCKET, SO_RCVTIMEO, $this->millisecToSolArray(self::$defaultRecvTimeout));
		}
		$it = new ArrayIterator($this->hosts);
		while ($it->valid()) {
			list($hostname, $port, $ip6s, $ip4s) = $it->current();
			if (!self::$forceIpv4 && !empty($ip6s)) { // Attempt IPv6s first
				foreach ($ip6s as $ip) {
					if ($this->debug)
						call_user_func($this->debugHandler, "Connecting to $ip:$port...");
					$r = @socket_connect($socket6, $ip, $port);
					if ($r) {
						if ($this->debug)
							call_user_func($this->debugHandler, "Connected to $ip:$port!");
						@socket_close($socket4);
						$this->socket = $socket6;
						return;
					} else if ($this->debug) {
						call_user_func($this->debugHandler, "Socket connect to $ip:$port failed; " . socket_strerror(socket_last_error()));
					}
				}
			}
			if (!self::$forceIpv6 && !empty($ip4s)) {
				foreach ($ip4s as $ip) {
					if ($this->debug)
						call_user_func($this->debugHandler, "Connecting to $ip:$port...");
					$r = @socket_connect($socket4, $ip, $port);
					if ($r) {
						if ($this->debug)
							call_user_func($this->debugHandler, "Connected to $ip:$port!");
						if (isset($socket6)) // HEIP: Added this line
							@socket_close($socket6);
						$this->socket = $socket4;
						return;
					} else if ($this->debug) {
						call_user_func($this->debugHandler, "Socket connect to $ip:$port failed; " . socket_strerror(socket_last_error()));
					}
				}
			}
			$it->next();
		}
		throw new SocketTransportException('Could not connect to any of the specified hosts');
	}
}