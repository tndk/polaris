<?php
declare(strict_types=1);

namespace Telenor\System;

use GuzzleHttp\Client;
use Psr\Log\LogLevel;
use Telenor\Logging\LoggingController;
use Telenor\Logging\TLog;

class HttpClient extends Client
{
	use TLog;

	public function __construct(array $config = [])
	{
		parent::__construct(array_merge($config, [
			'http_errors' => false,
			'debug'       => LoggingController::getInstance()->getLogLevel() === LogLevel::DEBUG,
		]));
	}

	public function request($method, $uri = '', array $options = [])
	{
		$fullUri = $this->getConfig('base_uri') . $uri;

		$this->logInfo("Sending request to $fullUri", [
			'url'     => $fullUri,
			'method'  => $method,
			'headers' => array_merge($this->getConfig('headers') ?? [], $options['headers'] ?? []),
			'body'    => $options['body'] ?? '',
		]);
		
		$response = parent::request($method, $uri, $options);

		$this->logInfo("Got response from $fullUri", [
			'url'          => $fullUri,
			'statuscode'   => $response->getStatusCode(),
			'statusphrase' => $response->getReasonPhrase(),
			'headers'      => $response->getHeaders(),
			'body'         => $response->getBody(),
		]);

		return $response;
	}
}