<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use Telenor\System\Exception\InvalidDataException;
use Telenor\System\Exception\InvalidRequestException;
use Telenor\System\Exception\ValidationException;
use Telenor\Worker\TicketWorker\Controller\RemedyController;

class RemedyControllerTest extends TestCase
{
	/** @var RemedyController */
	protected $controller;

	/**
	 * @param null $name
	 * @param array $data
	 * @param string $dataName
	 */
	public function __construct($name = null, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);
		$this->controller = new RemedyController();
	}

	public function _ymlDataProviderCreateTicket(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_CreateTicket.yml', __DIR__, __CLASS__));
	}

	public function _ymlDataProviderUpdateTicket(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_UpdateTicket.yml', __DIR__, __CLASS__));
	}

	public function _ymlDataProviderAddAttachment(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_AddAttachment.yml', __DIR__, __CLASS__));
	}

	public function _ymlDataProviderMyWFM(): array
	{
		return Yaml::parseFile(sprintf('%s/%s_MyWFM.yml', __DIR__, __CLASS__));
	}

	/**
	 * @dataProvider _ymlDataProviderCreateTicket
	 * @param $input
	 * @param $output
	 * @throws InvalidRequestException
	 * @throws InvalidDataException
	 */
	public function testCreateTicket($input, $output): void
	{
		$msg = $this->controller->buildCreateTicketMessage($input);

		# Correlation id must not be null - set it empty here
		$msg->setCorrelationId('');

		# Remove the dynamically created values (changes for every test)
		$msgAttributes = $msg->getAttributes();
		unset($msgAttributes['request_id'], $msgAttributes['transaction_id'], $msgAttributes['correlation_id'], $msgAttributes['timestamp']);

		# TODO: As putenv("WORKER_CLASS") cannot be set from test to test, this will fail if other tests have set the env
		# As a result, we should refactor a bit, so that WORKER_CLASS is given as input argument to App.php, instead of
		# reading it from an environment variable. We remove it here in these test cases until refactor is complete
		unset($msgAttributes['worker']);

		static::assertSame($output, $msgAttributes);
	}

	/**
	 * @testdox Update Ticket
	 * @dataProvider _ymlDataProviderUpdateTicket
	 * @param $ticketID
	 * @param $input
	 * @param $output
	 * @throws InvalidDataException
	 * @throws InvalidRequestException
	 */
	public function testUpdateTicket($ticketID, $input, $output): void
	{
		$msg = $this->controller->buildUpdateTicketMessage($input, $ticketID);

		# Remove the dynamically created values (changes for every test)
		$msgAttributes = $msg->getAttributes();
		unset($msgAttributes['request_id'], $msgAttributes['transaction_id'], $msgAttributes['timestamp']);

		# TODO: As putenv("WORKER_CLASS") cannot be set from test to test, this will fail if other tests have set the env
		# As a result, we should refactor a bit, so that WORKER_CLASS is given as input argument to App.php, instead of
		# reading it from an environment variable. We remove it here in these test cases until refactor is complete
		unset($msgAttributes['worker']);

		static::assertSame($output, $msgAttributes);
	}

	/**
	 * @dataProvider _ymlDataProviderAddAttachment
	 * @param $ticketID
	 * @param $input
	 * @param $output
	 * @throws InvalidDataException
	 * @throws InvalidRequestException
	 */
	public function testAddAttachment($ticketID, $input, $output): void
	{
		$msg = $this->controller->buildUpdateTicketMessageWithAttachments($input, $ticketID);

		# Remove the dynamically created values (changes for every test)
		$msgAttributes = $msg->getAttributes();
		unset($msgAttributes['request_id'], $msgAttributes['transaction_id'], $msgAttributes['timestamp']);

		# TODO: As putenv("WORKER_CLASS") cannot be set from test to test, this will fail if other tests have set the env
		# As a result, we should refactor a bit, so that WORKER_CLASS is given as input argument to App.php, instead of
		# reading it from an environment variable. We remove it here in these test cases until refactor is complete
		unset($msgAttributes['worker']);

		static::assertSame($output, $msgAttributes);
	}

	/**
	 * @testdox MyWFM Updates
	 * @dataProvider _ymlDataProviderMyWFM
	 * @param $ticketID
	 * @param $input
	 * @param $output
	 * @throws InvalidDataException
	 * @throws InvalidRequestException
	 * @throws ValidationException
	 */
	public function testMyWFMUpdates($ticketID, $input, $output): void
	{
		$msg = $this->controller->buildUpdateTaskTicketMessage($input, $ticketID);

		# Remove the dynamically created values (changes for every test)
		$msgAttributes = $msg->getAttributes();
		unset($msgAttributes['request_id'], $msgAttributes['transaction_id'], $msgAttributes['timestamp']);

		# TODO: As putenv("WORKER_CLASS") cannot be set from test to test, this will fail if other tests have set the env
		# As a result, we should refactor a bit, so that WORKER_CLASS is given as input argument to App.php, instead of
		# reading it from an environment variable. We remove it here in these test cases until refactor is complete
		unset($msgAttributes['worker']);

		static::assertSame($output, $msgAttributes);
	}

	public function testMapTicketPriorities(): void
	{
		static::assertSame([1, 1], $this->controller->mapTicketPriorities(1, 1));
		static::assertSame([1, 2], $this->controller->mapTicketPriorities(1, 2));
		static::assertSame([1, 3], $this->controller->mapTicketPriorities(1, 3));
		static::assertSame([1, 3], $this->controller->mapTicketPriorities(1, 4));
		static::assertSame([2, 1], $this->controller->mapTicketPriorities(2, 1));
		static::assertSame([2, 2], $this->controller->mapTicketPriorities(2, 2));
		static::assertSame([2, 2], $this->controller->mapTicketPriorities(2, 3));
		static::assertSame([2, 3], $this->controller->mapTicketPriorities(2, 4));
		static::assertSame([2, 2], $this->controller->mapTicketPriorities(3, 1));
		static::assertSame([2, 2], $this->controller->mapTicketPriorities(3, 2));
		static::assertSame([2, 3], $this->controller->mapTicketPriorities(3, 3));
		static::assertSame([3, 2], $this->controller->mapTicketPriorities(3, 4));
		static::assertSame([2, 3], $this->controller->mapTicketPriorities(4, 1));
		static::assertSame([3, 2], $this->controller->mapTicketPriorities(4, 2));
		static::assertSame([3, 2], $this->controller->mapTicketPriorities(4, 3));
		static::assertSame([3, 3], $this->controller->mapTicketPriorities(4, 4));
	}

	public function testMapTaskPriorities(): void
	{
		static::assertSame([1, 1], $this->controller->mapTaskPriorities('Telenor P0'));
		static::assertSame([1, 2], $this->controller->mapTaskPriorities('Telenor P1'));
		static::assertSame([2, 1], $this->controller->mapTaskPriorities('Telenor P2'));
		static::assertSame([2, 3], $this->controller->mapTaskPriorities('Telenor P3'));
		static::assertSame([3, 2], $this->controller->mapTaskPriorities('Telenor P4'));
		static::assertSame([3, 3], $this->controller->mapTaskPriorities('Telenor P5'));
		static::assertSame([3, 3], $this->controller->mapTaskPriorities('Telenor P6'));
	}
}