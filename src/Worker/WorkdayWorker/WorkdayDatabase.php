<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker;

use ErrorException;
use PDO;
use Telenor\Logging\TLog;
use Telenor\Model\WorkdayUser;
use Telenor\System\Exception\ValidationException;
use function preg_replace;

class WorkdayDatabase
{
	private $servername;
	private $username;
	private $password;
	private $dbname;
	protected $connection;
	use TLog;

	//Perhaps not the best way to do this

	public function getAllUsers(): array
	{

		$yesterday = (new \DateTimeImmutable('yesterday'))->format('Y-m-d H:i:s');
		$fallBackManager = self::headOfHR($yesterday);

		$sql = "select ifnull(e.lokations_navn,'') lokations_navn, 
				ifnull(e.organisations_navn,'') organisations_navn, 
				ifnull(e.efternavn,'') efternavn, 
				ifnull(e.fornavn,'') fornavn,
				ifnull(e.global_employee_number,'') global_employee_number,
				ifnull(e.e_mail,'') e_mail, 
				ifnull(e.job,'') job,  
				ifnull(e.efternavn,'') efternavn, 
		 		ifnull(e.selskap_navn,'') selskap_navn, 
		 		ifnull(e.gsm,'') gsm, 
		 		ifnull(e.initialer,'') initialer, 
		 		ifnull(e.organisations_id,'') organisations_id, 
		 		ifnull(m.initialer manager,'') manager
		 		from workday.e_son_personer_workday e
		 		left join
				(select initialer , GLOBAL_EMPLOYEE_NUMBER from workday.e_son_personer_workday 
				where 
				((FRATRAD_DATO > now()) or (FRATRAD_DATO is null))
				and proc_time = '$yesterday'
				and aktiv = 'Y'
				) m on m.GLOBAL_EMPLOYEE_NUMBER = e.CHEF_LONNR
				where e.aktiv = 'Y'
				##and lower(e.initialer) in ('TPN','CLN','HEIP' )
				#and datediff(curdate(), proc_time) =2
				and e.proc_time = '$yesterday'
				and ((e.FRATRAD_DATO > now()) or (e.FRATRAD_DATO is null))
";

		$stmt = $this->getConnection()->query($sql);
		$count = $stmt->rowCount();

		if ($count < 100) {
			throw new ErrorException('Too few users found in Workday DB');
		}

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			try {
				$orgname = $row["organisations_navn"];
				$orgname = preg_replace("/^dk(.*)/i", "$1", $orgname);
				$orgname = preg_replace("/^-|-\s+(.*)/i", "$1", $orgname);
				$orgname = preg_replace("/^\s+(.*)/i", "$1", $orgname);
				$orgname = preg_replace("/ \(.*/i", "", $orgname, 1);

				$wd_user = new WorkdayUser();
				$wd_user->setLastname($row['efternavn']);
				$wd_user->setFirstname($row['fornavn']);
				$wd_user->setFullname($row['fornavn'] . ' ' . $row['efternavn']);
				$wd_user->setLocation($row['lokations_navn']);
				$wd_user->setGlobalEmployeeNumber((int)$row['global_employee_number']);
				$wd_user->setGlobalEmployeeNumber1((int)$row['global_employee_number']);
				$wd_user->setEmail($row['e_mail']);
				$wd_user->setJob($row['job']);
				$wd_user->setGsm($row['gsm']);
				$wd_user->setCompany($row['selskap_navn']);
				$wd_user->setInitials($row['initialer']);
				$wd_user->setOrganization($orgname);
				$wd_user->setManager($row['manager']);
				if ($wd_user->getManager() === null) {
					$wd_user->setManager($fallBackManager);
				}
				$wd_users[$row['initialer']] = $wd_user;
			} catch (ValidationException $e) {
				$this->logError("Error validating Workday user " . $row['initialer'], [
					'property' => $e->getProperty(),
					'value' => $e->getValue(),
					'dbrow' => $row,
					'exception' => $e->getMessage(),
				]);
			}
		}

		$this->closeConnection();
		return $wd_users;
	}

}