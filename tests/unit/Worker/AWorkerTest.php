<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\Worker\AWorker\AWorker;
use Telenor\Worker\IWorker;

final class AWorkerTest extends TestCase
{
	protected $worker;

	protected function setUp(): void
	{
		$this->worker = new class extends AWorker
		{
			public function __construct()
			{
			}

			public function start(): void
			{
			}

			public function stop(): void
			{
			}
		};
	}

	public function testInstance(): void
	{
		$this->assertInstanceOf(IWorker::class, $this->worker);
		$this->assertInstanceOf(AWorker::class, $this->worker);
	}
}