<?php
declare(strict_types=1);

namespace Telenor\Worker\QueryWorker;

use Amp\Http\Server\Router;
use Telenor\Worker\AWorker\AWebWorker;

class QueryWorker extends AWebWorker
{
	protected $groupsController;
	protected $categoriesController;
	protected $itemsController;

	public function __construct(GroupsController $groupsController, CategoriesController $categoriesController, ItemsController $itemsController)
	{
		$this->groupsController = $groupsController;
		$this->categoriesController = $categoriesController;
		$this->itemsController = $itemsController;
	}

	protected function setupRoutes(Router $router): void
	{
		$this->addGetRouteHandler('/api/query/groups[/]', $this->groupsController, 'index');

		// Some legacy API endpoints used by Skywalker.. TODO: This could probably be refactored in the future
		$this->addGetRouteHandler('/api/query/categories[/]', $this->categoriesController, 'index');
		$this->addGetRouteHandler('/api/query/items/changes/{item}', $this->itemsController, 'changes');
		$this->addGetRouteHandler('/api/query/items/history/{item}', $this->itemsController, 'history');
	}
}