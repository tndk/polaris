<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\System\Error\BadConfigurationError;
use Telenor\System\Utils;

final class UtilsTest extends TestCase
{
	public function testIsStringNullOrEmpty(): void
	{
		$this->assertTrue(Utils::isStringNullOrEmpty(null));
		$this->assertTrue(Utils::isStringNullOrEmpty(''));
		$this->assertFalse(Utils::isStringNullOrEmpty('hai'));
	}

	/** @testdox Generate UUID */
	public function testGenerateUUID(): void
	{
		$this->assertSame(36, strlen(Utils::generateUUID()));
		$this->assertRegExp('#[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}#', Utils::generateUUID());
	}

	public function testGetShortClassName(): void
	{
		$this->assertSame('Utils', Utils::getShortClassName(Utils::class));
		$this->assertSame('BadConfigurationError', Utils::getShortClassName(new BadConfigurationError('', '')));
		$this->assertSame('(UNKNOWN CLASS)', Utils::getShortClassName('invalidClassName'));
	}
}