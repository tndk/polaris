<?php
declare(strict_types=1);
namespace Telenor\Worker\AWorker;

use Amp\Http\Server\Middleware;
use Amp\Http\Server\Options;
use Amp\Http\Server\Request;
use Amp\Http\Server\RequestHandler;
use Amp\Http\Server\Response;
use Amp\Http\Server\Router;
use Amp\Http\Server\Server;
use Amp\Http\Status;
use Amp\Loop;
use Amp\Promise;
use Amp\Socket;
use Telenor\Logging\LoggingController;
use Telenor\Logging\TLog;
use Telenor\System\Config;
use Telenor\System\GlobalStorage;
use Telenor\System\Utils;
use Throwable;
use function Amp\call;

/**
 * A common class for all workers that act as a web server. This class starts
 * a PHP web server and forward requests to a child class.
 */
abstract class AWebWorker extends AWorker
{
	/** @var Server */
	protected $server;

	/** @var Router */
	protected $router;

	/**
	 * Start the web worker. This initializes a web server which
	 * will be ready for any incoming web requests.
	 */
	public function start()
	{
		parent::start();

		Loop::defer(function () {

			// Define the listen interfaces and ports.
			$sockets = [
				Socket\listen(Config::envString('WEB_LISTEN_INTERFACE', '0.0.0.0:80')),
			];

			// Add a middleware to the route stack. This will append a transaction id on all
			// incoming requests. This UUID will be available in all routes defined for the worker.
			$this->getRouter()->stack(new class implements Middleware
			{
				use TLog;

				public function handleRequest(Request $request, RequestHandler $requestHandler): Promise
				{
					return call(function (Request $request, RequestHandler $requestHandler) {
						GlobalStorage::setUUID(Utils::generateUUID());

						$this->logInfo('Incoming request with uuid: ' . GlobalStorage::getUUID(), [
							'request' => [
								'body'    => yield $request->getBody()->buffer(),
								'headers' => $request->getHeaders(),
								'method'  => $request->getMethod(),
								'cookies' => $request->getCookies(),
								'path'    => $request->getUri()->getPath(),
								'query'   => $request->getUri()->getQuery(),
							],
							'client'  => [
								'clientId'   => $request->getClient()->getId(),
								'remoteAddr' => $request->getClient()->getRemoteAddress(),
								'remotePort' => $request->getClient()->getRemotePort(),
								'status'     => $request->getClient()->getStatus(),
							],
						]);
						try {
							return yield $requestHandler->handleRequest($request);
						} catch (Throwable $t) {

							$this->logError('Failed to handle web request', [$t]);

							return call(static function () {
								return new Response(Status::INTERNAL_SERVER_ERROR, ['Content-Type' => 'text/plain'],
									"Internal Server Error\r\n");
							});
						}

					}, $request, $requestHandler);
				}
			});

			// Set fallback request handler. This happens if none of the registered routes match.
			$this->getRouter()->setFallback(new RequestHandler\CallableRequestHandler(function (Request $request) {
				$this->logNotice('Incoming unknown request. No matching routes.', [
					'request' => [
						'body'    => yield $request->getBody()->buffer(),
						'headers' => $request->getHeaders(),
						'method'  => $request->getMethod(),
						'cookies' => $request->getCookies(),
						'path'    => $request->getUri()->getPath(),
						'query'   => $request->getUri()->getQuery(),
					],
					'client'  => [
						'clientId'   => $request->getClient()->getId(),
						'remoteAddr' => $request->getClient()->getRemoteAddress(),
						'remotePort' => $request->getClient()->getRemotePort(),
						'status'     => $request->getClient()->getStatus(),
					],
				]);

				return new Response(Status::NOT_FOUND, ['Content-Type' => 'text/plain'], "Invalid API endpoint.");
			}));

			// Now force the worker to confiugre the routes
			$this->setupRoutes($this->getRouter());

			$options = new Options();
			$options = $options->withBodySizeLimit(Config::envInt('WEBSERVER_MAX_BODY_SIZE', 1048576)); // Default: 1MB

			$this->logInfo('Configured max request body size: ' . $options->getBodySizeLimit() . ' bytes');

			// Define a server instance
			$this->server = new Server($sockets, $this->getRouter(), LoggingController::getInstance(), $options);

			yield $this->server->start();
		});
	}

	/**
	 * Stops the web worker. The application (event loop) is
	 * cancelled and the worker will be shutdown gracefully.
	 */
	public function stop(): void
	{
		if ($this->server !== null)
			$this->server->stop();

		parent::stop();
	}

	/**
	 * Retrives the single router instance to be used. If an instance
	 * has not been created yet, it will be created first.
	 *
	 * @return Router The single router instance to use.
	 */
	public function getRouter(): Router
	{
		if ($this->router === null)
			$this->router = new Router();
		return $this->router;
	}

	#region Route handler
	protected function addRouteHandler(string $method, string $uri, $controller, string $controllerMethod): void
	{
		$this->getRouter()->addRoute($method, $uri, new class ($controller, $controllerMethod) implements RequestHandler {
			protected $controller;
			protected $controllerMethod;

			public function __construct($controller, string $controllerMethod)
			{
				$this->controller = $controller;
				$this->controllerMethod = $controllerMethod;
			}

			public function handleRequest(Request $request): Promise
			{
				return call([$this->controller, $this->controllerMethod], $request);
			}
		});
	}

	protected function addGetRouteHandler(string $uri, $controller, string $controllerMethod): void
	{
		$this->addRouteHandler('GET', $uri, $controller, $controllerMethod);
	}

	protected function addPostRouteHandler(string $uri, $controller, string $controllerMethod): void
	{
		$this->addRouteHandler('POST', $uri, $controller, $controllerMethod);
	}

	protected function addPutRouteHandler(string $uri, $controller, string $controllerMethod): void
	{
		$this->addRouteHandler('PUT', $uri, $controller, $controllerMethod);
	}

	protected function addPatchRouteHandler(string $uri, $controller, string $controllerMethod): void
	{
		$this->addRouteHandler('PATCH', $uri, $controller, $controllerMethod);
	}

	protected function addDeleteRouteHandler(string $uri, $controller, string $controllerMethod): void
	{
		$this->addRouteHandler('DELETE', $uri, $controller, $controllerMethod);
	}
	#endregion

	/**
	 * Method that is required to be implemented by a child class. This
	 * is intended to configure worker specific routes.
	 *
	 * @param Router $router The instance of the router.
	 */
	abstract protected function setupRoutes(Router $router): void;
}
