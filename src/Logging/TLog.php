<?php
declare(strict_types=1);
namespace Telenor\Logging;

use Psr\Log\LogLevel;

trait TLog
{
	protected function logEmergency(string $message, array $context = []): void
	{
		LoggingController::getInstance()->log(LogLevel::EMERGENCY, $message, $context);
	}

	protected function logError(string $message, array $context = []): void
	{
		LoggingController::getInstance()->log(LogLevel::ERROR, $message, $context);
	}

	protected function logCritical(string $message, array $context = []): void
	{
		LoggingController::getInstance()->log(LogLevel::CRITICAL, $message, $context);
	}

	protected function logAlert(string $message, array $context = []): void
	{
		LoggingController::getInstance()->log(LogLevel::ALERT, $message, $context);
	}

	protected function logWarning(string $message, array $context = []): void
	{
		LoggingController::getInstance()->log(LogLevel::WARNING, $message, $context);
	}

	protected function logNotice(string $message, array $context = []): void
	{
		LoggingController::getInstance()->log(LogLevel::NOTICE, $message, $context);
	}

	protected function logInfo(string $message, array $context = []): void
	{
		LoggingController::getInstance()->log(LogLevel::INFO, $message, $context);
	}

	protected function logDebug(string $message, array $context = []): void
	{
		LoggingController::getInstance()->log(LogLevel::DEBUG, $message, $context);
	}
}