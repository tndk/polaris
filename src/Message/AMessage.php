<?php
declare(strict_types=1);

namespace Telenor\Message;

use DateTime;
use Telenor\System\GlobalStorage;

abstract class AMessage implements IMessage
{
	/** @var string */
	protected $requestId;

	/** @var DateTime */
	protected $timestamp;

	public function __construct(array $attributes = [])
	{
		$timestamp = $attributes['timestamp'] ?? '';
		$this->timestamp = ($timestamp instanceof DateTime) ? $timestamp : new DateTime('' . $timestamp);
		$requestId = ($attributes['request_id'] ?? GlobalStorage::getUUID());

		$this->setRequestId($requestId);
		//$this->setRequestId($attributes['request_id'] ?? GlobalStorage::getUUID());
	}

	public function getTimestamp(): DateTime
	{
		return $this->timestamp;
	}

	public function getRequestId(): ?string
	{
		return $this->requestId;
	}

	public function setRequestId(?string $requestId): void
	{
		$this->requestId = $requestId;
	}

	public function getAttributes(): array
	{
		return [
			'request_id' => $this->getRequestId(),
			'timestamp'  => $this->getTimestamp()->format(DateTime::RFC3339_EXTENDED),
		];
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		$msg = "";
		$msg .= json_encode($this->getAttributes(), JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR);
		return $msg;
	}

	public function validate(): void
	{

	}
}
