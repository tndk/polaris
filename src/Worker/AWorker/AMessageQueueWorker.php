<?php
declare(strict_types=1);
namespace Telenor\Worker\AWorker;

use Telenor\Message\IMessage;
use Telenor\MessageQueue\IMessageConsumeHandler;
use Telenor\MessageQueue\MessageQueueController;
use Telenor\System\Exception\ConnectionException;

abstract class AMessageQueueWorker extends AWorker
{
	public function start()
	{
		parent::start();

		try {
			MessageQueueController::getInstance()->registerOnMessageConsumed(new class ($this) implements IMessageConsumeHandler
			{
				protected $parent;

				public function __construct(AMessageQueueWorker $parent)
				{
					$this->parent = $parent;
				}

				public function messageConsumed(IMessage $message, array $metaData = null): bool
				{
					return $this->parent->onMessageConsumed($message, $metaData);
				}
			});
		} catch (ConnectionException $e) {
		}
	}

	abstract public function onMessageConsumed(IMessage $message, array $metaData = null): bool;
}