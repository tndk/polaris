<?php
declare(strict_types=1);

namespace Telenor\Message;

use function get_class;

class NotificationMessage extends AMessage
{
	/** @var IMessage */
	protected $attachedMessage;

	/** @var string */
	protected $status;

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);

		$this->setStatus($attributes['status'] ?? 'unknown');

		$expiredMessageName = $attributes['attachedmessageclass'] ?? null;
		if ($expiredMessageName !== null) {
			$expiredMessage = new $expiredMessageName($attributes['attachedmessage']);
			$this->setAttachedMessage($expiredMessage);
		}
	}

	public function setAttachedMessage(?IMessage $message): void
	{
		$this->attachedMessage = $message;
	}

	public function getAttachedMessage(): ?IMessage
	{
		return $this->attachedMessage;
	}

	public function getAttributes(): array
	{
		$extraAttributes = [
			'status' => $this->getStatus(),
		];

		if ($this->getAttachedMessage() !== null) {
			$extraAttributes['attachedmessage'] = $this->getAttachedMessage()->getAttributes();
			$extraAttributes['attachedmessageclass'] = get_class($this->getAttachedMessage());
		}

		return array_merge(parent::getAttributes(), $extraAttributes);
	}

	public function getStatus(): string
	{
		return $this->status;
	}

	public function setStatus(string $status): void
	{
		$this->status = $status;
	}
}