<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker;

use Amp\Failure;
use Amp\Http\Server\Request;
use Amp\Http\Server\RequestHandler;
use Amp\Http\Server\Response;
use Amp\Http\Server\Router;
use Amp\Promise;
use Generator;
use http\Exception\InvalidArgumentException;
use ReflectionClass;
use Telenor\Logging\TLog;
use Telenor\Message\IMessage;
use Telenor\Message\UserMessage;
use Telenor\MessageQueue\TMessageQueue;
use Telenor\Model\IUser;
use Telenor\System\Exception\ConnectionException;
use Telenor\System\Exception\InvalidDataException;
use Telenor\System\Exception\ValidationException;
use Telenor\System\WebUtils;
use Telenor\Worker\WorkdayWorker\UserSync\IUserSync;
use Telenor\Worker\WorkdayWorker\UserSync\ServicenowAllUsersSync;
use Telenor\Worker\WorkdayWorker\UserSync\ServicenowSingleUserSync;
use Telenor\Worker\WorkdayWorker\UserSync\WorkdayAllUsersSync;
use Telenor\Worker\WorkdayWorker\UserSync\WorkdaySingleUsersSync;
use function Amp\call;
use function array_key_exists;
use function in_array;
use function strtoupper;

class WorkdayController implements RequestHandler
{
	use TMessageQueue;
	use TLog;
	protected $webRequestMethods = [];
	protected $numberOfUpdates;
	protected $numberOfCreates;
	protected $numberOfDeletes;
	//protected $user;
	protected $sn;

	public function __construct()
	{
		$this->sn = new ServicenowClient();

		$reflectionClass = new ReflectionClass($this);

		foreach ($reflectionClass->getMethods() as $method) {

			if ($method->getReturnType() === null)
				continue;

			if ($method->getReturnType()->getName() !== Response::class)
				continue;

			if ($method->isPublic() !== true)
				continue;

			$this->webRequestMethods[] = $method->getName();
		}

	}

	/**
	 * @param Request $request
	 * @return Promise
	 */
	public function handleRequest(Request $request): Promise
	{
		$this->numberOfUpdates = 0;
		$this->numberOfCreates = 0;
		$this->numberOfDeletes = 0;
		$args = $request->getAttribute(Router::class);
		$method = $args['method'];
		if (in_array($method, $this->webRequestMethods, true)) {
			return call([$this, $method], $request);
		}

		return new Failure(new \Exception('Method not found'));

	}

	/**
	 * @param Request $req
	 * @return Response
	 */
	public function user(Request $req): Response
	{

		$args = $req->getAttribute(Router::class);            //$method = $args['method'];
		$username = strtoupper($args['username']);

		try {
			foreach ($this->compareUsers(new WorkdaySingleUsersSync($username), new ServicenowSingleUserSync($username)) as $msg) {

				$this->publishMessage($msg);
			}
		} catch (ConnectionException $e) {
			return WebUtils::jsonResponseServerError('Connection Error');
		} catch (InvalidDataException $e) {
			return WebUtils::jsonResponseServerError('Invalid data Error');
		}
		$this->logInfo("Sync finished.\nNumber of Creates $this->numberOfCreates\nNumber of Updates $this->numberOfUpdates\nNumber of Deletes $this->numberOfDeletes");
		return WebUtils::jsonResponseOK();

	}

	public function all(Request $req): Response
	{
		try {
			foreach ($this->compareUsers(new WorkdayAllUsersSync(), new ServicenowAllUsersSync()) as $msg) {

				$this->publishMessage($msg);
			}
		} catch (ConnectionException $e) {
			return WebUtils::jsonResponseServerError('Connection Error');
		} catch (InvalidDataException $e) {
			return WebUtils::jsonResponseServerError('Invalid data Error');
		}
		$this->logInfo("Sync finished.\nNumber of Creates $this->numberOfCreates\nNumber of Updates $this->numberOfUpdates\nNumber of Deletes $this->numberOfDeletes");
		return WebUtils::jsonResponseOK();
	}

	protected function publishMessage(IMessage $msg): void
	{
		$this->publishToQueue($msg);
	}

	/**
	 * @param IUserSync $wd
	 * @param IUserSync $sn
	 * @return Generator
	 */
	protected function compareUsers(IUserSync $wd, IUserSync $sn): Generator
	{
		$wdusers = [];
		foreach ($wd->getUsers() as $user)
			$wdusers[$user->getInitials()] = $user;

		$snusers = [];
		foreach ($sn->getUsers() as $user)
			$snusers[$user->getInitials()] = $user;

		foreach ($wdusers as $key => $wduser) {
			try {
				$this->logDebug("Compare Worday and Servicenow user: $key");
				$msg = $this->buildUser($wduser, $snusers[$key] ?? null);
				if ($msg !== null)
					yield $msg;

			} catch (ValidationException $e) {
				$this->logError("Error validating user $key", [
					'property'  => $e->getProperty(),
					'value'     => $e->getValue(),
					'errormsg'  => $e->getContent(),
					'exception' => $e->getMessage(),
				]);

			}

		}
		//Delete
		foreach ($snusers as $key => $snuser) {
			if (!array_key_exists($key, $wdusers)) {
				try {
					$msg = $this->buildUser(null, $snuser);
					if ($msg !== null)
						yield $msg;

				} catch (ValidationException $e) {
					$this->logError("Error validating user $key", [
						'property' => $e->getProperty(),
						'value'    => $e->getValue(),
						'message'  => $e->getMessage(),
					]);
				}
			}
		}
	}

	public function buildUser(?IUser $wdUser, ?IUser $snUser): ?UserMessage
	{
		if ($wdUser === null && $snUser === null) {
			throw new ValidationException(__METHOD__, 'Users has null value');
		}

		if ($wdUser !== null && $snUser === null) //Create
		{
			$msg = new UserMessage(UserMessage::ACTION_CREATE);
			$msg->setInitials($wdUser->getInitials());
			$msg->setFirstname($wdUser->getFirstname());
			$msg->setLastname($wdUser->getLastname());
			$msg->setPhone($wdUser->getPhone());
			$msg->setEmail($wdUser->getEmail());
			$msg->setCity($wdUser->getCity());
			$msg->setCompany($wdUser->getCompany());
			$msg->setTitle($wdUser->getTitle());
			$msg->setGlobalEmployeeNumber($wdUser->getEmployeeNumber());
			$msg->setGlobalEmployeeNumber1($wdUser->getEmployeeNumber());
			$msg->setDepartment($wdUser->getDepartment());
			$msg->setManager($wdUser->getManager());
			$msg->setFullname($wdUser->getFullname());

			++$this->numberOfCreates;

			$this->logInfo("Build Create user:" . $wdUser->getInitials(), ['msg' => $msg]);

		} else if ($wdUser !== null && $wdUser !== null && $wdUser->getInitials() === $snUser->getInitials()) //Update
		{

			$msg = new UserMessage(UserMessage::ACTION_UPDATE);
			$msg->setInitials($wdUser->getInitials());

			$changed = [];
			if ($wdUser->getFirstname() !== $snUser->getFirstname()) {
				$msg->setFirstname($wdUser->getFirstname());
				$changed[] = "Firstname";
			}
			if ($wdUser->getLastname() !== $snUser->getLastname()) {
				$msg->setLastname($wdUser->getLastname());
				$changed[] = "Lastname";
			}
			if ($wdUser->getPhone() !== $snUser->getPhone()) {
				$msg->setPhone($wdUser->getPhone());
				$changed[] = "Phone";
			}
			if ($wdUser->getEmail() !== $snUser->getEmail()) {
				$msg->setEmail($wdUser->getEmail());
				$changed[] = "Email";
			}
			if ($wdUser->getCity() !== $snUser->getCity()) {
				$msg->setCity($wdUser->getCity());
				$changed[] = "City";
			}
			if ($wdUser->getCompany() !== $snUser->getCompany()) {
				$msg->setCompany($wdUser->getCompany());
				$changed[] = "Company";
			}
			if ($wdUser->getTitle() !== $snUser->getTitle()) {
				$msg->setTitle($wdUser->getTitle());
				$changed[] = "Title";
			}
			if ($wdUser->getEmployeeNumber() !== $snUser->getEmployeeNumber()) {
				$msg->setGlobalEmployeeNumber($wdUser->getEmployeeNumber());
				$changed[] = "GlobalEmployeeNumber";
			}
			if ($wdUser->getEmployeeNumber1() !== $snUser->getEmployeeNumber1()) {
				$msg->setGlobalEmployeeNumber1($wdUser->getEmployeeNumber1());
				$changed[] = "GlobalEmployeeNumber1";
			}
			if ($wdUser->getDepartment() !== $snUser->getDepartment()) {
				$msg->setDepartment($wdUser->getDepartment());
				$changed[] = "Department";
			}
			if ($wdUser->getManager() !== $snUser->getManager()) {
				$msg->setManager($wdUser->getManager());
				$changed[] = "Manager";
			}
			if ($wdUser->getFullname() !== $snUser->getFullname()) {
				$msg->setFullname($wdUser->getFullname());
				$changed[] = "Fullname";
			}
			if (empty($changed)) {
				$this->logDebug("No changes for user " . $wdUser->getInitials());
				return null;
			}

			++$this->numberOfUpdates;

			$this->logInfo("Build Update user: " . $wdUser->getInitials(), [
				'Firstname'             => '(WD) ' . $wdUser->getFirstname() . ' - ' . '(SN) ' . $snUser->getFirstname(),
				'Lastname'              => '(WD) ' . $wdUser->getLastname() . ' - ' . '(SN) ' . $snUser->getLastname(),
				'Phone'                 => '(WD) ' . $wdUser->getPhone() . ' - ' . '(SN) ' . $snUser->getPhone(),
				'Email'                 => '(WD) ' . $wdUser->getEmail() . ' - ' . '(SN) ' . $snUser->getEmail(),
				'City'                  => '(WD) ' . $wdUser->getCity() . ' - ' . '(SN) ' . $snUser->getCity(),
				'Company'               => '(WD) ' . $wdUser->getCompany() . ' - ' . '(SN) ' . $snUser->getCompany(),
				'Title'                 => '(WD) ' . $wdUser->getTitle() . ' - ' . '(SN) ' . $snUser->getTitle(),
				'GlobalEmployeeNumber'  => '(WD) ' . $wdUser->getEmployeeNumber() . ' - ' . '(SN) ' . $snUser->getEmployeeNumber(),
				'GlobalEmployeeNumber1' => '(WD) ' . $wdUser->getEmployeeNumber() . ' - ' . '(SN) ' . $snUser->getEmployeeNumber(),
				'Department'            => '(WD) ' . $wdUser->getDepartment() . ' - ' . '(SN) ' . $snUser->getDepartment(),
				'Fullname'              => '(WD) ' . $wdUser->getFullname() . ' - ' . '(SN) ' . $snUser->getFullname(),
				'Initials'              => '(WD) ' . $wdUser->getInitials() . ' - ' . '(SN) ' . $snUser->getInitials(),
				'Manager'               => '(WD) ' . $wdUser->getManager() . ' - ' . '(SN) ' . $snUser->getManager(),
			]);

		} else if ($wdUser === null && $snUser->getInitials() !== null) {
			if ($snUser->getActive()) {

				$msg = new UserMessage(UserMessage::ACTION_DELETE);

				$msg->setInitials($snUser->getInitials());
				++$this->numberOfDeletes;

				$this->logInfo('Build Delete user: ' . $snUser->getInitials());
			} else {
				return null;
			}
		} else {
			throw new InvalidArgumentException('Invalid User input');
		}

		return $msg;
	}

}
