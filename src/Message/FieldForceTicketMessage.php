<?php
declare(strict_types=1);

namespace Telenor\Message;

use DateTime;
use Telenor\System\Exception\InvalidDataException;

class FieldForceTicketMessage extends ATicketMessage
{
	public const STATUS_OPEN = 'Open';
	public const STATUS_PENDING = 'Pending';
	public const STATUS_WORK_IN_PROGRESS = 'Work in Progress';
	public const STATUS_CLOSED_COMPLETE = 'Closed Complete';
	public const STATUS_CLOSED_INCOMPLETE = 'Closed Incomplete';
	public const STATUS_CLOSED_SKIPPED = 'Closed Skipped';
	public const STATUS_RESOLVE = 'Resolve';
	public const STATUS_CANCEL = 'Cancel';
	public const STATUS_REJECT = 'Reject';

	/** @var DateTime|null */
	protected $dueDate;

	public function __construct()
	{
		parent::__construct('fieldforce', ATicketMessage::ACTION_UPDATE);
	}

	/**
	 * @return DateTime|null
	 */
	public function getDueDate(): ?DateTime
	{
		return $this->dueDate;
	}

	/**
	 * @param DateTime|null $dueDate
	 */
	public function setDueDate(?DateTime $dueDate): void
	{
		$this->dueDate = $dueDate;
	}

	protected function getValidStates(): array
	{
		return [
			self::STATUS_OPEN,
			self::STATUS_PENDING,
			self::STATUS_WORK_IN_PROGRESS,
			self::STATUS_RESOLVE,
			self::STATUS_CLOSED_COMPLETE,
			self::STATUS_CLOSED_INCOMPLETE,
			self::STATUS_CLOSED_SKIPPED,
			self::STATUS_REJECT,
			self::STATUS_CANCEL,
		];
	}

	/**
	 * @return array
	 * @throws InvalidDataException
	 */
	public function toArray(): array
	{
		return array_merge(parent::toArray(), [
			'due_date' => $this->convertDateTime($this->getDueDate()),
		]);
	}
}