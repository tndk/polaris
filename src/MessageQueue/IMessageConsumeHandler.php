<?php
declare(strict_types=1);

namespace Telenor\MessageQueue;

use Telenor\Message\IMessage;

interface IMessageConsumeHandler
{
	public function messageConsumed(IMessage $message, array $metaData = null): bool;
}