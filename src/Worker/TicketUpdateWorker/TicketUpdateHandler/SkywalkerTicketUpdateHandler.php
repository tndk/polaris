<?php
declare(strict_types=1);

namespace Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler;

use GuzzleHttp\Client;
use Telenor\Logging\TLog;
use Telenor\Message\TicketUpdateMessage;
use function explode;

class SkywalkerTicketUpdateHandler implements ITicketUpdateHandler
{
	use TLog;

	public function handleMessage(TicketUpdateMessage $message): bool
	{
		/*
		 * We only want to handle tickets with state 'Resolved'. Therefore we return true in
		 * order to ack the message from the queue - hence, we handled the message correctly.
		 */
		if ($message->getStatus() !== TicketUpdateMessage::STATUS_RESOLVED) {
			$this->logInfo('Received message with status ' . $message->getStatus() . ' - ignoring request');
			return true;
		}

		[$host, $eventId] = explode('|', $message->getExtendedPayload()['ExternalTicketId'] ?? '');
		$correlationId = $message->getCorrelationId();

		$url = "/cgi-telenor/api.pl?request=ticketclosed&eventid=$eventId&ticketid=$correlationId";

		$this->logInfo("Sending ticket update request to Skywalker: $host$url", [
			'host'     => $host,
			'endpoint' => $url,
		]);

		$client = new Client(['base_uri' => "http://$host"]);
		$response = $client->get($url);

		$additionalLogData = [
			'response' => [
				'headers' => $response->getHeaders(),
				'code'    => $response->getStatusCode(),
				'body'    => $response->getBody()->getContents(),
			],
		];

		if ($response->getStatusCode() !== 200) {
			$this->logWarning('Failed to send GET request to Skywalker', $additionalLogData);
			return false;
		}

		$this->logInfo('Successfully sent GET request to Skywalker', $additionalLogData);

		return true;
	}
}