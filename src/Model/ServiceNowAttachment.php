<?php
declare(strict_types=1);

namespace Telenor\Model;

class ServiceNowAttachment extends AServiceNowModel
{
	public function getEndpoint(): string
	{
		return 'now/attachment';
	}

	public function getFields(): array
	{
		return [
			'size_bytes',
			'file_name',
			'sys_created_by',
			'file' => 'download_link',
		];
	}

	/** @noinspection PhpMissingParentCallCommonInspection */
	public function getFilters(string $taskID): array
	{
		return ['table_sys_id' => $taskID];
	}
}