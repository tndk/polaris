<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker\UserSync;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Telenor\Logging\TLog;
use Telenor\Model\IUser;
use Telenor\Model\ServicenowUser;
use Telenor\System\Config;
use Telenor\System\Exception\ConnectionException;
use function json_decode;
use function strtoupper;

abstract class AServicenowUserSync implements IUserSync
{
	use TLog;
	protected $servicenowApiUri;
	protected $servicenowUsername;
	protected $servicenowPassword;

	public function __construct()
	{
		$this->setServicenowUsername(Config::envString('SERVICENOW_USERNAME'));
		$this->setServicenowPassword(Config::envString('SERVICENOW_PASSWORD'));
		$this->setServicenowApiUri('https://' . Config::envString('SERVICENOW_INSTANCE') . '.service-now.com/api/now/table/sys_user?sysparm_fields=user_name%2Csys_id%2Cemail%2Cu_telenor_employee_id%2Cemployee_number%2Ctitle%2Cu_company%2Cu_department%2Cmanager%2Cfirst_name%2Clast_name%2Cname%2Ccity%2Cphone%2Cactive&sysparm_query=u_company!=TCS^ORu_companyISEMPTY^web_service_access_only!=true^ORweb_service_access_onlyISEMPTY');

	}

	/**
	 * @return string
	 * @throws ConnectionException
	 */
	public function query(): string
	{

		try {
			$client = new Client();

			$res = $client->request('GET', $this->servicenowApiUri, [
				'auth' => [$this->getServicenowUsername(), $this->getServicenowPassword()],
			]);
		} catch (GuzzleException $e) {
			$this->logError('Error fetching data from Servicenow', ['exception' => $e->getMessage()]);
			throw new ConnectionException('Error connecting to Servicenow API');
		}

		$statusCode = $res->getStatusCode();

		if ($statusCode !== 200) {
			$this->logError("Unexpected status code received $statusCode");
			throw new ConnectionException("Unexcepted return code from Servicenow $statusCode");
		}

		return (string)$res->getBody();
	}

	public function getAllUsers(): array
	{
		$sn_user_rebuild = [];
		$sn_users = [];
		$result = $this->query();
		$object = json_decode($result, false);

		if (!isset($object->result)) {
			return $sn_user_rebuild;
		}

		foreach ($object->result as $key => $value) {
			$sn_users[$value->sys_id] = $value;
		}

		$this->logInfo('Number of Servicenow user feteched:' . count($sn_users));

		foreach ($sn_users as $value) {
			$sn_user = new ServicenowUser();
			$sn_user->setInitials($value->user_name);
			$sn_user->setLastname($value->last_name);
			$sn_user->setFirstname($value->first_name);
			$sn_user->setFullname($value->first_name . ' ' . $value->last_name);
			$sn_user->setCity($value->city);
			$sn_user->setEmployeeNumber((int)$value->employee_number);
			$sn_user->setEmployeeNumber1((int)$value->u_telenor_employee_id);
			$sn_user->setEmail($value->email);
			$sn_user->setTitle($value->title);
			$sn_user->setPhone($value->phone);
			$sn_user->setCompany($value->u_company);
			$sn_user->setDepartment($value->u_department);
			$sn_user->setActive($value->active === 'true');
			if ($value->manager) //Attach manager if exist
			{
				$sn_user->setManager($this->findManager($sn_users, $value->manager->value));
			} else {
				$sn_user->setManager('');

			}
			$this->logDebug('Servicenow user processed: ' . $sn_user->getInitials(), ['snuser' => $sn_user]);
			$sn_user_rebuild[strtoupper($value->user_name)] = $sn_user;
		}

		/** @return IUser */
		return $sn_user_rebuild;
	}

	public function findManager($array, $sys_id): ?string
	{
		return $array[$sys_id]->user_name ?? '';
	}

	public function getServicenowApiUri(): string
	{
		return $this->servicenowApiUri;
	}

	public function setServicenowApiUri($servicenowApiUri): void
	{
		$this->servicenowApiUri = $servicenowApiUri;
	}

	public function getServicenowUsername(): string
	{
		return $this->servicenowUsername;
	}

	public function setServicenowUsername($servicenowUsername): void
	{
		$this->servicenowUsername = $servicenowUsername;
	}

	public function getServicenowPassword(): string
	{
		return $this->servicenowPassword;
	}

	public function setServicenowPassword($servicenowPassword): void
	{
		$this->servicenowPassword = $servicenowPassword;
	}

}