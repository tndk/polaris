<?php
declare(strict_types=1);
namespace Telenor\System\Exception;

/**
 * Request is missing fields or has incorrect headers.
 */
class InvalidRequestException extends PolarisException
{
}