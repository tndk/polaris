<?php
declare(strict_types=1);

namespace Telenor\MessageQueue\Driver;

use Telenor\System\Exception\ConnectionException;

/**
 * Definition of any message queue application to be used in this project.
 */
interface IMessageQueueDriver
{
	/**
	 * Establish connection to a message queue.
	 */
	public function connect(): void;

	/**
	 * Determine whether a connection is enabled and active.
	 *
	 * @return bool True if connected, false otherwise.
	 */
	public function isConnected(): bool;

	/**
	 * Gracefully disconnect from the message queue.
	 */
	public function disconnect(): void;

	/**
	 * Publish a message to the preconfigured message queue.
	 *
	 * @param string $data The serialized data to send as message.
	 * @param string|null $queueName An alternative queue name to publish to.
	 * @throws ConnectionException If failed to reconnect when a connection is lost.
	 */
	public function publish(string $data, string $queueName = null): void;

	public function onMessageConsumed(callable $callback, string $queueName = null): void;

	public function onMessageExpired(callable $callback): void;

	public function onConnectionChanged(callable $callback): void;
}