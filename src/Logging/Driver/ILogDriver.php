<?php
declare(strict_types=1);
namespace Telenor\Logging\Driver;

use Psr\Log\LoggerInterface;

interface ILogDriver extends LoggerInterface
{
}