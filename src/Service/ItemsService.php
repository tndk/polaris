<?php
declare(strict_types=1);

namespace Telenor\Service;

use Telenor\Logging\TLog;
use Telenor\Model\ServiceNowChangesByItem;
use Telenor\Model\ServiceNowIncidentsByItem;
use function preg_replace;

class ItemsService
{
	use TLog;

	/** @var ServiceNowTableClient */
	protected $changesClient;

	/** @var ServiceNowTableClient */
	protected $incidentsClient;

	public function __construct()
	{
		$this->changesClient = new ServiceNowTableClient(new ServiceNowChangesByItem());
		$this->incidentsClient = new ServiceNowTableClient(new ServiceNowIncidentsByItem());
	}

	public function getChanges(string $item): array
	{
		return $this->trim($this->changesClient->query(['item' => $item]));
	}

	public function getHistory(string $item): array
	{
		return $this->trim($this->incidentsClient->query(['item' => $item]));
	}

	protected function trim(array $entries): array
	{
		// Many description/short description contains newlines with a number of spaces (ident) following
		// Here we ensure to trim those indentations away from the actual content
		foreach ($entries as &$entry) {
			if (is_array($entry)) {
				$this->trimEntry($entry, 'Description');
				$this->trimEntry($entry, 'Short Description');
			}
		}

		return $entries;
	}

	protected function trimEntry(array &$entries, string $key): void
	{
		if (isset($entries[$key]))
			$entries[$key] = preg_replace("#(\n\s+|\s+$)#", "\n", $entries[$key]);
	}
}