<?php
declare(strict_types=1);

namespace Telenor\Worker\HealthWorker;

use Amp\Http\Server\Request;
use Amp\Http\Server\RequestHandler;
use Amp\Http\Server\Response;
use Amp\Http\Server\Router;
use Amp\Promise;
use Telenor\Worker\AWorker\AWebWorker;
use function Amp\call;

class HealthWorker extends AWebWorker
{
	protected function setupRoutes(Router $router): void
	{
		$router->addRoute('GET', '/health/?', new class implements RequestHandler
		{
			public function handleRequest(Request $request): Promise
			{
				return call(static function () {
					return new Response();
				});
			}
		});
	}
}