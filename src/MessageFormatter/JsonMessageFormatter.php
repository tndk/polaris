<?php
/** @noinspection PhpRedundantCatchClauseInspection */
declare(strict_types=1);

namespace Telenor\MessageFormatter;

use JsonException;
use Telenor\Logging\TLog;
use Telenor\Message\APayloadMessage;
use Telenor\Message\GenericMessage;
use Telenor\Message\IMessage;
use Telenor\System\Config;
use Telenor\System\Exception\BadFormatException;
use Telenor\System\Exception\InvalidMessageException;
use Throwable;
use function is_array;
use function json_decode;
use function json_encode;

class JsonMessageFormatter implements IMessageFormatter
{
	use TLog;

	protected $unserializeClass;

	public function __construct()
	{
		$this->unserializeClass = Config::envString('INCOMING_MESSAGE_CLASS', GenericMessage::class);
	}

	/**
	 * Converts a Message instance into a json payload message.
	 *
	 * @param IMessage $message The message to convert.
	 * @return string The parsed message payload as a string.
	 * @throws BadFormatException If the message cannot be encoded to json correctly.
	 */
	public function serialize(IMessage $message): string
	{
		$attributes = $message->getAttributes();

		// We do not want to ship attributes in messages with
		// NULL values so we simply remove these attributes here
		if ($message instanceof APayloadMessage)
			$this->removeNullRecursively($attributes);

		try {
			return json_encode($attributes,
				JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_INVALID_UTF8_SUBSTITUTE);
		} catch (JsonException $e) {
			throw new BadFormatException('Unable to serialize message', $e);
		}
	}

	public function removeNullRecursively(array &$attributes): void
	{
		foreach ($attributes as $key => &$value) {
			if (is_array($value)) {
				$this->removeNullRecursively($value);
			} else if ($value === null) {
				unset($attributes[$key]);
			}
		}
	}

	/**
	 * Parses a message payload in string format and converts it into the configured Message class instance.
	 *
	 * @param string $message The message string to parse.
	 * @return IMessage An instance of a correctly formatted message.
	 * @throws BadFormatException If the payload is malformed or does not follow the specification.
	 */
	public function unserialize(string $message): IMessage
	{
		try {
			$data = json_decode($message, true, 512, JSON_THROW_ON_ERROR);
			return new $this->unserializeClass($data);

		} catch (InvalidMessageException|JsonException|Throwable $e) {

			if ($e instanceof InvalidMessageException) {
				$logMessage = 'Received invalid message, did not receive required data within the payload';
			} else if ($e instanceof JsonException) {
				$logMessage = 'Received invalid message, failed to parse json data';
			} else {
				$logMessage = 'Exception while unserializing message';
			}

			$this->logError($logMessage, [
				$e,
				'rawmessage' => $message,
			]);

			throw new BadFormatException('Unable to unserialize message', $e);
		}
	}
}