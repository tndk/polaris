<?php
declare(strict_types=1);
namespace Telenor\Worker\WorkdayWorker\UserSync;

use Telenor\Model\IUser;

interface IUserSync
{

	/**
	 * @return IUser[]
	 */
	public function getUsers(): array;

}