<?php
declare(strict_types=1);
namespace Telenor\Worker\TicketWorker\Controller;

use Amp\Http\Server\Request;
use Amp\Http\Server\Response;
use Telenor\Message\ChangeTicketMessage;
use Throwable;

class ChangeController extends ATicketController
{
	public function show(Request $request): Response
	{
		return new Response(200, ['Content-Type' => 'application/json'], json_encode(['message' => 'Show!!!']));
	}

	public function insert(Request $request)
	{
		try {
			$data = yield from $this->getBody($request);

			$message = new ChangeTicketMessage(ChangeTicketMessage::ACTION_CREATE);

			$this->PopulateChangeProperties($message, $data);

			$this->createTicket($data, $message);
			return $this->handleMessage($message);

		} catch (Throwable $t) {
			return $this->handleException($t);

		}

	}

	public function update(Request $request)
	{
		try {
			$data = yield from $this->getBody($request);

			$message = new ChangeTicketMessage(ChangeTicketMessage::ACTION_UPDATE);

			$this->updateTicket($data, $message);
			$this->PopulateChangeProperties($message, $data);

			return $this->handleMessage($message);

		} catch (Throwable $t) {
			return $this->handleException($t);

		}

	}

	/**
	 * @param ChangeTicketMessage $message
	 * @param array $data
	 * @throws \Telenor\System\Exception\InvalidDataException
	 * @throws \Telenor\System\Exception\InvalidRequestException
	 */
	public function PopulateChangeProperties(ChangeTicketMessage $message, array $data): void
	{
		if ($message->getAction() === ChangeTicketMessage::ACTION_CREATE) {
			$message->setChangeType(ChangeTicketMessage::CHANGE_TYPE_STANDARD);
			$message->setChangeTemplate($this->getFieldString($data, 'change_template', false));

		}
		$message->setPlannedEndDate($this->getFieldDateTime($data, 'planned_enddate'));
		$message->setPlannedStartDate($this->getFieldDateTime($data, 'planned_startdate'));
		$message->setActualStartDate($this->getFieldDateTime($data, 'actual_startdate'));
		$message->setActualEndDate($this->getFieldDateTime($data, 'actual_enddate'));
		$message->setCABDate($this->getFieldDateTime($data, 'cab_date'));
		$message->setCloseCode($this->getFieldString($data, 'close_code', false));
		$message->setCloseNotes($this->getFieldString($data, 'close_notes', false));

		//	$message->setChangeTemplate($this->getFieldString($data, 'change_template'));
		//	$message->addExtendedPayload('Item', $this->getFieldString($data, 'Item', false));
	}

}