<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker;

use Amp\Http\Server\Router;
use Telenor\Worker\AWorker\AWebWorker;

class WorkdayWorker extends AWebWorker
{

	protected function setupRoutes(Router $router): void
	{
		$router->addRoute('GET', '/workday/sync/{method}[/{username}]', new WorkdayController());

		//$this->logInfo('Handling new request!!');

	}

}


