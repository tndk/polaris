<?php
declare(strict_types=1);
namespace Telenor\System\Exception;

use Throwable;

/**
 * Exception to use when a file does not exist.
 */
class FileNotFoundException extends PolarisException
{
	protected $fileName;

	public function __construct(string $message, string $fileName, Throwable $previous = null)
	{
		$this->fileName = $fileName;
		parent::__construct($message, $previous);
	}

	public function getFileName(): string
	{
		return $this->fileName;
	}
}