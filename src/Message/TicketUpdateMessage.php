<?php
declare(strict_types=1);

namespace Telenor\Message;

use DateTime;
use DateTimeZone;
use Exception;
use Telenor\System\Exception\InvalidMessageException;
use Telenor\System\Exception\ValidationException;
use Telenor\System\Utils;
use function in_array;
use function strtolower;

class TicketUpdateMessage extends APayloadMessage
{
	# TODO: This class is starting to look much like the ATicketMessage class - maybe they could be merged?

	public const STATUS_NEW = 'New';
	public const STATUS_IN_PROGRESS = 'In Progress';
	public const STATUS_ON_HOLD = 'On Hold';
	public const STATUS_PENDING_VENDOR = 'Pending Vendor';
	public const STATUS_RESOLVED = 'Resolved';
	public const STATUS_CLOSED = 'Closed';
	public const STATUS_CANCELED = 'Canceled';
	public const STATUS_AUTHORIZE = 'Authorize';
	public const STATUS_ASSESS = 'Assess';
	public const STATUS_SCHEDULED = 'Scheduled';
	public const STATUS_IMPLEMENT = 'Implement';
	public const STATUS_REVIEW = 'Review';
	public const STATUS_CLOSED_COMPLETE = 'Closed Complete';
	public const STATUS_CLOSED_SKIPPED = 'Closed Skipped';
	public const STATUS_CLOSED_INCOMPLETE = 'Closed Incomplete';
	public const STATUS_WORK_IN_PROGRESS = 'Work in Progress';
	public const STATUS_PENDING = 'Pending';
	public const STATUS_OPEN = 'Open';
	public const STATUS_REJECT = 'Reject';
	public const STATUS_CANCEL = 'Cancel';
	public const STATUS_RESOLVE = 'Resolve';

	public const TICKET_TYPE_INCIDENT = 'incident';
	public const TICKET_TYPE_CHANGE = 'change';
	public const TICKET_TYPE_FIELDFORCE = 'fieldforce';

	public const CLOSE_CODE_NO_CODE = '';
	public const CLOSE_CODE_SUCCESSFUL = 'Successful';
	public const CLOSE_CODE_SUCCESSFUL_WITH_ISSUES = 'Successful with issues';
	public const CLOSE_CODE_UNSUCCESSFUL = 'Unsuccessful';
	public const CLOSE_CODE_NULL = null;

	public const RES_CODE_SOLVED_WORKAROUND = 'Solved (Work Around)';
	public const RES_CODE_SOLVED_PERMANENTLY = 'Solved (Permanently)';
	public const RES_CODE_NOT_SOLVED = 'Not Solved';
	public const RES_CODE_CLOSED_RESOLVED_BY_CALLER = 'Closed/Resolved by Caller';
	public const RES_CODE_MISSING_INFORMATION = 'Missing Information';

	/** @var string */
	protected $correlationId;

	/** @var string */
	protected $source;

	/** @var string */
	protected $worknotes;

	/** @var string */
	protected $status;

	/** @var bool */
	protected $statusChanged;

	/** @var string */
	protected $description;

	/** @var string */
	protected $shortDescription;

	/** @var string */
	protected $assignedTo;

	/** @var string[] */
	protected $attachments = [];

	/** @var string[] */
	protected $CIs = [];

	/** @var int */
	protected $impact;

	/** @var int */
	protected $urgency;

	/** @var DateTime|null */
	protected $timeCreated;

	/** @var DateTime|null */
	protected $resolutionDate;

	/** @var DateTime|null */
	protected $closedDate;

	/** @var string */
	protected $onHoldReason;

	/** @var DateTime|null */
	protected $plannedStartDate;

	/** @var DateTime|null */
	protected $plannedEndDate;

	/** @var DateTime|null */
	protected $actualStartDate;

	/** @var DateTime|null */
	protected $actualEndDate;

	/** @var string|null */
	protected $closeCode;

	/** @var bool */
	protected $CABRequired;

	/** @var string|null */
	protected $changeType;

	/** @var DateTime|null */
	protected $dueDate;

	/** @var string|null */
	protected $openedBy;

	/** @var string|null */
	protected $userID;

	/** @var string|null */
	protected $approval_state;

	/** @var array */
	protected $extendedPayload;

	/** @var string|null */
	protected $assignmentGroup;

	/** @var string|null */
	protected $location;

	/** @var bool */
	protected $dueDateChanged;

	/**
	 * SMSMessage constructor.
	 *
	 * @param array $attributes
	 * @throws InvalidMessageException
	 * @throws ValidationException
	 */
	public function __construct(array $attributes = [])
	{
		if (!isset($attributes['transaction_id']))
			throw new InvalidMessageException('Did not find any transaction id in the message');

		if (!isset($attributes['correlation_id']))
			throw new InvalidMessageException('Did not find any correlation id in the message');

		if (!isset($attributes['request_id']))
			throw new InvalidMessageException('Did not find any request id in the message');

		if (!isset($attributes['timestamp']))
			throw new InvalidMessageException('Did not find any timestamp in the message');

		if (!isset($attributes['type']))
			throw new InvalidMessageException('Did not find any type in the message');

		if (!isset($attributes['action']))
			throw new InvalidMessageException('Did not find any action in the message');

		if (!isset($attributes['source']))
			throw new InvalidMessageException('Did not find any source in the message');

		try {
			$this->timestamp = new DateTime($attributes['timestamp']);
			$timeZoneUTC = new DateTimeZone('UTC');
			if (!Utils::isStringNullOrEmpty($created = $attributes['payload']['created'] ?? null))
				$this->setTimeCreated(new DateTime($created, $timeZoneUTC));

			if (!Utils::isStringNullOrEmpty($created = $attributes['payload']['created'] ?? null))
				$this->setTimeCreated(new DateTime($created, $timeZoneUTC));

			if (!Utils::isStringNullOrEmpty($resolutionDate = $attributes['payload']['resolution_date'] ?? null))
				$this->setResolutionDate(new DateTime($resolutionDate, $timeZoneUTC));

			if (!Utils::isStringNullOrEmpty($closedDate = $attributes['payload']['closed_date'] ?? null))
				$this->setClosedDate(new DateTime($closedDate, $timeZoneUTC));

			if (!Utils::isStringNullOrEmpty($plannedStartDate = $attributes['payload']['planned_startdate'] ?? null))
				$this->setPlannedStartDate(new DateTime($plannedStartDate, $timeZoneUTC));

			if (!Utils::isStringNullOrEmpty($plannedEndDate = $attributes['payload']['planned_enddate'] ?? null))
				$this->setPlannedEndDate(new DateTime($plannedEndDate, $timeZoneUTC));

			if (!Utils::isStringNullOrEmpty($actualStartDate = $attributes['payload']['actual_startdate'] ?? null))
				$this->setActualStartDate(new DateTime($actualStartDate, $timeZoneUTC));

			if (!Utils::isStringNullOrEmpty($actualEndDate = $attributes['payload']['actual_enddate'] ?? null))
				$this->setActualEndDate(new DateTime($actualEndDate, $timeZoneUTC));

			if (!Utils::isStringNullOrEmpty($dueDate = $attributes['payload']['due_date'] ?? null))
				$this->setDueDate(new DateTime($dueDate, $timeZoneUTC));

		} catch (Exception $e) {
			throw new InvalidMessageException('Invalid timestamp found in the message', $e);
		}

		if (!in_array($attributes['type'],
			[self::TICKET_TYPE_INCIDENT, self::TICKET_TYPE_CHANGE, self::TICKET_TYPE_FIELDFORCE], true))
			throw new InvalidMessageException('Invalid type found in message');

		parent::__construct(strtolower($attributes['type']), $attributes['action']);

		$this->transactionId = $attributes['transaction_id'];
		$this->correlationId = $attributes['correlation_id'];
		$this->setRequestId($attributes['request_id']);
		$this->setSource($attributes['source']);
		$this->setWorknotes($attributes['payload']['worknotes'] ?? null);
		$this->setStatus($attributes['payload']['status']);
		$this->setStatusChanged($attributes['payload']['status_changed'] ?? null);
		$this->setShortDescription($attributes['payload']['short_description'] ?? null);
		$this->setDescription($attributes['payload']['description'] ?? null);
		$this->setAssignedTo($attributes['payload']['assigned_to'] ?? '');
		$this->setCIs($attributes['payload']['cis'] ?? []);
		$this->setAttachments($attributes['payload_attachment'] ?? []);
		$this->setImpact(isset($attributes['payload']['impact']) ? (int)$attributes['payload']['impact'] : 0);
		$this->setUrgency(isset($attributes['payload']['urgency']) ? (int)$attributes['payload']['urgency'] : 0);
		$this->setOnHoldReason($attributes['payload']['on_hold_reason'] ?? '');
		$this->setCloseCode($attributes['payload']['close_code'] ?? null);
		$this->setCABRequired(isset($attributes['payload']['cab_required']) && $attributes['payload']['cab_required'] === 'true');
		$this->setChangeType($attributes['payload']['change_type'] ?? null);
		$this->setOpenedBy($attributes['payload']['opened_by'] ?? null);
		$this->setUserID($attributes['servicenow_user'] ?? null);
		$this->setApprovalState($attributes['payload']['approval_state'] ?? null);
		$this->setAssigmentGroup($attributes['payload']['assignment_group'] ?? null);
		$this->setLocation($attributes['payload']['location'] ?? null);
		$this->setExtendedPayload($attributes['payload_original'] ?? []);
		$this->setDueDateChanged($attributes['payload']['due_date_changed'] ?? null);
	}

	/**
	 * @return string|null
	 */
	public function getLocation(): ?string
	{
		return $this->location;
	}

	/**
	 * @param string|null $location
	 */
	public function setLocation(?string $location): void
	{
		$this->location = $location;
	}

	/**
	 * @return bool
	 */
	public function getDueDateChanged(): bool
	{
		return $this->dueDateChanged;
	}

	/**
	 * @param bool|null $dueDateChanged
	 */
	public function setDueDateChanged($dueDateChanged): void
	{
		$this->dueDateChanged = $dueDateChanged;
	}

	/**
	 * @return string
	 */
	public function getSource(): string
	{
		return $this->source;
	}

	/**
	 * @param string $source
	 */
	public function setSource(string $source): void
	{
		$this->source = $source;
	}

	/**
	 * @return string|null
	 */
	public function getWorknotes(): ?string
	{
		return $this->worknotes;
	}

	/**
	 * @param string|null $worknotes
	 */
	public function setWorknotes(?string $worknotes): void
	{
		$this->worknotes = $worknotes;
	}

	/**
	 * @return string
	 */
	public function getCorrelationId(): string
	{
		return $this->correlationId;
	}

	/**
	 * @return string
	 */
	public function getStatus(): string
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 * @throws ValidationException
	 */
	public function setStatus(string $status): void
	{
		if (!in_array($status, [
			self::STATUS_NEW,
			self::STATUS_IN_PROGRESS,
			self::STATUS_ON_HOLD,
			self::STATUS_PENDING_VENDOR,
			self::STATUS_RESOLVED,
			self::STATUS_CLOSED,
			self::STATUS_CANCELED,
			self::STATUS_AUTHORIZE,
			self::STATUS_ASSESS,
			self::STATUS_SCHEDULED,
			self::STATUS_IMPLEMENT,
			self::STATUS_REVIEW,
			self::STATUS_CLOSED_COMPLETE,
			self::STATUS_CLOSED_SKIPPED,
			self::STATUS_CLOSED_INCOMPLETE,
			self::STATUS_WORK_IN_PROGRESS,
			self::STATUS_PENDING,
			self::STATUS_OPEN,
			self::STATUS_REJECT,
			self::STATUS_CANCEL,
			self::STATUS_RESOLVE,
		], true))
			throw new ValidationException('status', $status, 'Invalid status');

		$this->status = $status;
	}

	/**
	 * @return string|null
	 */
	public function getShortDescription(): ?string
	{
		return $this->shortDescription;
	}

	/**
	 * @param string|null $shortDescription
	 */
	public function setShortDescription(?string $shortDescription): void
	{
		$this->shortDescription = $shortDescription;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription(?string $description): void
	{
		$this->description = $description;
	}

	/**
	 * @return bool
	 */
	public function getStatusChanged(): bool
	{
		return $this->statusChanged;
	}

	/**
	 * @param bool|null $statusChanged
	 */
	public function setStatusChanged($statusChanged): void
	{
		// MAALS: Jeg er træt...
		if (is_bool($statusChanged)) {
			$this->statusChanged = $statusChanged;
		} else if ($statusChanged === 'true') {
			$this->statusChanged = true;
		} else {
			$this->statusChanged = false;
		}
	}

	/**
	 * @return string|null
	 */
	public function getAssignedTo(): ?string
	{
		return $this->assignedTo;
	}

	/**
	 * @param string|null $assignedTo
	 */
	public function setAssignedTo(?string $assignedTo): void
	{
		$this->assignedTo = $assignedTo;
	}

	/**
	 * @return string[]
	 */
	public function getAttachments(): array
	{
		return $this->attachments;
	}

	/**
	 * @param string[] $attachments
	 */
	public function setAttachments(array $attachments): void
	{
		$this->attachments = $attachments;
	}

	/**
	 * @return string[]
	 */
	public function getCIs(): array
	{
		return $this->CIs;
	}

	/**
	 * @param string[] $CIs
	 */
	public function setCIs(array $CIs): void
	{
		$this->CIs = $CIs;
	}

	/**
	 * @return int
	 */
	public function getImpact(): int
	{
		return $this->impact;
	}

	/**
	 * @param int $impact
	 */
	public function setImpact(int $impact): void
	{
		$this->impact = $impact;
	}

	/**
	 * @return int
	 */
	public function getUrgency(): int
	{
		return $this->urgency;
	}

	/**
	 * @param int $urgency
	 */
	public function setUrgency(int $urgency): void
	{
		$this->urgency = $urgency;
	}

	/**
	 * @return DateTime|null
	 */
	public function getTimeCreated(): ?DateTime
	{
		return $this->timeCreated;
	}

	/**
	 * @param DateTime|null $timeCreated
	 */
	public function setTimeCreated(?DateTime $timeCreated): void
	{
		$this->timeCreated = $timeCreated;
	}

	/**
	 * @return DateTime|null
	 */
	public function getResolutionDate(): ?DateTime
	{
		return $this->resolutionDate;
	}

	/**
	 * @param DateTime|null $resolutionDate
	 */
	public function setResolutionDate(?DateTime $resolutionDate): void
	{
		$this->resolutionDate = $resolutionDate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getClosedDate(): ?DateTime
	{
		return $this->closedDate;
	}

	/**
	 * @param DateTime|null $closedDate
	 */
	public function setClosedDate(?DateTime $closedDate): void
	{
		$this->closedDate = $closedDate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getPlannedStartDate(): ?DateTime
	{
		return $this->plannedStartDate;
	}

	/**
	 * @param DateTime|null $plannedStartDate
	 */
	public function setPlannedStartDate(?DateTime $plannedStartDate): void
	{
		$this->plannedStartDate = $plannedStartDate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getPlannedEndDate(): ?DateTime
	{
		return $this->plannedEndDate;
	}

	/**
	 * @param DateTime|null $plannedEndDate
	 */
	public function setPlannedEndDate(?DateTime $plannedEndDate): void
	{
		$this->plannedEndDate = $plannedEndDate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getActualStartDate(): ?DateTime
	{
		return $this->actualStartDate;
	}

	/**
	 * @param DateTime|null $actualStartDate
	 */
	public function setActualStartDate(?DateTime $actualStartDate): void
	{
		$this->actualStartDate = $actualStartDate;
	}

	/**
	 * @return DateTime|null
	 */
	public function getActualEndDate(): ?DateTime
	{
		return $this->actualEndDate;
	}

	/**
	 * @param DateTime|null $actualEndDate
	 */
	public function setActualEndDate(?DateTime $actualEndDate): void
	{
		$this->actualEndDate = $actualEndDate;
	}

	/**
	 * @return string|null
	 */
	public function getCloseCode(): ?string
	{
		return $this->closeCode;
	}

	/**
	 * @param string|null $closeCode
	 * @throws ValidationException
	 */
	public function setCloseCode(?string $closeCode): void
	{

		if (!in_array($closeCode, [
			self::CLOSE_CODE_SUCCESSFUL,
			self::CLOSE_CODE_SUCCESSFUL_WITH_ISSUES,
			self::CLOSE_CODE_UNSUCCESSFUL,
			self::CLOSE_CODE_NO_CODE,
			self::CLOSE_CODE_NULL,
			self::RES_CODE_SOLVED_WORKAROUND,
			self::RES_CODE_SOLVED_PERMANENTLY,
			self::RES_CODE_NOT_SOLVED,
			self::RES_CODE_CLOSED_RESOLVED_BY_CALLER,
			self::RES_CODE_MISSING_INFORMATION,

		], true))
			throw new ValidationException('Close code', $closeCode, 'Invalid close code');

		$this->closeCode = $closeCode;
	}

	/**
	 * @return bool
	 */
	public function isCABRequired(): bool
	{
		return $this->CABRequired;
	}

	/**
	 * @param bool $CABRequired
	 */
	public function setCABRequired(bool $CABRequired): void
	{
		$this->CABRequired = $CABRequired;
	}

	/**
	 * @return string|null
	 */
	public function getChangeType(): ?string
	{
		return $this->changeType;
	}

	/**
	 * @param string|null $changeType
	 */
	public function setChangeType(?string $changeType): void
	{
		$this->changeType = $changeType;
	}

	/**
	 * @return array
	 */
	public function getExtendedPayload(): array
	{
		return $this->extendedPayload;
	}

	/**
	 * @param array $extendedPayload
	 */
	public function setExtendedPayload(array $extendedPayload): void
	{
		$this->extendedPayload = $extendedPayload;
	}

	/**
	 * @return string
	 */
	public function getOnHoldReason(): string
	{
		return $this->onHoldReason;
	}

	/**
	 * @param string $onHoldReason
	 */
	public function setOnHoldReason(string $onHoldReason): void
	{
		$this->onHoldReason = $onHoldReason;
	}

	/**
	 * @return DateTime|null
	 */
	public function getDueDate(): ?DateTime
	{
		return $this->dueDate;
	}

	/**
	 * @param DateTime|null $dueDate
	 */
	public function setDueDate(?DateTime $dueDate): void
	{
		$this->dueDate = $dueDate;
	}

	/**
	 * @return string|null
	 */
	public function getOpenedBy(): ?string
	{
		return $this->openedBy;
	}

	/**
	 * @param string|null $openedBy
	 */
	public function setOpenedBy(?string $openedBy): void
	{
		$this->openedBy = $openedBy;
	}

	/**
	 * @return string|null
	 */
	public function getUserID(): ?string
	{
		return $this->userID;
	}

	/**
	 * @param string|null $userID
	 */
	public function setUserID(?string $userID): void
	{
		$this->userID = $userID;
	}

	/**
	 * @return string|null
	 */
	public function getApprovalState(): ?string
	{
		return $this->approval_state;
	}

	/**
	 * @param string|null $approvalState
	 */
	public function setApprovalState(?string $approvalState): void
	{
		$this->approval_state = $approvalState;
	}

	public function getAssignmentGroup(): ?string
	{
		return $this->assignmentGroup;

	}

	public function setAssigmentGroup(?string $assignmentGroup): void
	{
		$this->assignmentGroup = $assignmentGroup;

	}

	public function getAttributes(): array
	{
		return array_merge(parent::getAttributes(), [
			'correlation_id'   => $this->getCorrelationId(),
			'source'           => $this->getSource(),
			'payload_original' => $this->getExtendedPayload(),
		]);
	}

	public function toArray(): array
	{
		return [
			'worknotes'          => $this->getWorknotes(),
			'status'             => $this->getStatus(),
			'short_description'  => $this->getShortDescription(),
			'description'        => $this->getDescription(),
			'impact'             => (string)$this->getImpact(),
			'urgency'            => (string)$this->getUrgency(),
			'opened_by'          => $this->getOpenedBy(),
			'cis'                => $this->getCIs(),
			'location'           => $this->getLocation(),
			'due_date'           => $this->getDueDate(),
			'time_created'       => $this->getTimeCreated(),
			'change_type'        => $this->getChangeType(),
			'close_code'         => $this->getCloseCode(),
			'actual_start_date'  => $this->getActualStartDate(),
			'actual_end_date'    => $this->getActualEndDate(),
			'planned_start_date' => $this->getPlannedStartDate(),
			'planned_end_date'   => $this->getPlannedEndDate(),
			'assigned_to'        => $this->getAssignedTo(),
			'closed_date'        => $this->getClosedDate(),
			'on_hold_reason'     => $this->getOnHoldReason(),
			'resolution_date'    => $this->getResolutionDate(),
			'cab_required'       => $this->isCABRequired(),
			'userid'             => $this->getUserID(),
			'approval_state'     => $this->getApprovalState(),
		];
	}
}
