<?php
declare(strict_types=1);

namespace Telenor\Model;

class ServiceNowChangesByItem extends AServiceNowModel
{
	public function getEndpoint(): string
	{
		return 'tgssa/tndk_get_change_details_by_item';
	}

	public function getFields(): array
	{
		return ['Description'];
	}
}