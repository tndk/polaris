<?php
declare(strict_types=1);

namespace Telenor\Worker\TicketNotificationWorker;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Telenor\Message\GenericMessage;
use Telenor\Message\IMessage;
use Telenor\System\Config;
use Telenor\Worker\AWorker\AMessageQueueWorker;
use function json_encode;

class TicketNotificationWorker extends AMessageQueueWorker
{

	public function onMessageConsumed(IMessage $message, array $metaData = null): bool
	{
		if ($message instanceof GenericMessage) {

			return $this->handleTicket($message);
		}

		$this->logError('Unexpected message reveived', ['message' => $message]);
		return true;

	}

	protected function handleTicket(GenericMessage $message): bool
	{
		try {
			$ticket = json_encode($message->getData(), JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR);
			$this->logInfo('Handling new Ticket', ['message' => $ticket]);

			$username = Config::envString('SERVICENOW_USERNAME');
			$password = Config::envString('SERVICENOW_PASSWORD');
			$applicationCode = Config::envString('SERVICENOW_APPLICATION_CODE', 'td');

			$method = 'POST';
			$url = 'https://' . Config::envString('SERVICENOW_INSTANCE') . ".service-now.com/api/$applicationCode/tndk_integration_create_ticket";

			$headers = [
				'headers'     => ['Accept' => 'application/json'],
				'body'        => $ticket,
				'http_errors' => false, # Treat http code serie 400 as none exception
			];
			$this->logInfo('Sending request to ServiceNow', [
				'method'   => $method,
				'url'      => $url,
				'username' => $username,
				'headers'  => $headers,
			]);

			$client = new Client();
			$response = $client->request($method, $url, array_merge($headers, ['auth' => [$username, $password]]));

			$status = $response->getStatusCode() . ' ' . $response->getReasonPhrase();

			$logArgs = [
				'url'          => $url,
				'username'     => $username,
				'statuscode'   => $response->getStatusCode(),
				'statusphrase' => $response->getReasonPhrase(),
				'headers'      => $response->getHeaders(),
				'protocol'     => $response->getProtocolVersion(),
				'body'         => $response->getBody(),

			];

			#Succesful response from ServiceNow, remove message from queue
			if ($response->getStatusCode() === 201) {
				$this->logInfo("Got successful response from ServiceNow: $status", $logArgs);
				return true;
			}

			# error response from ServiceNow, remove message from queue. Nothing else we can do
			if ($response->getStatusCode() === 400) {
				$this->logNotice("Got bad request from ServiceNow: $status", $logArgs);
				return true;
			}

			#Unknown status code received. Remove message from queue. Nothing else we can do
			$this->logError("Unknown response from ServiceNow: $status", $logArgs);
			return true;

		} catch (JsonException $e) {
			##Invalid Json. Remove message from queue. Nothing else we can do
			$this->logError('Invalid Json received', [$e]);
			return true;

		} catch (GuzzleException | Exception $e) {
			# Exception response. Connection timeout etc. We will try again later
			$this->logError('Failed to handle ServiceNow api request', [$e]);
			return false;

		}

	}

}