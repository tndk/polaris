<?php
declare(strict_types=1);
namespace Telenor\System\Exception;

/**
 * Exception to use when a format does not meet the requirements.
 */
class BadFormatException extends PolarisException
{
}