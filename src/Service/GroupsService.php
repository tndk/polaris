<?php
declare(strict_types=1);

namespace Telenor\Service;

use Telenor\Model\ServiceNowGroup;

class GroupsService
{
	/** @var ServiceNowTableClient */
	protected $client;

	public function __construct()
	{
		$this->client = new ServiceNowTableClient(new ServiceNowGroup());
	}

	public function getGroups(): array
	{
		# Eeerhmm... TODO :)
		# There is a script API available at: /api/tgssa/tndk_get_groups_members
		return [
			'DEV OSS' => [
				'HEIP',
				'TPN',
			],
		];
	}
}