<?php
declare(strict_types=1);
namespace Telenor\Model;

interface IUser
{
	public function getLastname(): ?string;

	public function getFirstname(): ?string;

	public function getFullname(): ?string;

	public function getCity(): ?string;

	public function getEmployeeNumber(): ?int;

	public function getEmployeeNumber1(): ?int;

	public function getEmail(): ?string;

	public function getTitle(): ?string;

	public function getPhone(): ?string;

	public function getCompany(): ?string;

	public function getInitials(): ?string;

	public function getDepartment(): ?string;

	public function getManager(): ?string;

	public function getActive(): bool;

	public function setLastname(?string $value): void;

	public function setFirstname(?string $value): void;

	public function setFullname(?string $value): void;

	public function setCity(?string $value): void;

	public function setEmployeeNumber(?int $value): void;

	public function setEmployeeNumber1(?int $value): void;

	public function setEmail(?string $value): void;

	public function setTitle(?string $value): void;

	public function setPhone(?string $value): void;

	public function setCompany(?string $value): void;

	public function setInitials(?string $value): void;

	public function setDepartment(?string $value): void;

	public function setManager(?string $value): void;

	public function setActive(bool $value): void;

}