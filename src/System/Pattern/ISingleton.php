<?php
declare(strict_types=1);

namespace Telenor\System\Pattern;

/**
 * Common interface to use in order to make a class a singleton.
 * Preferably use the @{see TSingleton} because it implements the complete logic.
 */
interface ISingleton
{
	/**
	 * Should return an instance of the class itself.
	 */
	public static function getInstance();
}