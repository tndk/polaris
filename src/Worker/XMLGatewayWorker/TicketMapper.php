<?php
declare(strict_types=1);
namespace Telenor\Worker\XMLGatewayWorker;

use Telenor\Logging\TLog;
use Telenor\Message\AMessage;
use Telenor\Message\IncidentTicketMessage;
use Telenor\System\Config;
use Telenor\System\Error\PolarisError;
use Telenor\System\Exception\InvalidDataException;
use Telenor\System\Exception\ValidationException;
use Telenor\System\Pattern\TSingleton;
use function ctype_digit;
use function explode;
use function strpos;

class TicketMapper
{
	use TSingleton;
	use TLog;

	protected $config;
	protected $quickcases;

	public function __construct(Config $config)
	{
		$this->config = $config->yml();
		$this->quickcases = $this->loadQuickCases();
	}

	/**
	 * Builds a message to be sent to a message queue exchange from
	 *
	 * @param array $parsed
	 * @return AMessage
	 * @throws InvalidDataException
	 * @throws ValidationException
	 */
	public function build(array $parsed): AMessage
	{
		// TODO: add validation and error handling for missing/incorrect fields
		$message = new IncidentTicketMessage($parsed['type'], $parsed['action']);
		$message->setCorrelationId($parsed['correlation_id']);
		$message->setSource($parsed['source']);
		$message->setTemplate($parsed['template']);

		// Veris can send both tenant id and veris id in the VerisId xml field separated with a |
		// We simply split at the first | here and uses the second value. 12|1234 becomes 1234
		// We also map it to the ExternalTicketId field, so Veris, Nokia and Sky1walker uses same field
		if (isset($parsed['fields']['VerisId'])) {
			$parsed['fields']['ExternalTicketId'] = $parsed['fields']['VerisId'];
			if (strpos($parsed['fields']['ExternalTicketId'], '|'))
				[, $parsed['fields']['ExternalTicketId']] = explode('|', $parsed['fields']['ExternalTicketId'], 2);
		}

		##Fetch initials from Veris ticket if possible: Format ex 21RUXA|500075257 or ticketAutoCreation|500075257
		if (isset($parsed['fields']['UserId'])) {
			$m = '';
			$pattern = "/^\d+([a-zA-Z]+)\|\d+|(ticketAutoCreation)\|\d+/";
			if (preg_match($pattern, $parsed['fields']['UserId'], $m)) {
				$parsed['fields']['UserId'] = empty($m[1]) ? $m[2] : $m[1];
			}
		}

		$message->setExtendedPayload($parsed['fields']);

		$this->cleanEmptyFields($message);
		$this->moveAttachments($message);
		$this->applyQuickcase($message);
		$this->setDefaultFields($message);
		$this->translatePayload($message);
		$this->overrideAttributes($message);
		$this->trimExtendedPayload($message);

		return $message;
	}

	// These just make an estimate. It seems there are 4 urgency and impact levels in Remedy,
	// but in ServiceNow we only have 3. Based on these, we "calculate" the priority. What
	// is severity? Who knows
	private function convertUrgency(string $input): int
	{
		$urgency = (int)$input[0];
		if ($urgency === 1)
			return 1;
		if ($urgency === 2 || $urgency === 3)
			return 2;
		if ($urgency === 4 || $urgency === 5)
			return 3;

		return 2;
	}

	private function convertImpact(string $input): int
	{
		$impact = (int)$input[0];
		if ($impact === 1)
			return 1;
		if ($impact === 2 || $impact === 3)
			return 2;
		if ($impact === 4 || $impact === 5)
			return 3;

		return 2;
	}

	/**
	 * Reads 'defaults' section of template configuration and sets the raw payload of $msg accordingly.
	 *
	 * @param IncidentTicketMessage $msg
	 */
	protected function setDefaultFields(IncidentTicketMessage $msg): void
	{
		/* @var array Field map */
		$template = $msg->getTemplate();
		if (!empty($this->config[$template]['defaults'])) {
			/* @var array $defaults */
			$defaults = $this->config[$template]['defaults'];
		} else {
			$this->logDebug("No default fields found for template '$template'");
			return;
		}

		$rawPayload = $msg->getExtendedPayload();
		foreach ($defaults as $field => $value) {
			if (!isset($rawPayload[$field]))
				$rawPayload[$field] = $value;
		}

		$msg->setExtendedPayload($rawPayload);
	}

	/**
	 * Translates raw input data to ServiceNow format and fills into payload
	 *
	 * @param IncidentTicketMessage $msg
	 * @throws InvalidDataException
	 * @throws ValidationException
	 */
	protected function translatePayload(IncidentTicketMessage $msg): void
	{
		$raw = $msg->getExtendedPayload();
		$template = $msg->getTemplate();

		if ($template === 'AILK_CREATE_TICKET') {
			if (empty($raw['Impact']))
				$raw['Impact'] = 2; else
				$raw['Impact'] = $this->convertImpact($raw['Impact']);

			if (empty($raw['Urgency']))
				$raw['Urgency'] = 2; else
				$raw['Urgency'] = $this->convertUrgency($raw['Urgency']);

			// TODO: HARDCORDED FOR VERIS, PLEASE SEND HELP
			// Mandatory fields
			$msg->setShortDescription($this->getFieldString($raw, 'Description'));
			$msg->setDescription($this->getFieldString($raw, 'QuickCaseTitle'));
			$msg->setOpenedBy('veris');

			// Optional fields
			$msg->setImpact($this->getFieldInt($raw, 'Impact', false));
			$msg->setUrgency($this->getFieldInt($raw, 'Urgency', false));
			// $msg->setPriority($this->getFieldInt($raw, 'Priority', false));
			$msg->setAssignedGroup($this->getFieldString($raw, 'AssignedGroup', false));
			$msg->setWorknotes($this->getFieldString($raw, 'Worklog', false));
			$msg->setStatus(IncidentTicketMessage::STATUS_NEW);

		} else if ($template === 'AILK_CLOSE_TICKET') {
			$msg->setWorknotes('Ticket closed in Veris');
			//to avoid resolution codes and notes to be overwritten i SN, we just set the status
			//to an empty string when dealing with close messages from Veris
			$msg->setStatus(IncidentTicketMessage::STATUS_RESOLVED);

		} else if ($template === 'AILK_REOPEN_TICKET') {
			$msg->setWorknotes($this->getFieldString($raw, 'Worklog', false));
			$msg->setStatus(IncidentTicketMessage::STATUS_IN_PROGRESS);
		}
	}

	/**
	 * Reads 'overrides' section of template configuration and sets message metadata accordingly.
	 *
	 * @param IncidentTicketMessage $msg
	 */
	protected function overrideAttributes(IncidentTicketMessage $msg): void
	{
		$template = $msg->getTemplate();
		if (!empty($this->config[$template]['overrides'])) {
			/* @var string[] $overrides */
			$overrides = $this->config[$template]['overrides'];
		} else {
			$this->logDebug("No overrides found for template '$template'");
			return;
		}

		foreach ($overrides as $field => $value) {
			switch ($field) {
				case 'type':
					$msg->setType($value);
					break;
				case 'action':
					$msg->setAction($value);
					break;
				case 'source':
					$msg->setSource($value);
					break;
			}
		}
	}

	private function loadQuickCases(): array
	{
		$file = APP_ROOT . '/config/workers/XMLGatewayWorker/quickcases.csv';
		if (!file_exists($file))
			throw new PolarisError("File '$file' was not found");

		$csv = [];
		$out = [];
		$fh = fopen($file, 'rb');

		while (!feof($fh) && ($line = fgetcsv($fh, 0, ';')) !== false) {
			$csv[] = $line;
		}
		$headers = array_shift($csv);

		// Remove BOM characters from the first value
		$bom = pack('H*', 'EFBBBF');
		$headers[0] = preg_replace("/^$bom/", '', $headers[0]);

		// Move headers to each key in csv and
		foreach ($csv as $i => $row) {
			$csv[$i] = array_combine($headers, $row);
			foreach ($csv[$i] as $key => $value) {
				if ($value === 'NULL')
					unset($csv[$i][$key]);
			}
			$out[$csv[$i]['QuickCase_Titel']] = $csv[$i];
		}
		return $out;
	}

	private function applyQuickcase(IncidentTicketMessage $msg): void
	{
		$rawPayload = $msg->getExtendedPayload();
		if (empty($rawPayload['QuickCaseTitle'])) {
			return;
		}

		$quickcaseTitle = $rawPayload['QuickCaseTitle'];
		if (!isset($this->quickcases[$quickcaseTitle])) {
			$this->logDebug('Supplied QuickCaseTitle \'' . $rawPayload['QuickCaseTitle'] . '\' not found');
			return;
		}

		$quickcase = $this->quickcases[$quickcaseTitle];

		// TODO: We should find a better solution. We can restructure
		// the most of this file in a better and more efficient way
		$fieldsToOverwrite = [
			'Impact'        => 'Impact',
			'Severity'      => 'Severity',
			'Priority'      => 'Priority',
			'Category'      => 'Category',
			'Type'          => 'Type',
			'Subject'       => 'Subject',
			'AssignedGroup' => 'Assigned_Group',
		];

		foreach ($fieldsToOverwrite as $configFieldName => $quickcaseFieldName) {
			if (empty($rawPayload[$configFieldName]) && !empty($quickcase[$quickcaseFieldName]))
				$rawPayload[$configFieldName] = $quickcase[$quickcaseFieldName];
		}

		$msg->setExtendedPayload($rawPayload);
	}

	protected function getTypeFromTicketType(int $ticketType): ?string
	{
		switch ($ticketType) {
			case TicketTypes::PROBLEM:
			case TicketTypes::QUESTION:
			case TicketTypes::ASSIGNMENT:
			case TicketTypes::RFI_REQUEST_FOR_INFO:
			case TicketTypes::SUPPORT:
			case TicketTypes::DRIFT:
			case TicketTypes::BESTILLING:
			case TicketTypes::ITIL_INCIDENT:
			case TicketTypes::ITIL_KNOWN_ERROR:
			case TicketTypes::ITIL_PROBLEM:
				return 'incident';

			case TicketTypes::ITIL_RFC:
				return 'change';

			case TicketTypes::ITIL_SERVICE_REQUEST:
			case TicketTypes::UDVIDET_SERVICE_REQUEST:
			case TicketTypes::ITIL_SR_FAST_TRACK:
				return 'service request';

			case TicketTypes::_CLEAR_:
			default:
				return null;
		}
	}

	private function moveAttachments(IncidentTicketMessage $message): void
	{
		$rawPayload = $message->getExtendedPayload();
		// Rip out attachments from raw and put them into their own array
		foreach ($rawPayload as $field => $value) {
			if (preg_match('/^attachment/i', $field)) {
				if (empty($value))
					continue;
				$attachmentsRaw[$field] = $value;
				unset($rawPayload[$field]);
			}
		}

		if (empty($attachmentsRaw))
			return;

		# $this->logDebug(var_export($attachmentsRaw, true));
		$names = $values = [];
		foreach ($attachmentsRaw as $key => $value) {
			if (preg_match('/name/i', $key))
				$names[] = $value; else
				$values[] = $value;
		}
		$attachments = array_combine($names, $values);

		foreach ($attachments as $name => $value) {
			$message->addAttachment($name, $value);
		}

		if (!empty($attachments)) {
			$message->setExtendedPayload($rawPayload);
		}
	}

	private function cleanEmptyFields(IncidentTicketMessage $msg): void
	{
		$rawPayload = $msg->getExtendedPayload();
		foreach ($rawPayload as $key => $value) {
			if (empty($value))
				unset($rawPayload[$key]);
		}

		$msg->setExtendedPayload($rawPayload);
	}

	private function trimExtendedPayload(IncidentTicketMessage $msg): void
	{
		$rawPayload = $msg->getExtendedPayload();
		$allowedFields = $this->config[$msg->getTemplate()]['allowed_reference_fields'] ?? [];
		foreach (array_keys($rawPayload) as $field) {
			if (!in_array($field, $allowedFields, false))
				unset($rawPayload[$field]);
		}

		$msg->setExtendedPayload($rawPayload);
	}

	/**
	 * @param array $data
	 * @param string $key
	 * @param bool $required
	 * @param string|null $defaultValue
	 * @return string|null
	 * @throws InvalidDataException
	 */
	private function getFieldString(array &$data, string $key, bool $required = true,
	                                string $defaultValue = null): ?string
	{
		return (string)$this->getField($data, $key, $required, $defaultValue);
	}

	/**
	 * @param array $data
	 * @param string $key
	 * @param bool $required
	 * @param int|null $defaultValue
	 * @return int|null
	 * @throws InvalidDataException
	 */
	private function getFieldInt(array &$data, string $key, bool $required = true, ?int $defaultValue = null): ?int
	{
		$value = $this->getField($data, $key, $required, $defaultValue);

		if ($value !== null && !is_int($value) && !ctype_digit($value))
			throw new InvalidDataException("Expected integer value for key '$key'", $key);

		if ($value !== null)
			return (int)$value;

		return $value;
	}

	private function getField(array &$data, string $key, bool $required = true, $defaultValue = null)
	{
		if (!isset($data[$key])) {
			if ($required)
				throw new InvalidDataException("Required '$key' key missing", $key);
			return $defaultValue;
		}

		return $data[$key];
	}
}