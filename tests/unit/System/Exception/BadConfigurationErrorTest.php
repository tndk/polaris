<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\System\Error\BadConfigurationError;

final class BadConfigurationErrorTest extends TestCase
{
	public function testExtend()
	{
		$this->assertInstanceOf(Error::class, new BadConfigurationError('something'));
	}

	public function testNewInstance()
	{
		$instance = new BadConfigurationError('something');
		$this->assertSame('something', $instance->getConfigName());
	}

	public function testDefaultMessage()
	{
		$instance = new BadConfigurationError('something');
		$this->assertSame('Missing or invalid configuration parameter: something', $instance->getMessage());
	}

	public function testNonEmptyMessage()
	{
		$instance = new BadConfigurationError('something else', 'Here is a message');
		$this->assertSame('Here is a message', $instance->getMessage());
	}
}