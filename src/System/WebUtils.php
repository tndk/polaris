<?php
declare(strict_types=1);

namespace Telenor\System;

use Amp\Http\Server\Response;

final class WebUtils
{
	public static function jsonResponseOK(string $message = 'Success', int $statusCode = 200, array $data = []): Response
	{
		return self::jsonResponse($message, 'OK', $statusCode, $data);
	}

	public static function jsonResponseClientError(string $message, int $statusCode = 400, array $data = []): Response
	{
		return self::jsonResponse($message, 'Client Error', $statusCode, $data);
	}

	public static function jsonResponseServerError(string $message = '', int $statusCode = 500, array $data = []): Response
	{
		return self::jsonResponse($message, 'Server Error', $statusCode, $data);
	}

	public static function jsonResponse(string $message, string $statusText, int $statusCode, array $data = []): Response
	{
		return new Response($statusCode, ['content-type => application/json'], json_encode(array_merge([
				'status'  => $statusText,
				'message' => $message,
			], $data)) . "\n");
	}
}