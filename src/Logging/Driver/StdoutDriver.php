<?php
declare(strict_types=1);
namespace Telenor\Logging\Driver;

use DateTime;
use Telenor\System\Config;
use Telenor\System\Utils;
use Throwable;
use function fopen;
use function fwrite;
use function str_pad;

class StdoutDriver extends ALogDriver
{
	protected $logStackTrace;
	protected $stream;

	public function __construct()
	{
		$this->logStackTrace = Config::envBool('LOG_STDOUT_STACKTRACE', false);
	}

	public function __destruct()
	{
		if ($this->stream !== null)
			fclose($this->stream);
	}

	public function log($level, $message, array $context = []): void
	{
		$this->writeToLog((new DateTime())->format('Y-m-d H:i:s.v') . str_pad(" [$level]", 13) . $message);

		foreach ($context as $item) {
			if ($item instanceof Throwable) {
				$this->writeToLog('. Exception (' . Utils::getShortClassName($item) . '): ' . $item->getMessage());
				$this->writeToLog(' at ' . $item->getFile() . ':' . $item->getLine());
				if ($this->logStackTrace) {
					$this->writeToLog("\nStacktrace:\n" . $item->getTraceAsString());
				}
			}
		}

		$this->writeToLog("\n");
	}

	protected function openStream(): void
	{
		$this->stream = fopen('php://stdout', 'wb');
	}

	protected function writeToLog(string $text): void
	{
		try {
			if (!$this->stream)
				$this->openStream();
			fwrite($this->stream, $text);
		} catch (Throwable $t) {
			echo $text;
		}
	}
}