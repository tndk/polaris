<?php namespace Telenor\Service;

use GuzzleHttp\Client;
use Psr\Log\LogLevel;
use Telenor\Logging\LoggingController;
use Telenor\Logging\TLog;
use Telenor\Model\AServiceNowModel;
use Telenor\System\Config;
use Telenor\System\Exception\InvalidRequestException;
use Telenor\System\Exception\PolarisException;
use function array_filter;
use function json_decode;

class ServiceNowTableClient
{
	use TLog;

	/** @var Client */
	protected $http;

	/** @var AServiceNowModel */
	protected $model;

	/**
	 * ServiceNowTableClient constructor.
	 *
	 * @param AServiceNowModel $model The model to use.
	 */
	public function __construct(AServiceNowModel $model)
	{
		$this->model = $model;

		$this->http = new Client([
			'base_uri'    => 'https://' . Config::envString('SERVICENOW_INSTANCE') . '.service-now.com/api/',
			'auth'        => [Config::envString('SERVICENOW_USERNAME'), Config::envString('SERVICENOW_PASSWORD')],
			'headers'     => ['Accept' => 'application/json', 'Content-Type' => 'application/json'],
			'http_errors' => false,
			'debug'       => LoggingController::getInstance()->getLogLevel() === LogLevel::DEBUG,
		]);
	}

	protected function getModel(): AServiceNowModel
	{
		return $this->model;
	}

	public function query(array $arguments = []): array
	{
		$response = $this->http->get($this->getModel()->getEndpoint(), ['query' => $arguments]);
		if ($response->getStatusCode() !== 200)
			throw new PolarisException('ServiceNow responded with code ' . $response->getStatusCode());

		$body = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
		$result = $body['result'];
		if (!is_array($result))
			$result = ['info' => $result];

		return $result;
	}

	/**
	 * @param string $id ID to search for.
	 * @param string[][] $arguments Query arguments
	 * @return array List of results
	 * @throws InvalidRequestException If invalid response returned from Service Now.
	 */
	public function get(string $id, array $arguments = []): array
	{
		$fields = $this->getModel()->getFields();

		$linkedTables = array_filter($fields, static function ($key) {
			return is_string($key);
		}, ARRAY_FILTER_USE_KEY);

		$filters = array_merge($this->getModel()->getFilters($id), ['sysparm_fields' => implode(',', $fields)]);

		$response = $this->http->get($this->getModel()->getEndpoint(), [
			'query' => array_merge($arguments, $filters),
		]);
		if ($response->getStatusCode() !== 200)
			throw new InvalidRequestException("Item with ID '$id' could not be found");

		$result = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR)['result'];

		#region Handle linked tables
		foreach ($linkedTables as $alias => $link) {
			foreach ($result as &$entry) {
				if (!isset($entry[$link]))
					continue;

				$response = $this->http->get($entry[$link]);
				if ($response->getStatusCode() !== 200)
					throw new InvalidRequestException("Failed to find linked entry for ID '$id'");

				$entry[$alias] = $response->getBody()->getContents();
			}
		}
		#endregion

		return $result;
	}

	/**
	 * @param string $id ID of the resource to update
	 * @param string[][] $fields Associative array of fields to update
	 * @return array Updated resource
	 */
	public function update(string $id, array $fields): array
	{
		// TODO: implement?
		return [];
	}
}