<?php
declare(strict_types=1);

namespace Telenor\Worker\QueryWorker;

use Amp\Http\Server\Response;
use Telenor\Logging\TLog;
use Telenor\Service\CategoriesService;
use Throwable;
use function json_encode;

class CategoriesController
{
	use TLog;

	/** @var CategoriesService */
	protected $categoriesService;

	/** @var string $data Simply read the categories at initialization because this does not change.. at least while using legacy remedy structure */
	protected $data = '{}';

	public function __construct(CategoriesService $categoriesService)
	{
		$this->categoriesService = $categoriesService;

		// Fetch the data from the yaml config file. It makes very little sense to convert array to json
		// on every request, so that is why we only do that once (during worker initialization).
		$this->data = json_encode($this->categoriesService->getCategories());
	}

	public function index(): Response
	{
		try {
			return new Response(200, ['Content-Type' => 'application/json'], json_encode($this->data));
		} catch (Throwable $t) {
			$this->logError('Failed to find groups', [$t]);
			return new Response(404, ['Content-Type' => 'application/json'], json_encode(['error' => 'Failed to find groups']));
		}
	}
}