<?php
declare(strict_types=1);

namespace Telenor\Worker\QueryWorker;

use Amp\Http\Server\Response;
use Telenor\Logging\TLog;
use Telenor\Service\GroupsService;
use Throwable;
use function json_encode;

class GroupsController
{
	use TLog;

	/** @var GroupsService */
	protected $groupsService;

	public function __construct(GroupsService $groupsService)
	{
		$this->groupsService = $groupsService;
	}

	public function index(): Response
	{
		try {
			return new Response(200, ['Content-Type' => 'application/json'], json_encode($this->groupsService->getGroups()));
		} catch (Throwable $t) {
			$this->logError('Failed to find groups', [$t]);
			return new Response(404, ['Content-Type' => 'application/json'], json_encode(['error' => 'Failed to find groups']));
		}
	}
}