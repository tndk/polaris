<?php
declare(strict_types=1);

namespace Telenor\Logging\Driver;

use Psr\Log\AbstractLogger;

abstract class ALogDriver extends AbstractLogger implements ILogDriver
{
}