<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\Message\TicketUpdateMessage;
use Telenor\System\Exception\InvalidMessageException;

class TicketUpdateMessageTest extends TestCase
{
	public function testCreationFailed1(): void
	{
		$this->expectException(InvalidMessageException::class);
		new TicketUpdateMessage([]);
	}

	public function testCreationFailed2(): void
	{
		$this->expectException(InvalidMessageException::class);
		new TicketUpdateMessage([
			'type'             => 'non sense',
			'transaction_id'   => '1234',
			'payload'          => [
				'worklog' => 'something',
				'status'  => 'Resolved',
			],
			'payload_original' => [
				'ExternalTicketId' => '4321',
			],
		]);
	}

	public function testCreationFailed3(): void
	{
		$this->expectException(InvalidMessageException::class);
		new TicketUpdateMessage([
			'type'             => 'incident',
			'transaction_id'   => '1234',
			'correlation_id'   => '1234',
			'payload'          => [
				'worklog' => 'something',
				'status'  => 'Some random status',
			],
			'payload_original' => [
				'ExternalTicketId' => '4321',
			],
		]);
	}

	public function testCreationOk1(): void
	{
		$msg = new TicketUpdateMessage([
			'type'               => 'incident',
			'transaction_id'     => '1234',
			'correlation_id'     => '1234',
			'request_id'         => '1234',
			'action'             => 'update',
			'source'             => 'veris',
			'timestamp'          => '2019-05-29T09:10:17+0100',
			'payload'            => [
				'worklog'           => 'something',
				'status'            => 'Resolved',
				'short_description' => 'hest',
				'description'       => 'hest',
				'assigned_to'       => 'HEIP',
				'created'           => '2019-05-29T09:10:17+0100',
				'cis'               => [],
			],
			'payload_original'   => [
				'ExternalTicketId' => '4321',
			],
			'payload_attachment' => [],
		]);

		static::assertInstanceOf(TicketUpdateMessage::class, $msg);
	}
}
