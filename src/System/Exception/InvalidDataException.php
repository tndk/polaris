<?php
declare(strict_types=1);
namespace Telenor\System\Exception;

use Throwable;

/**
 * Exception to use when a connection problem occurs.
 */
class InvalidDataException extends PolarisException
{
	protected $entry;

	public function __construct(string $message, string $entry = null, Throwable $previous = null)
	{
		parent::__construct($message, $previous);
		$this->entry = $entry;
	}

	public function getEntry(): ?string
	{
		return $this->entry;
	}
}