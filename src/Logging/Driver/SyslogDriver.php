<?php
declare(strict_types=1);
namespace Telenor\Logging\Driver;

class SyslogDriver extends ALogDriver
{
	public function log($level, $message, array $context = []): void
	{
		syslog($level, $message);
	}
}