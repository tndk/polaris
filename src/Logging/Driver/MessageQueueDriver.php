<?php
declare(strict_types=1);
namespace Telenor\Logging\Driver;

use Psr\Log\LogLevel;
use Telenor\Logging\LoggingController;
use Telenor\Message\LogMessage;
use Telenor\MessageQueue\MessageQueueController;
use Telenor\System\Config;
use Telenor\System\Exception\ConnectionException;
use Telenor\System\Exception\OverflowException;
use Telenor\System\Utils;
use Throwable;
use function is_array;
use function is_object;
use function is_resource;
use function is_scalar;
use function method_exists;

class MessageQueueDriver extends ALogDriver
{
	public const LOG_QUEUE_NAME = 'LOG_QUEUE_NAME';
	public const LOG_QUEUE_BUFFER_SIZE = 'LOG_QUEUE_BUFFER_SIZE';
	public const MAX_LOG_LENGTH = 786432;

	/** @var string */
	protected $queueName;

	/** @var int */
	protected $bufferSize;

	protected $started = false;

	/** @var LogMessage[] */
	protected $bufferedMessages = [];

	protected $fallback;

	protected $messageQueueControllerConfigured = false;

	public function __construct()
	{
		$this->queueName = Config::envString(self::LOG_QUEUE_NAME, 'pol.log');
		$this->bufferSize = Config::envInt(self::LOG_QUEUE_BUFFER_SIZE, 10000);
		$this->fallback = LoggingController::getInstance()->getFallbackDriver();
	}

	public function log($level, $message, array $context = []): void
	{
		if (!$this->messageQueueControllerConfigured) {
			$this->messageQueueControllerConfigured = true;
			MessageQueueController::getInstance()->registerOnConnectionChanged(function ($connected) {
				if ($connected)
					$this->emptyBuffer();
			});
		}

		/*
		 * Ensure that complex types will be logged. If whole exceptions are logged to
		 * the queue, it will fail if the complex type contains any resource (or any
		 * non-serializable type) or is not stringable.
		 */
		$additionalInfo = $this->convertLogContextRecursively($context);

		$this->sendToQueue(new LogMessage($level, $message, $additionalInfo));
	}

	protected function convertLogContextRecursively(array $source): array
	{
		# TOOD: If $source['exception'] exists and is not an array, it should convert it into an array (fixes a problem for indexing in elasticsearch)

		$target = [];
		foreach ($source as $key => $value) {
			if ($value instanceof Throwable) {
				$target[$key] = [
					'exception' => [
						'type'       => Utils::getShortClassName($value),
						'message'    => $value->getMessage(),
						'stacktrace' => $value->getTraceAsString(),
					],
				];
			} else if (is_string($value)) {
				$target[$key] = Utils::truncateString($value, self::MAX_LOG_LENGTH);
			} else if (is_scalar($value)) {
				$target[$key] = $value;
			} else if (is_object($value) && method_exists($value, '__toString')) {
				$target[$key] = Utils::truncateString((string)$value, self::MAX_LOG_LENGTH);
			} else if (is_array($value)) {
				$target[$key] = $this->convertLogContextRecursively($value);
			} else if (is_resource($value)) {
				$target[$key] = '(resource)';
			} else if ($value === null) {
				$target[$key] = '(null)';
			} else {
				$target[$key] = '(unloggable complex type: ' . Utils::getShortClassName($value) . ')';
			}
		}
		return $target;
	}

	protected function emptyBuffer(): void
	{
		if (empty($this->bufferedMessages))
			return;

		try {
			$logMsg = new LogMessage(LogLevel::NOTICE,
				'Flushing message queue log buffer (size: ' . count($this->bufferedMessages) . ')');
			$this->fallback->log($logMsg->getLevel(), $logMsg->getMessage(), $logMsg->getContext());
			$this->sendToQueue($logMsg);

			foreach ($this->bufferedMessages as $key => &$bufferedMessage) {
				$this->sendToQueue($bufferedMessage);
				unset($this->bufferedMessages[$key]);
			}
			unset($bufferedMessage);

			if (empty($this->bufferedMessages)) {
				$logMsg = new LogMessage(LogLevel::NOTICE, 'Message queue log buffer empty');
				$this->fallback->log($logMsg->getLevel(), $logMsg->getMessage(), $logMsg->getContext());
				$this->sendToQueue($logMsg);
			}
		} catch (ConnectionException $e) {
		}
	}

	/**
	 * @param LogMessage $message
	 * @throws ConnectionException
	 * @throws OverflowException
	 */
	protected function sendToQueue(LogMessage $message): void
	{
		try {
			if ($this->started && !MessageQueueController::getInstance()->isConnected())
				throw new ConnectionException('Not connected');

			$this->started = true;
			MessageQueueController::getInstance()->publish($message, $this->queueName);
		} catch (ConnectionException $e) {

			if ($message->getBuffered())
				throw $e;

			if (empty($this->bufferedMessages)) {
				$logMsg = new LogMessage(LogLevel::WARNING, 'Started to buffer message queue logs');
				$this->bufferedMessages[] = $logMsg;
				$this->fallback->log($logMsg->getLevel(), $logMsg->getMessage(), $logMsg->getContext());
			}

			$message->setBuffered(true);
			$message->addContext('exception', $e->getMessage());
			if ($e->getPrevious() !== null)
				$message->addContext('prev_exception', $e->getPrevious()->getMessage());
			$this->bufferedMessages[] = $message;

			if (count($this->bufferedMessages) > $this->bufferSize)
				throw new OverflowException('Message queue log buffer size exceeded', $e);
		}
	}
}