<?php
declare(strict_types=1);
namespace Telenor\Worker\XMLGatewayWorker;

use Amp\Failure;
use Amp\Http\Server\Request;
use Amp\Http\Server\RequestHandler;
use Amp\Http\Server\Response;
use Amp\Promise;
use Exception;
use Generator;
use Telenor\Logging\TLog;
use Telenor\MessageQueue\TMessageQueue;
use Telenor\System\Config;
use Telenor\System\Exception\InvalidRequestException;
use Telenor\System\Exception\UnknownTemplateException;
use Telenor\System\GlobalStorage;
use Telenor\System\Utils;
use Telenor\System\WebUtils;
use function Amp\call;
use function implode;
use function in_array;
use function str_replace;

class XMLGatewayController implements RequestHandler
{
	use TLog;
	use TMessageQueue;

	/**
	 * @var TicketMapper Message builder
	 */
	protected $messageBuilder;

	protected $whitelistedIPs = [];

	public function __construct(TicketMapper $messageBuilder)
	{
		$this->messageBuilder = $messageBuilder;

		$whitelistConf = Config::envString('WHITELISTED_IPS', '');
		if (!Utils::isStringNullOrEmpty($whitelistConf)) {
			$whitelistConf = str_replace([' ', ','], ['', ';'], $whitelistConf);
			$this->whitelistedIPs = explode(';', $whitelistConf);
			$this->logInfo('IP whitelisting enabled. Allowed IPs: ' . implode(', ', $this->whitelistedIPs));
		}
	}

	public function handleRequest(Request $request, array $params = []): Promise
	{
		#if (isset($params['method']) && $params['method'] === 'query.do') {
		#	return call([$this, 'handleQuery'], $request);
		#}
		return call([$this, 'handle'], $request);
	}

	#public function handleQuery(Request $request)
	#{
	#	$body = yield $request->getBody()->buffer();

	#	$raw = sprintf("Request headers:\n> %s %s HTTP/%s\n", $request->getMethod(), $request->getUri(), $request->getProtocolVersion());
	#	foreach ($request->getHeaders() as $header => $value) {
	#		$val = implode(';', $value);
	#		$raw .= "> $header: $val\n";
	#	}
	#	$raw .= "\n$body";
	#	$this->logDebug($raw);

	#	$parsed = XMLParser::queryToArray($body);

	#	return new Response(200, ['Content-Type' => 'text/json'], json_encode($parsed, JSON_UNESCAPED_SLASHES));
	#}

	/**
	 * Determines type of body in request (from header), parses the body into an
	 * array, and passes that to be handled by the MessageBuilder.
	 *
	 * @param Request $request
	 * @return Failure|Response|Generator
	 */
	public function handle(Request $request)
	{
		if (!empty($this->whitelistedIPs)) {
			$sourceAddr = $request->getClient()->getLocalAddress();
			$this->logDebug("Checking IP whitelist. IP address: $sourceAddr");
			if (!in_array($sourceAddr, $this->whitelistedIPs, true)) {
				$this->logWarning("Source IP is not allowed to send requests: $sourceAddr", [
					'sourceAddr' => $sourceAddr,
				]);
				return WebUtils::jsonResponseClientError('Not allowed', 403);
			}
		}

		$body = yield $request->getBody()->buffer();

		// TODO: remove or log properply
		$raw = sprintf("Request headers:\n> %s %s HTTP/%s\n", $request->getMethod(), $request->getUri(),
			$request->getProtocolVersion());
		foreach ($request->getHeaders() as $header => $value) {
			$val = implode(';', $value);
			$raw .= "> $header: $val\n";
		}
		$raw .= "\n$body";
		$this->logDebug($raw);

		// Determine type of request (params/XML/SOAP)
		[$contentType] = explode(';', $request->getHeader('Content-Type'), 2);

		if ($contentType === 'text/xml' || $contentType === 'application/xml') {
			$this->logDebug('Detected SOAP request, parsing...');
			try {
				$parsed = XMLParser::soapToArray($body);
			} catch (InvalidRequestException $e) {
				$this->logError($e->getMessage());
				return new Response(400, ['Content-Type' => 'text/json'],
					json_encode(['error' => 'Invalid XML input']));
			}
		} else if ($contentType === 'application/x-www-form-urlencoded') {
			$this->logDebug('Detected URL-encoded request, parsing...');
			try {
				$parsed = XMLParser::paramsToArray($body);
			} catch (Exception $e) {
				$this->logError($e->getMessage());
				return new Response(400, ['Content-Type' => 'text/json'], json_encode(['error' => 'Invalid input']));
			}
		} else {
			$uuid = GlobalStorage::getUUID();
			$this->logError("Unable to parse request $uuid: Content-Type '$contentType' not supported.");
			return new Response(501, ['Content-Type' => 'text/json'],
				json_encode(['error' => 'Content-Type not supported']));
		}

		return $this->handleParsedRequest($parsed);
	}

	private function handleParsedRequest(array $parsed): Response
	{
		if (isset($parsed['action']) && $parsed['action'] === 'query') {
			$responseBody = json_encode($parsed, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
			return new Response(200, ['Content-Type' => 'text/xml'], $responseBody);
		}

		try {
			$msg = $this->messageBuilder->build($parsed);
		} catch (UnknownTemplateException $e) {
			$this->logError("Failed to build message: {$e->getMessage()}");
			return new Response(501, ['Content-Type' => 'text/json'], json_encode(['error' => 'Unknown template']));
		}

		try {
			$this->publishToQueue($msg);
		} catch (Exception $e) {
			$this->logError("Failed to send message: {$e->getMessage()}");
			return new Response(502, ['Content-Type' => 'text/json'],
				json_encode(['error' => 'No connection to Polaris message queue']));
		}

		$responseBody = XMLParser::makeResponse($parsed);
		$jsonMessage = json_encode($msg->getAttributes(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
		$this->logDebug(sprintf("Publishing message for request '%s':\n%s", $msg->getRequestId(), $jsonMessage));

		return new Response(200, ['Content-Type' => 'text/xml'], $responseBody);
	}
}