<?php
declare(strict_types=1);

namespace Telenor\MessageQueue;

use Telenor\Message\IMessage;

interface IMessageExpiredHandler
{
	public function messageExpired(IMessage $message, array $metaData = null): bool;
}