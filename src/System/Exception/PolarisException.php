<?php
declare(strict_types=1);

namespace Telenor\System\Exception;

use Exception;
use Throwable;

class PolarisException extends Exception
{
	public function __construct(string $message, Throwable $previous = null)
	{
		parent::__construct($message, 0, $previous);
	}
}