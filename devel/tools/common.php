<?php
declare(strict_types=1);

define('ROOT_DIR', dirname(__DIR__) . '/..');

define('DOCKER_DIR', ROOT_DIR . '/docker');
define('SCRIPT_NAME', basename($argv[0]));

define('DOCKER_COMPOSE_FILE', ROOT_DIR . '/docker-compose.yml');
define('DOCKER_COMPOSE_OVERRIDE_FILE', ROOT_DIR . '/docker-compose.override.yml');

//define('REGISTRY_HOST_API', 'nexus.iux.sonofon.dk:8081');
define('REGISTRY_HOST_API', 'devtools.iux.sonofon.dk/nexus3');
define('PORTAINER_USERNAME', 'deploy');
define('PORTAINER_PASSWORD', 'Telenor01');
define('PORTAINER_STACK_NAME', 'polaris');
define('PORTAINER_ENDPOINT_ID', 1);

define('DEBUG', isEnvSet('DEBUG'));

require ROOT_DIR . '/vendor/autoload.php';

use GuzzleHttp\Client;
use Symfony\Component\Yaml\Yaml;

function isEnvSet(string $name): bool
{
	return (preg_match('#^true|1$#i', (string)getenv($name)) === 1);
}

function setEnv(string $name, string $value, bool $overrideIfExists = false): void
{
	if (!$overrideIfExists && getenv($name))
		return;

	putenv(sprintf('%s=%s', $name, $value));
}

function colorPrint(string $message, string $color): void
{
	echo "\e[${color}m${message}\e[0m\n";
}

function debug(string $fmt, ...$args): void
{
	if (DEBUG)
		printf('> ' . $fmt . "\n", ... $args);
}

function error(string $message, int $exitCode = 1): void
{
	colorPrint("ERROR: $message", '0;31');
	exit($exitCode);
}

function warning(string $message): void
{
	colorPrint("WARNING: $message", '1;33');
}

function notice(string $message): void
{
	colorPrint($message, '1;37');
}

function parseDockerCompose(): array
{
	$compose = Yaml::parseFile(DOCKER_COMPOSE_FILE);

	if (file_exists(DOCKER_COMPOSE_OVERRIDE_FILE)) {
		$composeOverride = Yaml::parseFile(DOCKER_COMPOSE_OVERRIDE_FILE);
		$composeCombined = array_merge_recursive($compose, $composeOverride);
	} else {
		$composeOverride = [];
		$composeCombined = $compose;
	}
	return [$compose, $composeOverride, $composeCombined];
}

function pushToPortainer(string $portainerBaseUrl, string $registryHost): void
{
	[$compose] = parseDockerCompose();

	#region API Authentication
	$client = new Client([
		'http_errors' => false,
		'verify'      => false,
		'base_uri'    => $portainerBaseUrl,
	]);
	$response = $client->post('api/auth', [
		'body' => json_encode(['username' => PORTAINER_USERNAME, 'password' => PORTAINER_PASSWORD],
			JSON_THROW_ON_ERROR),
	]);

	if ($response->getStatusCode() !== 200) {
		warning('Failed to authenticate towards portainer - please deploy docker-compose manually');
		warning('Response: ' . $response->getBody());
		return;
	}

	$token = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR)['jwt'];
	debug('Got authentication token: %s', $token);

	// Set new default options, now that we have a auth token to put on all future requests
	$client = new Client(array_merge($client->getConfig(), [
		'headers' => [
			'Content-Type'  => 'application/json',
			'Authorization' => "Bearer $token",
		],
	]));
	#endregion

	#region Ensure network existence
	if (isset($compose['networks'])) {

		// Fetch the external networks in the docker-compose file
		$externalNetworks = [];
		foreach ($compose['networks'] as $networkName => $composeNetwork) {
			if (isset($composeNetwork['external']) && $composeNetwork['external'])
				$externalNetworks[] = $networkName;
		}

		// Fetch the networks configured in the swarm
		$response = $client->get('api/endpoints/' . PORTAINER_ENDPOINT_ID . '/docker/networks');

		if ($response->getStatusCode() !== 200) {
			warning('Failed to fetch configured Docker networks - please deploy docker-compose manually');
			warning('Response: ' . $response->getBody());
			return;
		}
		$networks = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
		$existingNetworks = [];
		foreach ($networks as $network) {
			if (!in_array($network['Name'], $existingNetworks, true))
				$existingNetworks[] = $network['Name'];
		}

		// Check if we need to create some of the swarm network(s)
		foreach ($externalNetworks as $externalNetwork) {
			if (in_array($externalNetwork, $existingNetworks, true))
				continue;

			debug("Network not found in the swarm: $externalNetwork - creating now");

			$body = [
				'Driver' => 'overlay',
				'Name'   => $externalNetwork,
			];
			$response = $client->post('api/endpoints/' . PORTAINER_ENDPOINT_ID . '/docker/networks/create', [
				'body' => json_encode($body, JSON_THROW_ON_ERROR),
			]);
			debug('Got response: (%d): %s', $response->getStatusCode(), $response->getBody()->getContents());
			if ($response->getStatusCode() !== 201) {
				warning("Failed to create swarm network: $externalNetwork - please deploy docker-compose manually");
				warning('Response: ' . $response->getBody());
				return;
			}
		}
	}
	#endregion

	#region Get stack information
	$response = $client->get('api/stacks');
	if ($response->getStatusCode() !== 200) {
		warning('Failed to fetch Docker stacks - please deploy docker-compose manually');
		warning('Response: ' . $response->getBody());
		return;
	}

	$stackId = null;
	$stacks = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
	foreach ($stacks as $stack) {
		if (strtolower($stack['Name']) === PORTAINER_STACK_NAME)
			$stackId = $stack['Id'];
	}
	#endregion

	// The stack is removed completely in order to ensure that ALL containers
	// is being taken down and started correctly up again with the new release

	#region Remove stack
	if ($stackId !== null) {
		debug('Stack already exists with ID %d - removing..', $stackId);
		$response = $client->delete("api/stacks/$stackId");
		debug('Got response: %s', $response->getBody()->getContents());
		if ($response->getStatusCode() !== 204) {
			warning('Failed to remove Docker stack - please deploy docker-compose manually');
			warning('Response: ' . $response->getBody());
			return;
		}

		debug('Successfully removed stack');
	}
	#endregion

	#region Create the new stack
	debug('Finding Swarm ID');
	$response = $client->get('api/endpoints/' . PORTAINER_ENDPOINT_ID . '/docker/swarm');
	if ($response->getStatusCode() !== 200) {
		warning('Failed to fetch Docker swarm information - please deploy docker-compose manually');
		warning('Response: ' . $response->getBody());
		return;
	}

	$swarmID = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR)['ID'];
	debug('Got swarm id: %s', $swarmID);

	$body = [
		'StackFileContent' => file_get_contents(DOCKER_COMPOSE_FILE),
		'Env'              => [['name' => 'REGISTRY_HOST', 'value' => $registryHost]],
		'Name'             => PORTAINER_STACK_NAME,
		'SwarmID'          => $swarmID,
	];
	$response = $client->post('api/stacks?endpointId=' . PORTAINER_ENDPOINT_ID . '&type=1&method=string', [
		'body' => json_encode($body, JSON_THROW_ON_ERROR),
	]);
	if ($response->getStatusCode() !== 200) {
		warning('Failed to create stack in portainer - please deploy docker-compose manually');
		warning('Response: ' . $response->getBody());
		return;
	}

	debug('Successfully deployed stack: %s', PORTAINER_STACK_NAME);
	#endregion
}

function pushToRegistry(array $services, string $registryHost): void
{
	global $compose;

	foreach ($services as $service) {
		$image = str_replace('${REGISTRY_HOST}', $registryHost, $compose['services'][$service]['image']);
		debug('Pushing image: %s', $image);
		cmd("docker push '%s'", $image) or error("Failed to push service: $service");
	}
}

function checkRegistryVersion(string $host, string $repository, string $imageName, string $version): bool
{
	$registryClient = new Client(['base_uri' => "http://$host/service/rest/v1/search", 'verify' => false]);

	$repoQuery = sprintf('?repository=%s&name=%s&version=%s', $repository, $imageName, $version);
	debug('Checking if version already exists in repo. Query: %s%s', $registryClient->getConfig('base_uri'),
		$repoQuery);

	$result = $registryClient->get($repoQuery);

	if ($result->getStatusCode() !== 200)
		error('Registry API returned wrong status code: ' . $result->getStatusCode());

	$jsonResult = json_decode($result->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
	if (!empty($jsonResult['items']))
		return true;

	return false;
}

function buildDockerImage(string $dockerfile, array $tags = [], array $buildArgs = []): bool
{
	!file_exists($dockerfile) and error("Dockerfile does not exist: $dockerfile");
	(empty($tags)) and error("No tags specified for $dockerfile");

	# If the Dockerfile is not extending our polaris-base image, we need to set the build path
	# to the docker image directory, in order to be able do any COPY operations within Dockerfile
	$isPolarisImage = false;
	foreach (file($dockerfile) as $line)
		$isPolarisImage |= preg_match('#^FROM\s+polaris#', $line);
	$buildPath = $isPolarisImage ? ROOT_DIR : dirname($dockerfile);

	$buildCmd = 'docker build';
	foreach ($tags as $tag)
		$buildCmd .= sprintf(' --tag %s', $tag);
	foreach ($buildArgs as $buildArgName => $buildArgValue)
		$buildCmd .= sprintf(' --build-arg %s="%s"', $buildArgName, $buildArgValue);
	$buildCmd .= sprintf(' -f %s %s', $dockerfile, $buildPath);

	return cmd($buildCmd) !== false;
}

function cmd(string $fmt, ...$args)
{
	$cmd = sprintf($fmt, ... $args);
	debug('Executing command: %s', $cmd);

	$h = popen($cmd, 'r');
	$output = '';
	while (!feof($h)) {
		$line = fread($h, 2048);
		debug($line);
		$output .= $line;
	}
	$output = trim($output);

	if (pclose($h) !== 0)
		return false;

	return empty($output) ? true : $output;
}

function extractImageAttributes(string $image): array
{
	if (!preg_match('#^.*/(.*):(.*)$#U', $image, $matches))
		error("Invalid image name format found in docker-compose file: $image");

	[, $imageName, $imageVersion] = $matches;
	debug('Extracted image name: %s', $imageName);
	debug('Extracted image version: %s', $imageVersion);

	return [$imageName, $imageVersion];
}

function runUnitTests(): bool
{
	return cmd(ROOT_DIR . '/vendor/bin/phpunit --stderr -c ' . ROOT_DIR . '/tests/unit/phpunit.xml 2> /dev/null') !== false;
}

function getGitInfo(): array
{
	$gitName = cmd('git config --get user.name') or error('No git name found.');
	$gitEmail = cmd('git config --get user.email') or error('No git e-mail found.');

	debug('Found git name: %s', $gitName);
	debug('Found git e-mail: %s', $gitEmail);

	return [$gitName, $gitEmail];
}
