<?php
declare(strict_types=1);

namespace Telenor\Message;

use InvalidArgumentException;
use Telenor\System\Utils;

abstract class APayloadMessage extends AMessage
{
	public const ACTION_UPDATE = 'update';
	public const ACTION_CREATE = 'create';
	public const ACTION_DELETE = 'delete';

	/** @var string */
	protected $action;

	/** @var string */
	protected $type;

	/** @var string */
	protected $transactionId;

	/** @var string */
	protected $worker;

	public function __construct(string $type, string $action)
	{
		parent::__construct();
		$this->setType($type);
		$this->setAction($action);
		$this->transactionId = Utils::generateUUID();
		$this->worker = Utils::getShortWorkerName();
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type): void
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getAction(): string
	{
		return $this->action;
	}

	public function getTransactionId(): ?string
	{
		return $this->transactionId;
	}

	/**
	 * @return string
	 */
	public function getWorker(): string
	{
		return $this->worker;
	}

	public function setAction(string $action): void
	{
		if ($action !== self::ACTION_UPDATE && $action !== self::ACTION_CREATE && $action !== self::ACTION_DELETE) {
			throw new InvalidArgumentException('Invalid action option');
		}

		$this->action = $action;
	}

	public function getAttributes(): array
	{
		return array_merge(parent::getAttributes(), [
			'transaction_id' => $this->getTransactionId(),
			'action'         => $this->getAction(),
			'type'           => $this->getType(),
			'worker'         => $this->getWorker(),
			'payload'        => $this->toArray(),
		]);
	}

	abstract public function toArray(): array;
}