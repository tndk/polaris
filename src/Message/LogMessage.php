<?php
declare(strict_types=1);

namespace Telenor\Message;

use Telenor\System\Utils;
use function gethostname;

class LogMessage extends AMessage
{
	/** @var string */
	protected $level;

	/** @var string */
	protected $message;

	/** @var array */
	protected $context;

	/** @var string */
	protected $worker;

	/** @var string */
	protected $hostname;

	/** @var bool */
	protected $buffered = false;

	public function __construct(string $level, string $message, array $context = [])
	{
		parent::__construct();
		$this->level = $level;
		$this->message = $message;
		$this->context = $context;
		$this->worker = Utils::getShortWorkerName();
		$this->hostname = gethostname();
	}

	public function getLevel(): string
	{
		return $this->level;
	}

	public function getMessage(): string
	{
		return $this->message;
	}

	public function getContext(): array
	{
		return $this->context;
	}

	public function addContext(string $key, ?string $value): void
	{
		$this->context[$key] = $value;
	}

	public function getWorker(): string
	{
		return $this->worker;
	}

	public function getHostname(): string
	{
		return $this->hostname;
	}

	public function getBuffered(): bool
	{
		return $this->buffered;
	}

	public function setBuffered(bool $buffered): void
	{
		$this->buffered = $buffered;
	}

	public function getAttributes(): array
	{
		return array_merge(parent::getAttributes(), [
			'level'    => $this->getLevel(),
			'message'  => $this->getMessage(),
			'worker'   => $this->getWorker(),
			'hostname' => $this->getHostname(),
			'buffered' => $this->getBuffered(),
			'context'  => $this->getContext(),
		]);
	}
}