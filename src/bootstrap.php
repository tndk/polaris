<?php

use Psr\Container\ContainerInterface;
use Telenor\System\Config;
use Telenor\Worker\DummyMessageQueueWorker\DummyMessageQueueWorker;
use Telenor\Worker\DummyWebWorker\DummyWebWorker;
use Telenor\Worker\NotificationWorker\NotificationWorker;
use Telenor\Worker\QueryWorker\QueryWorker;
use Telenor\Worker\SMSWorker\SMSWorker;
use Telenor\Worker\TicketNotificationWorker\TicketNotificationWorker;
use Telenor\Worker\TicketUpdateWorker\TicketUpdateWorker;
use Telenor\Worker\TicketWorker\TicketWorker;
use Telenor\Worker\WorkdayWorker\WorkdayWorker;
use Telenor\Worker\XMLGatewayWorker\XMLGatewayWorker;
use function DI\autowire;
use function DI\create;
use function DI\env;

const WORKER_CLASS_CONFIG = 'WORKER_CLASS';

return [
	// General
	Config::class                   => create(Config::class),
	WORKER_CLASS_CONFIG             => env(WORKER_CLASS_CONFIG),

	// XMLGateway worker
	XMLGatewayWorker::class         => autowire(XMLGatewayWorker::class),

	// Ticket worker
	TicketWorker::class             => autowire(TicketWorker::class),

	// TicketUpdate worker
	TicketUpdateWorker::class       => autowire(TicketUpdateWorker::class),

	// TicketNotification worker
	TicketNotificationWorker::class => autowire(TicketNotificationWorker::class),

	//SMS worker
	SMSWorker::class                => autowire(SMSWorker::class),

	// Notification worker
	NotificationWorker::class       => autowire(NotificationWorker::class),

	// Workday worker
	WorkdayWorker::class            => autowire(WorkdayWorker::class),

	// Query worker
	QueryWorker::class              => autowire(QueryWorker::class),

	// Ref to worker instance
	'worker'                        => static function (ContainerInterface $c) {
		$workerClass = $c->get(WORKER_CLASS_CONFIG);
		return $c->get($workerClass);
	},
];
