<?php
declare(strict_types=1);

namespace Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler;

use DateTime;
use Exception;
use GuzzleHttp\Client;
use Telenor\Logging\TLog;
use Telenor\Message\TicketUpdateMessage;
use Telenor\System\Config;
use function explode;
use function random_int;
use function sprintf;
use function str_replace;
use function strpos;

class VerisTicketUpdateHandler implements ITicketUpdateHandler
{
	use TLog;

	/** @var string */
	protected $verisHost;

	/** @var string */
	protected $verisReplyEndpoint;

	/** @var string */
	protected $verisNotifyEndpoint;

	/** @var string */
	protected $verisAppKey;

	/** @var string */
	protected $verisChannelType;

	/** @var string */
	protected $verisTenantId;

	/** @var Client */
	protected $client;

	public function __construct()
	{
		$this->verisHost = Config::envString('VERIS_HOST');
		$this->verisReplyEndpoint = Config::envString('VERIS_REPLY_ENDPOINT', '/');
		$this->verisNotifyEndpoint = Config::envString('VERIS_NOTIFY_ENDPOINT', '/');
		$this->verisAppKey = Config::envString('VERIS_APP_KEY', '8000310001');
		$this->verisChannelType = Config::envString('VERIS_CHANNEL_TYPE', 'EXT284');
		$this->verisTenantId = Config::envString('VERIS_TENANT_ID', '21');

		$this->logInfo('Configured veris host: ' . $this->verisHost);
		$this->logInfo('Configured veris reply endpoint: ' . $this->verisReplyEndpoint);
		$this->logInfo('Configured veris notify endpoint: ' . $this->verisNotifyEndpoint);
		$this->logInfo('Configured veris app key: ' . $this->verisAppKey);
		$this->logInfo('Configured veris channel type: ' . $this->verisChannelType);
		$this->logInfo('Configured veris tenant id: ' . $this->verisTenantId);

		$this->client = new Client(['base_uri' => $this->verisHost]);
	}

	public function handleMessage(TicketUpdateMessage $message): bool
	{

		// Veris can send both tenant id and veris id in the external ticket id field separated with a |
		// We already take chare of this in the XMLGatewayWorker\XMLParser class, but we should
		// handle the same scenario here as well.
		// We simply split at the first | here and uses the second value. 12|1234 becomes 1234
		$externalTicketId = $message->getExtendedPayload()['ExternalTicketId'] ?? '';

		if (strpos($externalTicketId, '|'))
			[, $externalTicketId] = explode('|', $externalTicketId, 2);

		$description = $message->getShortDescription();
		$worknotes = $message->getWorknotes();

		switch ($message->getStatus()) {
			case TicketUpdateMessage::STATUS_RESOLVED:
			case TicketUpdateMessage::STATUS_PENDING_VENDOR:
				return $this->sendReplyRequest($externalTicketId, 'RESOLVED', $description, $worknotes);
			case TicketUpdateMessage::STATUS_IN_PROGRESS:
				return $this->sendNotifyRequest($externalTicketId, 'WORKINPROGRESS', $description, $worknotes);
			default:
				$this->logInfo('Rejecting to handle message due to ticket update status: ' . $message->getStatus(), [
					'message' => $message,
				]);
				return true;
		}
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	protected function generateTransactionId(): string
	{
		return sprintf('%015d', random_int(0, 999999999999999));
	}

	protected function generateRequestTime(): string
	{
		return sprintf('%014d', (new DateTime())->format('YmdHis'));
	}

	protected function escape(string $data): string
	{
		return str_replace(['&', '<', '>'], ['&amp;', '&lt;', '&gt;'], $data);
	}

	protected function sendReplyRequest(string $serialNo, string $status, string $description, string $worknotes): bool
	{
		$transactionId = $this->generateTransactionId();
		$reqTime = $this->generateRequestTime();
		$worklogField = empty($worknotes) ? '<Worklog/>' : '<Worklog>' . $this->escape($worknotes) . '</Worklog>';
		$description = $this->escape($description);
		$body = <<<EOD
<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<S:Header xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
		<ns2:RequestHeader xmlns:ns2="http://soa.ailk.telenor.com/wsdl/soa">
			<AppKey>{$this->verisAppKey}</AppKey>
			<TransactionID>$transactionId</TransactionID>
			<ReqTime>$reqTime</ReqTime>
			<Version>1</Version>
			<TenantId>{$this->verisTenantId}</TenantId>
			<AcceptChannelType>{$this->verisChannelType}</AcceptChannelType>
			<AcceptStaffId>{$this->verisTenantId}{$this->verisChannelType}</AcceptStaffId>
		</ns2:RequestHeader>
	</S:Header>
	<S:Body xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
		<ns2:replyRequest xmlns:ns2="http://soa.ailk.telenor.com/wsdl/soa">
			<RequestBody>
				<Reply>
					<SerialNo>$serialNo</SerialNo>
					$worklogField
					<Status>$status</Status>
					<Description>$description</Description>
				</Reply>
			</RequestBody>
		</ns2:replyRequest>
	</S:Body>
</soapenv:Envelope>
EOD;
		return $this->sendRequest('Reply', $this->verisReplyEndpoint, $body);
	}

	protected function sendNotifyRequest(string $serialNo, string $status, string $description, string $worknotes): bool
	{
		$transactionId = $this->generateTransactionId();
		$reqTime = $this->generateRequestTime();
		$worklogField = empty($worknotes) ? '<Worklog/>' : '<Worklog>' . $this->escape($worknotes) . '</Worklog>';
		$description = $this->escape($description);
		$body = <<<EOD
<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<S:Header xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
		<ns2:RequestHeader xmlns:ns2="http://soa.ailk.telenor.com/wsdl/soa">
			<AppKey>{$this->verisAppKey}</AppKey>
			<TransactionID>$transactionId</TransactionID>
			<ReqTime>$reqTime</ReqTime>
			<Version>1</Version>
			<TenantId>{$this->verisTenantId}</TenantId>
			<AcceptChannelType>{$this->verisChannelType}</AcceptChannelType>
			<AcceptStaffId>{$this->verisTenantId}{$this->verisChannelType}</AcceptStaffId>
		</ns2:RequestHeader>
	</S:Header>
	<S:Body xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
		<ns2:notifyRequest xmlns:ns2="http://soa.ailk.telenor.com/wsdl/soa">
			<RequestBody>
				<Notify>
					<SerialNo>$serialNo</SerialNo>
					$worklogField
					<Status>$status</Status>
					<Description>$description</Description>
				</Notify>
			</RequestBody>
		</ns2:notifyRequest>
	</S:Body>
</soapenv:Envelope>
EOD;
		return $this->sendRequest('Notify', $this->verisNotifyEndpoint, $body);
	}

	protected function sendRequest(string $verisRequestType, string $endpoint, string $body): bool
	{
		$headers = [
			'Content-Type' => 'application/soap+xml; charset=utf-8',
			'SOAPAction'   => 'http://schemas.xmlsoap.org/soap/envelope/',
		];

		$this->logInfo("Sending $verisRequestType request to Veris", [
			'request' => [
				'host'     => $this->verisHost,
				'endpoint' => $endpoint,
				'headers'  => $headers,
				'body'     => $body,
			],
		]);

		$response = $this->client->post($endpoint, [
			'verify'  => false,
			'headers' => $headers,
			'body'    => $body,
		]);

		$additionalLogData = [
			'response' => [
				'headers' => $response->getHeaders(),
				'code'    => $response->getStatusCode(),
				'body'    => $response->getBody()->getContents(),
			],
		];

		$expectedReturnStatus = "<{$verisRequestType}Return>0</{$verisRequestType}Return>";

		if ($response->getStatusCode() !== 200 || !strpos((string)$response->getBody(), $expectedReturnStatus)) {
			$this->logWarning("Failed to post $verisRequestType request to Veris", $additionalLogData);
			return false;
		}

		$this->logInfo("Successfully posted $verisRequestType request to Veris", $additionalLogData);
		return true;
	}
}