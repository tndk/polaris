<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\System\Config;
use Telenor\System\Error\BadConfigurationError;

final class ConfigTest extends TestCase
{
	public function testEnvString(): void
	{
		putenv('TEST_ENV=hai1234');
		$this->assertSame('hai1234', Config::envString('TEST_ENV'));
		$this->assertSame('hest', Config::envString('NON_EXISTING_SHIT', 'hest'));
		$this->assertSame('', Config::envString('NON_EXISTING_SHIT', ''));

		$this->expectException(BadConfigurationError::class);
		Config::envString('NON_EXISTING_ENV');
	}

	public function testEnvInt(): void
	{
		putenv('TEST_ENV_INT=12');
		$this->assertSame(12, Config::envInt('TEST_ENV_INT'));
		$this->assertSame(56, Config::envInt('NON_EXISTING_SHIT', 56));
	}

	public function testEnvIntException1(): void
	{
		$this->expectException(BadConfigurationError::class);
		Config::envInt('NON_EXISTING_ENV');
	}

	public function testEnvIntException2(): void
	{
		$this->expectException(BadConfigurationError::class);
		putenv('TEST_ENV_STR="98"');
		Config::envInt('TEST_ENV_STR');
	}

	public function testEnvBool(): void
	{
		putenv('TEST_ENV_BOOL=true');
		$this->assertTrue(Config::envBool('TEST_ENV_BOOL'));
		putenv('TEST_ENV_BOOL=yes');
		$this->assertTrue(Config::envBool('TEST_ENV_BOOL'));
		putenv('TEST_ENV_BOOL=1');
		$this->assertTrue(Config::envBool('TEST_ENV_BOOL'));

		putenv('TEST_ENV_BOOL=false');
		$this->assertFalse(Config::envBool('TEST_ENV_BOOL'));
		putenv('TEST_ENV_BOOL=no');
		$this->assertFalse(Config::envBool('TEST_ENV_BOOL'));
		putenv('TEST_ENV_BOOL=0');
		$this->assertFalse(Config::envBool('TEST_ENV_BOOL'));

		$this->assertTrue(Config::envBool('NON_EXISTING_SHIT', true));
		$this->assertFalse(Config::envBool('NON_EXISTING_SHIT', false));
	}

	public function testEnvBoolException1(): void
	{
		$this->expectException(BadConfigurationError::class);
		Config::envBool('NON_EXISTING_ENV');
	}

	public function testEnvBoolException2(): void
	{
		$this->expectException(BadConfigurationError::class);
		putenv('TEST_ENV_STR="az98"');
		Config::envBool('TEST_ENV_STR');
	}

	public function testAcceptLine(): void
	{
		$configExposed = new class extends Config
		{
			public static function testExtractLine(string $line): ?array
			{
				return self::extractLine($line);
			}
		};

		// Accept these lines
		self::assertSame(['CONF_VAR', ''], $configExposed::testExtractLine('CONF_VAR='));
		self::assertSame(['CONF_VAR', ''], $configExposed::testExtractLine('CONF_VAR=""'));
		self::assertSame(['CONF_VAR', ''], $configExposed::testExtractLine("CONF_VAR=''"));
		self::assertSame(['CONF_VAR', '134'], $configExposed::testExtractLine('CONF_VAR=134'));
		self::assertSame(['CONF_VAR', '134'], $configExposed::testExtractLine('CONF_VAR="134"'));
		self::assertSame(['CONF_VAR', '134=432'], $configExposed::testExtractLine('CONF_VAR="134=432"'));
		self::assertSame(['CONF_VAR', '134'], $configExposed::testExtractLine("CONF_VAR='134'"));
		self::assertSame(['CONF_VAR', '134 and a space'], $configExposed::testExtractLine('CONF_VAR=134 and a space'));
		self::assertSame(['CONF_VAR', '134 & spaces',], $configExposed::testExtractLine('CONF_VAR="134 & spaces"'));
		self::assertSame(['CONF_VAR', '134&spaces',], $configExposed::testExtractLine("CONF_VAR='134&spaces'"));
		self::assertSame(['CONF_VAR', '134&spaces',], $configExposed::testExtractLine('CONF_VAR   =   134&spaces'));
		self::assertSame(['CONF_VAR', '134&spaces',], $configExposed::testExtractLine('CONF_VAR  =  "134&spaces"'));
		self::assertSame(['CONF_VAR', '134&spaces',], $configExposed::testExtractLine("CONF_VAR   =  '134&spaces'"));
		self::assertSame(['CONF_VAR', '134'], $configExposed::testExtractLine('export CONF_VAR=134'));
		self::assertSame(['CONF_VAR', '134'], $configExposed::testExtractLine('export CONF_VAR="134"'));
		self::assertSame(['CONF_VAR', '134'], $configExposed::testExtractLine("export CONF_VAR='134'"));
		self::assertSame(['CONF_VAR', '134'], $configExposed::testExtractLine('export CONF_VAR  =  134'));
		self::assertSame(['CONF_VAR', '134'], $configExposed::testExtractLine('export CONF_VAR  = "134"'));
		self::assertSame(['CONF_VAR', '134'], $configExposed::testExtractLine("export CONF_VAR ='134'"));

		// Do not accept these lines
		self::assertNull($configExposed::testExtractLine('CONF VAR=134'));
		self::assertNull($configExposed::testExtractLine('#CONF_VAR=134'));
		self::assertNull($configExposed::testExtractLine('CONF#VAR=134'));
		self::assertNull($configExposed::testExtractLine('CONF-VAR=134'));
		self::assertNull($configExposed::testExtractLine('--CONFVAR=134'));
	}
}