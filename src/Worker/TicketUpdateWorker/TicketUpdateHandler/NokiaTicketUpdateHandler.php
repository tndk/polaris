<?php
declare(strict_types=1);

namespace Telenor\Worker\TicketUpdateWorker\TicketUpdateHandler;

use DateTime;
use Generator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use JsonException;
use Telenor\Logging\TLog;
use Telenor\Message\APayloadMessage;
use Telenor\Message\ATicketMessage;
use Telenor\Message\ChangeTicketMessage;
use Telenor\Message\FieldForceTicketMessage;
use Telenor\Message\IncidentTicketMessage;
use Telenor\Message\TicketUpdateMessage;
use Telenor\MessageQueue\TMessageQueue;
use Telenor\System\Config;
use Telenor\System\Exception\ConnectionException;
use Telenor\System\Exception\InvalidRequestException;
use Telenor\System\Exception\UnhandledMessageException;
use Telenor\System\Exception\ValidationException;
use Telenor\System\HttpClient;
use Telenor\System\Utils;
use Throwable;
use function array_merge;
use function array_shift;
use function count;
use function implode;
use function iterator_to_array;
use function json_decode;
use function json_encode;
use function key;
use function min;
use function preg_replace_callback;
use function sprintf;
use function str_replace;
use function strtoupper;

/**
 * Ticket handler for Nokia incidents, changes, and field force tasks.
 * This class handles the incoming TicketUpdateMessage (from TicketUpdateWorker) and
 * maps/converts data and sends it to the very ugly Nokia API.
 *
 * All map* methods in this class does all the actual mapping, and we have
 * unit tests on all of those methods (+ a few more).
 */
class NokiaTicketUpdateHandler implements ITicketUpdateHandler
{
	use TLog;
	use TMessageQueue;

	// Nokia impact states
	public const NOKIA_IMPACT_1_EXTENSIVE = '1-Extensive/Widespread';
	public const NOKIA_IMPACT_2_SIGNIFICANT = '2-Significant/Large';
	public const NOKIA_IMPACT_3_MODERATE = '3-Moderate/Limited';
	public const NOKIA_IMPACT_4_MINOR = '4-Minor/Local';

	// Nokia urgency states
	public const NOKIA_URGENCY_1_CRITICAL = '1-Critical';
	public const NOKIA_URGENCY_2_HIGH = '2-High';
	public const NOKIA_URGENCY_3_MEDIUM = '3-Medium';
	public const NOKIA_URGENCY_4_LOW = '4-Low';

	// Nokia change types (actually, this is the legacy Remedy change types)
	public const CHANGE_TYPE_MINOR = 'Minor';
	public const CHANGE_TYPE_MAJOR = 'Major';
	public const CHANGE_TYPE_SIGNIFICANT = 'Significant';
	public const CHANGE_TYPE_EMERGENCY = 'Emergency';

	/** @var Client */
	protected $client;

	/** @var string */
	protected $nokiaDateTimeFormat;

	/** @var string */
	protected $nokiaUpdateQueueName;

	#region Constructor (read configuration)
	public function __construct()
	{
		// Build an HttpClient instance with the default Nokia + proxy config
		$this->client = new HttpClient([
			'base_uri' => 'https://' . Config::envString('NOKIA_SSDP_BASE_URL'),
			'headers'  => ['Content-Type' => 'application/json'],
			'proxy'    => Config::envString('DMZ_PROXY_SERVER', 'http://octopi.dmz.sonofon.dk:3128'),
			'verify'   => false,
		]);

		$this->nokiaDateTimeFormat = Config::envString('NOKIA_DATE_TIME_FORMAT', DateTime::RFC3339);
		$this->nokiaUpdateQueueName = Config::envString('NOKIA_UPDATE_QUEUE_NAME', 'sn.tickets');

		$this->logInfo('Configured Nokia SSDP base URL: ' . $this->client->getConfig('base_uri'));
		$this->logInfo('Configured Nokia Date Time Format: ' . $this->nokiaDateTimeFormat);
		$this->logInfo('Configured Nokia update queue name: ' . $this->nokiaUpdateQueueName);
	}
	#endregion

	#region Message dispatcher
	/**
	 * This method is implemented from the ITicketUpdateHandler interface.
	 * The $message argument is an instance of a model from the Payload sent from ServiceNow.
	 * This method simply dispatches the message to a relevant method based on the action and type properties.
	 * This is _THE_ entry point for the whole file - namely that we handle the message we got from ServiceNow.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return bool True if correctly handled. On errors an exception is thrown.
	 * @throws ConnectionException If reply could not be sent back to the MQ (connection lost).
	 * @throws JsonException If reply from Nokia is invalid JSON.
	 * @throws UnhandledMessageException If the Nokia API does not accept our request.
	 * @throws ValidationException If the reply back to ServiceNow does not follow the specification.
	 */
	public function handleMessage(TicketUpdateMessage $message): bool
	{
		if ($message->getAction() === APayloadMessage::ACTION_CREATE) {
			switch ($message->getType()) {
				case TicketUpdateMessage::TICKET_TYPE_INCIDENT:
					return $this->createIncident($message);
				case TicketUpdateMessage::TICKET_TYPE_CHANGE:
					return $this->createChange($message);
				case TicketUpdateMessage::TICKET_TYPE_FIELDFORCE:
					return $this->handleFieldForceTask($message);
				default:
					$this->logError('How is this possible ?!?! Received message with invalid type: ' . $message->getType());
			}
		} else if ($message->getAction() === APayloadMessage::ACTION_UPDATE) {
			switch ($message->getType()) {
				case TicketUpdateMessage::TICKET_TYPE_INCIDENT:
					return $this->updateIncident($message);
				case TicketUpdateMessage::TICKET_TYPE_CHANGE:
					return $this->updateChange($message);
				case TicketUpdateMessage::TICKET_TYPE_FIELDFORCE:
					return $this->handleFieldForceTask($message);
				default:
					$this->logError('How is this possible ?!?! Received message with invalid type: ' . $message->getType());
			}
		} else {
			$this->logError('Received message with invalid action: ' . $message->getAction());
		}
		return true;
	}
	#endregion

	#region Incidents
	/**
	 * Method for handling "create incident" scenarios. It will map the correct properties and push the
	 * data towards Nokias API. It will handle multiple requests if more than three attachments are found
	 * in the payload from ServiceNow, because Nokias API has a limit of three attachments for each request.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return bool True if correctly handled. On errors an exception is thrown.
	 * @throws ConnectionException If reply could not be sent back to the MQ (connection lost).
	 * @throws JsonException If reply from Nokia is invalid JSON.
	 * @throws UnhandledMessageException If the Nokia API does not accept our request.
	 * @throws ValidationException If the reply back to ServiceNow does not follow the specification.
	 */
	protected function createIncident(TicketUpdateMessage $message): bool
	{
		// Map data to fit Nokias API. This can return a multidimensional
		// array if more than three attachments are found in the payload.
		$data = $this->mapCreateIncident($message);

		// Send a create request to Nokia (from the 'create' property)
		$response = $this->sendRequest('TelenorTTM/TelenorRemedy/ITSM/incidentCreate', $data['create']);

		// Send update to ServiceNow with the remote ticket id
		$this->notifyServiceNow(new IncidentTicketMessage(IncidentTicketMessage::TYPE_INCIDENT,
			ATicketMessage::ACTION_UPDATE), true, $message->getCorrelationId(), $message->getRequestId(), $response);

		// Send additional attachments as updates (if any)
		foreach ($data['updates'] as $update) {
			try {
				$update['itsmIncidentID'] = $response['ticketID']; // This field is not part of unit test, so set it here
				$response = $this->sendRequest('TelenorTTM/TelenorRemedy/ITSM/incidentUpdate', $update);

				// Send a message back to ServiceNow with status from Nokia (will update work notes in ServiceNow)
				$this->notifyServiceNow(new IncidentTicketMessage(IncidentTicketMessage::TYPE_INCIDENT,
					ATicketMessage::ACTION_UPDATE), false, $message->getCorrelationId(), $message->getRequestId(),
					$response);
			} catch (Throwable $t) {
				// There is not much else to do than log any errors here..
				$this->logError('Received exception when trying to send update requests during ticket creation', [$t]);
			}
		}

		return true;
	}

	/**
	 * Maps a "create incident" scenario and builds a multidimensional array with a create
	 * map (and multiple update maps if more than three file attachments are found in the message.
	 *
	 * @noinspection DuplicatedCode
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return array Array with structure: [create => [], updates[0 => [], 1, => [], ..., n]]
	 */
	public function mapCreateIncident(TicketUpdateMessage $message): array
	{
		// Convert the ServiceNow impact/urgency to Nokias ITSM impact/urgency
		[$impact, $urgency] = $this->mapTicketPriorities($message->getImpact(), $message->getUrgency());

		// First build the common fields (for all types of tickets) and
		// then merge with these incident specific fields
		$createData = array_merge($this->createCommonTicketFields($message), [
			'applicationTarget'  => 'INC',
			'requestCategory'    => 'INCIDENT',
			'messageType'        => 'CREATE',
			'requestorFirstName' => 'Webservice',
			'requestorLastName'  => 'TelenorDK',
			'startOfImpact'      => $this->formatDateTime($message->getTimeCreated()),
			'urgency'            => $urgency,
			'impact'             => $impact,
			'impactedCiServices' => implode('; ', $message->getCIs()),
			'ciName'             => $message->getCIs()[0] ?? '',
			'reportedDate'       => $this->formatDateTime($message->getTimeCreated()),
			'status'             => 'New',
			'ownerSystem'        => 'Telenor',
			'assignedSystem'     => 'Nokia',
			'summary'            => $message->getShortDescription(),
			'note'               => $message->getDescription(),
			'worklogUpdate'      => $message->getWorknotes(),
		]);

		$attachments = $message->getAttachments();

		// Add at most 3 attachments in the incident create request
		for ($i = 1, $iMax = min(count($attachments), 3); $i <= $iMax; $i++) {
			$createData["attachmentName$i"] = key($attachments);
			$createData["attachmentData$i"] = array_shift($attachments);
		}

		// The first 3 attachments have been added already - now add the rest as update requests
		// (we have removed the first 3 attachments on a copy of the attachments in the message,
		// so we need to put the remaining attachments, back to the message object and generate
		// updates on the remaining attachments)
		$message->setAttachments($attachments);
		$updateData = iterator_to_array($this->mapUpdateAttachments($message));

		return ['create' => $createData, 'updates' => $updateData];
	}

	/**
	 * Method for handling "update incident" scenarios. It will map the correct properties and
	 * push the data towards Nokias API. Multiple requests might be sent to Nokia, if need be.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return bool True if correctly handled. On errors an exception is thrown.
	 * @throws ConnectionException If reply could not be sent back to the MQ (connection lost).
	 * @throws JsonException If reply from Nokia is invalid JSON.
	 * @throws UnhandledMessageException If the Nokia API does not accept our request.
	 * @throws ValidationException If the reply back to ServiceNow does not follow the specification.
	 */
	protected function updateIncident(TicketUpdateMessage $message): bool
	{
		// Fetch the updates to send to Nokia
		$updates = $this->mapUpdateIncident($message);

		foreach ($updates as $data) {
			// This field is not part of unit test, so set it here
			$data['itsmIncidentID'] = $message->getExtendedPayload()['ExternalTicketId'];

			// Set the correct API endpoint for the type of message to send
			$api = 'TelenorTTM/TelenorRemedy/ITSM/' . ($data['messageType'] === 'WORKLOG_UPDATE' ? 'incidentUpdate' : 'incidentStatusUpdate');

			// Send the request to Nokia
			$response = $this->sendRequest($api, $data);

			// Update the work notes in ServiceNow
			$this->notifyServiceNow(new IncidentTicketMessage(IncidentTicketMessage::TYPE_INCIDENT,
				ATicketMessage::ACTION_UPDATE), false, $message->getCorrelationId(), $message->getRequestId(),
				$response);
		}

		return true;
	}

	/**
	 * Builds the update incident(s) to send to Nokia and notify ServiceNow with updates.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return array
	 */
	public function mapUpdateIncident(TicketUpdateMessage $message): array
	{
		// Generate payload to send for attachments (if any)
		$updates = array_merge(iterator_to_array($this->mapUpdateAttachments($message)));

		// MAALS: Incident Tasks have different status mappings. We need to map the status
		// based off the correlation ID, which will tell us the REAL ticket type in Nokia's end
		$isTask = preg_match('#(^tas|-tas-)#i', $message->getCorrelationId()) === 1;
		$isCreatedByNokia = preg_match('#(^nokia-inc-)#i', $message->getCorrelationId()) === 1;

		// Generate payload to send when the status of the ticket has changed
		if ($message->getAssignmentGroup() === 'OPS Nokia' || ($message->getStatus() === 'Resolved' && $message->getStatusChanged())) {
			$stateChangeUpdate = array_merge($this->createUpdateFields($message), [
				'messageType'    => 'UPDATE',
				'worklogUpdate'  => $message->getWorknotes(),
				'status'         => $isTask ? $this->mapTaskStatus($message->getStatus()) : $this->mapIncidentStatus($message->getStatus()),
				'statusReason'   => '',
				'assignedSystem' => $message->getAssignedTo() ?: 'Nokia',

			]);

			// TPN: Enable severity and Impact to be sent on Incidents Only. Only Incident created by Telenor
			if (!$isCreatedByNokia && !$isTask) {
				[$impact, $urgency] = $this->mapTicketPriorities($message->getImpact(), $message->getUrgency());
				$stateChangeUpdate['urgency'] = $urgency;
				$stateChangeUpdate['impact'] = $impact;
			}

			if ($message->getStatus() === TicketUpdateMessage::STATUS_NEW) {
				$stateChangeUpdate['impactedCiServices'] = implode('; ', $message->getCIs());
				$stateChangeUpdate['ciName'] = $message->getCIs()[0] ?? '';

			}

			switch ($message->getStatus()) {
				case TicketUpdateMessage::STATUS_ON_HOLD:
					$stateChangeUpdate['statusReason'] = $message->getOnHoldReason();
					break;
				case TicketUpdateMessage::STATUS_CANCELED:
					$stateChangeUpdate['messageType'] = 'CANCEL';
					break;
				case TicketUpdateMessage::STATUS_RESOLVED:
					$stateChangeUpdate['messageType'] = 'RESOLVE';
					$stateChangeUpdate['resolutionDate'] = $this->formatDateTime($message->getResolutionDate());
					$stateChangeUpdate['statusReason'] = $isTask ? $this->mapStatusReason($message->getCloseCode()) : '';
					break;
				case TicketUpdateMessage::STATUS_CLOSED:
					$stateChangeUpdate['messageType'] = 'CLOSE';
					$stateChangeUpdate['closedDate'] = $this->formatDateTime($message->getClosedDate());
					break;
			}

			$updates[] = $stateChangeUpdate;

		} else if (!empty($message->getWorknotes())) {

			$updates[] = array_merge($this->createUpdateFields($message), [
				'messageType'   => 'WORKLOG_UPDATE',
				'worklogUpdate' => $message->getWorknotes(),
			]);
		}

		return $updates;
	}
	#endregion

	#region Changes
	/**
	 * Method for handling "create change" scenarios. It will map the correct properties and push the
	 * data towards Nokias API. It will handle multiple requests if more than three attachments are found
	 * in the payload from ServiceNow, because Nokias API has a limit of three attachments for each request.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return bool True if correctly handled. On errors an exception is thrown.
	 * @throws ConnectionException If reply could not be sent back to the MQ (connection lost).
	 * @throws JsonException If reply from Nokia is invalid JSON.
	 * @throws UnhandledMessageException If the Nokia API does not accept our request.
	 * @throws ValidationException If the reply back to ServiceNow does not follow the specification.
	 */
	protected function createChange(TicketUpdateMessage $message): bool
	{
		$data = $this->mapCreateChange($message);

		// If no requests are to be sent to Nokia, simply return true (thus ack'ing the message from the MQ)
		if ($data === null)
			return true;

		// Send a create request to Nokia
		$response = $this->sendRequest('TelenorTTM/TelenorRemedy/ITSM/woCreate', $data['create']);

		// Send update to SN with the remote ticket id
		$this->notifyServiceNow(new ChangeTicketMessage(ATicketMessage::ACTION_UPDATE), true,
			$message->getCorrelationId(), $message->getRequestId(), $response);

		// Send additional attachments as updates (if any)
		foreach ($data['updates'] as $update) {
			try {
				$update['itsmCRQID'] = $response['ticketID']; # This field is not part of unit test, so set it here
				$api = 'TelenorTTM/TelenorRemedy/ITSM/' . ($data['messageType'] === 'WORKLOG_UPDATE' ? 'woUpdate' : 'woStatusUpdate');
				$this->sendRequest($api, $update);
				$this->notifyServiceNow(new ChangeTicketMessage(ATicketMessage::ACTION_UPDATE), false,
					$message->getCorrelationId(), $message->getRequestId(), $response);
			} catch (Throwable $t) {
				// There is not much else to do than log any errors here..
				$this->logError('Received exception when trying to send update requests during change creation', [$t]);
			}
		}

		return true;
	}

	/**
	 * Builds a 'create change' request to send to Nokia. It will build a 'create' request
	 * - and multiple update requests if more than three file attachments were received from ServiceNow.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return array|null Null if no requests should be sent, otherwise an array of requests to send. Array structure:
	 *     [create => [], updates[0 => [], 1, => [], ..., n]]
	 * @noinspection DuplicatedCode
	 */
	public function mapCreateChange(TicketUpdateMessage $message): ?array
	{
		[$impact, $urgency] = $this->mapTicketPriorities($message->getImpact(), $message->getUrgency());
		$finalApproval = '';

		// Determine whether the change type to send is Minor, Major, Significant, or Emergency
		switch (strtolower($message->getChangeType() ?? '')) {
			case 'nokia emergency':
				$changeType = self::CHANGE_TYPE_EMERGENCY;
				break;
			case 'nokia major':
				//	$changeType = $message->isCABRequired() ? self::CHANGE_TYPE_MAJOR : self::CHANGE_TYPE_SIGNIFICANT;
				$changeType = self::CHANGE_TYPE_MAJOR;
				break;
			case 'nokia significant':
				$changeType = self::CHANGE_TYPE_SIGNIFICANT;
				break;
			case 'nokia minor':
			default:
				$changeType = self::CHANGE_TYPE_MINOR;
				$finalApproval = 'Approved';
				break;
		}

		$this->logDebug('Determined change type: ' . $changeType);

		#region Determine when to send a create transaction towards Nokia
		if ($changeType === self::CHANGE_TYPE_MINOR && $message->getStatus() !== TicketUpdateMessage::STATUS_SCHEDULED)
			return null;

		if ($changeType !== self::CHANGE_TYPE_MINOR && $message->getStatus() !== TicketUpdateMessage::STATUS_AUTHORIZE)
			return null;
		#endregion

		$createData = array_merge($this->createCommonTicketFields($message), [
			'requestCategory'    => 'CHANGE',
			'messageType'        => 'CREATE',
			'requestorFirstName' => 'Webservice',
			'requestorLastName'  => 'TelenorDK',
			'urgency'            => $urgency,
			'impact'             => $impact,
			//	'impactedCis'        => isset($message->getCIs()[1]) ? implode('; ', $message->getCIs()[1]) : '',
			'impactedCis'        => implode('; ', $message->getCIs()),
			// Add CIs from Item field (empty string if not found)
			'ciName'             => $message->getCIs()[0] ?? '', // Set the first CI (or empty string if none found)
			'status'             => 'New',
			'changeType'         => $changeType,
			'changeRFA'          => 'RFA',
			'changeApproval'     => $finalApproval,
			'scheduledStartDate' => $this->formatDateTime($message->getPlannedStartDate()),
			'scheduledEndDate'   => $this->formatDateTime($message->getPlannedEndDate()),
			'actualStartDate'    => $this->formatDateTime($message->getActualStartDate()),
			'actualEndDate'      => $this->formatDateTime($message->getActualEndDate()),
			'summary'            => $message->getShortDescription(),
			'note'               => sprintf("Change type: %s\nChange description: %s\n\n%s", $changeType,
				$message->getDescription(), $message->getWorknotes()),
		]);

		$attachments = $message->getAttachments();

		// Add at most 3 attachments in the incident create request
		for ($i = 1, $iMax = min(count($attachments), 3); $i <= $iMax; $i++) {
			$createData["attachmentName$i"] = key($attachments);
			$createData["attachmentData$i"] = array_shift($attachments);
		}

		// The first 3 attachments have been added already - now add the rest as update requests
		// (we have removed the first 3 attachments on a copy of the attachments in the message,
		// so we need to put the remaining attachments, back to the message object and generate
		// updates on the remaining attachments)
		$message->setAttachments($attachments);
		$updateData = iterator_to_array($this->mapUpdateAttachments($message));

		return ['create' => $createData, 'updates' => $updateData];
	}

	/**
	 * Builds the update change request(s) to send to Nokia and notify ServiceNow with updates.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return bool True if correctly handled. On errors an exception is thrown.
	 * @throws ConnectionException If reply could not be sent back to the MQ (connection lost).
	 * @throws JsonException If reply from Nokia is invalid JSON.
	 * @throws UnhandledMessageException If the Nokia API does not accept our request.
	 * @throws ValidationException If the reply back to ServiceNow does not follow the specification.
	 */
	protected function updateChange(TicketUpdateMessage $message): bool
	{
		// Firstly build all
		$updates = $this->mapUpdateChange($message);

		foreach ($updates as $data) {
			// This field is not part of unit test, so set it here
			$data['itsmCRQID'] = $message->getExtendedPayload()['ExternalTicketId'];

			//
			$api = 'TelenorTTM/TelenorRemedy/ITSM/' . ($data['messageType'] === 'WORKLOG_UPDATE' ? 'woUpdate' : 'woStatusUpdate');
			$response = $this->sendRequest($api, $data);
			$this->notifyServiceNow(new ChangeTicketMessage(ATicketMessage::ACTION_UPDATE), false,
				$message->getCorrelationId(), $message->getRequestId(), $response);
		}

		return true;
	}

	/**
	 * Maps 'update change' tickets to (multiple) requests to send to Nokia.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return array Array of structures of the JSON data to send to Nokia.
	 */
	public function mapUpdateChange(TicketUpdateMessage $message): array
	{

		// Generate payload to send for attachments (if any)
		$updates = array_merge(iterator_to_array($this->mapUpdateAttachments($message)));

		// If status has changed, send an UPDATE message instead of WORKLOG_UPDATE.
		// MAALS: We possibly need to also send UPDATE if any dates are set?
		$sendStatusUpdate = $message->getStatusChanged();

		// Generate payload to send when the status of the ticket has changed
		if ($sendStatusUpdate) {
			$stateChangeUpdate = array_merge($this->createUpdateFields($message), [
				'messageType'    => 'UPDATE',
				'status'         => $this->mapChangeStatus($message->getStatus()),
				'statusReason'   => '',
				'changeRFA'      => $message->getStatus() === TicketUpdateMessage::STATUS_AUTHORIZE ? 'RFA' : '',
				'changeApproval' => $message->getStatus() === TicketUpdateMessage::STATUS_SCHEDULED ? 'Approved' : '',
				'worklogUpdate'  => $message->getWorknotes(),
			]);

			if ($message->getPlannedStartDate() !== null)
				$stateChangeUpdate['scheduledStartDate'] = $this->formatDateTime($message->getPlannedStartDate());

			if ($message->getPlannedEndDate() !== null)
				$stateChangeUpdate['scheduledEndDate'] = $this->formatDateTime($message->getPlannedEndDate());

			if ($message->getActualStartDate() !== null)
				$stateChangeUpdate['actualStartDate'] = $this->formatDateTime($message->getActualStartDate());

			if ($message->getActualEndDate() !== null)
				$stateChangeUpdate['actualEndDate'] = $this->formatDateTime($message->getActualEndDate());

			if (!empty($message->getCIs()))
				$stateChangeUpdate['impactedCis'] = implode('; ', $message->getCIs());

			// In certain scenarios we need to send a reason along with the new status
			// Remeber that STATUS_NO_CHANGE will be sent from ServiceNow if a ticket
			// hasn't change state.
			switch ($message->getStatus()) {
				case TicketUpdateMessage::STATUS_ON_HOLD:
					$stateChangeUpdate['statusReason'] = $message->getOnHoldReason();
					break;
				case TicketUpdateMessage::STATUS_CANCELED:
					$stateChangeUpdate['messageType'] = 'CANCEL';
					$stateChangeUpdate['statusReason'] = 'Cancelled';

					$stateChangeUpdate['actualStartDate'] = $this->formatDateTime(new DateTime('now'));
					$stateChangeUpdate['actualEndDate'] = $this->formatDateTime(new DateTime('now'));

					break;
				//	case TicketUpdateMessage::STATUS_RESOLVED:
				case TicketUpdateMessage::STATUS_REVIEW:
					$stateChangeUpdate['messageType'] = 'RESOLVE';
					$stateChangeUpdate['statusReason'] = $this->mapStatusReason($message->getCloseCode());

					unset($stateChangeUpdate['worklogUpdate']); # Nokia should not see what we put in the close note
					$stateChangeUpdate['actualEndDate'] = $this->formatDateTime($message->getActualEndDate());
					break;
				case TicketUpdateMessage::STATUS_CLOSED:
					$stateChangeUpdate['messageType'] = 'CLOSE';
					$stateChangeUpdate['closedDate'] = $this->formatDateTime($message->getClosedDate());
					break;
			}

			$updates[] = $stateChangeUpdate;

		} else if (!empty($message->getWorknotes())) {

			$updates[] = array_merge($this->createUpdateFields($message), [
				'messageType'   => 'WORKLOG_UPDATE',
				'worklogUpdate' => $message->getWorknotes(),
			]);
		}

		// Why the heck is the fields not called the same thing through the API ?!?!
		// Well.. We'll fix a couple of things for "Change Update" scenario (non-state-change)
		foreach ($updates as &$update) {

			// In this scenario we do not send the "worklogUpdate" property, but the "worklog" property instead.
			if ($update['messageType'] === 'WORKLOG_UPDATE') {
				$update['worklog'] = $update['worklogUpdate'];
				unset($update['worklogUpdate']);
			}

			// Ensure that this property is set for all updates
			$update['modifiedByUser'] = 'Webservice TelenorDK';
		}

		return $updates;
	}
	#endregion

	#region Field force tasks
	/**
	 * Handles all kind of Field Force Task messages. This will build the requests to send,
	 * and afterwards it will send the request and send an update back to ServiceNow.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return bool True if correctly handled. On errors an exception is thrown.
	 * @throws ConnectionException If reply could not be sent back to the MQ (connection lost).
	 * @throws JsonException If reply from Nokia is invalid JSON.
	 * @throws UnhandledMessageException If the Nokia API does not accept our request.
	 * @throws ValidationException If the reply back to ServiceNow does not follow the specification.
	 */
	protected function handleFieldForceTask(TicketUpdateMessage $message): bool
	{
		$taskID = null;
		foreach ($this->buildFieldForceTaskRequests($message) as $request) {
			$endpoint = $request['endpoint'];
			$data = $request['data'];

			// If a taskID has been retrieved by Nokia (that is; a successful creation/update)
			// We will send the same ID on the subsequent requests
			if ($taskID !== null)
				$data['taskID'] = $taskID;

			// Send the request to Nokia
			$response = $this->sendRequest($endpoint, $data);

			// Send update back to ServiceNow
			$this->notifyServiceNow(new FieldForceTicketMessage(), $data['messageType'] === 'CREATE',
				$message->getCorrelationId(), $message->getRequestId(), $response);

			// Ensure that we set the variable (only one time) to the taskID
			if ($taskID === null)
				$taskID = $response['taskID'];
		}

		return true;
	}

	/**
	 * Builds all the field force task requests we need to send, based on a single update from
	 * ServiceNow. This method yields every time a new request should be sent to Nokia.
	 *
	 * @param TicketUpdateMessage $message The message coming from ServiceNow.
	 * @return Generator A yielded JSON structure to send. Format: [endpoint => , data => []], where 'endpoint'
	 * is the API endpoint to send the request to, and 'data' contains the JSON data to send to that endpoint.
	 */
	public function buildFieldForceTaskRequests(TicketUpdateMessage $message): Generator
	{
		$item = $message->getCIs()[0] ?? '';
		$userInitials = $message->getUserID() ?? '';

		// If any of these conditions are false, $isUpdateCall will be false. That means that for instance,
		// if the 'action' payload attribute is not 'update', $isUpdateCall will be false.
		// So every condition here specifies whether to send to the 'woUpdate' endpoint or the 'woCreate' endpoint.
		// Note that the 'woCreate' endpoint also accept updates!! Yes I know - god damn ugly and counter intuitive!
		$isUpdateCall = true;
		$isUpdateCall &= $message->getAction() === TicketUpdateMessage::ACTION_UPDATE;
		$isUpdateCall &= !Utils::isStringNullOrEmpty($message->getWorknotes());
		$isUpdateCall &= $message->getDueDateChanged() !== true;

		if ($isUpdateCall) {

			// Set the correct message type, based on the state on the ticket in ServiceNow
			// should only sent new message type if status changed in ServiceNow
			if ($message->getStatusChanged()) {
				$messageType = $this->mapTaskMessageType($message->getStatus());
			} else {
				$messageType = 'WORKLOG_UPDATE';
			}

			// A new result is 'ready' - yield it to the loop in the caller function
			yield [
				'endpoint' => 'WFMTNRemedy/TelenorRemedy/WFM/woUpdate',
				'data'     => [
					'taskID'         => $message->getCorrelationId(),
					'company'        => 'TN Field DK',
					'modifiedByUser' => $message->getOpenedBy(),
					'workLog'        => $message->getWorknotes(),
					'messageType'    => $messageType,
				],
			];
		} else {

			// Set the correct message type, based on the state on the ticket in ServiceNow
			if ($message->getAction() === TicketUpdateMessage::ACTION_CREATE) {
				$messageType = 'CREATE';
			} else if ($message->getDueDateChanged() === true) {
				$messageType = 'UPDATE-SLA';
			} else {
				$messageType = 'UPDATE';
			}

			// A new result is 'ready' - yield it to the loop in the caller function
			yield [
				'endpoint' => 'WFMTNRemedy/TelenorRemedy/WFM/woCreate',
				'data'     => [
					'company'                => 'TN Field DK',
					'taskType'               => 'CM',
					'ticketType'             => 'TROUBLE_TICKET',
					'messageType'            => $messageType,
					'taskID'                 => $message->getCorrelationId(),
					'region'                 => 'Denmark',
					'priority'               => $this->mapTaskPriorities($message->getImpact(), $message->getUrgency()),
					'requestor'              => $userInitials,
					'requestorLastName'      => $message->getOpenedBy(),
					'customerTicketID'       => $message->getCorrelationId(),
					'impactStartDate'        => $this->formatDateTime($message->getTimeCreated()),
					'serviceWindowStartDate' => $this->formatDateTime($message->getTimeCreated()),
					'serviceWindowEndDate'   => $this->formatDateTime($message->getDueDate()),
					'promisedSla'            => $this->formatDateTime($message->getDueDate()),
					'ciName'                 => $message->getLocation(),
					'taskTitle'              => $message->getShortDescription(),
					'alarm'                  => sprintf("User: %s\n\n Item: %s\n\n%s\n\n%s", $userInitials, $item,
						$message->getDescription(), $message->getWorknotes()),
				],
			];
		}

		// Build standalone request for each file attachment
		foreach ($message->getAttachments() as $filename => $data) {
			// A new result is 'ready' - yield it to the loop in the caller function
			yield [
				'endpoint' => 'WFMTNRemedy/TelenorRemedy/WFM/woUpdate',
				'data'     => [
					'company'         => 'TN Field DK',
					'modifiedByUser'  => $userInitials,
					'messageType'     => 'WORKLOG_UPDATE',
					'workLog'         => "New attachment: $filename",
					'attachmentName1' => $filename,
					'attachmentData1' => $data,
				],
			];
		}
	}
	#endregion

	#region Common functionality
	/**
	 * Sends a message back to ServiceNow with some sort of result. this is used when we receive valuable information
	 * from Nokia's API (ExternalTicketId, status message, etc.). This is done by accepting a type of a ATicketMessage
	 * (for instance IncidentTicketMessage, ChangeTicketMessage, FieldForceMessage, etc.) as the first argument, and
	 * fills in the missing details before sending. We use the existing interface to ServiceNow, where we generate a
	 * message and put it into the "sn.tickets" queue, which is used by TicketNotificationWorker.
	 *
	 * @param ATicketMessage $message An instance of a message to send to ServiceNow.
	 * @param bool $create Boolean value of whether it is a create scenario or not.
	 * @param string $correlationID The correlation ID of the ticket to send to ServiceNow.
	 * @param string $requestID The RequestID of the existing message (used for enhance searching in logs).
	 * @param array $response The complete response from Nokia's API.
	 * @throws ConnectionException If no successful connection has been established to the Message Queue.
	 * @throws ValidationException If the message sent back to ServiceNow does not validate correctly.
	 */
	protected function notifyServiceNow(ATicketMessage $message, bool $create, string $correlationID, string $requestID,
	                                    array $response): void
	{
		$worknotes = [];
		$responseMessage = $response['message'] ?? null;

		// Do not send updates back to ServiceNow on task updates, because the API always reports this back if no error occurs
		if (!$create && $message instanceof FieldForceTicketMessage && $responseMessage === 'Task created successfully in myWFM') {
			$this->logDebug('Update to myWFM went quite ok so no need to notify ServiceNow', [
				'ExternalTicketId' => $correlationID,
				'response'         => $responseMessage,
			]);
			return;
		}

		// For Field Force Tasks, the API endpoint returns 'taskID' instead of 'ticketID'. We map both
		// of them to the same field in the reference table in ServiceNow (as ExternalTicketId).
		if ($message instanceof FieldForceTicketMessage) {
			$externalTicketId = $response['taskID'] ?? null;
		} else {
			$externalTicketId = $response['ticketID'] ?? null;
		}

		// Add a work note when we are in a ticket creation scenario where we print the
		// ticket id returned by Nokia's API.
		if ($create && !Utils::isStringNullOrEmpty($externalTicketId))
			$worknotes[] = "Attached external ticket ID:\n$externalTicketId";

		// If the 'message' field in the JSON data returned from Nokia contains some data,
		// we will add it as a work note, so we at least know if something went wrong.
		if (!Utils::isStringNullOrEmpty($responseMessage))
			$worknotes[] = "Response message:\n$responseMessage";

		// Simply define some extra attributes to put into the logs
		$additionalLogs = [
			'response'         => $responseMessage,
			'ExternalTicketId' => $externalTicketId,
			'create'           => $create,
		];

		// No need to send any updates to ServiceNow if no work notes have been generated.
		if (empty($worknotes)) {
			$this->logInfo('Not updating ticket in ServiceNow due to no worknote updates to send', $additionalLogs);
			return;
		}

		// Set the required attributes and publish the message to the queue.
		$this->logInfo('Sending ticket update to ServiceNow', $additionalLogs);
		$message->setSource('nokia');
		$message->setRequestId($requestID);
		$message->setStatus(ATicketMessage::STATUS_NO_CHANGE);
		$message->setCorrelationId($correlationID);
		$message->addExtendedPayload('ExternalTicketId', $externalTicketId);
		$message->setWorknotes(implode("\n\n", $worknotes));
		$this->publishToQueue($message, $this->nokiaUpdateQueueName);
	}

	/**
	 * Sends a POST request to the Nokia API and returns an array based on the parsed JSON response.
	 *
	 * @param string $apiEndpoint The API endpoint to post to.
	 * @param array $data The data to send. The data will be encoded to JSON prior sending.
	 * @return array The data structure from the JSON response.
	 * @return bool True if correctly handled. On errors an exception is thrown.
	 * @throws JsonException If reply from Nokia is invalid JSON.
	 * @throws UnhandledMessageException If the Nokia API does not accept our request.
	 */
	protected function sendRequest(string $apiEndpoint, array $data): array
	{
		try {
			// Send a request to Nokia in JSON structure
			$response = $this->client->post($apiEndpoint,
				['body' => json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES)]);

			// Nokia's API almost always
			if ($response->getStatusCode() !== 200)
				throw new InvalidRequestException('Got unexpected status code in HTTP response');

			// Oooookay Nokia .. This is fucking retarded. Why the hell do you generate the JSON string manually ?!?!
			// .. Oh well, luckily we can fix it our side, because you can send invalid JSON to us in some cases.
			$body = $this->fixTheFuckingRetardedResponseFromNokiaAPI((string)$response->getBody());

			// Nokias load balancer can throw very special messages back to us when it is not correctly connected
			// to their ITSM. If there is a problem, the load balancer will send a body back starting with <ssdp-request-data>
			if (Utils::startsWith($body, '<ssdp-request-data>'))
				throw new InvalidRequestException('Received invalid response from endpoint');

			// Okay, we are good to go now. It *should* be valid JSON now. If the status field in the response
			// does not contain 'OK' the request can be considered invalid. We should not retry again, so throw
			// InvalidRequestException if that occurs (that will ack the message from the queue (in TicketUpdateWorker))
			$jsonBody = json_decode($body, true, 512, JSON_THROW_ON_ERROR);
			if ($jsonBody['status'] !== 'OK')
				throw new InvalidRequestException('Unsuccessful request received from endpoint. Message: ' . $jsonBody['message']);

			if (!empty($jsonBody['message'])) {
				$this->logWarning('Received status OK from Nokia with a response message: ' . $jsonBody['message']);
			} else {
				$this->logInfo('Received status OK from Nokia with no response message');
			}

			return $jsonBody;

		} catch (RequestException|InvalidRequestException $e) {
			$this->logNotice('Failed to send request', [$e]);
			throw new UnhandledMessageException('Failed to send request', $e);
		} /** @noinspection PhpRedundantCatchClauseInspection */ catch (JsonException $e) {
			$this->logError('Failed to encode data to JSON', [
				'data' => $data,
				$e,
			]);
			throw $e;
		}
	}

	/**
	 * Method for building required fields for all types of request to the Nokia API.
	 *
	 * @param TicketUpdateMessage $message The message from ServiceNow.
	 * @return array Array structure of all common fields.
	 */
	protected function createCommonTicketFields(TicketUpdateMessage $message): array
	{
		return [
			'applicationOrigin' => 'Telenor-Remedy',
			'company'           => 'Telenor DK',
			'customerTicketID'  => $message->getCorrelationId(),
		];
	}

	/**
	 * Method for generating seperate attachment requests. This method builds all
	 * required fields for sending a new attachment as a single request to Nokia.
	 *
	 * @param TicketUpdateMessage $message The message from ServiceNow.
	 * @return Generator Yields a new array structure every time a new request should be sent.
	 */
	protected function mapUpdateAttachments(TicketUpdateMessage $message): Generator
	{
		foreach ($message->getAttachments() as $attachmentName => $attachmentData) {
			yield array_merge($this->createUpdateFields($message), [
				'messageType'     => 'WORKLOG_UPDATE',
				'worklogUpdate'   => "Attached file: $attachmentName",
				'attachmentName1' => $attachmentName,
				'attachmentData1' => $attachmentData,
			]);
		}
	}

	/**
	 * Builds array structure of the common fields for all ticket updates.
	 *
	 * @param TicketUpdateMessage $message The message from ServiceNow.
	 * @return array Array of common fields.
	 */
	protected function createUpdateFields(TicketUpdateMessage $message): array
	{
		$data = $this->createCommonTicketFields($message);
		$data['requestCategory'] = strtoupper($message->getType());

		return $data;
	}

	/**
	 * Converts a ServiceNow "fake" task (really an incident) state to a Nokia state.
	 * The reason we need this is that states are different for tasks and incidents.
	 *
	 * @param string $status The status from ServiceNow.
	 * @return string The status to send to Nokia.
	 */
	public function mapTaskStatus(string $status): string
	{
		switch ($status) {
			case TicketUpdateMessage::STATUS_NEW:
				return 'New';
			case TicketUpdateMessage::STATUS_ON_HOLD:
			case TicketUpdateMessage::STATUS_IN_PROGRESS:
				return 'Pending';
			case TicketUpdateMessage::STATUS_CANCELED:
			case TicketUpdateMessage::STATUS_RESOLVED:
				return 'Resolved';
			case TicketUpdateMessage::STATUS_CLOSED:
				return 'Closed';
			default:
				return 'WorkInProgress';
		}
	}

	/**
	 * Converts a ServiceNow incident state to a Nokia state.
	 *
	 * @param string $status The status from ServiceNow.
	 * @return string The status to send to Nokia.
	 */
	public function mapIncidentStatus(string $status): string
	{
		switch ($status) {
			case TicketUpdateMessage::STATUS_NEW:
				return 'New';
			case TicketUpdateMessage::STATUS_CANCELED:
			case TicketUpdateMessage::STATUS_RESOLVED:
				return 'Resolved';
			case TicketUpdateMessage::STATUS_CLOSED:
				return 'Closed';
			case TicketUpdateMessage::STATUS_IN_PROGRESS:  //When a Incident created by Telenor is assigned in Nokia's end, the status in SN will change to In Progress
			case TicketUpdateMessage::STATUS_ON_HOLD:       //When Telenor send a status update we will send WorkInProgress which will change the status in Nokia's end
				//From assigned to WorkInProgress. Perhaps we should just send status = null
			default:
				return 'WorkInProgress';

		}
	}

	/**
	 * Converts a ServiceNow change request state to a Nokia state.
	 *
	 * @param string $status The status from ServiceNow.
	 * @return string The status to send to Nokia.
	 */
	public function mapChangeStatus(string $status): string
	{
		switch ($status) {
			case TicketUpdateMessage::STATUS_NEW:
			case TicketUpdateMessage::STATUS_ASSESS:
			case TicketUpdateMessage::STATUS_AUTHORIZE:
				return 'New';
			case TicketUpdateMessage::STATUS_SCHEDULED:
				return 'Assigned';
			case TicketUpdateMessage::STATUS_REVIEW:
				return 'Resolved';
			case TicketUpdateMessage::STATUS_CANCELED:
				return 'Cancelled';
			case TicketUpdateMessage::STATUS_CLOSED:
				return 'Closed';
			default:
				return 'WorkInProgress';
		}
	}

	public function mapStatusReason(string $closeCode): string
	{

		switch ($closeCode) {
			case TicketUpdateMessage::CLOSE_CODE_SUCCESSFUL:
			case TicketUpdateMessage::RES_CODE_SOLVED_PERMANENTLY:
			case TicketUpdateMessage::RES_CODE_CLOSED_RESOLVED_BY_CALLER:
				return 'Success';
			case TicketUpdateMessage::CLOSE_CODE_SUCCESSFUL_WITH_ISSUES:
			case TicketUpdateMessage::RES_CODE_SOLVED_WORKAROUND:
				return 'Success w. problems';
			case TicketUpdateMessage::CLOSE_CODE_UNSUCCESSFUL:
			case TicketUpdateMessage::RES_CODE_NOT_SOLVED:
			case TicketUpdateMessage::RES_CODE_MISSING_INFORMATION:
				return 'Failed';
			default:
				return '';

		}

	}

	/**
	 * Converts a ServiceNow Field Force Task status to a MyWFM message type.
	 *
	 * @param string $status A valid Field Force Task state from ServiceNow.
	 * @return string A valid MyWFM message type.
	 */
	public function mapTaskMessageType(string $status): string
	{
		switch ($status) {
			case TicketUpdateMessage::STATUS_CLOSED_COMPLETE:
				return 'CLOSE';
			case TicketUpdateMessage::STATUS_CANCEL:
				return 'CANCEL';
			case TicketUpdateMessage::STATUS_REJECT:
				return 'RE-ASSIGN';
			default:
				return 'WORKLOG_UPDATE';
		}
	}

	/**
	 * Correctly formats a DateTime object to the configured format (NOKIA_DATE_TIME_FORMAT env. variable).
	 *
	 * @param DateTime|null $dateTime The DateTime to format, or null if nothing should be formatted.
	 * @return string A correctly formatted DateTime, or empty string if null argument is given.
	 */
	protected function formatDateTime(?DateTime $dateTime): string
	{
		return ($dateTime === null) ? '' : $dateTime->format($this->nokiaDateTimeFormat);
	}

	/**
	 * Converts ServiceNow impact + urgency to a Nokia ITSM incident/change request priority.
	 *
	 * @param int $serviceNowImpact The impact of the ServiceNow incident/change request.
	 * @param int $serviceNowUrgency The urgency of the ServiceNow incident/change request.
	 * @return array Always returns two values in an array [impact, urgency].
	 */
	public function mapTicketPriorities(int $serviceNowImpact, int $serviceNowUrgency): array
	{
		if ($serviceNowImpact === 1 && $serviceNowUrgency === 1)
			return [self::NOKIA_IMPACT_1_EXTENSIVE, self::NOKIA_URGENCY_1_CRITICAL];

		if ($serviceNowImpact === 1 && $serviceNowUrgency === 2)
			return [self::NOKIA_IMPACT_2_SIGNIFICANT, self::NOKIA_URGENCY_1_CRITICAL];

		if ($serviceNowImpact === 1 && $serviceNowUrgency === 3)
			return [self::NOKIA_IMPACT_2_SIGNIFICANT, self::NOKIA_URGENCY_2_HIGH];

		if ($serviceNowImpact === 2 && $serviceNowUrgency === 1)
			return [self::NOKIA_IMPACT_1_EXTENSIVE, self::NOKIA_URGENCY_2_HIGH];

		if ($serviceNowImpact === 2 && $serviceNowUrgency === 2)
			return [self::NOKIA_IMPACT_3_MODERATE, self::NOKIA_URGENCY_2_HIGH];

		if ($serviceNowImpact === 2 && $serviceNowUrgency === 3)
			return [self::NOKIA_IMPACT_3_MODERATE, self::NOKIA_URGENCY_3_MEDIUM];

		if ($serviceNowImpact === 3 && $serviceNowUrgency === 1)
			return [self::NOKIA_IMPACT_1_EXTENSIVE, self::NOKIA_URGENCY_4_LOW];

		if ($serviceNowImpact === 3 && $serviceNowUrgency === 2)
			return [self::NOKIA_IMPACT_3_MODERATE, self::NOKIA_URGENCY_4_LOW];

		if ($serviceNowImpact === 3 && $serviceNowUrgency === 3)
			return [self::NOKIA_IMPACT_4_MINOR, self::NOKIA_URGENCY_4_LOW];

		return [self::NOKIA_IMPACT_4_MINOR, self::NOKIA_URGENCY_4_LOW];
	}

	/**
	 * Converts ServiceNow impact + urgency to a Field Force priority.
	 *
	 * @param int $serviceNowImpact The impact of the ServiceNow field force task.
	 * @param int $serviceNowUrgency The urgency of the ServiceNow field force task.
	 * @return string A string of P0..P5 based on the input.
	 */
	public function mapTaskPriorities(int $serviceNowImpact, int $serviceNowUrgency): string
	{
		if ($serviceNowImpact === 1 && $serviceNowUrgency === 1)
			return 'Telenor P0';

		if ($serviceNowImpact === 1 && $serviceNowUrgency === 2)
			return 'Telenor P1';

		if ($serviceNowImpact === 1 && $serviceNowUrgency === 3)
			return 'Telenor P1';

		if ($serviceNowImpact === 2 && $serviceNowUrgency === 1)
			return 'Telenor P2';

		if ($serviceNowImpact === 2 && $serviceNowUrgency === 2)
			return 'Telenor P2';

		if ($serviceNowImpact === 2 && $serviceNowUrgency === 3)
			return 'Telenor P3';

		if ($serviceNowImpact === 3 && $serviceNowUrgency === 1)
			return 'Telenor P3';

		if ($serviceNowImpact === 3 && $serviceNowUrgency === 2)
			return 'Telenor P4';

		if ($serviceNowImpact === 3 && $serviceNowUrgency === 3)
			return 'Telenor P5';

		return 'Telenor P5';
	}

	/**
	 * Nokia's API does not escape double quotes, when they put some text in the message
	 * field in the JSON response. We will fix that before trying to parse to array.
	 *
	 * For more information see the unit tests for this particular method.
	 *
	 * @param string $input The full response from Nokia's API.
	 * @return string The full response with escaped quotes in the message field.
	 */
	public function fixTheFuckingRetardedResponseFromNokiaAPI(string $input): string
	{
		return preg_replace_callback('#(^\s+"message" : ")(.*)"$#m', static function ($matches) {
			return $matches[1] . str_replace('"', '\"', $matches[2]) . '"';
		}, $input);
	}
	#endregion
}
