<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\Message\APayloadMessage;
use Telenor\Message\UserMessage;

class UserMessageTest extends TestCase
{

	public function testSetInitials()
	{
		$user = new UserMessage(APayloadMessage::ACTION_CREATE);
		$value = 'TPN';
		$user->setInitials($value);
		$this->assertEquals($value, $user->getInitials());
	}

	public function testSetInitialsLengthCut()
	{
		$user = new UserMessage(APayloadMessage::ACTION_CREATE);
		$value = 'TPNxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'; //41 char cut to 40
		$user->setInitials($value);
		$this->assertNotEquals($value, $user->getInitials());
	}

	public function testSetFullname()
	{
		$user = new UserMessage(APayloadMessage::ACTION_CREATE);
		$value = 'Hans Hansen';
		$user->setFullname($value);
		$this->assertSame($value, $user->getFullname());
	}

	public function testsetGlobalEmployeeNumberWithNegative()
	{
		$user = new UserMessage(APayloadMessage::ACTION_CREATE);
		$value = -123;
		$this->expectException(UnexpectedValueException::class);
		$user->setGlobalEmployeeNumber($value);
	}

	public function testsetGlobalEmployeeNumber()
	{
		$user = new UserMessage(APayloadMessage::ACTION_CREATE);
		$value = 12345;
		$user->setGlobalEmployeeNumber($value);
		$this->assertSame($value, $user->getGlobalEmployeeNumber());
	}

	public function testsetEmail()
	{
		$user = new UserMessage(APayloadMessage::ACTION_CREATE);
		$value = 'test@test.dk';
		$user->setEmail($value);
		$this->assertSame($value, $user->getEmail());
	}

}
