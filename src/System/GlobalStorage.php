<?php
declare(strict_types=1);

namespace Telenor\System;

abstract class GlobalStorage
{
	protected static $uuid;

	public static function getUUID(): ?string
	{
		return self::$uuid;
	}

	public static function setUUID(string $uuid): void
	{
		self::$uuid = $uuid;
	}
}