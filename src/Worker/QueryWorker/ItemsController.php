<?php
declare(strict_types=1);

namespace Telenor\Worker\QueryWorker;

use Amp\Http\Server\Request;
use Amp\Http\Server\Response;
use Amp\Http\Server\Router;
use Telenor\Logging\TLog;
use Telenor\Service\ItemsService;
use Telenor\System\WebUtils;
use Throwable;

class ItemsController
{
	use TLog;

	/** @var ItemsService */
	protected $itemsService;

	public function __construct(ItemsService $itemsService)
	{
		$this->itemsService = $itemsService;
	}

	public function changes(Request $request): Response
	{
		try {
			$item = $this->getAttribute($request, 'item');
			if ($item === null)
				return WebUtils::jsonResponseClientError('No item specified');

			return WebUtils::jsonResponseOK('Success', 200, ['entries' => $this->itemsService->getChanges($item)]);
		} catch (Throwable $t) {
			$error = 'Failed to find changes';
			$this->logError($error, [$t]);
			return WebUtils::jsonResponseServerError('Server Error', 500, ['error' => $error]);
		}
	}

	public function history(Request $request): Response
	{
		try {
			$item = $this->getAttribute($request, 'item');
			if ($item === null)
				return WebUtils::jsonResponseClientError('No item specified');

			$this->logInfo("Searching ServiceNow for item: $item");

			return WebUtils::jsonResponseOK('Success', 200, ['entries' => $this->itemsService->getHistory($item)]);
		} catch (Throwable $t) {
			$error = 'Failed to find history';
			$this->logError($error, [$t]);
			return WebUtils::jsonResponseServerError('Server Error', 500, ['error' => $error]);
		}
	}

	protected function getAttribute(Request $request, string $key): ?string
	{
		$attributes = $request->getAttribute(Router::class);
		return isset($attributes[$key]) ? (string)$attributes[$key] : null;
	}
}