<?php
declare(strict_types=1);
namespace Telenor\System;

/*
 * A generic class with static members which contains useful small
 * helper functions that can be used throughout the application.
 */

use Exception;
use ReflectionClass;
use ReflectionException;
use function class_exists;
use function class_implements;
use function explode;
use function get_class;
use function get_resource_type;
use function gethostname;
use function in_array;
use function is_array;
use function is_bool;
use function is_float;
use function is_resource;
use function is_string;
use function mb_strlen;
use function random_int;
use function str_pad;

final class Utils
{
	protected static $shortWorkerName;
	protected static $uniqueWorkerName;

	/**
	 * Checks if a string is null or empty.
	 *
	 * @param string|null $string The value to check.
	 * @return bool True if the string is empty or null, false otherwise.
	 */
	public static function isStringNullOrEmpty(?string $string): bool
	{
		return ($string === null || $string === '');
	}

	/**
	 * Determines whether the beginning of a string instance matches a specified string.
	 *
	 * @param string $haystack The complete string to test.
	 * @param string $needle The substring to find.
	 * @return bool True if needle is found in the beginning of the haystack, false otherwise.
	 */
	public static function startsWith(string $haystack, string $needle): bool
	{
		return strncmp($haystack, $needle, strlen($needle)) === 0;
	}

	/**
	 * Determines whether the end of a string instance matches a specified string.
	 *
	 * @param string $haystack The complete string to test.
	 * @param string $needle The substring to find.
	 * * @return bool True if needle is found in the end of the haystack, false otherwise.
	 */
	public static function endsWith($haystack, $needle): bool
	{
		$length = strlen($needle);
		if ($length === 0)
			return true;

		return (substr($haystack, -$length) === $needle);
	}

	/**
	 * Prints an error message to stdout and terminates the application with a non-zero exit code.
	 *
	 * @param string $message The error message to print.
	 * @param int $exitCode The exit code.
	 */
	public static function kill(string $message, int $exitCode = 1): void
	{
		echo "Error: $message\n";
		exit($exitCode);
	}

	/**
	 * Generates a pseudo-random v4 UUID.
	 *
	 * @return string A new (unique) UUID.
	 * @throws Exception If random values could not be generated.
	 */
	public static function generateUUID(): string
	{
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', // 32 bits for "time_low"
			random_int(0, 0xffff), random_int(0, 0xffff),

			// 16 bits for "time_mid"
			random_int(0, 0xffff),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			random_int(0, 0x0fff) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			random_int(0, 0x3fff) | 0x8000,

			// 48 bits for "node"
			random_int(0, 0xffff), random_int(0, 0xffff), random_int(0, 0xffff));
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public static function generateIncidentCorrelationId(): string
	{
		return self::generateCorrelationId('inc');
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public static function generateTaskCorrelationId(): string
	{
		return self::generateCorrelationId('tas');
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public static function generateChangeCorrelationId(): string
	{
		return self::generateCorrelationId('chg');
	}

	/**
	 * @param string $prefix
	 * @return string
	 * @throws Exception
	 */
	protected static function generateCorrelationId(string $prefix): string
	{
		$keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < 12; ++$i)
			$str .= $keyspace[random_int(0, $max)];
		return sprintf('%s-%s', $prefix, $str);
	}

	public static function getShortWorkerName(): string
	{
		if (self::isStringNullOrEmpty(self::$shortWorkerName))
			self::$shortWorkerName = self::getShortClassName(Config::envString('WORKER_CLASS', '(unknown worker)'));
		return self::$shortWorkerName;
	}

	public static function getUniqueWorkerName(): string
	{
		if (self::isStringNullOrEmpty(self::$uniqueWorkerName))
			self::$uniqueWorkerName = self::getShortWorkerName() . '_' . gethostname();
		return self::$uniqueWorkerName;
	}

	public static function toStringRecursive(string &$msg, array $arr, int $level = 1): void
	{
		$pad = str_pad(' ', $level * 2);
		foreach ($arr as $key => $value) {
			$msg .= "\n$pad";
			$msg .= "$key = ";
			if ($value === null) {
				$msg .= '(null)';
			} else if (is_array($value)) {
				$msg .= '[';
				self::toStringRecursive($msg, $value, $level + 1);
				$msg .= "\n$pad]";
			} else if (is_int($value) || is_float($value) || is_string($value)) {
				$msg .= $value;
			} else if (is_bool($value)) {
				$msg .= $value ? 'true' : 'false';
			} else if (is_resource($value)) {
				$msg .= '(resource: ' . get_resource_type($value) . ')';
			} else {
				$msg .= '(object: ' . get_class($value) . ')';
			}
		}
	}

	public static function getShortClassName($class): string
	{
		try {
			$name = (new ReflectionClass($class))->getShortName();
		} catch (ReflectionException $re) {
			if (class_exists($class)) {
				$name = get_class($class);
			} else {
				$name = '(UNKNOWN CLASS)';
			}
		}

		return $name;
	}

	/**
	 * Trunates a string to $maxLength characters. Replaces the end with [...].
	 *
	 * @param string $input
	 * @param int $maxLength
	 * @return string
	 */
	public static function truncateString(string $input, int $maxLength): string
	{
		if (strlen($input) > $maxLength) {
			return substr($input, 0, $maxLength - 6) . " [...]";
		}
		return $input;
	}

	public static function implementsInterface(string $class, string $interface): bool
	{
		return class_exists($class) && in_array($interface, class_implements($class), true);
	}

	public static function mapArrayToHashMap(array $input, string $delimiter = '='): array
	{
		$array = [];
		foreach ($input as $entry) {
			[$name, $value] = explode($delimiter, $entry, 2);
			$array[$name] = $value;
		}
		return $array;
	}

	public static function mapStringToHashMap(string $input, string $delimiter = ';', string $keyDelimiter = '='): array
	{
		return self::mapArrayToHashMap(explode($delimiter, $input), $keyDelimiter);
	}
}
