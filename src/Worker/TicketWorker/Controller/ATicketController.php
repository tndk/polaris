<?php
declare(strict_types=1);

namespace Telenor\Worker\TicketWorker\Controller;

use Amp\Http\Server\Request;
use Amp\Http\Server\Response;
use Amp\Http\Server\Router;
use DateTime;
use Exception;
use InvalidArgumentException;
use JsonException;
use Telenor\Logging\TLog;
use Telenor\Message\ATicketMessage;
use Telenor\MessageQueue\TMessageQueue;
use Telenor\System\Exception\ConnectionException;
use Telenor\System\Exception\InvalidDataException;
use Telenor\System\Exception\InvalidRequestException;
use Telenor\System\Exception\ValidationException;
use Telenor\System\Utils;
use Telenor\System\WebUtils;
use Throwable;
use function is_array;
use function json_decode;
use function ucfirst;
use const JSON_THROW_ON_ERROR;

abstract class ATicketController
{
	use TLog;
	use TMessageQueue;

	protected function getBody(Request $request)
	{
		$body = yield $request->getBody()->buffer();
		$json_body = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

		if (isset($request->getAttribute(Router::class)['id']))
			$json_body['correlation_id'] = $request->getAttribute(Router::class)['id'];
		return $json_body;

	}

	/**
	 * @param array $data
	 * @param ATicketMessage $message
	 * @throws InvalidDataException
	 * @throws ValidationException
	 * @throws Exception
	 */
	public function createTicket(array $data, ATicketMessage $message): void
	{

		// Mandatory fields
		$message->setSource($this->getFieldString($data, 'source'));
		$message->setCorrelationId($message->getSource() . '-' . (isset($data['change_type']) ? Utils::generateChangeCorrelationId() : Utils::generateIncidentCorrelationId()));
		//$message->setCorrelationId($message->getSource() . '-' . Utils::generateIncidentCorrelationId());
		$message->setShortDescription($this->getFieldString($data, 'short_description'));
		$message->setDescription($this->getFieldString($data, 'description'));
		$message->setOpenedBy($this->getFieldString($data, 'opened_by'));
		$message->addExtendedPayload('ExternalTicketId',
			$this->getFieldString($data, 'ExternalTicketId', false, $message->getCorrelationId()));

		$this->setOptionalFields($message, $data);

	}

	/**
	 * @param array $data
	 * @param ATicketMessage $message
	 * @throws InvalidDataException
	 * @throws ValidationException
	 */
	public function updateTicket(array $data, ATicketMessage $message): void
	{
		//$message->setSource(null); // Should be possible to change source in updata
		$message->setSource($this->getFieldString($data, 'source'));
		$message->setWorknotes($this->getFieldString($data, 'worknotes'));
		$message->setCorrelationId($this->getFieldString($data, 'correlation_id'));

		$message->setShortDescription($this->getFieldString($data, 'short_description', false, ''));
		$message->setDescription($this->getFieldString($data, 'description', false, ''));
		$message->setOpenedBy($this->getFieldString($data, 'opened_by', false, ''));

		$this->setOptionalFields($message, $data);

	}

	/**
	 * @param ATicketMessage $message
	 * @param array $data
	 * @throws InvalidDataException
	 * @throws ValidationException
	 */
	protected function setOptionalFields(ATicketMessage $message, array $data): void
	{

		// Optional fields
		$message->setImpact($this->getFieldInt($data, 'impact', false, 0));
		$message->setUrgency($this->getFieldInt($data, 'urgency', false, 0));
		$message->setPriority($this->getFieldInt($data, 'priority', false, 0));
		$message->setAssignedUser($this->getFieldString($data, 'assigned_user', false, ''));
		$message->setAssignedGroup($this->getFieldString($data, 'assigned_group', false, ''));
		$message->setRequestedFor($this->getFieldString($data, 'requested_for', false, ''));
		$message->setWorknotes($this->getFieldString($data, 'worknotes', false, ''));
		$message->addExtendedPayload('ExternalTicketId', $this->getFieldString($data, 'serial_number', false));
		$message->setStatus($this->getFieldString($data, 'status', false, 'New'));
		$message->addExtendedPayload('Item', $this->getFieldString($data, 'Item', false));

		// Attachment fields
		if (isset($data['attachments']) && is_array($data['attachments'])) {
			foreach ($data['attachments'] as $name => $file)
				$message->addAttachment($name, $file);
		}

		// Misc. extra unstructured data fields.. can be anything really
		if (isset($data['additional_info']) && is_array($data['additional_info'])) {
			foreach ($data['additional_info'] as $name => $file)
				$message->addExtendedPayload(ucfirst($name), $file);
		}

	}

	/**
	 * @param ATicketMessage $message
	 * @return Response
	 * @throws ConnectionException
	 */
	protected function handleMessage(ATicketMessage $message): Response
	{
		$this->publishToQueue($message);

		return WebUtils::jsonResponseOK('Accepted', 200, ['ticketid' => $message->getCorrelationId()]);

	}

	/**
	 * @param array $data
	 * @param string $key
	 * @param bool $required
	 * @param string|null $defaultValue
	 * @return string|null
	 * @throws InvalidDataException
	 */
	protected function getFieldString(array &$data, string $key, bool $required = true,
	                                  string $defaultValue = null): ?string
	{
		$value = $this->getField($data, $key, $required, $defaultValue);
		if ($value === null) {
			return null;
		}

		return (string)$value;
	}

	/**
	 * @param array $data
	 * @param string $key
	 * @param bool $required
	 * @param int|null $defaultValue
	 * @return int|null
	 * @throws InvalidDataException
	 */
	protected function getFieldInt(array &$data, string $key, bool $required = true, ?int $defaultValue = null): ?int
	{
		$value = $this->getField($data, $key, $required, $defaultValue);

		if ($value !== null && !ctype_digit((string)$value))
			throw new InvalidDataException('Expected integer JSON value', $key);

		if ($value !== null)
			return (int)$value;

		return $value;
	}

	/**
	 * @param array $data
	 * @param string $key
	 * @return DateTime|null
	 * @throws InvalidRequestException
	 */
	protected function getFieldDateTime(array &$data, string $key): ?DateTime
	{
		try {
			if (isset($data[$key]))
				return new DateTime($data[$key]);
		} catch (Exception $e) {
			$this->logError("Failed to parse date in key $key: " . $data[$key]);
			throw new InvalidRequestException("Failed to parse date in field: $key");
		}
		return null;
	}

	/**
	 * @param array $data
	 * @param string $key
	 * @param bool $required
	 * @param null $defaultValue
	 * @return mixed|null
	 * @throws InvalidDataException
	 */
	protected function getField(array &$data, string $key, bool $required = true, $defaultValue = null)
	{
		if (!isset($data[$key])) {
			if ($required)
				throw new InvalidDataException('Required JSON key missing', $key);
			return $defaultValue;
		}

		return $data[$key];
	}

	protected function handleException(Throwable $throwable): Response
	{
		if ($throwable instanceof InvalidArgumentException)
			return WebUtils::jsonResponseClientError($throwable->getMessage());

		if ($throwable instanceof ValidationException)
			return WebUtils::jsonResponseClientError($throwable->getMessage(), 400,
				['key' => $throwable->getProperty()]);

		if ($throwable instanceof InvalidDataException)
			return WebUtils::jsonResponseClientError($throwable->getMessage(), 400, ['key' => $throwable->getEntry()]);

		if ($throwable instanceof JsonException) {
			$this->logError('Failed to parse JSON body', [$throwable]);
			return WebUtils::jsonResponseClientError('Invalid JSON data received');
		}

		$this->logError('Unexpected error occurred', [$throwable]);
		return WebUtils::jsonResponseServerError();

	}

}