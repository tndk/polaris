<?php
declare(strict_types=1);

namespace Telenor\Model;

class ServiceNowIncidentsByItem extends AServiceNowModel
{
	public function getEndpoint(): string
	{
		return 'td/tickets/by_item';
	}

	public function getFields(): array
	{
		return ['Description'];
	}
}