<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Telenor\Message\SMSMessage;
use Telenor\System\Exception\InvalidMessageException;

class SMSMessageTest extends TestCase
{
	public function testCreationFailed1(): void
	{
		$this->expectException(InvalidMessageException::class);
		new SMSMessage([]);
	}

	public function testCreationFailed2(): void
	{
		$this->expectException(InvalidMessageException::class);
		new SMSMessage([
			'type'           => 'non sense',
			'transaction_id' => '1234',
			'payload'        => [
				'sender'    => '1234',
				'recipient' => '4321',
				'message'   => 'something',
			],
		]);
	}

	public function testCreationOk1(): void
	{
		$msg = new SMSMessage([
			'type'           => 'sms',
			'transaction_id' => '1234',
			'timestamp'      => '2019-05-29T09:10:17+0100',
			'payload'        => [
				'sender'    => '1234',
				'recipient' => '4321',
				'message'   => 'something',
			],
		]);

		static::assertInstanceOf(SMSMessage::class, $msg);
	}

	public function testCreationOk2(): void
	{
		$msg = new SMSMessage([
			'type'           => 'password sms',
			'transaction_id' => '1234',
			'timestamp'      => '2019-05-29T09:10:17+0100',
			'payload'        => [
				'sender'    => '1234',
				'recipient' => '4321',
				'message'   => 'something',
			],
		]);

		static::assertInstanceOf(SMSMessage::class, $msg);
	}

	public function testCreationOk3(): void
	{
		// Backslash escaped correctly
		$msg = new SMSMessage(json_decode('{"transaction_id":"85bd3c05-a64e-45d5-aa90-d2c81a7af1cd","timestamp":"2019-09-30 09:24:17","type": "password sms","action": "create","source": "ServiceNow-SMS","payload" :{"sender": "0000","recipient": "0000","message": "Your new windows / Citrix password is: hest#123  Username: DOMAIN\\\\hest\n\nRegards\nMETRO Servicedesk"}}',
			true, 512, JSON_THROW_ON_ERROR));

		static::assertInstanceOf(SMSMessage::class, $msg);
	}
}
