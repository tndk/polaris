<?php
declare(strict_types=1);
namespace Telenor\Worker\XMLGatewayWorker;

use Psr\Log\LogLevel;
use SimpleXMLElement;
use Telenor\Logging\LoggingController;
use Telenor\Logging\TLog;
use Telenor\System\Exception\InvalidRequestException;

class XMLParser
{
	use TLog;

	/**
	 * Parses a SOAP request to an array
	 *
	 * @param string $input Raw XML body
	 * @return array Parsed data
	 * @throws InvalidRequestException
	 */
	public static function soapToArray(string $input): array
	{
		$data = [];
		$clean_xml = self::cleanSoap($input);
		$xml = simplexml_load_string($clean_xml, null, LIBXML_NOCDATA);
		if ($xml === false || $xml->children()->count() < 1) {
			LoggingController::getInstance()->log(LogLevel::DEBUG, sprintf("XML input error. Cleaned input:\n%s", $clean_xml));
			throw new InvalidRequestException('XML input is empty or has no <Body> element');
		}

		if ($xml->children()->count() < 1) {
			throw new InvalidRequestException('XML input has no elements');
		}

		// Assign to array (and turn empty strings into NULL)
		// TODO: harden
		foreach ($xml->children() as $base) {
			if ($base->getName() === 'Body') {
				foreach ($xml->Body->children() as $child) {
					if (preg_match('/^\s*<\?*(xml|create|update)/i', trim((string)$child))) {
						$innerXml = simplexml_load_string(trim((string)$child));
						$data['action'] = self::getAction($innerXml->getName());
						$data[$child->getName()] = self::xmlObjectToArray($innerXml);
					} else {
						$data[$child->getName()] = $child == '' ? null : trim((string)$child);
					}
				}
			}
		}

		// Extract template from inner XML for queries
		if (isset($data['xml']['template'])) {
			$data['template'] = $data['xml']['template'];
			unset($data['xml']['template']);
		}

		if (isset($data['xml'])) {
			$data['fields'] = $data['xml'];
			unset($data['xml']);
		}

		$data['request_format'] = 'soap';
		return self::parse($data);
	}

	/**
	 * Converts and parses URL-encoded parameters
	 *
	 * @param string $input Raw body of HTTP request
	 * @return array
	 * @throws InvalidRequestException
	 */
	public static function paramsToArray(string $input): array
	{
		$parsed = [];
		foreach (explode('&', $input) as $chunk) {
			$param = explode('=', $chunk);
			if (!empty($param[1]))
				$parsed[urldecode($param[0])] = urldecode($param[1]);
		}

		$rawXml = trim(urldecode($parsed['xml']));
		if (preg_match('/^\s*<\?*(xml|(my)*(create|update))/i', $rawXml)) {
			$innerXml = simplexml_load_string($rawXml);
			$parsed['fields'] = self::xmlObjectToArray($innerXml);
			$parsed['action'] = self::getAction($innerXml->getName());
			unset($parsed['xml']);
		}

		$parsed['request_format'] = 'urlencoded';
		return self::parse($parsed);
	}

	/**
	 * @param array $data Unpacked request with XML still as string
	 * @return array
	 * @throws InvalidRequestException
	 */
	private static function parse(array $data): array
	{
		// Check that required fields are present
		if (!self::checkParsed($data)) {
			LoggingController::getInstance()->log(LogLevel::DEBUG, sprintf("Unexpected input. Dump:\n%s", print_r($data, true)));
			throw new InvalidRequestException('XML input is lacking \'xml\' or \'template\' fields');
		}

		$data['source'] = self::getSource($data);
		$data['type'] = self::getTypeFromTemplate($data['template']);
		$data['correlation_id'] = self::getCorrelationId($data);
		unset($data['xml'], $data['server']);
		return $data;
	}

	private static function checkParsed(array $data): bool
	{
		if (empty($data['fields']))
			return false;

		if (empty($data['template']))
			return false;

		return true;
	}

	/**
	 * Recursively unpack SimpleXMLElement into array. Does not extract properties!
	 *
	 * @param SimpleXMLElement $xmlObject
	 * @return array
	 */
	private static function xmlObjectToArray(SimpleXMLElement $xmlObject): array
	{
		$array = [];
		foreach ($xmlObject->children() as $node) {
			// TODO: extract properties? needed for queries
			$name = $node->getName();
			if ($node->count() > 0) {
				$array[$name] = self::xmlObjectToArray($node);
			} else if ($name === 'parameter') {
				$parameterName = $parameterValue = '';
				foreach ($node->attributes() as $attributeName => $attributeValue) {
					if ($attributeName === 'name')
						$parameterName = (string)$attributeValue;
					if ($attributeName === 'value')
						$parameterValue = (string)$attributeValue;
				}
				$array[$name][$parameterName] = $parameterValue;
			} else {
				$array[$name] = trim((string)$node);
			}
		}
		return $array;
	}

	private static function getAction(string $input): string
	{
		if (stripos($input, 'update') !== false)
			return 'update';

		if (stripos($input, 'create') !== false)
			return 'create';

		if (stripos($input, 'query') !== false)
			return 'query';

		return '';
	}

	private static function getSource(array $data): string
	{
		$fields = $data['fields'];

		/* @var string[][] $matrix */
		$matrix = [
			'veris' => [
				'AILK_CREATE_TICKET',
				'AILK_CLOSE_TICKET',
				'AILK_REOPEN_TICKET',
			],
			'7913'  => [
				'SC_HD_CREATE_7913',
			],
		];

		/* @var string[] $templates */
		foreach ($matrix as $source => $templates) {
			foreach ($templates as $template) {
				if ($template === $data['template'])
					return $source;
			}
		}

		// If template is unknown or used by several clients, try to determine from fields
		// TODO: add more checks
		if (isset($fields['VerisId']))
			return 'veris';

		if (isset($fields['Initiator']) && $fields['Initiator'] === 'Nokia')
			return 'nokia';

		if (isset($fields['Type']) && $fields['Type'] === 'Skywalker')
			return 'skywalker';

		if (isset($fields['Worklog']) && strpos($fields['Worklog'], 'Skywalker Incident') !== false)
			return 'skywalker';

		if (isset($fields['IncidentAssigned']) && $fields['IncidentAssigned'] === 'Nokia')
			return 'nokia';

		if (isset($fields['UserID']) && $fields['UserID'] === 'Nokia')
			return 'nokia';

		return '';
	}

	private static function getCorrelationId(array $data): string
	{
		$fields = $data['fields'];
		$id = '';
		$source = !empty($data['source']) ? $data['source'] . '-' : '';
		$action = $data['action'];

		if ($action === 'create') {
			//	if (!empty($fields['JiraID']))
			//		$id = $fields['JiraID'];

			//	if (!empty($fields['ExternalTicketId']))
			//		$id = $fields['ExternalTicketId'];

			//	if (!empty($fields['VerisId']))
			//		$id = $fields['VerisId'];

			//	if (isset($fields['Worklog']) && strpos($fields['Worklog'], 'Skywalker Incident') !== false) {
			//		$regex = '/opEvents\/events\/(\w+)\//';
			//		if (preg_match_all($regex, $fields['Worklog'], $matches) === 1)
			//			return 'skywalker-' . $matches[1][0];
			//	}

			//	if ($id === '') {
			return $source . self::generateCorrelationId();
			//	}
			//	return $source . $id;
		}

		if ($action === 'update') {
			if (!empty($fields['VendorTicketID']))
				$id = $fields['VendorTicketID'];

			if (!empty($fields['TicketID']))
				$id = $fields['TicketID'];

			return $id;
		}

		return $source . self::generateCorrelationId();
	}

	private static function generateCorrelationId(): string
	{
		$length = 12;
		$keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$pieces = [];
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$pieces [] = $keyspace[random_int(0, $max)];
		}
		return implode('', $pieces);
	}

	private static function getTypeFromTemplate(string $template): string
	{
		/* @var string[][] $matrix */
		$matrix = [
			'change' => [
				'HD_CREATE_ONLY_DARE',
				'HD_UPDATE_ONLY',
			],
		];

		foreach ($matrix['change'] as $match) {
			if ($template === $match)
				return 'change';
		}
		return 'incident';
	}

	private static function cleanSoap(string $input): string
	{
		$clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:', 'SOAPENV:'], '', $input);
		foreach (['ns1:query', 'ns1:post', 'xg:post'] as $tag) {
			$clean_xml = preg_replace("(<\/?$tag.*?>)", '', $clean_xml);
		}
		return trim($clean_xml);
	}

	public static function makeResponse(array $parsed): string
	{
		$remedyMessage = '';

		$operation = $parsed['action'];
		$type = $parsed['type'];
		$id = $parsed['correlation_id'];

		// TODO: handle changes
		if ($type === 'incident') {
			if ($operation === 'create') {
				$remedyMessage = "<success><object name='SC_Helpdesk'><change operation='created'>$id</change></object></success>";
			} else if ($operation === 'update') {
				if (!empty($parsed['fields']['TicketID']))
					$id = $parsed['fields']['TicketID'];
				$remedyMessage = "<success><object name='SC_Helpdesk'><change operation='updated'>$id</change></object></success>";
			} else {
				$remedyMessage = "<success><object name='SC_Helpdesk'><change></change></object></success>";
			}
		}

		// If the client is using SOAP, we need to wrap the Remedy XML in SOAP junk.
		if ($parsed['request_format'] === 'soap') {
			$encoded = htmlspecialchars($remedyMessage);
			return <<<EOD
<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<soapenv:Body>
		<ns1:postResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://ivabtst06:8080/xmlgateway/services/XMLGateway">
			<postReturn xsi:type="xsd:string">$encoded</postReturn>
		</ns1:postResponse>
	</soapenv:Body>
</soapenv:Envelope>
EOD;
		}

		return $remedyMessage;
	}
}