<?php
declare(strict_types=1);

namespace Telenor\Message;

use DateTime;
use Telenor\System\Exception\InvalidDataException;
use Telenor\System\Exception\ValidationException;
use Telenor\System\Utils;
use function array_merge;
use function in_array;
use function str_replace;
use function strlen;
use function substr;
use function trim;

abstract class ATicketMessage extends APayloadMessage
{
	public const STATUS_NO_CHANGE = '';

	public const DATETIME_FORMAT = 'Y-m-d H:i:s';

	public const CMDB_CI_UNKNOWN = 'cmdb_ci_unknown';

	/** @var string */
	protected $template;

	#region Root level fields

	/** @var string */
	protected $source = 'unknown';

	/** @var string */
	protected $correlationId;

	/** @var string[] */
	protected $extendedPayload = [];

	/** @var string[][] */
	protected $attachments = [];
	#endregion

	#region Payload fields
	/** @var string */
	protected $shortDescription;

	/** @var string */
	protected $description;

	/** @var int */
	protected $impact = 0;

	/** @var int */
	protected $urgency = 0;

	/** @var int */
	protected $priority = 0;

	/** @var string */
	protected $status;

	/** @var string */
	protected $assignedUser = '';

	/** @var string|null */
	protected $assignedGroup;

	/** @var string */
	protected $openedBy;

	/** @var string */
	protected $requestedFor = '';

	/** @var string|null */
	protected $worknotes;

	#endregion

	/**
	 * @return string
	 */
	public function getTemplate(): string
	{
		return $this->template;
	}

	/**
	 * @param string $template
	 */
	public function setTemplate(string $template): void
	{
		$this->template = $template;
	}

	/**
	 * @return string
	 */
	public function getSource(): ?string
	{
		return $this->source;
	}

	/**
	 * @param string $source
	 */
	public function setSource(?string $source): void
	{
		$this->source = $source;
	}

	/**
	 * @return string
	 */
	public function getCorrelationId(): string
	{
		return $this->correlationId;
	}

	/**
	 * @param string $correlationId
	 */
	public function setCorrelationId(string $correlationId): void
	{
		$this->correlationId = $correlationId;
	}

	public function setExtendedPayload(array $value): void
	{
		$this->extendedPayload = $value;
	}

	public function getExtendedPayload(): array
	{
		return $this->extendedPayload;
	}

	public function addExtendedPayload(string $key, ?string $value): void
	{
		if ($value !== null)
			$this->extendedPayload[$key] = $value;
	}

	/**
	 * @return string[][]
	 */
	public function getAttachments(): array
	{
		return $this->attachments;
	}

	/**
	 * @param string $name
	 * @param string $data
	 */
	public function addAttachment(string $name, string $data): void
	{
		$this->attachments[$name] = $data;
	}

	/**
	 * @return string
	 */
	public function getShortDescription(): string
	{
		return $this->shortDescription;
	}

	/**
	 * @param string $shortDescription
	 */
	public function setShortDescription(string $shortDescription): void
	{
		if (strlen($shortDescription) > 155) {
			$shortDescription = trim(str_replace(["\r", "\n"], '', $shortDescription));
			$shortDescription = substr($shortDescription, 0, 155) . '...';
		}

		$this->shortDescription = $shortDescription;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description): void
	{
		$this->description = $description;
	}

	/**
	 * @return int
	 */
	public function getImpact(): int
	{
		return $this->impact;
	}

	/**
	 * @param int $impact
	 * @throws ValidationException
	 */
	public function setImpact(int $impact): void
	{
		if ($impact < 0 || $impact > 3)
			throw new ValidationException('impact', '' . $impact, '', 'Value must be an integer in range 1-3.');

		$this->impact = $impact;
	}

	/**
	 * @return int
	 */
	public function getUrgency(): int
	{
		return $this->urgency;
	}

	/**
	 * @param int $urgency
	 * @throws ValidationException
	 */
	public function setUrgency(int $urgency): void
	{
		if ($urgency < 0 || $urgency > 3)
			throw new ValidationException('urgency', '' . $urgency, '', 'Value must be an integer in range 1-3.');

		$this->urgency = $urgency;
	}

	/**
	 * @return int
	 */
	public function getPriority(): int
	{
		return $this->priority;
	}

	/**
	 * @param int $priority
	 * @throws ValidationException
	 */
	public function setPriority(int $priority): void
	{
		if ($priority < 0 || $priority > 5)
			throw new ValidationException('priority', '' . $priority, '', 'Value must be an integer in range 1-5.');

		$this->priority = $priority;
	}

	/**
	 * @return string|null
	 */
	public function getStatus(): ?string
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 * @throws ValidationException
	 */
	public function setStatus(string $status): void
	{
		if (!in_array($status, array_merge([self::STATUS_NO_CHANGE], $this->getValidStates()), true))
			throw new ValidationException('status', $status, '', 'Invalid value');

		$this->status = $status;
	}

	abstract protected function getValidStates(): array;

	/**
	 * @return string
	 */
	public function getAssignedUser(): string
	{
		return $this->assignedUser;
	}

	/**
	 * @param string $assignedUser
	 */
	public function setAssignedUser(string $assignedUser): void
	{
		$this->assignedUser = $assignedUser;
	}

	/**
	 * @return string|null
	 */
	public function getAssignedGroup(): ?string
	{
		return $this->assignedGroup;
	}

	/**
	 * @param string $assignedGroup
	 */
	public function setAssignedGroup(string $assignedGroup): void
	{
		$this->assignedGroup = $assignedGroup;
	}

	/**
	 * @return string|null
	 */
	public function getOpenedBy(): ?string
	{
		return $this->openedBy;
	}

	/**
	 * @param string $openedBy
	 */
	public function setOpenedBy(string $openedBy): void
	{
		$this->openedBy = $openedBy;
	}

	/**
	 * @return string
	 */
	public function getRequestedFor(): string
	{
		return $this->requestedFor;
	}

	/**
	 * @param string $requestedFor
	 */
	public function setRequestedFor(string $requestedFor): void
	{
		$this->requestedFor = $requestedFor;
	}

	/**
	 * @return string|null
	 */
	public function getWorknotes(): ?string
	{
		return $this->worknotes;
	}

	/**
	 * @param string $worknotes
	 */
	public function setWorknotes(string $worknotes): void
	{
		$this->worknotes = $worknotes;
	}

	protected function convertDateTime(?DateTime $dateTime): ?string
	{
		return ($dateTime !== null) ? $dateTime->format(self::DATETIME_FORMAT) : null;
	}

	protected function getCmdb_ci(array $item): string
	{
		if (!isset($item['Item'])) {
			return self::CMDB_CI_UNKNOWN;

		}

		$array = preg_split("/[\,\.\;]+/", $item['Item']); //Delimiter order by SYEL

		return Utils::isStringNullOrEmpty($array[0]) ? self::CMDB_CI_UNKNOWN : $array[0];

	}

	/**
	 * @return array
	 */
	public function getAttributes(): array
	{

		// We need to override this method in order to set
		// properties at the payload root level
		return array_merge(parent::getAttributes(), [
			'source'             => $this->getSource(),
			'correlation_id'     => $this->getCorrelationId(),
			'payload_original'   => $this->getExtendedPayload(),
			'payload_attachment' => $this->getAttachments(),
		]);
	}

	public function toArray(): array
	{

		if ($this->getAction() === self::ACTION_CREATE) {
			return [
				'short_description' => $this->getShortDescription(),
				'description'       => $this->getDescription(),
				'impact'            => (string)$this->getImpact(),
				'urgency'           => (string)$this->getUrgency(),
				'priority'          => (string)$this->getPriority(),
				'status'            => $this->getStatus(),
				'assigned_user'     => $this->getAssignedUser(),
				'assigned_group'    => $this->getAssignedGroup(),
				'opened_by'         => $this->getOpenedBy(),
				'requested_for'     => $this->getRequestedFor(),
				'worknotes'         => $this->getWorknotes(),
				'cmdb_ci'           => $this->getCmdb_ci($this->getExtendedPayload()),
			];
		}

		if ($this->getAction() === self::ACTION_UPDATE) {
			return [
				'worknotes'      => $this->getWorknotes(),
				'status'         => $this->getStatus(),
				'assigned_group' => $this->getAssignedGroup(),
				'impact'         => (string)$this->getImpact(),
				'urgency'        => (string)$this->getUrgency(),
				'priority'       => (string)$this->getPriority(),
				'cmdb_ci'        => $this->getCmdb_ci($this->getExtendedPayload()),

			];
		}

		return [];

	}

	/**
	 * @throws InvalidDataException If data validation failed
	 */
	public function validate(): void
	{
		parent::validate();

		if (!in_array($this->getAction(), [self::ACTION_UPDATE, self::ACTION_CREATE], true))
			throw new InvalidDataException('Cannot build message due to invalid action');

		if ($this->getStatus() === null)
			throw new InvalidDataException('No or invalid status set. Current value: <' . $this->getStatus() . '>');

		if ($this->getAction() === 'create' && $this->getPriority() === 0) {
			if ($this->getUrgency() === 0 || $this->getImpact() === 0)
				throw new InvalidDataException("Expected either 'priority' or ('impact' and 'urgency')");
		}
	}
}