<?php
declare(strict_types=1);
namespace Telenor\System\Error;

use Throwable;

/**
 * Error to use when a required configuration parameter is not available.
 */
class BadConfigurationError extends PolarisError
{
	protected $configName;

	public function __construct(string $configName, string $message = null, Throwable $previous = null)
	{
		if ($message === null)
			$message = "Missing or invalid configuration parameter: $configName";

		parent::__construct($message, $previous);
		$this->configName = $configName;
	}

	/**
	 * Retrieve the config name that are not available.
	 *
	 * @return string The name of the required config name.
	 */
	public function getConfigName(): string
	{
		return $this->configName;
	}
}