<?php

namespace Telenor\Model;

use Telenor\System\Exception\InvalidDataException;

abstract class AServiceNowModel implements IServiceNowModel
{
	protected $values = [];

	public function __construct(array $values = [])
	{
		foreach ($values as $key => $value) {
			if (!isset($this->getFields()[$key]))
				throw new InvalidDataException("Key '$key' is not part of an Incident");
		}

		$this->values = $values;
	}

	public function getFilters(string $id): array
	{
		return ['sys_id' => $id];
	}

	public function getValues(): array
	{
		return $this->values;
	}
}