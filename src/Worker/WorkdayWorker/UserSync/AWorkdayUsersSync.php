<?php
declare(strict_types=1);

namespace Telenor\Worker\WorkdayWorker\UserSync;

use DateTimeImmutable;
use PDO;
use PDOException;
use Telenor\Logging\TLog;
use Telenor\Model\IUser;
use Telenor\Model\WorkdayUser;
use Telenor\System\Config;
use Telenor\System\Exception\ConnectionException;
use Telenor\System\Exception\InvalidDataException;
use Telenor\System\Exception\ValidationException;
use Telenor\System\Utils;
use function preg_replace;
use function strtoupper;

abstract class AWorkdayUsersSync implements IUserSync
{
	use TLog;

	protected $servername;
	protected $username;
	protected $password;
	protected $dbname;
	protected $connection;
	protected $sql;
	protected $sqlheadOfHR;
	protected $bindings;

	/**
	 * AWorkdayUsersSync constructor.
	 *
	 * @param array $bindings
	 * @throws ConnectionException In case of connection issues the ConnectionException will be thrown
	 *
	 */
	public function __construct(array $bindings)
	{

		$this->bindings = $bindings;
		$this->setServername(Config::envString('WORKDAY_SERVERNAME'));
		$this->setUsername(Config::envString('WORKDAY_USERNAME'));
		$this->setPassword(Config::envString('WORKDAY_PASSWORD'));
		$this->setDbname(Config::envString('WORKDAY_DBNAME'));

		try {
			$this->connection = new PDO("mysql:host={$this->getServername()}, dbname={$this->dbname}",
				$this->getUsername(), $this->getPassword(), [
					PDO::ATTR_TIMEOUT => 5, // in seconds
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				]);
		} catch (PDOException $e) {
			$this->logError("Failed to connect to db Server: " . $this->getServername() . ' DB:' . $this->getDbname(),
				['exception' => $e->getMessage()]);
			throw new ConnectionException('Error connecting to Workday DB. Server: ' . $this->getServername() . 'DB: ' . $this->getDbname());
		}
		$this->sqlCreate();

	}

	/**
	 *
	 */
	public function __destruct()
	{
		try {
			$this->connection = null; //Closes connection
		} catch (PDOException $e) {

			die($e->getMessage());
		}
	}

	/**
	 *
	 */
	public function closeConnection(): void
	{
		if ($this->connection !== null) {
			$this->connection = null;
		}
	}

	/**Fetch Initials for Head of HR Operations from Workday. Used as fallback
	 *
	 * @return string
	 * @throws InvalidDataException In case of no user found in Workday with job title "Head of HR Operations"  the
	 *     InvalidDataException will be thrown. The user with "Head of HR Operations" as title should be used as
	 *     fallback manager
	 */
	public function headOfHR(): string
	{
		$init = '';
		$stmt = $this->connection->query($this->sqlheadOfHR);
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$init = $row["initialer"];
		}

		if (Utils::isStringNullOrEmpty($init)) {

			$this->logError("No Head of HR found in Workday DB");
			throw new InvalidDataException('No Head of HR found in Workday DB');
		}
		return $init;
	}

	public function sqlCreate(): void
	{

		$today = (new DateTimeImmutable('today'))->format('Y-m-d H:i:s');

		$this->sql = "select ifnull(e.lokations_navn,'') lokations_navn,
				ifnull(e.organisations_navn,'') organisations_navn,
				ifnull(e.efternavn,'') efternavn,
				ifnull(e.fornavn,'') fornavn,
				ifnull(e.global_employee_number,'') global_employee_number,
				ifnull(e.e_mail,'') e_mail,
				ifnull(e.job,'') job,
				ifnull(e.efternavn,'') efternavn,
		 		ifnull(e.selskap_navn,'') selskap_navn,
		 		ifnull(e.gsm,'') gsm,
		 		ifnull(e.initialer,'') initialer,
		 		ifnull(e.organisations_id,'') organisations_id,
		 		ifnull(m.initialer,'') manager
		 		from workday.e_son_personer_workday e
		 		left join
				(select initialer , GLOBAL_EMPLOYEE_NUMBER from workday.e_son_personer_workday
				where
				((FRATRAD_DATO > now()) or (FRATRAD_DATO is null))
				and proc_time = '$today'
				and aktiv = 'Y'
				) m on m.GLOBAL_EMPLOYEE_NUMBER = e.CHEF_LONNR
				where e.aktiv = 'Y'
				##and lower(e.initialer) in ('TPN','CLN','HEIP' )
				#and datediff(curdate(), proc_time) =2
				and e.proc_time = '$today'
				and ((e.FRATRAD_DATO > now()) or (e.FRATRAD_DATO is null))
				
				
";
		$this->sqlheadOfHR = "select initialer from workday.e_son_personer_workday e where upper(e.initialer) ='DEAN' 
				and e.aktiv = 'Y'
				and e.proc_time = '$today'
				and ((e.FRATRAD_DATO > now()) or (e.FRATRAD_DATO is null))";

	}

	/** @return IUser */
	public function getAllUsers(): ?array
	{
		$wd_users = [];

		$fallBackManager = $this->headOfHR();

		if (empty($this->bindings)) {
			$stmt = $this->connection->prepare($this->sql);
			$stmt->execute();
		} else {
			$stmt = $this->connection->prepare($this->sql);
			$stmt->execute($this->bindings);
		}
		$count = $stmt->rowCount();

		$this->logInfo("Number of Workday user feteched: $count");

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			try {
				$orgname = $row["organisations_navn"];
				$orgname = preg_replace("/^dk(.*)/i", "$1", $orgname);
				$orgname = preg_replace("/^-|-\s+(.*)/i", "$1", $orgname);
				$orgname = preg_replace("/^\s+(.*)/i", "$1", $orgname);
				$orgname = preg_replace("/ \(.*/i", "", $orgname, 1);

				$wd_user = new WorkdayUser();
				$wd_user->setLastname($row['efternavn']);
				$wd_user->setFirstname($row['fornavn']);
				$wd_user->setFullname($row['fornavn'] . ' ' . $row['efternavn']);
				$wd_user->setCity($row['lokations_navn']);
				$wd_user->setEmployeeNumber((int)$row['global_employee_number']);
				$wd_user->setEmployeeNumber1((int)$row['global_employee_number']);
				$wd_user->setEmail($row['e_mail']);
				$wd_user->setTitle($row['job']);
				$wd_user->setPhone($row['gsm']);
				$wd_user->setCompany($row['selskap_navn']);
				$wd_user->setInitials($row['initialer']);
				$wd_user->setDepartment($orgname);
				$wd_user->setManager($row['manager']);
				if (Utils::isStringNullOrEmpty($wd_user->getManager())) {
					$wd_user->setManager($fallBackManager);
				}
				$wd_user->setActive(true);
				$wd_users[strtoupper($row['initialer'])] = $wd_user;
				$this->logDebug("Workday user processed: " . $wd_user->getInitials(), ['dbrow' => $row]);

			} catch (ValidationException $e) {
				$this->logError("Error validating Workday user " . $row['initialer'], [
					'property'  => $e->getProperty(),
					'value'     => $e->getValue(),
					'dbrow'     => $row,
					'exception' => $e->getMessage(),
				]);
			}
		}

		$this->closeConnection();
		return $wd_users;
	}

	public function getServername(): string
	{
		return $this->servername;
	}

	public function setServername($servername): void
	{
		$this->servername = $servername;
	}

	public function getUsername(): string
	{
		return $this->username;
	}

	public function setUsername($username): void
	{
		$this->username = $username;
	}

	public function getPassword(): string
	{
		return $this->password;
	}

	public function setPassword($password): void
	{
		$this->password = $password;
	}

	public function getDbname(): string
	{
		return $this->dbname;
	}

	public function setDbname($dbname): void
	{
		$this->dbname = $dbname;
	}

}