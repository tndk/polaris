<?php
declare(strict_types=1);

namespace Telenor\MessageQueue;

use Psr\Log\LogLevel;
use Telenor\Logging\LoggingController;
use Telenor\Message\IMessage;
use Telenor\System\Exception\ConnectionException;

/**
 * Trait to use for simplifying message queue functionality.
 */
trait TMessageQueue
{
	/**
	 * Publishes a message to a configured message queue.
	 *
	 * @param IMessage $message The message to publish to the queue.
	 * @param string|null $queueName Alternative message queue to send to.
	 * @throws ConnectionException If connection problems occurs.
	 */
	protected function publishToQueue(IMessage $message, string $queueName = null): void
	{
		try {
			MessageQueueController::getInstance()->publish($message, $queueName);
		} catch (ConnectionException $e) {
			LoggingController::getInstance()->getFallbackDriver()->log(LogLevel::EMERGENCY, "Failed to publish message to queue:\n$message\nException: " . $e->getMessage());
			throw $e;
		}
	}
}